import sys
import os
import re
# from pathlib import Path
# NB this script is not perfect as it only includes and does not take into account the lines inside AddInput command line for instance 
# --> future possible improvement 

"""
python scripts/singleLatexFile.py FileToFlatten

For instance 
python scripts/singleLatexFile.py ThesisRomainBouquet.tex

"""
# need to implement getting AddInputCommand

def main():
  if len(sys.argv) != 2: 
    raise RuntimeError("Please provide the file path as argument")
  thesisBaseName = sys.argv[1]
  if thesisBaseName.endswith(".tex"):
    thesisBaseName = thesisBaseName[:-4]
 
  singleFileThesis = "{}_diff_SingleFile.tex".format(thesisBaseName)

  dict_regex = {}
  dict_regex["inputRegex"] = re.compile(r"(?:|[^\n\\]+)\\(?:AddInput|include|input)(?:|\s\[\S\s\]?){[^}]+}")
  dict_regex["getContentBrackets"] = re.compile(r"{([^}]*)}")
  # block starts with % 
  dict_regex["isCommentedBlock"] = re.compile(r"^[\s]*?%[\S\s]+")

  # useful to remove the special character in a line 
  dict_regex["beginningSpecialCharacters"] = re.compile(r"^(?:[%\s]+)")
  dict_regex["endingSpecialCharacters"]    = re.compile(r"(?:[%\s]+)$")
   
  fullContent = getRecursiveInput(thesisBaseName, dict_regex)

  outFile = open(singleFileThesis, "w")
  outFile.write(fullContent)
  outFile.close()


def getFileContent(fileName):
  # check that file exists
  if not os.path.exists(fileName):
    raise RuntimeError("Not existing fileName = {}".format(fileName))
  
  # read content file 
  content = open(fileName, 'r').read()
  return content 

def isCommentedLine(line,dict_regex): 
  return (re.search(dict_regex["isCommentedBlock"], line) is not None)


def transFormPath(fileToRead,currentDir,dict_regex): 
  # print()
  if len(currentDir): 
    fileToRead = fileToRead.replace("\\CurrentDir",currentDir)
  else: 
    fileToRead = "{}".format(fileToRead)
  
  # result = re.search(dict_regex["beginningSpecialCharacters"],fileToRead)
  # replace special character % \n at the beggining 
  fileToRead = re.sub(dict_regex["beginningSpecialCharacters"],"",fileToRead)
  fileToRead = re.sub(dict_regex["endingSpecialCharacters"],"",fileToRead)

  return fileToRead

def getRecursiveInput(fileName,dict_regex,initialDir=""): 
  if fileName.endswith(".tex"): 
    tmpCurrentFile = fileName
  else: 
    tmpCurrentFile = "{}.tex".format(fileName)

  # directory of the initial latex file to compile
  # all files in the latex files are located with respect to that initial directory
  if initialDir == "": 
    initialDir = os.path.dirname(tmpCurrentFile)
    if initialDir == "":
      initialDir = "."
  
  if not tmpCurrentFile.startswith(initialDir):
    tmpCurrentFile = "{}/{}".format(initialDir,tmpCurrentFile)
  
  tmpCurrentDir  = os.path.dirname(tmpCurrentFile)
  print("Reading file = {}".format(tmpCurrentFile))
  
  fileContent = getFileContent(tmpCurrentFile)

  list_inputFiles = re.findall(dict_regex["inputRegex"], fileContent)
  for addInputFile in list_inputFiles: 
    commented = isCommentedLine(addInputFile,dict_regex)
    if commented:
      print("Commented inputFile = {}".format(addInputFile))
      continue
    # print("---")
    # print(addInputFile)
    pathInput = re.search(dict_regex["getContentBrackets"],addInputFile)
    if pathInput is not None:
      #print("pathInput.group(1)={}".format(pathInput.group(1)))
      tmpFileToRead = transFormPath(pathInput.group(1), tmpCurrentDir, dict_regex)
      #print("FileToReadPath = {}".format(tmpFileToReadPath))
      tmpOutputText = getRecursiveInput(tmpFileToRead,dict_regex,initialDir)
      # replace only one occurence 
      fileContent = fileContent.replace(addInputFile,tmpOutputText,1)
    else: 
      raise RuntimeError("Could not find regex in addInputFile = {}".format(addInputFile))

  #print(fileContent)
  return fileContent

main()