import fitz

def extract_chapter(input_pdf, chapter_title):
    doc = fitz.open(input_pdf)
    toc = doc.get_toc()
    start_page = None
    end_page = None

    for level, title, page in toc:
        if title == chapter_title:
            start_page = page - 1
        elif start_page is not None and level == 1:
            end_page = page - 1
            break

    if start_page is None:
        print(f"No chapter named '{chapter_title}' found.")
        return

    if end_page is None:  # The chapter extends to the end of the document.
        end_page = doc.page_count

    return list(range(start_page, end_page))


def write_pdf(input_pdf, pages, output_pdf):
    doc = fitz.open(input_pdf)
    writer = fitz.open()
    for page_number in pages:
        writer.insert_pdf(doc, from_page=page_number, to_page=page_number)

    writer.save(output_pdf)


# Use the function
original_pdf = '/Users/avencast/Desktop/Work/dissertation/main.pdf'
r1 = extract_chapter(original_pdf, 'Chapter 2 Higgs Physics in Standard Model and Beyond')
r2 = extract_chapter(original_pdf, 'Bibliography')

print(r1+r2)
write_pdf(original_pdf, r1+r2, './output.pdf')
