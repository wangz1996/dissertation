# Makefile for SJTUThesis

# Basename of thesis
THESIS = main

# Option for latexmk
LATEXMK_OPT = -time -file-line-error -halt-on-error -interaction=nonstopmode
LATEXMK_OPT_PVC = $(LATEXMK_OPT) -pvc

# make deletion work on Windows
ifdef SystemRoot
	RM = del /Q
	OPEN = start
else
	RM = rm -f
	OPEN = open
endif

.PHONY : all pvc view wordcount clean cleanall FORCE_MAKE

all : $(THESIS).pdf

$(THESIS).pdf : $(THESIS).tex FORCE_MAKE
	@latexmk $(LATEXMK_OPT) $<

pvc : $(THESIS).tex
	@latexmk $(LATEXMK_OPT_PVC) $(THESIS)

view : $(THESIS).pdf
	$(OPEN) $<

wordcount : $(THESIS).tex
	@if grep -v ^% $< | grep -q '\\documentclass\[[^\[]*en'; then \
		texcount $< -inc -char-only | awk '/total/ {getline; print "英文字符数\t\t\t:",$$4}'; \
	else \
		texcount $< -inc -ch-only   | awk '/total/ {getline; print "纯中文字数\t\t\t:",$$4}'; \
	fi
	@texcount $< -inc -chinese | awk '/total/ {getline; print "总字数（英文单词 + 中文字）\t:",$$4}'

clean :
	-@latexmk -c -silent $(THESIS).tex 2> /dev/null

cleanall :
	-@latexmk -C -silent $(THESIS).tex 2> /dev/null


# Reference diff
REFGITDIR = ./reference-dissertation
REFGITURL = https://gitlab.com/yulei_zhang/dissertation.git
BASENAME = main

run_refDiff : 
	@echo "Compare current document to reference version='${tag}' & url='$(REFGITURL)'"
	@echo "Cloning reference document... Takes time"
	# @rm -rf $(REFGITDIR)
	# @git clone $(REFGITURL) $(REFGITDIR)
	# @git --git-dir=$(REFGITDIR)/.git --work-tree=$(REFGITDIR)/ checkout ${tag}
	@echo "Flattening latest git tag document"
	@python scripts/singleLatexFile.py $(REFGITDIR)/$(BASENAME).tex
	@echo "Flattening current document"
	@python scripts/singleLatexFile.py $(BASENAME).tex
	@echo "Creating comparison between current and reference flattened documents... Takes time"
	@rm -f $(BASENAME)_diff.tex
	@latexdiff $(REFGITDIR)/$(BASENAME)_diff_SingleFile.tex $(BASENAME)_diff_SingleFile.tex > $(BASENAME)_diff.tex
	@latexmk -time -file-line-error -halt-on-error -interaction=nonstopmode $(BASENAME)_diff.tex

refDiff : 
	@make run_refDiff tag=${tag}