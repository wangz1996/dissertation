\chapter{Higgs Physics in Standard Model and Beyond}
\label{chap:2}
The Standard Model (SM) of particle physics is the pinnacle of over a century's worth of scientific research, 
culminating in our current understanding of the fundamental particles and the forces that govern their interactions. 
It is one of the most complicated theories, yet elegant in both mathematics and physics 
that has stood the test of time and experimental scrutiny. 

The story of the Standard Model starts in the mid $20^\text{th}$ century, when quantum electrodynamics (QED) was born. 
This theory is developed by the great physicist Richard Feynman\cite{PhysRev.76.769}, Julian Schwinger\cite{PhysRev.74.1439}, Sin-Itiro Tomonaga, and Freeman Dyson,  
describing how photons interact with electrons and positrons. 
The development of QED was a major achievement, as it successfully reconciled quantum mechanics, 
which governs the small scale, with special relativity, which describes the fast-moving world. 
It provided a framework that could make incredibly precise predictions, many of which have been confirmed by experiments. 
QED was the first example of a quantum field theory, which would serve as the template for the rest of the Standard Model.

In the 1960s and 70s, the understanding of the strong nuclear force, one of the four fundamental forces of nature, underwent a significant revolution with the introduction of partons, namely quarks and gluons. 
This force is responsible for holding together the protons and neutrons inside atomic nuclei, and the particles that mediate this force are called gluons. 
The theory that describes the strong force is called quantum chromodynamics (QCD), 
developed by scientists such as Murray Gell-Mann\cite{osti_4008239}\cite{GELLMANN1964214} and Harald Fritzsch\cite{FRITZSCH1973365}. 
The particles involved in this force, quarks and gluons, carry a property called "color charge" (analogous to electric charge in electromagnetism), 
and the theory gets its name from this color charge. 
QCD provided a framework for understanding how quarks combine to form protons, neutrons, and other particles.

Meanwhile, scientists were also making progress on understanding the weak nuclear force, 
which is responsible for radioactive decay and plays a key role in nuclear fusion in stars. 
The weak force was initially puzzling because it is very different from the other forces, which is much weaker (hence the name), 
and it only operates at very short distances. 
In the 1960s, Sheldon Glashow\cite{GLASHOW1961579}, Abdus Salam\cite{doi:10.1142/9789812795915_0034}, 
and Steven Weinberg\cite{1967PhRvL..19.1264W} developed a theory that unified the weak force with electromagnetism, 
resulting in the "electroweak" theory. 
This theory predicted the existence of three new particles, the $W^+$, $W^-$, and $Z$, which mediate the weak force. 
These particles were later discovered in experiments at CERN in the 1980s.


The final piece of the puzzle was electroweak symmetry breaking, and this happens through the Brout-Englert-Higgs mechanism. 
This mechanism necessitates the existence of a Higgs field, and after EWS breaking, a scalar boson (the Higgs boson) appears. 
This particle was predicted in 1964 by several scientists, including Peter Higgs, François Englert, and Robert Brout. 
According to the theory, particles, especially for fermions, gain mass by interacting with this field, which means the more they interact, the more mass they have. 
The Higgs boson remained elusive for many years, but it was finally discovered in 2012 at CERN's Large Hadron Collider\cite{Aad_2012}, 
in one of the most celebrated scientific achievements of the 21st century.

The discovery of the Higgs boson was indeed a watershed moment in the history of particle physics, 
confirming the last missing piece of the Standard Model puzzle. 
However, it is important to note that the Higgs boson itself is not the end of the story. 
Its discovery opened up new avenues of research, including the study of its properties and the search for possible new particles and forces associated with it. 

Moreover, the Standard Model, while extraordinarily successful, does not account for some observed phenomena, 
indicating that there is physics beyond it - often referred to as "Beyond the Standard Model" (BSM) physics. 
One of the major areas of research in BSM physics involves long-lived particles (LLPs). 
These are hypothetical particles that do not decay immediately after being produced in high-energy collisions, as most known particles do. 
Instead, they would travel a significant distance before decaying, potentially leaving a distinctive signature in detectors at particle accelerators. 
This unusual behavior could be linked to the mysteries of dark matter, neutrino masses, or the imbalance between matter and antimatter in the universe.

This chapter delves into the intricacies of the Standard Model and its fundamental components, beginning with an overview in Section \ref{sec:2:intro}.
Next, Section \ref{sec:2:higgs} illuminates the concept of Electroweak Symmetry Breaking and the Brout-Englert-Higgs Mechanism.
The subsequent focus in Section \ref{sec:2:higgs-lhc} is on the research surrounding the Higgs bosons at the Large Hadron Collider (LHC).
Finally, Section \ref{sec:2:llps} delves into the frontier of Long-Lived Particles (LLPs) and their potential to further the understanding of physics beyond the Standard Model. 

\section{Introduction to the Standard Model}
\label{sec:2:intro}

\subsection{Fermions and Bosons}
\input{contents/2-physics/intro_particles.tex}
\subsection{The Quantum Field Theory and the Lagrangian of the Standard Model}
\input{contents/2-physics/intro_forces.tex}

\section{Electroweak Symmetry Breaking and the Brout-Englert-Higgs Mechanism}
\label{sec:2:higgs}
\subsection{Electroweak unification}
\input{contents/2-physics/higgs_electroweak.tex}
\subsection{The role of the Higgs field in symmetry breaking}
\input{contents/2-physics/higgs_field.tex}

\section{Higgs Physics at the LHC}
\label{sec:2:higgs-lhc}
\subsection{Higgs boson production}
\input{contents/2-physics/lhc_production.tex}
\subsection{Di-Higgs production}
\label{sec:2:hh_production}
\input{contents/2-physics/lhc_hh_production.tex}

\section{Long-Lived Particles (LLPs) and Beyond Standard Model Search}
\label{sec:2:llps}
\input{contents/2-physics/llp.tex}
