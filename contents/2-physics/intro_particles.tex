The SM theoretical framework categorizes elementary particles into two principal classes, 
known as fermions and bosons, each with distinctive characteristics and roles in the cosmos, as summarized in Figure \ref{fig:2:sm_particles}.

Fermions comprise the building blocks of matter and adhere to the Pauli Exclusion Principle, 
implying that they cannot simultaneously occupy identical quantum states. 
Characterized by half-integer spins, fermions are sub-categorized into two classes: quarks and leptons.
\begin{itemize}
    \item \textbf{Quarks}: There exist six flavors of quarks: up, down, charm, strange, top, and bottom. 
    The up, charm, and top quarks carry a fractional electric charge of +2/3, 
    while the down, strange, and bottom quarks carry a fractional charge of -1/3. 
    Quarks are unique in that they interact via all three fundamental non-gravitational forces: strong, weak, and electromagnetic. 
    Additionally, quarks carry an intrinsic property termed 'color charge', which mediates their interaction via the strong force.
    There are 3 color charges (and 3 opposite charges) for each flavor.
    \item \textbf{Leptons}: Analogous to quarks, leptons are also divided into six types across three generations: 
    electron, muon, tau particles, and their corresponding neutrinos. 
    The electron, muon, and tau each carry a unit negative charge and interact via the weak and electromagnetic forces. 
    Neutrinos, on the other hand, being neutral, solely partake in weak interactions.
\end{itemize}

Bosons, contrarily, are force mediators and possess integer spins, permitting them to inhabit the same quantum state. 
The Standard Model recognizes the following bosons: photon, W and Z bosons, gluons, and the Higgs boson.
\begin{itemize}
    \item \textbf{Photon}: The photon is the force carrier for the electromagnetic force, governing interactions between electrically charged particles. It is a massless particle that propagates at the speed of light.
    \item \textbf{W and Z bosons}: These are the mediators of the weak nuclear force, which controls certain types of nuclear decay, such as beta decay. The W bosons carry a unit positive or negative charge, while the Z boson is electrically neutral. Their considerable masses lead to the short-range nature of the weak force.
    \item \textbf{Gluons}: Gluons mediate the strong nuclear force, responsible for binding quarks within protons, neutrons, and other hadronic particles. They carry a color charge, allowing them to interact among themselves.
    \item \textbf{Higgs boson}: The Higgs boson is associated with the Higgs field, as proposed by the Brout-Englert-Higgs mechanism, which also explains the unique "Yukawa" interaction\cite{Yukawa:1935xg} through which particles acquire mass.
\end{itemize}

\begin{figure}[!htb]
    \centering
    \resizebox{\columnwidth}{!}{%
    \begin{tikzpicture}[x=1.2cm, y=1.2cm]
        \draw[round] (-0.5,0.5) rectangle (4.4,-1.5);
        \draw[round] (-0.6,0.6) rectangle (5.0,-2.5);
        \draw[round] (-0.7,0.7) rectangle (5.6,-3.5);
      
        \node at(0, 0)   {\particle[gray!20!white]
                         {$u$}        {up}       {$2.3$ MeV}{1/2}{$2/3$}{R/G/B}};
        \node at(0,-1)   {\particle[gray!20!white]
                         {$d$}        {down}    {$4.8$ MeV}{1/2}{$-1/3$}{R/G/B}};
        \node at(0,-2)   {\particle[gray!20!white]
                         {$e$}        {electron}       {$511$ keV}{1/2}{$-1$}{}};
        \node at(0,-3)   {\particle[gray!20!white]
                         {$\nu_e$}    {$e$ neutrino}         {$<2$ eV}{1/2}{}{}};
        \node at(1, 0)   {\particle
                         {$c$}        {charm}   {$1.28$ GeV}{1/2}{$2/3$}{R/G/B}};
        \node at(1,-1)   {\particle 
                         {$s$}        {strange}  {$95$ MeV}{1/2}{$-1/3$}{R/G/B}};
        \node at(1,-2)   {\particle
                         {$\mu$}      {muon}         {$105.7$ MeV}{1/2}{$-1$}{}};
        \node at(1,-3)   {\particle
                         {$\nu_\mu$}  {$\mu$ neutrino}    {$<190$ keV}{1/2}{}{}};
        \node at(2, 0)   {\particle
                         {$t$}        {top}    {$173.2$ GeV}{1/2}{$2/3$}{R/G/B}};
        \node at(2,-1)   {\particle
                         {$b$}        {bottom}  {$4.7$ GeV}{1/2}{$-1/3$}{R/G/B}};
        \node at(2,-2)   {\particle
                         {$\tau$}     {tau}          {$1.777$ GeV}{1/2}{$-1$}{}};
        \node at(2,-3)   {\particle
                         {$\nu_\tau$} {$\tau$ neutrino}  {$<18.2$ MeV}{1/2}{}{}};
        \node at(3,-3)   {\particle[orange!20!white]
                         {$W^{\hspace{-.3ex}\scalebox{.5}{$\pm$}}$}
                                      {}              {$80.4$ GeV}{1}{$\pm1$}{}};
        \node at(4,-3)   {\particle[orange!20!white]
                         {$Z$}        {}                    {$91.2$ GeV}{1}{}{}};
        \node at(3.5,-2) {\particle[green!50!black!20]
                         {$\gamma$}   {photon}                        {}{1}{}{}};
        \node at(3.5,-1) {\particle[purple!20!white]
                         {$g$}        {gluon}                    {}{1}{}{color}};
        \node at(5,0)    {\particle[gray!50!white]
                         {$H$}        {Higgs}              {$125.1$ GeV}{0}{}{}};
        \node at(6.1,-3) {\particle
                         {}           {graviton}                       {}{}{}{}};
      
        \node at(4.25,-0.5) [force]      {strong nuclear force (color)};
        \node at(4.85,-1.5) [force]    {electromagnetic force (charge)};
        \node at(5.45,-2.4) [force] {weak nuclear force (weak isospin)};
        \node at(6.75,-2.5) [force]        {gravitational force (mass)};
      
        \draw [<-] (2.5,0.3)   -- (2.7,0.3)          node [legend] {charge};
        \draw [<-] (2.5,0.15)  -- (2.7,0.15)         node [legend] {colors};
        \draw [<-] (2.05,0.25) -- (2.3,0) -- (2.7,0) node [legend]   {mass};
        \draw [<-] (2.5,-0.3)  -- (2.7,-0.3)         node [legend]   {spin};
      
        \draw [mbrace] (-0.8,0.5)  -- (-0.8,-1.5)
                       node[leftlabel] {6 quarks\\(+6 anti-quarks)};
        \draw [mbrace] (-0.8,-1.5) -- (-0.8,-3.5)
                       node[leftlabel] {6 leptons\\(+6 anti-leptons)};
        \draw [mbrace] (-0.5,-3.6) -- (2.5,-3.6)
                       node[bottomlabel]
                       {12 fermions\\increasing mass $\to$};
        \draw [mbrace] (2.5,-3.6) -- (5.5,-3.6)
                       node[bottomlabel] {5 bosons\\(+1 opposite charge $W$)};
      
        \draw [brace] (-0.5,.8) -- (0.5,.8) node[toplabel]         {standard matter};
        \draw [brace] (0.5,.8)  -- (2.5,.8) node[toplabel]         {unstable matter};
        \draw [brace] (2.5,.8)  -- (4.5,.8) node[toplabel]          {force carriers};
        \draw [brace] (4.5,.8)  -- (5.5,.8) node[toplabel]       {Goldstone\\bosons};
        \draw [brace] (5.5,.8)  -- (7,.8)   node[toplabel] {outside\\standard model};
      
        \node at (0,1.2)   [generation] {1\tiny st};
        \node at (1,1.2)   [generation] {2\tiny nd};
        \node at (2,1.2)   [generation] {3\tiny rd};
        \node at (2.8,1.2) [generation] {\tiny generation};
      \end{tikzpicture}
    
    }
    \caption{The properties of elementary particles in the Standard Model.}
    \label{fig:2:sm_particles}
\end{figure}