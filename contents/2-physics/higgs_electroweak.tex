Electroweak unification is a fundamental concept in particle physics that ties together two of the four known forces of nature, 
namely the weak nuclear force and electromagnetism. 
This unification is a cornerstone of the Standard Model (SM) of particle physics, 
providing an elegant theoretical framework to understand the interactions of elementary particles.

At the heart of electroweak unification is the realization that at high enough energies, 
electromagnetism, mediated by photons ($\gamma$), and the weak nuclear force, mediated by W and Z bosons (W$^{+}$, W$^{-}$, and Z$^{0}$), 
manifest as facets of the same force: the electroweak force. 
The connection between these two fundamental interactions was initially proposed by Sheldon Glashow, Abdus Salam, and Steven Weinberg, 
earning them the Nobel Prize in Physics in 1979.

The theory begins with an invariant Lagrangian under the $SU(2)_L \times U(1)_Y$ gauge group, 
with $SU(2)_L$ and $U(1)_Y$ representing the weak isospin and hypercharge symmetries, respectively. 
The gauge fields corresponding to these symmetries are $W^a_{\mu}$ (a = 1, 2, 3) and $B_{\mu}$.
The electroweak part of the SM Lagrangian can be written as follows:
\begin{equation}
\mathcal{L}_{E W}=-\frac{1}{4} W_{\mu \nu}^a W^{a \mu \nu}-\frac{1}{4} B_{\mu \nu} B^{\mu \nu}
\end{equation}
Here, $W_{\mu \nu}^a$ and $B_{\mu \nu}$ are the field strength tensors for the $SU(2)_L$ and $U(1)_Y$ gauge fields, defined as:
\begin{equation}
W_{\mu \nu}^a=\partial_\mu W_\nu^a-\partial_\nu W_\mu^a+g \epsilon^{a b c} W_\mu^b W_\nu^c 
\end{equation}
\begin{equation}
B_{\mu \nu}=\partial_\mu B_\nu-\partial_\nu B_\mu
\end{equation}

Here, $\epsilon^{abc}$ is the totally antisymmetric tensor, $g$ is the $SU(2)_L$ gauge coupling constant, and $\mu, \nu$ are space-time indices.
This Lagrangian describes four massless gauge bosons, which correspond to the three generators of $SU(2)_L$ and the generator of $U(1)_Y$. 
However, empirical evidence contravenes this: the weak force has a short range because its mediators, the W and Z bosons, are massive, while the photon is massless.

To reconcile this discrepancy, the Brout-Englert-Higgs mechanism is invoked, 
which introduces a complex scalar field, the Higgs field. 
This field spontaneously breaks the $SU(2)_L \times U(1)_Y$ symmetry down to $U(1)_{em}$, 
the gauge group of electromagnetism. 
Consequently, three of the original massless gauge bosons acquire mass, becoming the W and Z bosons, 
while the fourth remains massless, recognized as the photon.

It is worth noting that the electroweak symmetry breaking also involves the generation of fermion masses through their Yukawa couplings to the Higgs field. 
As we know it, without this mechanism, the elementary particles would all be massless, zipping through space at the speed of light, and the universe would not exist.

The unification of the electromagnetic and weak forces happens at a sufficiently high energy scale, 
known as the electroweak scale, at approximately 100 GeV. 
At energies below this scale, the weak and electromagnetic forces appear distinct because of the large masses of the W and Z bosons. 
Above the electroweak scale, however, the distinction between these forces blurs, and they behave as a singular electroweak force. 

Experimental confirmation of electroweak unification came in the 1980s with the discovery of W and Z bosons at CERN's Super Proton Synchrotron\cite{DiLella:2015yit}. 
Further, precision measurements at the HERA\cite{Klein:2008di} ($e-p$ collider), the Large Electron-Positron (LEP) collider\cite{Myers:1991ym} and the Tevatron\cite{Stephen_Holmes_2011} confirmed the predictions of the electroweak theory, 
solidifying it as an integral part of the SM. 
At HERA we could see the unification of charged current (CC) and neutral current (NC) cross sections at high $Q^2$ (squared momentum transfer)\cite{HERA}.
