The production of two Higgs bosons, a process referred to as double Higgs boson production, 
is of significant interest due to the wealth of information it provides about the Higgs potential. 
Specifically, this process gives insights into the trilinear self-coupling of the Higgs. 
The primary mechanism for double Higgs production is through gluon fusion ($gg \to HH$), 
which accounts for more than $90\%$ of the total cross-section.  
Figure \ref{fig:2:lhc_hh_ggF} depicts the major Feynman diagrams for the production of two Higgs bosons via gluon fusion.
These two diagrams interfere destructively, leading to a very small $HH$ cross-section, 
namely $\sigma_{\text{ggF}, HH}^{SM} =  31.05\pm 3\%$ (PDF+$\alpha_{s}$) $^{+ 6\%}_{-23\%}$ (Scale + $m_{\text{top}}$)\,fb, 
calculated at next-to-next-to-leading-order (NNLO) accuracy 
in the finite top-quark mass approximation for $m_{H}=125$~GeV and $\sqrt{s}=13$~TeV

Under the conditions at the LHC ($\sqrt{s} = 14\text{TeV}$), the cross-section for the bbH mode can reach up to $550\text{fb}$, 
albeit this is still two orders of magnitude below the cross-section for ggF. 
It is noteworthy that in alternative models such as the two Higgs doublet model or a SUSY model, 
the Higgs self-coupling is proportional to the ratio of neutral Higgs boson vacuum expectation values. 
For large values of this ratio, the coupling undergoes significant amplification, potentially elevating the \( bbH \) mode to be the dominant mechanism for Higgs boson production, a deviation from the Standard Model predictions.
Other sub-leading production mechanisms are also present, 
including VBF HHjj ($\sigma_{\text{VBF}, HH}^{SM} =  1.726\pm 2.1\%$ (PDF+$\alpha_{s}$) $^{+0.03\%}_{-0.04\%}$(Scale)\,fb at $\sqrt{s} = 13\text{TeV}$) (Figure \ref{fig:2:lhc_hh_VBF}), HHW ($0.50\text{fb}$), HHZ ($0.36\text{fb}$), and ttHH ($0.8\text{fb}$). 
QCD corrections, computed in the infinite top mass limit, significantly influence the cross-section, 
effectively doubling it from LO to NLO\cite{PhysRevD.58.115012} and further enhancing it by around $20\%$ from NLO to NNLO \cite{PhysRevLett.111.201801}. 

In the recent past, complete NLO corrections incorporating all top quark mass effects\cite{Baglio_2019} have been determined numerically. 
These findings reveal a k-factor less flat than that predicted in large top mass approximations\cite{Baglio_2019}. 
This unexpected dependency of the results on the renormalisation scheme 
and scale for the top quark mass raises questions about the assessment of scale uncertainty and calls for a more accurate NNLO computation, 
although such a task is likely to remain challenging for some time.

On the differential level, the destructive interference between the box and triangle contributions 
makes the predictions made in the infinite top mass limit for both the HH invariant mass 
and the leading Higgs boson pT distributions complex. 
With an inclusive cross-section of around $35~\text{fb}$ at $\sqrt{s} = 13~\text{TeV}$ and challenging signal-background discrimination, 
the double Higgs boson production remains a difficult channel to probe 
and is expected to significantly benefit from the high-luminosity run of the LHC\cite{cepeda2019higgs}.

\begin{figure}[!htb]
    \centering
    \resizebox{\columnwidth}{!}{%
        \includegraphics[width=0.9\textwidth]{figures/2/dihiggs_ggF}
    }
    \caption{
        The major Feynman diagrams for the production of two Higgs bosons via gluon fusion. 
        (Left) Triangle diagram sensitive to the self-coupling vertex $\kappa_\lambda$, 
        (Right) Box diagram, which interferes destructively at the self-coupling vertex $\kappa_\lambda$.
    }
    \label{fig:2:lhc_hh_ggF}
\end{figure}

\begin{figure}[!htb]
    \centering
    \resizebox{\columnwidth}{!}{%
        \includegraphics[width=0.9\textwidth]{figures/2/dihiggs_VBF}
    }
    \caption{
        Depiction of Vector Boson Fusion (VBF) processes in Higgs boson pair production: 
        (a) the VVHH vertex process, 
        (b) the trilinear coupling, 
        and (c) the VVH production mode.
    }
    \label{fig:2:lhc_hh_VBF}
\end{figure}


Investigations into both resonant and non-resonant Higgs boson pair production 
can reveal intriguing insights into various BSM theories. 
Resonant production involves the creation of unstable, 
heavier particles that decay into two Higgs bosons, 
thereby forming a characteristic peak in the energy spectrum. 
Non-resonant production, however, involves processes that create two Higgs bosons directly, 
without the intermediate step of a heavier particle. 
These mechanisms offer different perspectives and carry distinctive signatures, broadening the scope of the search for new physics.
During Run 1 and Run 2, both ATLAS and CMS experiments performed searches for resonant 
and non-resonant Higgs boson pair production via the following channels:

\begin{enumerate}
    \item \(HH \rightarrow bb\gamma\gamma\),
    \item \(HH \rightarrow bb\tau^{+}\tau^{-}\),
    \item \(HH \rightarrow bbbb\),
    \item \(HH \rightarrow bbVV\),
    \item \(HH \rightarrow bbll\),
    \item Final states containing multiple leptons (electrons or muons), covering the \(WW^{*}WW^{*}\), \(WW^{*}ZZ^{*}\), \(ZZ^{*}ZZ^{*}\), \(ZZ^{*}\tau^{+}\tau^{-}\), \(WW^{*}\tau^{+}\tau^{-}\), \(ZZ^{*}bb\), and \(\tau^{+}\tau^{-}\tau^{+}\tau^{-}\) channels,
\end{enumerate}


