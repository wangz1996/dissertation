The Standard Model of particle physics provides an extraordinarily successful framework for understanding elementary particles and their interactions. 
Central to the model is the electroweak theory, which describes electromagnetism and the weak nuclear force as two aspects of a unified electroweak force. 
However, in order to reconcile the massless nature required by gauge invariance with the observed massive particles, 
we need to introduce the Brout-Englert-Higgs (BEH) mechanism, which is a process of spontaneous symmetry breaking.

The spontaneous symmetry breaking\cite{PhysRevLett.13.321} occurs in the scalar sector of the electroweak theory, 
which contains the Higgs field\cite{PhysRevLett.13.508}, denoted by $\Phi$. 
The Lagrangian density responsible for this process is given by:
\begin{equation}
\mathcal{L}=\left(D^\mu \Phi\right)^{\dagger}\left(D_\mu \Phi\right)-V(\Phi)
\end{equation}
where $D_\mu$ is the covariant derivative that contains the $W$ and $B$ fields, and $V(\Phi)$ is the Higgs potential. 
In the Standard Model, $\Phi$ is a doublet of complex scalar fields given by:
\begin{equation}
\Phi=\frac{1}{\sqrt{2}}\left(\begin{array}{c}
0 \\
v+h(x)
\end{array}\right)
\end{equation}
where $v$ is the vacuum expectation value (VEV) of the field and $h(x)$ signifies the real scalar field representing fluctuations about the vacuum. 
The Higgs potential $V(\Phi)$ is given by:
\begin{equation}
    V(\Phi)=\mu^2 \Phi^{\dagger} \Phi+\lambda\left(\Phi^{\dagger} \Phi\right)^2
\end{equation}
with the parameters satisfying $\mu^2 < 0$ and $\lambda > 0$. 
The negative $\mu^2$ term allows for a non-zero minimum of the potential away from the origin, 
while the positive $\lambda$ term ensures that the potential is bounded from below. 
This generates a "Mexican hat" shape in the Higgs potential. 
Figure \ref{fig:2:higgs_potential} illustrates the Higgs potential for $\mu^2 < 0$ and $\mu^2 > 0$.

This non-zero minimum of the potential, also known as the vacuum expectation value (VEV), 
is given by $v = \sqrt{\frac{-\mu^2}{2\lambda}}$. 
It is around this value that the Higgs field oscillates, not around zero, 
and thus the electroweak symmetry is spontaneously broken, 
resulting in the W and Z bosons acquiring mass. 
It's also beneficial to understand that the fluctuations about this vacuum state, 
represented by $h(x)$, correspond to the physical Higgs boson.

Upon expanding the kinetic term $(D^\mu \Phi)^\dagger (D_\mu \Phi)$ and the potential $V(\Phi)$ around the vacuum expectation value, 
we find terms proportional to $W^\mu W_\mu$, $B^\mu B_\mu$, 
and $h^2$, which give rise to the masses of the W boson, Z boson, and the Higgs boson, respectively. 
Specifically, the Higgs boson mass can be identified as $m_H = \sqrt{2 \lambda} v$.

The mass generation for fermions is also an outcome of the Higgs mechanism, 
however it involves a different process, namely the Yukawa interaction between fermions and the Higgs field.
After the spontaneous symmetry breaking, the Higgs doublet acquires a vacuum expectation value $v$,
and the Yukawa interaction term in the Standard Model Lagrangian becomes:
\begin{equation}
\mathcal{L}_{\text {Yukawa }}=-\frac{y_f v}{\sqrt{2}} \bar{\Psi}_f \Psi_f-\frac{y_f h}{\sqrt{2}} \bar{\Psi}_f \Psi_f+\text { h.c. }
\end{equation}
The first term now clearly signifies the mass term for the fermions, $m_f=y_f v / \sqrt{2}$
 , which is non-zero due to the non-zero vacuum expectation value of the Higgs field. 
 This is how fermions acquire their mass in the Higgs mechanism. 
 The second term represents the interaction of the Higgs boson with the fermions, which is proportional to the mass of the fermions. 
 These interactions are crucial for the production and decay of the Higgs boson at colliders.
In the Standard Model, the Yukawa couplings $y_f$ are free parameters and must be determined by experiment.

The Brout-Englert-Higgs (BEH) mechanism and its associated spontaneous symmetry breaking are fundamental components of the Standard Model of particle physics. 
They provide a consistent framework that unifies the weak nuclear force and electromagnetism, offering a profound explanation for the observed masses of fundamental particles. 
The mechanism relies on the Higgs field, which has a non-zero vacuum expectation value and plays a crucial role in this process. 
Rather than actually "breaking" the symmetry, the mechanism merely conceals it. 
The underlying theory remains symmetric, but the ground state does not exhibit this symmetry. 
This distinction is crucial for our modern understanding of particle physics.

It is worth emphasizing that direct mass terms for gauge bosons would violate gauge invariance, which is a foundational symmetry of the Standard Model. 
The Higgs mechanism becomes indispensable in this regard. 
Starting from a massless theory that requires gauge invariance, 
the Higgs field and its corresponding mechanism give rise to effective mass terms for particles after electroweak symmetry breaking (EWSB). 
This ingenious solution ensures that the theory remains consistent and gauge invariant while also explaining the origin of mass for particles.

\begin{figure}[!htp]
    \centering
    \begin{subfigure}{0.48\textwidth}
      \centering
      \begin{tikzpicture}
        \begin{axis}[
            axis lines=center,
            view={140}{25},
            axis equal,
            domain=0:360,
            y domain=0:1.25,
            xmax=1.5,ymax=1.5,zmin=0,zmax=1.5,
            x label style={at={(axis description cs:0.18,0.29)},anchor=north},
            y label style={at={(axis description cs:0.82,0.25)},anchor=north},
            z label style={at={(axis description cs:0.44,0.8)},anchor=north},
            xlabel = $\mathrm{Re}(\phi)$,
            ylabel=$\mathrm{Im}(\phi)$,
            zlabel=$V(\phi)$,
            ticks=none,
            clip bounding box=upper bound
          ]
      
          \addplot3 [surf, shader=flat, draw=black, fill=white, z buffer=sort] ({sin(x)*y}, {cos(x)*y}, {0.5*y^2});
        \end{axis}
      \end{tikzpicture}
      \caption{$\mu^2 < 0$}
    \end{subfigure}
    % \hspace{1cm}
    \begin{subfigure}{0.48\textwidth}
      \centering
      \begin{tikzpicture}
        \begin{axis}[
            axis lines=center,
            view={140}{25},
            axis equal,
            domain=0:360,
            y domain=0:1.25,
            xmax=1.5,ymax=1.5,zmin=0,zmax=1.5,
            x label style={at={(axis description cs:0.18,0.29)},anchor=north},
            y label style={at={(axis description cs:0.82,0.25)},anchor=north},
            z label style={at={(axis description cs:0.44,0.8)},anchor=north},
            xlabel = $\mathrm{Re}(\phi)$,
            ylabel=$\mathrm{Im}(\phi)$,
            zlabel=$V(\phi)$,
            ticks=none,
            clip bounding box=upper bound
          ]
      
          \addplot3 [surf, shader=flat, draw=black, fill=white, z buffer=sort] ({sin(x)*y}, {cos(x)*y}, {(y^2-1)^2});
        \end{axis}
        \shade (3.47,3.5) circle [radius=0.15cm];
        \shade (5.1,2.2) circle [radius=0.15cm];
        \node[anchor=east] at (4.05,3.71) (text) {A};
        \node[anchor=west] at (5.5,3.0) (description) {B};
        \draw (description) edge[out=180,in=0,<-] (text);
      \end{tikzpicture}
      \caption{$\mu^2 > 0$}
    \end{subfigure}
    \caption{The illustration of Higgs potential $V(\phi)$. 
    Point B represents for the non-zero vacuum expectation value (VEV) of the field.}
    \label{fig:2:higgs_potential}
  \end{figure}

