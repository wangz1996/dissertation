Quantum Field Theory (QFT) is the theoretical framework that combines classical field theory \cite{peskin1995introduction}, 
quantum mechanics, and special relativity. QFT treats particles as excited states, or quanta, of underlying quantum fields. 
These fields are mapped across spacetime, and particles are represented by field oscillations.
The most successful application of QFT is the Standard Model of particle physics. 
The mathematical structure that describes the dynamics and interactions of the quantum fields in the Standard Model is given by the so-called Standard Model Lagrangian. 
This Lagrangian includes terms for each of the particles and their interactions in the model.

To describe the Standard Model in the language of QFT, the concept of gauge symmetry is introduced. 
This is a fundamental symmetry principle that states that the laws of physics should not change under certain transformations, 
known as gauge transformations. 
In the context of QFT, gauge transformations change the phase of the quantum fields, 
and different types of gauge transformations lead to different types of forces.

The Lagrangian of the Standard Model encompasses all known elementary particles and their interactions, excluding gravity. 
The full Lagrangian is the sum of these parts: 
\begin{equation}
L = -\frac{1}{4}F_{\mu\nu}F^{\mu\nu} + i\bar{\psi}\slashed{D}\psi + h.c. + \psi_i y_{i j} \psi_j \phi + h.c. + |D_{\mu}\phi|^2 + -V(\phi),
\end{equation}
which is usually divided into several parts that describe different kinds of interactions\cite{Woithe_2017}:

\begin{enumerate}
    \item \textbf{$-\frac{1}{4}F_{\mu\nu}F^{\mu\nu}$} This term represents the kinetic energy of the matter fields. It describes the propagation of matter particles in space and time. This term includes the fields of quarks and leptons, which are the building blocks of matter.
    
    \item \textbf{$i\bar{\psi}\slashed{D}\psi$} This term describes the interactions between matter particles and force particles. It includes the electromagnetic interaction, the strong interaction, and the weak interaction. The electromagnetic interaction is mediated by the photon, the strong interaction by the gluon, and the weak interaction by the W and Z bosons. The weak interaction is unique in that it can transform one type of matter particle into another.
    
    \item \textbf{$h.c.$} This term represents the 'hermitian conjugate' of term 2. The hermitian conjugate is necessary if arithmetic operations on matrices produce complex numbers.
    
    \item \textbf{$\psi_i y_{i j} \psi_j \phi$} This term represents how matter particles couple to the Brout-Englert-Higgs (BEH) field $\phi$ and thereby obtain mass. It includes the coupling of quarks and leptons to the Higgs field.
    
    \item \textbf{$h.c.$} This term describes is necessary since this hermitian conjugate of term 4 describes the same interaction, but with antimatter particles.
    
    \item \textbf{$|D_{\mu}\phi|^2$} This term represents how the propagator particles of weak interaction couple to the BEH field. It only appies to the W and Z bosons, because photons and gluons are massless.
    
    \item \textbf{$-V(\phi)$} This term describes the Brout-Englert-Higgs field potential and how Higgs bosons couple to each other.

\end{enumerate}

The specific form of each of these terms is dictated by the principles of gauge symmetry, 
special relativity, and quantum mechanics. 
The parameters in the Lagrangian (the particle masses, the coupling constants, etc.) are determined by experiments.
In this framework, particles interact by exchanging gauge bosons. 
For instance, two electrons repel each other by exchanging a photon, the gauge boson of the electromagnetic field. 
Similarly, quarks interact by exchanging gluons, the gauge bosons of the strong force.
The Higgs field, as introduced in the Lagrangian, plays a critical role in the Standard Model. 
When the Higgs field undergoes spontaneous symmetry breaking, it gives rise to the masses of the other particles. 
The interaction of the particles with the Higgs field is proportional to their mass. More details about the Higgs field are discussed in Section \ref{sec:2:higgs}.
