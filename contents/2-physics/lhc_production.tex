
The key production mechanisms for the Higgs boson at the Large Hadron Collider (LHC) include gluon fusion (ggF), 
vector-boson fusion (VBF), associated production with a gauge boson (VH), 
as well as associated production with a pair of top quarks ($t\bar{t}H$) or with a single top quark (tHq). 
Figure \ref{fig:2:lhc_higgs_diagram} illustrates these dominant Higgs boson production processes.
In \ref{fig:2:lhc_higgs_xsec} (left), the cross sections corresponding to the production of a SM Higgs boson 
are presented as a function of the center of mass energy, $\sqrt{s}$, for proton-proton (pp) collisions. 
This representation also includes bands to indicate theoretical uncertainties. 

A comprehensive discussion on uncertainties in the theoretical calculations, 
resulting from missing higher-order effects and the experimental uncertainties on the determination of SM parameters used in the calculations, 
can be found in references \cite{cyrm-2017-002}. 
These sources also offer advanced discussions on the impact of parton distribution function (PDF) uncertainties, 
Quantum Chromodynamics (QCD) scale uncertainties, 
uncertainties stemming from different procedures for including higher-order corrections matched to parton shower simulations, 
as well as uncertainties due to hadronisation and parton-shower events.

Table~\ref{tab:2:lhc_production} tabulates the production cross sections for a SM Higgs boson with a mass of 125 GeV in proton-proton collisions. These values are presented as functions of the center-of-mass energy \( \sqrt{s} \).
The projections for the Large Hadron Collider (LHC) energies have been sourced from reference\cite{cyrm-2017-002}. 
The estimates for the ggF channel at the LHC are inclusive of the most recent next-to-next-to-next-to leading order (N3LO) results, 
which have significantly cut down the theoretical uncertainties by approximately a factor of two, 
in comparison to the next-to-next-to leading order plus next-to leading logarithm (NNLO+NLL) results. 
It's worth noting that the total uncertainties were calculated under the assumption of no correlations between strong coupling constant ($\alpha_S$) and PDF uncertainties.

\begin{figure}[!htb]
    \centering
    \resizebox{\columnwidth}{!}{%
        \includegraphics[width=0.9\textwidth]{figures/2/higgs_production_diagram}
    }
    \caption{
        A collection of the primary Feynman diagrams at leading order contributing to Higgs boson production. 
        (a) Represents the gluon fusion process, 
        (b) showcases Vector-boson fusion, 
        (c) displays Higgs-strahlung or associated production with a gauge boson arising from a quark-quark interaction at the tree level, 
        (d) outlines associated production with a gauge boson from a gluon-gluon interaction at the loop level, 
        (e) depicts the associated production with a pair of top quarks (a similar diagram would represent the associated production with a pair of bottom quarks), 
        (e-f) illustrates the process of production in association with a single top quark.
    }
    \label{fig:2:lhc_higgs_diagram}
\end{figure}

\begin{figure}[!htb]
    \centering
    \resizebox{\columnwidth}{!}{%
        \includegraphics[width=0.9\textwidth]{figures/2/higgs_production_xsec}
    }
    \caption{
        (Left) The production cross sections of the SM Higgs boson as a function of the center of mass energy, 
        $\sqrt{s}$, for proton-proton collisions \cite{LHC_HIGGS}. 
        The VBF process is denoted here as $qqH$. 
        (Right) Branching ratios for the primary decays of the SM Higgs boson near a mass ($m_H$) of 125 GeV \cite{cyrm-2017-002}. 
        Bands represent theoretical uncertainties. 
    }
    \label{fig:2:lhc_higgs_xsec}
\end{figure}


\begin{table}
    \centering
    \resizebox{0.8\columnwidth}{!}{%
    \begin{tabular}{lcccccc}
        \toprule$\sqrt{s}(\mathrm{TeV})$ & \multicolumn{6}{c}{ Production cross section (in pb) for $m_H=125 \mathrm{GeV}$} \\
        \midrule & ggF & VBF & $W H$ & $Z H$ & $t \bar{t} H$ & total \\
        \midrule 
        7 & $16.9_{-7.0 \%}^{+4.4 \%}$ & $1.24_{-2.1 \%}^{+2.1 \%}$ & $0.58_{-2.3 \%}^{+2.2 \%}$ & $0.34_{-3.0 \%}^{+3.1 \%}$ & $0.09_{-10.2 \%}^{+5.6 \%}$ & 19.1 \\
        8 & $21.4_{-6.9 \%}^{+4.4 \%}$ & $1.60_{-2.1 \%}^{+2.3 \%}$ & $0.70_{-2.2 \%}^{+2.1 \%}$ & $0.42_{-2.9 \%}^{+3.4 \%}$ & $0.13_{-10.1 \%}^{+5.9 \%}$ & 24.2 \\
        13 & $48.6_{-6.7 \%}^{+4.6 \%}$ & $3.78_{-2.2 \%}^{+2.2 \%}$ & $1.37_{-2.6 \%}^{+2.6 \%}$ & $0.88_{-3.5 \%}^{+4.1 \%}$ & $0.50_{-9.9 \%}^{+6.8 \%}$ & 55.1 \\
        14 & $54.7_{-6.7 \%}^{+4.6 \%}$ & $4.28_{-2.2 \%}^{+2.2 \%}$ & $1.51_{-2.0 \%}^{+1.9 \%}$ & $0.99_{-3.7 \%}^{+4.1 \%}$ & $0.60_{-9.8 \%}^{+6.9 \%}$ & 62.1 \\
        \bottomrule
    \end{tabular}
    }
    \caption{Production cross sections of the Standard Model Higgs boson, with a mass of 125 GeV, in proton-proton collision}
    \label{tab:2:lhc_production}
\end{table}


At the LHC, two of the most prominent higgs production modes are the ggF and VBF.

The ggF is the dominant production mechanism for Higgs boson at the LHC. 
This process is a quantum loop process, 
where two gluons (g) emitted by incoming protons interact to produce a Higgs boson (H), 
with the help of a top quark loop.
Although gluons are massless and the Higgs boson couples to particles with mass, 
the strong coupling between gluons and top quarks allows this process via quantum loops. 
The process can be represented as $g + g \to H + X$. 
Given the high abundance of gluons in the proton at the LHC energies, 
this process has a high rate of occurrence, making it the primary channel for Higgs boson production at the LHC.

VBF is the second most common process for producing Higgs bosons at the LHC. 
In this process, the two protons each emit a W or Z boson, 
which then interact to produce the Higgs boson. 
This can be represented as $q + q \to q' + q' + H$, where $q$ and $q'$ are quarks. 
Unlike ggF, this process is not a loop process but a t-channel process. 
The scattered quarks usually result in two forward jets in the detector, 
with the Higgs boson produced centrally. 
This characteristic jet activity often assists in distinguishing the VBF process from other production mechanisms.