In the ATLAS experiment, the accurate reconstruction of charged particle tracks is crucial for a wide array of physics analyses, 
from the study of the Higgs boson to the search for new phenomena beyond the Standard Model. 
These tracks provide key insights into the underlying collision events and are fundamental for vertex identification, 
momentum measurement, and isolation criteria, among other parameters. 
The structure of the ATLAS Inner Detector is introduced in Section~\ref{sec:3:ID}, 
and the nomenclature of track coordinate is in Section~\ref{sec:3:coordinate}. 
Accurate track reconstruction\cite{track_1, NEWT} is especially vital for studies involving heavy flavor tagging, 
precision measurements of Standard Model processes, and the identification of long-lived particles. 
The process is complex, involving several steps to convert raw detector hits into particle trajectories. 
Each stage, from clusterization of signals to resolving track ambiguities, 
plays a critical role in ensuring the accuracy of the physics outcomes: 
\begin{enumerate}
    \item \textbf{The ID Reconstruction Sequences}\cite{NEWT}:
    In contemporary ID track reconstruction, the boundaries between pattern finding and track fitting have blurred. 
    Many modern pattern finding strategies go beyond the classical histogram-based methods 
    and include both global and local pattern recognition. 
    In this context, track fitting is often incorporated into the pattern finding process.
    Similarly, modern track fitters, like the combinatorial extension of the standard Kalman filter\cite{pei2019elementary} 
    or the deterministic annealing filter, 
    integrate pattern recognition as part of the fitting process. 
    Therefore, the sequence for ID reconstruction in ATLAS combines pattern finding and track fitting into a unified chain.

    The ID reconstruction adopts two primary sequences: 
    the main inside-out track reconstruction and a subsequent outside-in tracking. 
    These sequences are heavily influenced by the pre-existing ATLAS ID reconstruction program XKALMAN 
    but have been further enhanced with additional components following the NEWT approach. 
    There's also a third sequence for the finding of V0 vertices, kink objects, 
    and their associated tracks using common tracking tools and EDM, although this is not specific to the NEWT approach.
    \item \textbf{The Inside-Out Sequence}\cite{NEWT}:
    The inside-out tracking sequence begins with seed formation in the inner silicon tracker and progresses 
    towards the outer regions of the ID. 
    This process involves both global and local pattern recognition stages. 
    The global stage narrows down the possibilities, and the local stage works on this reduced set of candidates.
    The first phase in the inside-out sequence is the construction of three-dimensional representations of the silicon detector measurements. 
    Track seeds are formed from these 3D objects. 
    A window search is then performed based on the seed direction to build track candidates. 
    Hits from the detector elements that lie within this "road window" are collected and evaluated 
    based on a simplified Kalman filtering-smoothing approach. 
    These hits are then either added to the track candidates or rejected.
    \item \textbf{Iterative Combinatorial Track Finding}\cite{track_1}:
    The process starts by forming track seeds from sets of three space-points.
    This is a trade-off to allow a large number of combinations while still getting a rough initial momentum estimate. 
    The algorithm then estimates the impact parameters of these seeds with respect to the interaction region's center, 
    assuming a perfect helical trajectory in a uniform magnetic field. 
    Multiple criteria are then applied to these seeds to ensure a high level of purity. 
    These criteria include type-dependent momentum and impact parameter requirements, 
    as well as the necessity for one additional space-point that is compatible with the estimated particle's trajectory. 
    Subsequently, a Combinatorial Kalman filter\cite{track_3} is used to build track candidates 
    by incorporating additional space-points from the remaining layers of the pixel and SCT detectors.
    \item \textbf{Track Candidates and Ambiguity Solving}\cite{track_1, NEWT}:
    The process begins by assigning a score to each track candidate based on a variety of factors that assess the quality of the track. 
    This includes the number of clusters the track intersects with and the chi-square ($\chi^2$) value of the track fit. 
    After these scores are assigned, the track candidates are sorted in descending order. 
    The candidates are processed individually in this order. 
    This is under the premise that higher scores likely represent tracks that more accurately correspond to the trajectories of primary charged particles.

    Once sorted, each track candidate is rigorously evaluated against a set of basic quality criteria. 
    These criteria include a transverse momentum ($p_T$) greater than 400 MeV, and an absolute value of $\eta$ that is less than 2.5. 
    Additionally, the candidate must have a minimum of seven pixel and SCT clusters; 
    although 12 are typically expected. 
    Limits are also imposed on shared clusters: 
    a maximum of one shared pixel cluster or two shared SCT clusters on the same layer can be a part of the track. 
    Other parameters include not having more than two "holes" in the combined pixel and SCT detectors, 
    not having more than one hole in the pixel detector alone,  
    a transverse impact parameter ($|d^{BL}_0|$) less than 2.0 mm, and a longitudinal impact parameter ($|z^{BL}_0 \sin\theta|$|) less than 3.0 mm.

    Following the application of quality criteria, the ambiguity solver moves on to resolve any issues with shared clusters. 
    This step is crucial, as clusters can be attributed to more than one track. 
    In such cases, preference is given to tracks that were processed earlier. 
    Penalties are applied for sharing clusters that haven't been identified as 'merged'. 
    If a track candidate doesn't meet any of the quality criteria, it gets rejected. 
    The track is then re-scored and placed back into the list of remaining candidates for another round of processing. 
    This ensures that only the most promising track candidates make it into the final dataset, 
    thus enhancing the reliability and accuracy of particle track reconstruction. 
    An overview of this ambiguity solving procedures is shown in Figure~\ref{fig:4:ambiguity}. 
    \item \textbf{Neural-Network Pixel Clustering}\cite{track_1}:
    An artificial neural network\cite{track_4} is employed to assist in identifying merged clusters, 
    clusters which are created by the charge deposits from multiple particles. 
    This identification is based on the measured charge, relative positions of pixels within a cluster, 
    and additional information about the particle's incident angle. 
    The network has high efficiency in recognizing these merged clusters. 
    However, it doesn't break down these merged clusters into individual energy deposits.
    \item \textbf{Cluster Identification Logic}:
    Merged clusters can be identified in two distinct ways. 
    First, they can be flagged by the neural network when they are used in multiple track candidates. 
    Alternatively, clusters can be identified as merged if they appear on two consecutive layers and are used by the same track candidates. 
    Clusters identified as merged are used without penalty, whereas clusters not identified as merged, 
    but which are shared, can still be used but with a penalty as described earlier.
\end{enumerate}

\begin{figure}[!htb]
    \centering
    \resizebox{\columnwidth}{!}{%
        \includegraphics[width=\textwidth]{figures/4/ambiguity}
    }
    \caption{
        Overview of Track Candidate Progression within the Ambiguity Solver\cite{track_1}.
    }
    \label{fig:4:ambiguity}
\end{figure}

