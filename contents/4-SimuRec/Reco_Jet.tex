\subsubsection{Jet Reconstruction using PFlow Algorithm}
Two principal methodologies are used for jet reconstruction in the ATLAS experiment: calorimeter-based jet reconstruction and particle flow (PFlow) jet reconstruction. The former method utilizes topological clusters of calorimeter cells\cite{Aad_2017} and employs a Jet Energy Scale (JES) correction factor for calibration\cite{ATLAS-CONF-2015-002, ATLAS-CONF-2015-017}. 
This approach, while widely used, has limitations in resolution. The latter, PFlow reconstruction\cite{Aaboud:2257597}, integrates measurements from both the calorimeter and tracking systems to form an ensemble of 'particle flow objects,' aiming for enhanced granularity and resolution. The focus of the subsequent sections will be on the PFlow method, detailing its intricacies, advantages, and potential limitations in the jet reconstruction process.

\begin{figure}[!htb]
    \centering
    \resizebox{\columnwidth}{!}{%
        \includegraphics[width=\textwidth]{Figures/4/jet_PFlow}
    }
    \caption{
        Flow chart depicting the sequential steps of the Particle Flow algorithm from track selection to energy subtraction in the calorimeter\cite{Aaboud:2257597}. The end result comprises charged particles, unmodified topo-clusters, and topo-cluster remnants with partially removed energy.
    }
    \label{fig:4:PFlow}
\end{figure}

The PFlow algorithm operates through a systematic, multi-step procedure to optimally utilize both tracking and calorimetric information for the reconstruction of hadronic jets and soft activity. 
The procedure is shown in Figure~\ref{fig:4:PFlow}.
Initially, tracks are sorted in descending \(p_T\) order and selected based on stringent criteria. Each of these tracks is then matched to a single topo-cluster in the calorimeter. For each matched track-topo-cluster system, the algorithm calculates the expected energy deposition in the calorimeter based on the topo-cluster position and the track momentum. Given that a single particle may deposit energy in multiple topo-clusters, the algorithm evaluates this possibility and, when required, additional topo-clusters are added to the system to recover the full shower energy. Subsequently, cell-by-cell energy subtraction is performed for the matched topo-clusters. Any remnants that remain are removed if the residual energy aligns with the expected shower fluctuations of a single particle's signal. This meticulous process is fundamental for the accurate calculation of the missing transverse momentum \(E_{\text{miss}}^T\), in the event. The algorithm's validation relies on single-pion and dijet Monte Carlo samples without pile-up, with charged pions typically contributing approximately two-thirds of the visible jet energy.

PFlow jets are reconstructed using the anti-$k_t$ algorithm\cite{Cacciari_2008} with a radius parameter $R = 0.4$. Topo-clusters and tracks are the primary inputs, selected based on $|z_0 \sin \theta| < 2$ mm to minimize pile-up contributions. Calorimeter jets employ the same algorithm but use LC-scale topo-clusters. For both jet types, jet ghost-area subtraction\cite{Cacciari_2008_1, Cacciari_2008_2} is applied for pile-up correction, while a numerical inversion restores the jet response. The transverse energy density, $\rho$, in PFlow jets is computed from both charged and neutral PFlow objects, and is found to have a lower per-event value compared to calorimeter jets due to the exclusion of pile-up tracks. JES corrections account for variables such as charged fraction and energy fractions in different calorimeter layers. Data/MC comparisons\cite{ATLAS-CONF-2015-037} show a maximum deviation of 2\% in jet characteristics, confirming the robustness of the PFlow algorithm for jet reconstruction.


\subsubsection{b-jet Identification using RNN}
Jet flavor tagging, particularly the identification of jets originating from bottom quarks (b-jets), plays an indispensable role in the ATLAS experiment. While the baseline high-level b-tagging algorithm termed MV2c10\cite{ATL-PHYS-PUB-2016-012, b_tag_1} in ATLAS , utilizes a Boosted Decision Tree (BDT) in combination with kinematic features and algorithms like IP3D, SV1, and JetFitter, the nature of the data prompts an exploration into more advanced machine learning techniques. One key observation is that the transverse impact parameter significances (\(S_{d0}\)) of charged particles emanating from a b-hadron decay are not independently distributed but exhibit intrinsic correlations. MV2c10 and IP3D, which calculate per-flavor conditional likelihoods in a product space of 29,400 bins---specifically \(35 \times 20 \times 14 \times 3\) for \(S_{d0}\), \(S_{z0}\), and track category---operate under the assumption of feature independence and, thus, do not capture these interdependencies.

These limitations are addressed by employing Recurrent Neural Networks (RNNs)\cite{RNN_book}, which are capable of processing sequences of arbitrary lengths and capturing time-dependent correlations. Mathematically, given a sequence of tracks \( \{ x_1, x_2, \ldots, x_T \} \), an RNN computes its internal state \(h_t\) as follows:

\[
h_t = \phi(W_{hh}h_{t-1} + W_{xh}x_t + b_h),
\]

where \( \phi \) is an activation function, and \( W_{hh \text{,}} W_{xh} \) are the weight matrices. The final state \( h_T \) serves as a fixed-dimensional representation of the jet's properties and can be further processed by a fully connected layer to yield a b-tagging discriminant. RNN variants like Long Short-Term Memory (LSTM) and Gated Recurrent Units (GRUs) further extend this capability, enabling the efficient learning of long-term dependencies via specialized gating mechanisms.

\begin{figure}[!htb]
    \centering
    \resizebox{\columnwidth}{!}{%
        \includegraphics[width=0.47\textwidth]{Figures/4/b_tag_1}
        \includegraphics[width=0.47\textwidth]{Figures/4/b_tag_2}
    }
    \caption{Comparison of light-jet (left) and c-jet (right) rejection against b-tagging efficiency for jets with \( p_T > 20 \, \text{GeV} \) and \( |\eta| < 2.5 \). Statistical errors are within 3\%. MV2c10 serves as a high-level BDT-based benchmark, incorporating both IP3D and additional vertex metrics from JetFitter and SV1\cite{ATL-PHYS-PUB-2017-003_figure}.
    }
    \label{fig:4:b_tag}
\end{figure}

In an evaluation of the RNN-based b-tagging algorithm, both the b-tagging efficiency and background rejection were systematically examined across varying jet \( p_T \) values. A discriminant function \( D_{\text{RNN}} \), defined as 
\[
D_{\text{RNN}} = \ln \left( \frac{p_b}{f_c \cdot p_c + f_{\tau} \cdot p_{\tau} + (1 - f_c - f_{\tau}) \cdot p_{\text{light}}} \right),
\]
was employed, where \( f_c = 0.07 \) and \( f_{\tau} = 0 \). At a preset b-tagging efficiency of 70\%, the RNN algorithm surpassed IP3D, the baseline algorithm. Specifically, RNN exhibited a 2.5-fold increase in light-jet rejection and a 1.2-fold increase in c-jet rejection compared to IP3D. The comparative performances of RNN and other algorithms are encapsulated in Figure~\ref{fig:4:b_tag}. 



