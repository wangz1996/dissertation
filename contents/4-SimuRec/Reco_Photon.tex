In the ATLAS experiment, the techniques used for photon reconstruction closely parallel those used for electrons, as described in Section~\ref{sec:4:electron}. Given the similarities in how both particles interact within the electromagnetic calorimeter, the foundational reconstruction and identification methodologies are almost identical. As such, this section will not revisit the reconstruction algorithms for photons, but will directly proceed to discuss the specific identification (ID) and isolation procedures tailored for photons.

\subsubsection{Photon Identification and Isolation}
The ATLAS experiment employs a multi-tiered identification system for photons, aimed to effectively segregate prompt, isolated photons from backgrounds predominantly from hadronic jets. This identification is based on a set of one-dimensional selection criteria, commonly referred to as shower shape variables in Table~\ref{tab:4:electron_id}. The photon identification efficiencies are measured in 68 bins of photon \( E_T \) and \( \eta \) for both converted and unconverted photons. The bin edges are set at 
\begin{equation*}
    E_T = \{10, 15, 20, 25, 30, 35, 40, 45, 50, 60, 80, 100, 125, 150, 175, 250, 350, \infty \} \text{~GeV}
\end{equation*}
 and
 \begin{equation*}
    |\eta| = \{0, 0.6, 1.37, 1.52, 1.81, 2.37\}.
 \end{equation*}
No measurements are conducted for \( |\eta| = \{1.37, 1.52\} \) due to its overlap with the transition region of the ATLAS calorimeter.

Three distinct methods are employed to measure photon identification efficiencies\cite{Aaboud_2019}:
\begin{enumerate}
    \item \textbf{Radiative Z:} Using probe photons from \( Z \rightarrow \ell\ell\gamma \) decays with specific cuts on the invariant mass of the three-body system. The dominant uncertainty comes from low statistics at high \( E_T \) and MC generator modelling.
    \item \textbf{Matrix Method:} Application of a matrix method on an inclusive photon sample. The efficiency for Loose photon candidates to pass the Tight identification is extracted and corrected. The dominant systematic uncertainty arises from non-closure related to the assumption that isolation and identification efficiencies are independent.
    \item \textbf{Electron Extrapolation:}  Using electrons from \( Z \rightarrow e^+e^- \) decays to emulate photon behavior. The method closure uncertainty is dominant at low \( E_T \) but decreases with increasing \( E_T \).
\end{enumerate}

The efficiencies, as shown in Figure~\ref{fig:4:photon_ID_efficiency} are first measured in MC simulation, and the photon shower shapes are corrected to better align with the data\cite{Aaboud_2019}. The data-to-MC simulation ratios are then applied to account for residual differences between identification selection efficiencies. These are determined from a weighted average of the three methods in each \( E_T-\eta \) bin. 

\begin{figure}[!htb]
    \centering
    \begin{subfigure}{\textwidth}
        \centering
        \begin{subfigure}{.24\textwidth}
            \includegraphics[width=\linewidth]{figures/4/y_id_u_1.pdf}
        \end{subfigure}
        \begin{subfigure}{.24\textwidth}
            \includegraphics[width=\linewidth]{figures/4/y_id_u_2.pdf}
        \end{subfigure}
        \begin{subfigure}{.24\textwidth}
            \includegraphics[width=\linewidth]{figures/4/y_id_u_3.pdf}
        \end{subfigure}
        \begin{subfigure}{.24\textwidth}
            \includegraphics[width=\linewidth]{figures/4/y_id_u_4.pdf}
        \end{subfigure}
        \caption{Unconverted photons.}
    \end{subfigure}
    \vspace{1em} % Add a vertical space between the two sets of figures
    \begin{subfigure}{\textwidth}
        \centering
        \begin{subfigure}{.24\textwidth}
            \includegraphics[width=\linewidth]{figures/4/y_id_c_1.pdf}
        \end{subfigure}
        \begin{subfigure}{.24\textwidth}
            \includegraphics[width=\linewidth]{figures/4/y_id_c_2.pdf}
        \end{subfigure}
        \begin{subfigure}{.24\textwidth}
            \includegraphics[width=\linewidth]{figures/4/y_id_c_3.pdf}
        \end{subfigure}
        \begin{subfigure}{.24\textwidth}
            \includegraphics[width=\linewidth]{figures/4/y_id_c_4.pdf}
        \end{subfigure}
        \caption{Converted photons.}
    \end{subfigure}
    
    \caption{Efficiency measurements for unconverted and converted photons across different $\eta$ bins\cite{ATLAS_EGAM_2021_01}.}
    \label{fig:4:photon_ID_efficiency}
\end{figure}


\begin{table}[!htb]
    \centering
    \resizebox{0.80\columnwidth}{!}{
        \begin{tabular}{llc}
            \hline \hline Working point & Calorimeter isolation & Track isolation \\
            \hline Loose & \(E_{\mathrm{T}}^{\text {cone20 }}<0.065 \times E_{\mathrm{T}}\) & \(p_{\mathrm{T}}^{\text {cone20 }} / E_{\mathrm{T}}<0.05\) \\
            Tight & \(E_{\mathrm{T}}^{\text {cone40 }}<0.022 \times E_{\mathrm{T}}+2.45 \mathrm{GeV}\) & \(p_{\mathrm{T}}^{\text {cone20 }} / E_{\mathrm{T}}<0.05\) \\
            TightCaloOnly & \(E_{\mathrm{T}}^{\text {cone40 }}<0.022 \times E_{\mathrm{T}}+2.45 \mathrm{GeV}\) & - \\
            \hline \hline
            \end{tabular}
    }
    \caption{Definition of the photon isolation working points\cite{Aad_2019}.}
    \label{tab:4:photon_iso}
\end{table}

Table~\ref{tab:4:photon_iso} outlines three distinct operating points for photon isolation, focusing on both calorimeter and track isolation variables. The study\cite{Aad_2019} identifies discrepancies between data and simulations in the calorimeter-based isolation variables, which are then corrected using data-driven shifts. The effectiveness of these corrections is examined across two primary photon signatures: radiative Z decays and inclusive photons.



\subsubsection{Improvement of photon ID against electron fakes}
\label{sec:4:photon_id_electron_fakes}

The ATLAS experiment employs a sophisticated method for resolving ambiguities between electron and photon objects, primarily centered on a data structure known as a supercluster. These superclusters for electrons and photons are generated independently and are subjected to an initial round of energy calibration and position correction. Following this, tracks are correlated with electron superclusters, and conversion vertices are matched with photon superclusters using the same algorithmic approach that was employed for EM topo-clusters.

\begin{figure}[!htb]
    \centering
    \resizebox{\columnwidth}{!}{%
        \includegraphics[width=\textwidth]{Figures/4/ambiguity_flow}
    }
    \caption{
        Flowchart of the ambiguity resolution procedure for electron and photon objects\cite{Aad_2019}.
    }
    \label{fig:4:ambiguity_flow}
\end{figure}

A key point of differentiation arises here. If a seed cluster gives rise to both an electron and a photon supercluster, a specialized procedure outlined in Figure~\ref{fig:4:ambiguity_flow} is initiated. The aim is twofold: First, if a given object can be conclusively identified as a photon due to the absence of a 'good' associated track, then only a photon object is generated for downstream analysis. Similarly, if it can only be identified as an electron based on a good track and absence of a suitable photon conversion vertex, only an electron object is created. Second, if the object is intrinsically ambiguous, both electron and photon objects are generated and marked explicitly for future classification depending on the requirements of specific analyses.

After this ambiguity resolution step, another round of energy recalibration is performed, given that the initial calibration precedes the final matching of tracks and conversion vertices. This recalibration is conducted according to a previously described procedure.

Lastly, discriminative variables such as shower shape are computed for both electrons and photons. Interestingly, lateral shower shapes are independent of the clustering algorithm used and are based solely on the position of the most energetically active cell. These variables are pivotal for the final identification and classification of the electron and photon objects.

% Motivation for the ambiguity resolution procedure
With the current ambiguity solver, there is still a comparable amount of objects (electrons and photons) which are labelled as “ambiguity” type. With the mixture of pair production and photon conversion, separation between electrons and photons becomes challenging. 
This study is considered as the Qualification Task (QT) in the ATLAS experiment\cite{Zhang:2808007}.
In order to have better photon identification efficiency and high electron fake rejection efficiency, it is worth combining the topo-cluster information\cite{Guo:2774734} with the tracks information, which provides a set of variables with high separation power and is studied in the ambiguity tool. The ambiguity tool is a tool which is used during the EGamma reconstruction of electrons and photons. Due to the parallel reconstruction of electrons and photons, it occurs that the same constituents, such as topo-clusters and tracks, contribute to the formation of both electrons and photons.  To avoid to save all the combinations the ambiguity tool removes the easiest cases when the reconstructed particles are "for sure not electron" or "for sure not photon". In this cases the particles are saved only under one hypothesis. In the other case the reconstructed particles are saved both as electron and as photon and marked as "ambiguous".
Using the ambiguity objects is a good starting point to improve the photon identification criteria against photons faked by electrons. 

The single photon process "$\gamma$ + jets" is chosen to be the physics sample in order to study the variables of truth prompt photon.
There are two main mechanisms to produce photon + jets with p-p collision:
\begin{itemize}
    \item quark-gluon Compton scattering, $qg\rightarrow q\gamma$
    \item quark-antiquark annihilation, $q\Bar{q}\rightarrow g\gamma$
\end{itemize}

In order to study the photon and the electron fakes, two full simulated samples have been considered, summarized in Table~\ref{tab:4:mc_sample}. The inclusive single photon sample generated with \Pythia\xspace 8\cite{Sj_strand_2008}, including the leading order $\gamma$ + jets events are treated as signal. 
The background samples are generated with \POWHEG\xspace + \Pythia\xspace 8\cite{Alioli_2010}, including the \Zepem process. 


\begin{table}[!htb]
    \centering
    \resizebox{0.95\columnwidth}{!}{
        \begin{tabular}{llll}
            \toprule
            Process                 & Generator                & DSID          & Tag                                        \\
            \midrule
            \multirow{3}{*}{$\gamma$ + jets} & \multirow{3}{*}{\Pythia\xspace 8} & 423101        & e3904\_s3126\_r9364\_r9315\_p4191          \\
                                    &                          & 423102-423106 & e3791\_s3126\_r9364\_r9315\_p4191          \\
                                    &                          & 423107-423112 & e4453\_s3126\_r9364\_r9315\_p4191          \\
            \midrule
            \Zepem                 & PowhegPythia8EvtGen       & 361106        & e3601\_e5984\_s3126\_r10201\_r10210\_p4323 \\
            \bottomrule
            \end{tabular}
    }
    \caption{
        Single Photon signal samples and \Zepem background samples.
        % \textcolor{red}{TODO: redo the table with more generator information.}
    }
    \label{tab:4:mc_sample}
\end{table}

%%% Zepem reweighting
In the \( Z \rightarrow e^+ e^- \) process, what is categorized as a "fake" photon is in fact one of the two electrons misidentified as a photon at the reconstruction level. Compared to a single-photon signal sample (\( \gamma \) + jets), these fake photons typically exhibit lower \( p_T \) values, a consequence of originating from the decay of a 90 GeV object. 
Given these differences in basic kinematic variables (\( p_T \) and \( \eta \)), it becomes essential to reweight the \( |\eta|-p_T \) distribution of these fake photons in \( Z \rightarrow e^+ e^- \) events to align with the photon distribution in \( \gamma \) + jets signal events. This ensures that the optimization procedures that distinguish true photons from fake ones are not biased by these kinematic discrepancies. 
Thus, the \( Z \rightarrow e^+ e^- \) process serves as an essential background process where one of the decay electrons is misclassified, affecting the overall kinematic distribution. By contrast, in true \( \gamma \) events, the kinematic variables are inherently different, necessitating this reweighting for a more accurate analysis.

A direct 2D ($p_T$ and $\eta$) reweighting is applied on \Zepem background samples in order to scale the yields of background samples to signal yields in each $p_T$ and $\eta$ bins. 
The reweighting factor is simply dividing the weighted sum of yields of "$\gamma$ + jets" samples by the the weighted sum of yields of \Zepem samples for each $p_T$ and $\eta$ bins. 
In order to have finer reweighting, the kinematics binning strategy has been changed as followed:
\begin{itemize}
\label{fined_bin}
    \item $p_T$ [GeV]: 13 bins 
    \begin{itemize}
        \item $[25,30), [30,35), [35,40), [40,45),[45,50),[50, 60), [60,80), [80, 100)$ \\
        $[100,125), [125, 150), [150,175), [175, 250), [250, 1500)$
    \end{itemize}
    
    \item $|\eta|$: 6 bins
    \begin{itemize}
        \item $[0.0, 0.6),[0.6, 0.8),[0.8, 1.37), (1.52,1.81),[1.81, 2.01),[2.01, 2.37)$
    \end{itemize}
\end{itemize}

The 2D reweighting scale factors can be found in figure \ref{fig:4:reweighting_sf}.
 The 1D basic kinematic distributions are summarized in figure \ref{fig:4:distribution_1D}. 

\begin{figure}[!htbp]
    \subfloat[unconverted]{
      \includegraphics[width=0.47\textwidth]{figures/4/QT/reweighting/unc_2D_SF}
    }
    \hfill
    \subfloat[converted]{
      \includegraphics[width=0.47\textwidth]{figures/4/QT/reweighting/unc_2D_SF}
    }
    \caption{The distribution of reweighting scale factor (signal over background) in each $p_T$ and $\eta$ bin for both unconverted and converted case.}
    \label{fig:4:reweighting_sf}
\end{figure}

\begin{figure}[!htb]
    \centering
    \begin{subfigure}{\textwidth}
        \centering
        \begin{subfigure}{.24\textwidth}
            \includegraphics[width=\linewidth]{figures/4/QT/reweighting/distribution_1D/unc_eta_unscaled}
        \end{subfigure}
        \begin{subfigure}{.24\textwidth}
            \includegraphics[width=\linewidth]{figures/4/QT/reweighting/distribution_1D/unc_pt_unscaled}
        \end{subfigure}
        \begin{subfigure}{.24\textwidth}
            \includegraphics[width=\linewidth]{figures/4/QT/reweighting/distribution_1D/con_eta_unscaled}
        \end{subfigure}
        \begin{subfigure}{.24\textwidth}
            \includegraphics[width=\linewidth]{figures/4/QT/reweighting/distribution_1D/con_eta_unscaled}
        \end{subfigure}
        \caption{Uscaled.}
    \end{subfigure}
    \vspace{1em} % Add a vertical space between the two sets of figures
    \begin{subfigure}{\textwidth}
        \centering
        \begin{subfigure}{.24\textwidth}
            \includegraphics[width=\linewidth]{figures/4/QT/reweighting/distribution_1D/unc_eta_scaled}
        \end{subfigure}
        \begin{subfigure}{.24\textwidth}
            \includegraphics[width=\linewidth]{figures/4/QT/reweighting/distribution_1D/unc_pt_scaled}
        \end{subfigure}
        \begin{subfigure}{.24\textwidth}
            \includegraphics[width=\linewidth]{figures/4/QT/reweighting/distribution_1D/con_eta_scaled}
        \end{subfigure}
        \begin{subfigure}{.24\textwidth}
            \includegraphics[width=\linewidth]{figures/4/QT/reweighting/distribution_1D/con_eta_scaled}
        \end{subfigure}
        \caption{Scaled.}
    \end{subfigure}
    
    \caption{The 1D basic kinematic distributions before (top) and after (bottom) reweighting the basic kinematics of \Zepem to those of $\gamma$ + jets events.}
    \label{fig:4:distribution_1D}
\end{figure}

%%% Event Selection
In order to achieve good object quality, several basic selections need to be applied:
\begin{itemize}
    \item Event Quality: duplicated events are removed.
    \item Truth matching: signal photons are required to be matched to the truth photon, while background photons should be matched to the truth electron.
    \item Object removal (only signal sample): each signal sample, aims at producing a definite photon $p_T$ interval. Events out of the $p_T$ intervals at the truth level are excluded. 
    \item Loose photon ID: loose identification working point for photons is the minimal requirement for good object quality. 
    \item Loose photon Isolation: \textsf{FixedCutLoose\_proRec}\footnote{isolation defined as $E_{T}^{\text{cone}20} < 0.065p_{T}$ \&\& $p_{T}^{\text{cone}20} < 0.05p_{T}$ } working point is used in order to maximize the selection of good photon quality.
    \item Ambiguity type: event is selected if there is at least one ambiguous photon. Yields of ambiguous events and total events are summarized in table \ref{tab:4:ambiguity_table}.
\end{itemize}

\begin{table}[!htb]
    \centering
    \begin{tabular}{ccccc}
        \toprule
         &  & Ambiguous events & Total & Ratio \\ 
        \midrule
        \multirow{2}{*}{$\gamma$ + jets} & unconverted & 679,097 & 5,368,897 & 11.3\% \\
         & converted & 1,352,243 & 2,540,905 & 49.8\% \\ 
        \midrule
        \multirow{2}{*}{\Zepem} & unconverted & 1,564,090 & 3,125,334 & 50.7\% \\
         & converted & 7,077,013 & 7,701,844 & 91.9\% \\
         \bottomrule
    \end{tabular}
    \caption{Yields of pre-selected ambiguous events and total pre-selected events before ambiguity requirement for $\gamma$ + jets and \Zepem. }
    \label{tab:4:ambiguity_table}
\end{table}

%%% Variables
In order to improve the identification power to separate true photons from fake photons, this study uses a combination of variables from tracker and calorimeter. Many variables have been studied for distinguishing photon and photons faked by jets. They are the good starting to study photons faked by electrons. This analysis will focus on three sets of variables: shower shapes, Topo-clusters variables and ambiguity (tracker related) variables. 

For shower shapes, most of the variables have already been introduced in Table~\ref{tab:4:electron_id} and \Rhad and \Rhadone has been combined as one variable with different \Eta region. 
Three new shower shape variables from EM first layer are added to this study:
\begin{itemize}
    \item \Wetaone or \ensuremath{w_{s3}}: 
    Lateral shower width calculated from three strips around the strip with the highest energy deposit.
    \item \Fside: Energy fraction outside core of three central cells, within seven cells
    \item \DeltaE: Difference between the energy of the cell associated with the second maximum, 
    and the energy reconstructed in the cell with the smallest value found between the first and second maxima.
\end{itemize}

In total five topo-cluster variables are considered in this study (Only variables from the leading topo-cluster are included):
\begin{itemize}
    \item \Etopo: total positive energy of the leading topo-cluster.
    \item \secR: the second moment of the radial (shortest) distance of cell to the shower axis.
    \item \secL: the second moment of the longitudinal distance of cell from the cluster centre of gravity measured along shower axis. 
    \item \absR: the radial (shortest) distance of cell to the shower axis, $\sqrt{x^2+y^2+z^2}$.
    \item \cenL: shower depth at its centroid of the topo-cluster.
\end{itemize}
These variables were studied in detail, and have been proved to be the most discriminating among all moments in the previous analysis \cite{Guo:2774734}. Other powerful variables such as signal moments can be studied in the future. 

In the current cutflow of the ambiguity tools in figure \ref{fig:4:ambiguity_flow}, there are 13 variables used to help separating electrons, photons and ambiguity type. In order to optimize the ambiguity efficiency on photon and electron fakes, the following 5 variables are chosen:
\begin{itemize}
    \item \ambPT: the transverse momentum of the reconstructed electron which is reconstructed using the same cluster of the reconstructed photon.
    \item \ambP: the $p_{T}$ measured using the track associated to the reconstructed electron which is using the same cluster of the reconstructed photon.
    \item \EP: the ratio of energy (measured from the cluster) and momentum (measured from the track) of the ambiguity object. If the track is a random track (for example if the object is a true unconverted photon and it is) this variable is expected to be far from 1. On the contrary if it is a real electron, this variable is expected to be close to 1.
    \item \phit: number of pixel hits from the track.
    \item \shit: number of silicon hits from the track.
\end{itemize}

%%% Optimization
Due to the detector performance and the reconstruction efficiency, the behaviour of photons will be different in various 
$p_T$ and $\eta$ region. This prompts the kinematics region splitting so that in each region, an individual optimization is studied due to the distinct physics performance. 
The $p_T$ and $\eta$ binning is chosen to ensure enough statistics in both low $p_T$ (large \Zepem statistics) and high $p_T$ (large $\gamma$ + jets statistics) region: 
\begin{itemize}
    \item $p_T$ [GeV]: 4 bins 
    \begin{itemize}
        \item $[25, 40), [40, 55), [55,80), [80, 1500)$
    \end{itemize}
    \item $|\eta|$: 5 bins
    \begin{itemize}
        \item $[0, 0.6), [0.6, 1.37), (1.52,1.81), [1.81, 2.01), [2.01, 2.37)$
    \end{itemize}
\end{itemize}

Rectangular optimization is considered to be the strategy in this study.
In some photon-related analyses such as the $H \to \gamma\gamma $ analysis, the selections in the photon identification need to be partially reversed in order to estimate photon fakes. For this specific reason, BDT or deep learning cannot be used in the optimization strategy.
There are 19 available discriminating variables, which are unnecessary for rectangular optimization. Rectangular optimization does not heavily rely on the correlation and the underlying effect. So a better way is to shrink the size of available discriminating variables to a relatively small number, reducing the training time. 
The selection of this number depends on the overall performance of the final results. The cases of 5, 9, 10, and 19 have been tested for the final performance. Results with 9 variables are better than with 5, and give similar performance than when using more variables.
It is thus better to use 9 discriminating variables to train for more stable and smooth performance\cite{Zhang:2808007}. 
The variable separation ranking from the BDT method \cite{TMVA} can be a good guide to select discriminating variables, as listed in table \ref{tab:4:variable_separation}. 
There are two strategies for selecting variables:
\begin{enumerate}
    \item Use the variable separation ranking from the inclusive region (not splitting $p_T$ and $\eta$ into finer bins) and then choose the fixed top 9 variables for all sub-regions. 
    \item Run the variable separation ranking for each $p_T$ and $\eta$ region, and then choose the top 9 variables for each region. 
\end{enumerate}
Both strategies have been studied and the difference in the final signal identification efficiency is small. This study will focus on the first strategy using fixed top 9 variables from the inclusive region. (The performance of these two methods is similar, but for simplicity and stability, method one is preferred.)

\begin{table}[!htb]
    \centering
    \resizebox{0.65\columnwidth}{!}{
        \begin{tabular}{ccccc}
            \toprule
            \multirow{2}{*}{rank} & \multicolumn{1}{c}{variable} & \multicolumn{1}{c}{separation} & \multicolumn{1}{c}{variable} & \multicolumn{1}{c}{separation} \\
            \cmidrule{2-5}
             & \multicolumn{2}{c}{unconverted} & \multicolumn{2}{c}{converted} \\ \midrule
            1 & \multicolumn{1}{l}{\EP} & 0.057100 & \multicolumn{1}{l}{\phit} & 0.071860 \\ 
            2 & \multicolumn{1}{l}{\ambP} & 0.031760 & \multicolumn{1}{l}{\shit} & 0.037530 \\ 
            3 & \multicolumn{1}{l}{\Rphi} & 0.016490 & \multicolumn{1}{l}{\Rphi} & 0.026240 \\ 
            4 & \multicolumn{1}{l}{\phit} & 0.016280 & \multicolumn{1}{l}{\EP} & 0.021060 \\ 
            5 & \multicolumn{1}{l}{\shit} & 0.015300 & \multicolumn{1}{l}{\DeltaE} & 0.019500 \\ 
            6 & \multicolumn{1}{l}{\secR} & 0.008835 & \multicolumn{1}{l}{\Fone} & 0.018310 \\ 
            7 & \multicolumn{1}{l}{\DeltaE} & 0.008517 & \multicolumn{1}{l}{\Eratio} & 0.016430 \\ 
            8 & \multicolumn{1}{l}{\Eratio} & 0.006444 & \multicolumn{1}{l}{\Wetatwo} & 0.009212 \\ 
            9 & \multicolumn{1}{l}{\Wetatwo} & 0.004687 & \multicolumn{1}{l}{\Reta} & 0.007168 \\ 
            10 & \multicolumn{1}{l}{\Fone} & 0.004124 & \multicolumn{1}{l}{\ambPT} & 0.005839 \\ 
            11 & \multicolumn{1}{l}{\secL} & 0.002756 & \multicolumn{1}{l}{\secR} & 0.005554 \\ 
            12 & \multicolumn{1}{l}{\cenL} & 0.002653 & \multicolumn{1}{l}{\cenL} & 0.003968 \\ 
            13 & \multicolumn{1}{l}{\ambPT} & 0.002644 & \multicolumn{1}{l}{\Etopo} & 0.001319 \\ 
            14 & \multicolumn{1}{l}{\Rhadone} & 0.002242 & \multicolumn{1}{l}{\absR} & 0.001108 \\ 
            15 & \multicolumn{1}{l}{\Reta} & 0.001873 & \multicolumn{1}{l}{\secL} & 0.000545 \\ 
            16 & \multicolumn{1}{l}{\Wetaone} & 0.001412 & \multicolumn{1}{l}{\Rhadone} & 0.000455 \\
            17 & \multicolumn{1}{l}{\Etopo} & 0.000907 & \multicolumn{1}{l}{\Wetaone} & 0.000274 \\
            18 & \multicolumn{1}{l}{\absR} & 0.000466 & \multicolumn{1}{l}{\ambP} & 0.000051 \\ 
            19 & \multicolumn{1}{l}{\Wtots} & 0.000049 & \multicolumn{1}{l}{\Wtots} & 0.000004 \\ 
            \bottomrule
        \end{tabular}
    }
    \caption{Separation power of shower shapes, topo-cluster, and ambiguity variables, which are retrieved from the BDT method from TMVA package, since the \textsf{kCut} method does not provide the variable ranking}
    \label{tab:4:variable_separation}
\end{table}


Table \ref{tab:4:variable_separation} shows that the most discriminating variable for both unconverted and converted cases is from the ambiguity variables, which follows the physics intuition that the main difference for photon and electron is if the track is well matching the cluster. For the converted case, the main separating variables are ambiguity variables and Shower shapes. Topo-cluster variables have a relatively low separation power compared with the other two sets of variables. But for the unconverted case, all three sets of variables play an important role in separation. \EP and \Rphi have a high rank for both two cases.
So the final top 9 discriminating variables are:
\begin{itemize}
    \item \textbf{unconverted}: \EP, \ambP, \secR, \Rphi, \DeltaE, \Eratio, \phit, \shit and \Wetatwo.
    \item \textbf{converted}: \phit, \shit, \EP, \Eratio, \DeltaE, \Rphi, \Fone, \Wetatwo, and \Reta.
\end{itemize}
The finer rectangular optimization will be based on these 9 variables. 


The real application of this rectangular optimization is based on full photon container, which includes both photons identified as photons and photons identified as ambiguous objects. 
The performance of this study can be measured using the background efficiency when the signal efficiency is at a certain level ($80\%$, $60\%$, and $50\%$). The overall performance for the unconverted case is better than for the converted case. As expected, photons faked by electrons concentrate in the low $p_T$ and low $\eta$ regions. The fake photon rate decreases when $p_T$ and $\eta$ increase. 
The corresponding ROC curves are summarized in figure \ref{fig:4:roc}. 
Cut efficiencies both for $\gamma$ + jets and \Zepem when signal efficiency is 80\%, 60\% and 50\% respectively can be found in Figure~\ref{fig:4:efficiency_comparison}. 

\begin{figure}[!htb]
\centering
    \subfloat[unconverted]{
      \includegraphics[width=0.45\textwidth]{figures/4/QT/ROC_unc}
    }
    \subfloat[converted]{
      \includegraphics[width=0.45\textwidth]{figures/4/QT/ROC_con}
    }
    \caption{The ROC performance of rectangular optimization for all $p_T$ and $\eta$ bins. The grey line is signal efficiency of 80\%.}
    \label{fig:4:roc}
\end{figure}

\begin{figure}[!htb]
    \centering
    % First row: Level 81
    \includegraphics[width=0.24\textwidth]{figures/4/QT/results_full/effS_81_unconv.pdf}
    \includegraphics[width=0.24\textwidth]{figures/4/QT/results_full/effB_81_unconv.pdf}
    \includegraphics[width=0.24\textwidth]{figures/4/QT/results_full/effS_81_conv.pdf}
    \includegraphics[width=0.24\textwidth]{figures/4/QT/results_full/effB_81_conv.pdf}
    \caption*{80\%: Left(S,B): unconverted $\gamma$; Right(S,B): converted $\gamma$}
    
    % Second row: Level 61
    \includegraphics[width=0.24\textwidth]{figures/4/QT/results_full/effS_61_unconv.pdf}
    \includegraphics[width=0.24\textwidth]{figures/4/QT/results_full/effB_61_unconv.pdf}
    \includegraphics[width=0.24\textwidth]{figures/4/QT/results_full/effS_61_conv.pdf}
    \includegraphics[width=0.24\textwidth]{figures/4/QT/results_full/effB_61_conv.pdf}
    \caption*{60\%: Left(S,B): unconverted $\gamma$; Right(S,B): converted $\gamma$}
    
    % Third row: Level 51
    \includegraphics[width=0.24\textwidth]{figures/4/QT/results_full/effS_51_unconv.pdf}
    \includegraphics[width=0.24\textwidth]{figures/4/QT/results_full/effB_51_unconv.pdf}
    \includegraphics[width=0.24\textwidth]{figures/4/QT/results_full/effS_51_conv.pdf}
    \includegraphics[width=0.24\textwidth]{figures/4/QT/results_full/effB_51_conv.pdf}
    \caption*{50\%: Left(S,B): unconverted $\gamma$; Right(S,B): converted $\gamma$}

    \caption{Comparison of Photon Conversion Efficiency: Rows correspond to 80\%, 60\%, and 50\% efficiency levels. Each row comprises unconverted and converted $\gamma$ for signal and background.}
    \label{fig:4:efficiency_comparison}
\end{figure}




