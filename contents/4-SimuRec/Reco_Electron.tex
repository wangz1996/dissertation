\subsubsection{Electron Reconstruction}
The updated algorithm\cite{ATL-PHYS-PUB-2017-022} for reconstructing electrons and photons commences by identifying topo-clusters 
that are viable candidates for forming these particles. 
Following this, the tracks are refitted with an understanding of bremsstrahlung processes 
and then matched to the previously selected topo-clusters. 
The refitted tracks also serve as the foundation for constructing conversion vertices, 
which are then aligned with the selected topo-clusters.

Subsequent to this initial phase of track-cluster association and conversion vertex formation, 
separate routines for electron and photon superclustering are invoked concurrently. 
The resulting superclusters undergo preliminary positional adjustments, 
and are then matched with tracks for electrons and with conversion vertices for photons.

An electron is conventionally described as an entity that pairs a calorimeter-based supercluster with a compatible track or tracks. 
Conversely, a converted photon pairs a calorimeter cluster with one or multiple conversion vertices, 
while an unconverted photon pairs with neither an electron track nor a conversion vertex. 

Because a single object can be simultaneously reconstructed as both an electron and a photon, 
a specialized ambiguity resolution step is employed to minimize such overlaps. 
However, a certain degree of overlap is deliberately preserved to ensure efficient reconstruction rates for both particle types, 
enabling subsequent physics analyses to apply more specialized criteria as needed.

The calibrated final forms of the electrons and photons are then produced, 
paving the way for the computation of various additional metrics that are essential for quality control and further ambiguity resolution. 
Prior methodologies for electron and photon reconstruction are detailed in References \cite{Aaboud_2017} and \cite{Aaboud_2016}. 
A visual representation summarizing the flow of the revamped algorithm for electron and photon reconstruction can be found in Figure~\ref{fig:4:e_y_reco},
and a conceptual diagram depicting an electron's journey through the various components of the detector is shown in Figure~\ref{fig:4:electron_reco_id}.


\begin{figure}[!htb]
    \centering
    \resizebox{\columnwidth}{!}{%
        \includegraphics[width=\textwidth]{figures/4/electron_reco_id}
    }
    \caption{
        A conceptual diagram depicting an electron's journey through the various components of the detector\cite{electron_reco_id}. 
        The solid red line represents the speculated path of an electron as it moves initially through the tracking system, 
        starting with the pixel detectors, followed by the silicon-strip detectors, 
        and finally through the TRT, before entering the electromagnetic calorimeter. 
        The dotted red line illustrates the trajectory of a photon generated 
        due to the electron's interaction with the tracking system's material.
    }
    \label{fig:4:electron_reco_id}
\end{figure}


\begin{figure}[!htb]
    \centering
    \resizebox{0.8\columnwidth}{!}{%
        \includegraphics[width=\textwidth]{figures/4/e_y_recon}
    }
    \caption{
        Algorithm flow diagram for the new electron and photon reconstruction\cite{ATL-PHYS-PUB-2017-022}.
    }
    \label{fig:4:e_y_reco}
\end{figure}


In the formation of topological clusters (topo-clusters) within the ATLAS detector, 
the concept of cell significance plays a pivotal role. 
This is represented by the variable $\zeta^\text{EM}_{\text{cell}}$. 
The equation for calculating cell significance is given by:

\begin{equation}
    \zeta^\text{EM}_{\text{cell}} = \frac{|E^\text{EM}_{\text{cell}}|}{\sigma^\text{EM}_{\text{noise, cell}}}
\end{equation}

Here, $|E^\text{EM}_{\text{cell}} |$ is the absolute energy of the cell at the electromagnetic (EM) scale, 
and $ \sigma^\text{EM}_{\text{noise, cell}} $ is the expected noise for that cell. 
The initial clustering process starts with seed cells having $ \zeta^\text{EM}_{\text{cell}} \geq 4 $. 
These seed cells then collect neighboring cells with $ \zeta^\text{EM}_{\text{cell}} \geq 2 $.

When proto-clusters share a cell and the energy of that cell significantly exceeds the noise threshold, 
these proto-clusters are merged. 
After collecting all cells with energies above the noise threshold, 
an additional set of neighboring cells with $ \zeta^\text{EM}_{\text{cell}} \geq 0$ are added to form the cluster. 
This process is often referred to as the "4-2-0" topo-cluster reconstruction methodology.

For clusters requiring splitting due to the presence of multiple local energy maxima, 
fractional cell weights are calculated based on cluster energies and distances from the cell to the centers of gravity of the clusters. 
The formulas for these weights\cite{Aad_2017} are as follows:
\begin{equation}
    \begin{aligned}
w_{\text{cell},1} & = \frac{E^\text{EM}_{\text{clus},1}}{E^\text{EM}_{\text{clus},1} + r E^\text{EM}_{\text{clus},2}} \\
w_{\text{cell},2} & = 1 - w_{\text{cell},1} = \frac{r E^\text{EM}_{\text{clus},2}}{E^\text{EM}_{\text{clus},1} + r E^\text{EM}_{\text{clus},2}} \\
    \end{aligned}
\end{equation}
Here, $ r = \exp(d_1 - d_2) $, where $ d_1 $ and $ d_2 $ are the distances of the cell to the centers of gravity of the first and second clusters, respectively.

Electromagnetic (EM) topological clusters (topo-clusters) are constructed by an algorithm that initially uses the universal "4-2-0" rules 
but later refines these to consider only cells from the EM calorimeter. 
This procedure serves multiple broader detector goals, 
including particle flow reconstruction and future isolation calculation enhancements.

The algorithm starts by duplicating the existing set of 4-2-0 topo-clusters. 
It then focuses on isolating clusters originating primarily from EM showers by applying an EM fraction ($ f_{\text{EM}} $) criterion, 
calculated as follows:

\begin{equation}
    f_{\mathrm{EM}}=\frac{E_{\mathrm{L} 1}+E_{\mathrm{L} 2}+E_{\mathrm{L} 3}+w \cdot\left(E_{\mathrm{E} 4}+E_{\mathrm{PS}}\right)}{E_{\text {clus }}}, \quad w= \begin{cases}1, & 1.37<|\eta|<1.63 \\ 0, & \text { otherwise }\end{cases}
\end{equation}

Here, $w$ takes different values based on the $ \eta $ region of the cluster, 
$E_{Lx}$ is the energy in layer $x$ of the calorimeter, 
and $E_{\text{clus}}$ is the total cluster energy. 
After removing cells from the hadronic calorimeter to reduce noise, the cluster's kinematics are recalculated.

A minimum energy threshold of $E_T > 400$ MeV is enforced to eliminate noise and irrelevant clusters. 
Through extensive Monte Carlo simulations, the $f_{\text{EM}}$ value was optimized, 
leading to a pre-selection requirement of $f_{\text{EM}} > 0.5$. 
This criterion successfully filters out approximately 60\% of pile-up-induced clusters while maintaining efficient electron identification.

% Track Maching to Cluster
In the electron track matching procedure, refitted tracks that are loosely matched initially undergo more stringent selection to be matched to EM clusters. 
This selection is defined with tighter constraints in $\eta$ and $\phi$. 
When there are multiple candidate tracks for a single cluster, 
a hierarchy is applied to determine the track that will define the electron's properties:
\begin{enumerate}
\item Tracks with hits in the pixel detector are prioritized.
\item Next, preference is given to tracks with hits in the SCT, but not in the pixel detector.
\end{enumerate}
Within each category, tracks are further ranked based on their $\Delta R$ in $\eta - \phi$ space. 
Two types of $\Delta R$ calculations are performed:
\begin{enumerate}
\item The track momentum is rescaled to the cluster energy for the first calculation.
\item The second calculation uses the original, unrescaled track momentum.
\end{enumerate}

The track with the better $\Delta R$ match is preferred, unless the differences are minimal, 
in which case the track with more pixel hits is chosen. 
Special weight is given to a hit in the innermost pixel layer. 
Momentum rescaling aids in achieving better track-cluster matching, 
especially when track momentum undergoes significant changes due to bremsstrahlung radiation. 
Distance-related variables between the track and cluster are also utilized for this matching, as detailed in the referenced studies\cite{Aaboud_2017}.

% Supercluster reconstruction
The electron supercluster reconstruction\cite{He:2841383} occurs in two main stages:
\begin{enumerate}
    \item \textbf{Seed Cluster Identification}: 
    EM topoclusters are scrutinized to act as seed cluster candidates. 
    A cluster needs to have a minimum $ E_T $ of 1 GeV 
    and must be associated with a track having at least four hits in the silicon trackers to qualify as a seed.
    \item \textbf{Satellite Cluster Identification}: 
    EM topoclusters near the seed are identified as potential satellite clusters, 
    which can arise from bremsstrahlung radiation or topo-cluster splitting. 
    If these satellite clusters satisfy specific criteria, they are added to the seed cluster to form the final supercluster.
\end{enumerate}

Spatial criteria for defining a satellite cluster involve two conditions:
\begin{itemize}
    \item A cluster is categorized as a satellite if it lies 
    within a $ \Delta \eta \times \Delta \phi = 0.075 \times 0.125 $ window around the seed cluster barycenter.
    \item For electrons, a cluster is also considered a satellite if it is 
    within a $ \Delta \eta \times \Delta \phi = 0.125 \times 0.300 $ window, 
    and its 'best-matched' track is also the best-matched track for the seed cluster.
\end{itemize}

After superclusters are assembled, initial energy calibration is performed. The superclusters, therefore, improve the energy resolution of electron candidates, quantified by the IQE (Interquartile Energy), defined as:
\begin{equation}
    \text{IQE} = \frac{Q_3 - Q_1}{1.349}
\end{equation}
Here, $ Q_1 $ and $ Q_3 $ are the first and third quartiles of the $E_{\text{calib}} / E_{\text{true}}$ distribution.
The normalization factor of 1.349 ensures that the IQE for a Gaussian distribution is equivalent to its standard deviation.


% Electron ID
\subsubsection{Electron Identification}
One commonly used approach for electron identification\cite{Aaboud_2019} is the likelihood-based (LH) method, 
which excels in differentiating prompt electrons from other similar signals, such as jets or non-prompt electrons.

The LH method utilizes a series of measurements obtained from both the tracking and calorimeter systems of the detector. These measurements are modeled using probability density functions (PDFs). Mathematically, the likelihoods for signal $L_S$ and background $L_B$ are calculated using the equation
\begin{equation}
\label{eq:lh}
L_{S(B)}(x) = \prod_{i=1}^{n} P_{S(B),i}(x_i).
\end{equation}
To make a decision based on these likelihoods, a discriminant $d_L$ is formed as
$d_L = \frac{L_S}{L_S + L_B}.$
However, $d_L$ has a sharp peak, making it difficult to set a threshold for identification. 
To mitigate this, $d_L$ is further transformed using an inverse sigmoid function:
$d'_{L} = -\tau^{-1} \ln(d^{-1}_L - 1),$
where $\tau$ is a constant set to 15\cite{TMVA}. 
This transformation makes it easier to differentiate between signal and background.

\begin{table}[!htb]
    \centering
    \resizebox{0.92\columnwidth}{!}{
        \input{tabulars/Eelectron_ID}
    }
    \caption{Summary of Quantities for Electron Identification.}
    \label{tab:4:electron_id}
\end{table}

In the Table~\ref{tab:4:electron_id} that summarizes quantities for electron identification, 
various metrics are presented that are instrumental for the identification of prompt electrons. 
Columns labeled ``Rejects'' are provided to indicate the discriminative power of each metric against other signals 
such as light-flavour (LF) jets, photon conversions ($\gamma$), 
and non-prompt electrons originating from heavy-flavour (HF) quarks like b- or c-quarks. 
Another column, labeled ``Usage,'' is included to specify how each metric is used in the identification process. 
Metrics marked with ``LH'' are incorporated into the likelihood functions $L_S$ and $L_B$, 
as specified in Equation~\ref{eq:lh}. 
Metrics marked with ``C'' are utilized as direct selection criteria. 
For metrics that make use of the second layer of the calorimeter, 
notations like $3\times3$, $3\times5$, $3\times7$, 
and $7\times7$ are employed to indicate the specific regions in $\Delta \eta \times \Delta \phi$ space that are covered, 
with each unit being $0.025\times0.025$.

There are two major advantages to using the LH method over traditional cut-based identification techniques. 
First, it provides a more holistic evaluation of an electron candidate. 
In cut-based identification, failing to meet even a single condition could result in a false negative. 
In contrast, the LH method takes into account the collective information from all variables. 
Second, the LH method can include additional discriminating variables 
that may not be distinct enough for cut-based methods but still provide valuable information.

\begin{figure}[!htb]
    \centering
    \begin{subfigure}{0.47\textwidth}
        \includegraphics[width=\textwidth]{figures/4/e_eff_ET}
        \caption{}
        \label{fig:4:e_ID_eff:sub1}
    \end{subfigure}%
    \hfill
    \begin{subfigure}{0.47\textwidth}
        \includegraphics[width=\textwidth]{figures/4/e_eff_eta}
        \caption{}
        \label{fig:4:e_ID_eff:sub2}
    \end{subfigure}
    \caption{
        Efficiency plots of electron ID working points\cite{ATLAS_EGAM_2022_02}.
        }
    \label{fig:4:e_ID_eff}
\end{figure}

Figure~\ref{fig:4:e_ID_eff} comprises two plots that present the efficiencies of various electron identification working points 
during the 2015-2018 Run 2 data from the LHC in $Z \rightarrow e^+e^-$ events. 
Figure~\ref{fig:4:e_ID_eff:sub1} on the left illustrates these efficiencies as they relate to electron transverse energy $(E_T$). 
It not only showcases the aggregate data but also highlights the outcomes of individual efficiency assessment techniques, 
specifically the Z-mass and Z-isolation methods. 
These methods and working points can be found in the related study~\cite{Aad_2019}. 
Error indicators on this plot encompass both statistical and systematic uncertainties. 

Figure~\ref{fig:4:e_ID_eff:sub2} on the right focuses on the efficiencies as functions of the electron pseudorapidity ($\eta$). 
This plot also provides statistical and systematic uncertainty bands. 
A noteworthy feature of this plot is the decreased efficiency in the transitional zone 
between the electromagnetic calorimeter barrel and endcap, 
specifically for $1.37 < |\eta| < 1.52$, implemented to maintain manageable background levels. 


\subsubsection{Electron Isolation}
Isolation for electrons in the study is quantified through two primary methods: calorimeter-based and track-based isolation variables. 

In the calorimeter-based approach, the raw transverse energy \(E_{T,\text{raw}}^{\text{isol}}\) is calculated by summing the transverse energies of topological clusters found within a specific cone around the electron or photon cluster center. The EM particle energy \(E_{T,\text{core}}\) is subtracted out from this raw value. Corrections for energy leakage and pile-up are then applied. The final corrected calorimeter isolation variable is given by
\begin{equation}
    \label{eq:isolation:calo}
    E_{\mathrm{T}}^{\text {coneXX }}=E_{\mathrm{T}, \text { raw }}^{\text {isolXX }}-E_{\mathrm{T} \text {, core }}-E_{\mathrm{T} \text {, leakage }}\left(E_{\mathrm{T}}, \eta, \Delta R\right)-E_{\mathrm{T} \text {, pile-up }}(\eta, \Delta R),
\end{equation}
where \(\Delta R = \frac{XX}{100}\). For electron working points, a cone size of \(\Delta R = 0.2\) is utilized, while for photons, cone sizes of \(\Delta R = 0.2\) and \(\Delta R = 0.4\) are employed.

The track isolation variable \(p_{T}^{\text{coneXX}}\) considers the transverse momentum of selected tracks within a cone centered around the electron or photon. For electrons, a variable cone size \(p_{T}^{\text{varconeXX}}\) is used, defined as
\begin{equation}
    \label{eq:isolation:track}
    \Delta R=\min \left(\frac{10}{p_{\mathrm{T}}[\mathrm{GeV}]}, \Delta R_{\max }\right)
\end{equation}
with \(\Delta R_{\text{max}}\) typically being 0.2. The tracks in this approach must have \(p_T > 1\) GeV and \(|\eta| < 2.5\), along with certain hit and hole requirements in the silicon detectors.


\begin{table}[!htb]
    \centering
    \resizebox{\columnwidth}{!}{
        \begin{tabular}{lcc}
            \toprule Working point & Calorimeter isolation & Track isolation \\
            \midrule Gradient & \(\epsilon=0.1143 \times p_{\mathrm{T}}+92.14 \%\left(\right.\) with \(\left.E_{\mathrm{T}}^{\text {cone20 }}\right)\) & \(\epsilon=0.1143 \times p_{\mathrm{T}}+92.14 \%\) (with \(\left.p_{\mathrm{T}}^{\text {varcone20 }}\right)\) \\
            \midrule HighPtCaloOnly & \(E_{\mathrm{T}}^{\text {cone20 }}<\max \left(0.015 \times p_{\mathrm{T}}, 3.5 \mathrm{GeV}\right)\) & - \\
            Loose & \(E_{\mathrm{T}}^{\text {cone20 }} / p_{\mathrm{T}}<0.20\) & \(p_{\mathrm{T}}^{\text {varcone20 }} / p_{\mathrm{T}}<0.15\) \\
            Tight & \(E_{\mathrm{T}}^{\text {cone20 }} / p_{\mathrm{T}}<0.06\) & \(p_{\mathrm{T}}^{\text {varcone20 }} / p_{\mathrm{T}}<0.06\) \\
            \bottomrule
        \end{tabular}
    }
    \caption{Description of electron isolation benchmarks and their corresponding performance metrics. For the Gradient strategy, the transverse momentum (\(p_T\)) is expressed in GeV. A consistent cone dimension of \(\Delta R = 0.2\) is employed for both calorimeter and track-based isolation methods, with a maximum cone size of \(\Delta R_{\text{max}} = 0.2\) designated for track isolation.
    }
    \label{tab:4:electron_isolation_benchmark}
\end{table}

\begin{figure}[!htb]
    \centering
    \begin{subfigure}{0.45\textwidth}
        \includegraphics[width=\textwidth]{Figures/4/e_iso_ET_loose}
    \end{subfigure}
    \hfill % Fill the empty space
    \begin{subfigure}{0.45\textwidth}
        \includegraphics[width=\textwidth]{Figures/4/e_iso_ET_Tight}
    \end{subfigure}
    \\
    \begin{subfigure}{0.45\textwidth}
        \includegraphics[width=\textwidth]{Figures/4/e_iso_Eta_loose}
    \end{subfigure}
    \hfill % Fill the empty space
    \begin{subfigure}{0.45\textwidth}
        \includegraphics[width=\textwidth]{Figures/4/e_iso_Eta_Tight}
    \end{subfigure}
    \caption{Efficiency plots of electron Isolation working points\cite{ATL-PHYS-PUB-2017-022}.}
    \label{fig:4:electron_isolation_efficiency}
\end{figure}

The electron isolation strategies and their corresponding efficiencies are comprehensively detailed in Table~\ref{tab:4:electron_isolation_benchmark}. Figure~\ref{fig:4:electron_isolation_efficiency} provides a comparative analysis of electron isolation efficiencies for \Zepem events collected during the 2018 Run 2 of the LHC. This figure presents data based on two crucial parameters—transverse energy \(E_T\) and pseudorapidity \(\eta\), along with two selection criteria: Loose and Tight, which are derived from likelihood-based electron identification methods~\cite{Aad_2019}.




