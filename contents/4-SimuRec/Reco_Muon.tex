\subsubsection{Muon reconstruction}
\label{sec:4:muon_reco}
Muons in the ATLAS experiment are predominantly identified based on their minimum-ionizing particle signature. 
This is evident either through a track in the Muon Spectrometer (MS) or unique energy depositions in the calorimeters. 
The primary sources of information for muon reconstruction\cite{Cornelissen:1020106, ATLAS-CONF-2010-072} are the Inner Detector (ID) and MS tracking detectors, 
while calorimeter data is also used for refining track parameters and for MS-independent candidate tagging. 

Track reconstruction in the MS initiates with the identification of local, 
straight-line track segments that are based on hits in individual MS stations. 
These segments are detected via a Hough transform~\cite{ILLINGWORTH198887}. 
Preliminary track candidates are then formulated by applying a loose pointing constraint to 
the interaction point (IP) via a parabolic trajectory approximation. This accounts for the muon bending in the magnetic field. 
A global $\chi^2$ fit is employed to optimize the muon trajectory, 
taking into account possible interactions in the detector material and potential misalignments between detector chambers.

Global muon reconstruction employs data from both the ID and MS along with calorimeter information. 
There are five main strategies for muon identification: 
combined (CB), inside-out combined (IO), muon-spectrometer extrapolated (ME), segment-tagged (ST), and calorimeter-tagged (CT). 
CB muons are identified by aligning MS and ID tracks followed by a combined track fit, factoring in energy loss in the calorimeters. 
For regions with $|\eta| > 2.5$, a specific class of muons, known as silicon-associated forward (SiF) muons, is identified. 
IO muons are detected by extrapolating ID tracks to the MS and requiring at least three loosely-aligned MS hits. 
In cases where an MS track cannot be aligned with an ID track, it is extrapolated to the beamline to identify an ME muon, 
thereby extending the detector's acceptance to $|\eta| = 2.7$. 
ST muons necessitate tight angular matching between an ID track extrapolated to the MS and at least one MS segment. 
CT muons are identified by examining energy depositions in the calorimeters consistent with a minimum-ionizing particle, 
applying a $p_T$ threshold of 5 GeV to mitigate background noise.

Several advancements have been made in muon reconstruction techniques as compared to earlier models~\cite{Aad_2016}:
\begin{itemize}
  \item A parabolic trajectory in pattern recognition enhances segment matching across different stations.
  \item The introduction of SiF muons optimizes the utility of the ID near its acceptance boundaries.
  \item Alignment uncertainties are now incorporated into the track fits.
  \item The calorimeter-tagging algorithm has been fine-tuned for enhanced purity in regions with limited MS coverage.
\end{itemize}


\subsubsection{Muon identification}
After reconstruction, high-quality muon candidates used for physics analyses are 
selected based on a set of requirements concerning the number of hits in different ID subdetectors and MS stations, 
track fit properties, and compatibility tests between the ID and MS measurements. 
A specific set of requirements for each muon type, as described in Section~\ref{sec:4:muon_reco}, 
is called a selection working point (WP). 
Multiple WPs are defined to cater to a variety of physics analyses involving muons.

Different analyses necessitate varying levels of prompt-muon identification efficiency, 
momentum measurement resolution, and background rejection capabilities. 
Special attention is paid to differentiating between muon candidates originating from light hadrons and those from heavy-flavor hadrons. 
The selection WPs primarily target the rejection of light hadrons 
which produce lower-quality tracks due to in-flight decays within the detector.

\paragraph{The Loose, Medium, and Tight selection working points}
The selection working points (WPs) are parameterized by several numerical criteria, 
often dependent on the pseudorapidity \( \eta \) and transverse momentum \( p_T \) of the muon candidate.
For the Medium WP, the criteria are:
\begin{itemize}
  \item ID Acceptance: \( |\eta| < 2.5 \)
  \item Number of Precision Stations: \( \geq 2 \) (Exception: \( |\eta| < 0.1 \) can have one)
  \item \( q/p \) Compatibility: \( < 7 \)
\end{itemize}
For the Loose WP, it includes all muons passing the Medium WP plus additional cases:
\begin{itemize}
  \item For \( |\eta| < 0.1 \): Includes CT and ST muons
  \item For \( p_T < 7 \) GeV and \( |\eta| < 1.3 \): Includes IO muons with only one precision station
\end{itemize}
Efficiency Increase: \( \approx 20\% \) for \( 3 \) GeV \( < p_T < 5 \) GeV; \( \approx 1\%-2\% \) for \( p_T > 5 \) GeV.
The Tight WP imposes additional constraints:
\begin{itemize}
  \item \( \chi^2/\text{ndf} < 8 \) for the combined track fit
  \item Optimized \( q/p \) and \( \rho_0 \) based on \( p_T \) and \( |\eta| \)
\end{itemize}
Efficiency Loss: \( \approx 6\% \) for \( 6 \) GeV \( < p_T < 20 \) GeV; Background Reduction: \( > 50\% \) compared to Medium WP.

\begin{figure}[!htb]
    \centering
    \resizebox{\columnwidth}{!}{%
        \includegraphics[width=0.44\textwidth]{figures/4/muon_ID_eta}
        \includegraphics[width=0.44\textwidth]{figures/4/muon_ID_pt}
    }
    \caption{
        Efficiency vs \( \eta \) and \( p_T \) for different WPs in simulated \( t\bar{t} \) events\cite{MUON-2018-03}.
    }
    \label{fig:4:muon_id_eff}
\end{figure}


In Figure~\ref{fig:4:muon_id_eff}, the efficiencies of various Working Points (WPs) 
are plotted as functions of pseudorapidity (\( \eta \)) and transverse momentum (\( p_T \)) under 
the scope of simulated \( t\bar{t} \) events. 
The figure serves a dual purpose: first, it reveals how the efficiency of each WP correlates with \( \eta \) and \( p_T \); 
second, it differentiates between the efficiencies for muons that are promptly produced and those that result from hadron decays.

\subsubsection{Muon isolation}
Selection requirements are imposed on the impact parameters of the muon track to reject muons originating from hadron decays in-flight and from non-hard-scattering interactions. Two impact parameters are considered: the transverse impact parameter \( |d_0| \) and the longitudinal impact parameter \( z_0 \).
The \( |d_0| \) is measured relative to the actual beam position and is defined in terms of its significance, \({|d_0|}/{\sigma(d_0)}\), which is required to be less than three.
The longitudinal distance \( |z_0| \sin \theta \) is used, where \( \theta \) is the polar angle of the muon track. For tracks with \( p_T > 10 \, \text{GeV} \), the impact parameter resolution is approximately 10 \um in the transverse plane and 50 \um in the longitudinal direction.

The isolation variable for track-based isolation is \( p_{T}^{\text{cone}} \), defined as the scalar sum of \( p_T \) of ID tracks in an \( \eta-\phi \) cone of size \( \Delta R \) around the muon, excluding the muon itself. \( \Delta R \) varies as either \( 0.2 \) or \( \min({10 \, \text{GeV}}/{p_{T}^\mu}, 0.3) \).
Calorimeter-based isolation is denoted as \( E_{T}^{\text{topocone20}} \), defined in a cone \( \Delta R = 0.2 \) around the muon after correcting for pile-up effects.
Particle-flow-based isolation combines track-based and calorimeter-based methods, using a weighting factor \( w = 0.4 \) to optimize for heavy-flavour hadron decays.

Several isolation WPs are defined to balance various performance metrics. These are categorized based on track-based isolation variables, possibly with additional criteria for calorimeter-based or particle-flow-based isolation.


\begin{table}[!htb]
    \centering
    
    \resizebox{0.95\columnwidth}{!}{
            \begin{tabular}{ccc}
            \toprule
            Isolation WP                                                                                     & Definition                                                                                                                                                                                                                                                                                             & Track \(p_{\mathrm{T}}\) requirement \\ 
            \midrule
            \(\begin{array}{c}\text { PflowLoose* } \\            \text { PflowTight* }\end{array}\)          & \(\begin{array}{c}\left(p_{\mathrm{T}}^{\text {varcone30 }}+0.4 \cdot E_{\mathrm{T}}^{\text {neflow20 }}\right)<0.16 \cdot p_{\mathrm{T}}^\mu \\            \left(p_{\mathrm{T}}^{\text {varcone30 }}+0.4 \cdot E_{\mathrm{T}}^{\text {neflow20 }}\right)<0.045 \cdot p_{\mathrm{T}}^\mu\end{array}\) & \(p_{\mathrm{T}}>500 \mathrm{MeV}\)  \\ 
            \midrule
            \(\begin{array}{c}\text { Loose* } \\            \text { Tight* }\end{array}\)                   & \(\begin{array}{c}p_{\mathrm{T}}^{\text {varcone30 }}<0.15 \cdot p_{\mathrm{T}}^\mu, E_{\mathrm{T}}^{\text {topocone20 }}<0.3 \cdot p_{\mathrm{T}}^\mu \\ p_{\mathrm{T}}^{\text {vane20 }}<0.04 \cdot p_{\mathrm{T}}^\mu, E_{\mathrm{T}}^{\text {topocone20 }}<0.15 \cdot p_{\mathrm{T}}^\mu \end{array}\)                                                                                                                                                              & \(p_{\mathrm{T}}>1 \mathrm{GeV}\)    \\ 
            \midrule
            \(\begin{array}{c}\text { HighPtTrackOnly } \\            \text { TightTrackOnly* }\end{array}\) & \(\begin{array}{c}p_{\mathrm{T}}^{\text {cone20 }}<1.25~\text{GeV}    \\            p_{\mathrm{T}}^{\text {varcone30 }}<0.06 \cdot p_{\mathrm{T}}^\mu\end{array}\)                                                      & \(p_{\mathrm{T}}>1 \mathrm{GeV}\)    \\ 
            \midrule
            PLBDTLoose \((\) PLBDTTight \()\)                                                                & \(\begin{array}{c}p_{\mathrm{T}}^{\text {varcone30 }}<\max \left(1.8 \mathrm{GeV}, 0.15 \cdot p_{\mathrm{T}}^\mu\right) \\            \text { BDT cut to mimic TightTrackOnly }(\text {Tight}) \text { efficiency }\end{array}\)                                                                     & \(p_{\mathrm{T}}>1 \mathrm{GeV}\)    \\ 
            \bottomrule
            \end{tabular}
    }
    \caption{Definitions of the muon isolation WPs.}
    \label{tab:4:muon_iso_wp}
\end{table}



The various isolation WPs are summarized in Table~\ref{tab:4:muon_iso_wp},
which also provides the criteria used for each WP in its second column and 
the minimum track \( p_{T} \) requirements in the third column. 
Some WPs are marked with an asterisk (*) and exist in two variants:
 one where the cone \( \Delta R \) parameter decreases with \( p_{T}^\mu \) as \( \min(10 \, \text{GeV}/p_{T}^\mu, 0.3) \), 
 and the other remaining constant at \( \Delta R = 0.2 \) for \( p_{T}^\mu > 50 \, \text{GeV} \). 
 A track-only isolation WP is the most robust with respect to pile-up and suffers the lowest drop in efficiency from nearby objects. 
 Two loose isolation WPs are defined using track isolation and either calorimeter or neutral particle-flow isolation, 
 and are optimized for cases where high prompt-muon efficiency is prioritized over rejection of non-prompt muons. 
 Two tight isolation WPs are defined using track isolation and either calorimeter or neutral particle-flow isolation, 
 and are optimized for cases suffering from large backgrounds from non-prompt muons. 
 Moreover, two isolation WPs are defined using the prompt lepton BDT: PLBDTLoose and PLBDTTight. 
 In addition to a loose cut on the track isolation, 
 a \( p_{T} \)-dependent BDT threshold selection is applied in each of these to achieve the same prompt-muon efficiency as
  the TightTrackOnly and Tight isolation WPs, respectively.
