\subsubsection{General-Purpose Monte Carlo Generators}

General-purpose Monte Carlo (GPMC) generators like 
\Herwig\cite{HERWIG_1, HERWIG_2}, 
\Pythia\cite{PYTHIA_1, PYTHIA_2}, 
and Sherpa\cite{SHERPA}
are integral in simulating high energy particle collisions. 
These generators serve a dual purpose: 
first, as theoretical tools for probing Quantum Chromodynamics (QCD) beyond fixed-order perturbation, 
and second, as practical instruments for data analysis and experimental planning. 
They function at different scales, ranging from the perturbative to the nonperturbative, 
capturing the physics from sub-femtometer dimensions to the larger scales of hadron formation and decay.

The architecture of GPMC generators is organized into four fundamental components. 
Each component serves a distinct function in simulating high-energy particle interactions, which are interrelated:

\begin{itemize}
    \item \textbf{Short-Distance, Perturbative Phenomena}: At sub-femtometer scales, 
    GPMC generators utilize perturbative QCD to model hard scattering and other partonic-level processes\cite{PDG2021QCD}. 
    Matrix Elements (ME) are employed to provide fixed-order calculations for specific scattering processes, 
    and they often serve as the starting point for parton shower evolution. 
    The parton shower (PS) technique further refines these calculations 
    by capturing the sequential emission of gluons and quarks at lower transverse momenta. 
    Additional features like Initial-State Radiation (ISR) and Final-State Radiation (FSR) 
    are implemented to account for particle emissions both before and after the core hard-scattering event. 
    These mechanisms collectively influence the kinematic attributes of the simulated collision. 
    Furthermore, the strong coupling constant, denoted as $\alpha_s$, serves as a key parameter in these simulations, 
    dictating the strength of strong force interactions and contributing to theoretical uncertainties.
    \item \textbf{Nonperturbative Transition}: Beyond the perturbative regime, 
    these generators employ phenomenological models like the Lund String or Cluster models 
    to simulate the transition from quarks and gluons to observable hadrons.
    \item \textbf{Soft Hadron Physics Models}: For larger-scale, nonperturbative interactions, 
    the generators incorporate models of the underlying event and minimum-bias interactions, 
    along with specialized treatments for Bose-Einstein correlations and color reconnection.
    \item \textbf{MC Uncertainty Estimates and Tuning}: The generators also provide frameworks 
    for uncertainty quantification and parameter tuning, which are crucial for assessing the reliability of simulation results.
\end{itemize}

In elucidating how $pp$ collision events are simulated by \textsc{Pythia}\cite{PYTHIA8.3}, 
Figure~\ref{fig:4:pp} offers a schematic overview, albeit with certain simplifications for the sake of visual clarity; 
these include a reduced number of shower branchings and final-state hadrons, approximate recoil effects, 
and the exclusion of weak decays in light-flavor hadrons, 
among other details (see caption for a complete list of simplifications).


\begin{figure}[!htb]
    \centering
    \resizebox{\columnwidth}{!}{%
        \includegraphics[width=\textwidth]{figures/4/pythia_pp}
    }
    \caption{
        Schematic representation of a $pp \rightarrow t\bar{t}$ event as simulated by \textsc{Pythia}\cite{PYTHIA8.3}, 
        with certain simplifications for visual clarity. 
        1) The number of shower branchings and final-state hadrons is reduced compared to an actual \textsc{Pythia} simulation; 
        2) Recoil effects are not depicted to scale; 
        3) Weak decays of light-flavor hadrons are omitted, implying, 
        for example, a $K^0_S$ meson is shown as stable; 
        4) Incoming momenta are illustrated as inversed ($p \rightarrow -p$), 
        necessitating that the momentum direction for both beam remnants and 
        ISR branchings should be interpreted as outward-facing to avoid diagrammatic complexity. 
        This convention prevents the need for beam remnants and ISR emissions to intersect in the central region of the figure.
    }
    \label{fig:4:pp}
\end{figure}

\subsubsection{Specialized Generators}

Specialized generators serve as auxiliary tools in the high-energy physics ecosystem, 
designed to work in conjunction with GPMC generators such as \textsc{Pythia} and \textsc{Herwig}. 
Unlike GPMCs, specialized generators do not produce complete events amenable to direct simulation. 
Instead, their function is to provide a more accurate representation of specific decay processes or specialized final states. 
These generators often employ advanced theoretical models and methodologies, 
ranging from next-to-leading order (NLO) perturbative calculations to intricate decay models. 
By generating partonic four-vectors in compatible formats, they enable seamless integration with GPMC generators, 
thereby enhancing the overall predictive power and accuracy of event simulations.
These specialized generators often produce outputs in ASCII formats compatible with "Les Houches" standards\cite{Les_Houches}, 
which are then read and processed by GPMC generators via interfaces like Athena.

EvtGen\cite{EvtGen}, originally conceived by the CLEO collaboration, 
specializes in providing a nuanced description of B meson and hadron decays, 
thereby enhancing the default capabilities of general-purpose generators. 
Recent extensions of EvtGen have incorporated data from experiments at the Tevatron, BaBar, and Belle, 
allowing for more accurate modeling of $B_s$ and b-baryon decays. 
EvtGen is unique in its incorporation of angular correlations, 
which are crucial for calculating the acceptance rates for certain decay modes of B mesons and baryons. 
It has been particularly useful in ATLAS studies that focus on exclusive B decays.

MC@NLO\cite{MC_NLO}, another specialized generator, operates as a standalone "Les Houches" type generator. 
It is notable for its employment of NLO QCD perturbation theory for evaluating hard scattering processes. 
This makes it especially relevant for generating events involving top quarks, 
offering a more precise representation of their transverse momentum distributions as compared to general-purpose generators. 
One of its defining features is the inclusion of one-loop corrections, 
resulting in events with both positive and negative weights that must be carefully accounted for in subsequent data analysis.

Parton Distribution Functions (PDFs) serve as essential components to model the internal structure of protons. 
These PDFs are incorporated into all event generators as external inputs. 
Specifically, ATLAS employs the Les Houches Accord PDF Interface (LHAPDF)\cite{bourilkov2006lhapdf}, 
a robust library that has effectively replaced the older PDFLIB\cite{PDFLIB}. 
This library offers an extensive repository of PDFs, with the default being the CTEQ PDFs\cite{tung2007global}. 
It is noteworthy that MC@NLO employs NLO PDFs, 
whereas all other generators utilize Leading Order (LO) PDFs. 
The choice of PDFs is intrinsically tied to the tuning parameters associated with initial state radiation\cite{ZEUS_1, tev4lhcqcdworkinggroup2006tevatronforlhc}. 
Due to this interconnectedness, altering PDFs independently can result in inconsistent outcomes. 
Hence, whenever a new set of PDFs is adopted, a corresponding retuning of the event generator's parameters is performed to ensure result consistency.

Monte Carlo truth is preserved in a HepMC event record, serving as a complete list of particles that are directly produced or involved in high-energy interactions. 
These particles often serve as progenitors to a cascade of sub-processes and decays that ultimately interact with the detector. 
The MC truth at the generator level differentiates between particles originating from the initial collision 
and those produced through subsequent interactions or decays, 
allowing for a precise understanding of the underlying physical processes. 
This granularity is essential for calibrating the simulation and validating the detector's response to specific types of events.

