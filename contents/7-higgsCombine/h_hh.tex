In this study, the combined single-Higgs and double-Higgs analyses offer enhanced constraints on the coupling modifier $\kappa_{\lambda}$, as detailed in Table~\ref{tab:7:workspace}. Leveraging statistical methodologies delineated in Section~\ref{sec:7:theory}, multiple fitting procedures are conducted under varying assumptions on the coupling modifiers.

Figure~\ref{fig:7:h-hh-klambda-scan} presents a thorough investigation into the test statistic $-2\ln{\Lambda}$, plotted against the Higgs self-coupling modifier $\kappa_{\lambda}$. Two distinct scenarios are examined: observed data and projected or Asimov data. Figure~\ref{fig:7:subfig-kl-data} depicts the observed $-2\ln{\Lambda}$ values for both single-Higgs and double-Higgs analyses, color-coded in blue and red respectively. Their combined results are shown in black. In addition, a general model allowing for free-floating $\kappa_t$, $\kappa_b$, $\kappa_V$, and $\kappa_\tau$ is represented by the green curve. Figure~\ref{fig:7:subfig-kl-asimov} mirrors this structure but uses the expected or Asimov data. This figure serves as a cornerstone for the constraint evaluation on $\kappa_{\lambda}$, facilitating an intercomparison among various scenarios and assumptions.

\begin{figure}[htbp]
    \centering
    \subfloat[\label{fig:7:subfig-kl-data}]{\includegraphics[width=0.47\textwidth]{figures/7/H_HH_k2Vfixed_data_final}}
    \quad
    \subfloat[\label{fig:7:subfig-kl-asimov}]{\includegraphics[width=0.47\textwidth]{figures/7/H_HH_k2Vfixed_asimov_final.pdf}}
    \caption{Observed (a) and projected (b) test statistics in terms of $\kappa_{\lambda}$.}\label{fig:7:h-hh-klambda-scan}
\end{figure}


One of the advantages of integrating the single-Higgs analyses into the overall fit is the additional flexibility it provides in relaxing certain coupling assumptions without substantially compromising the robustness of our constraints. Specifically, the assumption on the Higgs boson to top-quark coupling modifier, $\kappa_t$, can be considerably relaxed. Thanks to the robust boundaries set by single-Higgs measurements, the constraints on $\kappa_{\lambda}$ are hardly affected when the value of $\kappa_t$ is allowed to float freely, a claim substantiated by the data presented in Table~\ref{tab:7:results_kl}.

Against this backdrop, Figure~\ref{fig:7:h-hh-klambda-kt-scan} serves as a critical asset for understanding the relationship between $\kappa_{\lambda}$ and $\kappa_t$. The figure comprises two sub-figures: Figure~\ref{fig:7:subfig-kl-kt-data} employs observed data, and Figure~\ref{fig:7:subfig-kl-kt-asimov} is constructed using expected, or Asimov, data. Both sub-figures feature 68\% and 95\% confidence level contours, marked by solid and dashed lines respectively. In Figure~\ref{fig:7:subfig-kl-kt-data}, constraints solely based on single-Higgs analyses are denoted by blue contours, whereas those emerging from double-Higgs analyses are depicted in red. The black contours represent the synthesized outcome of both these analyses. Importantly, red contours are restricted to $\kappa_t$ values below 1.2. Figure~\ref{fig:7:subfig-kl-kt-asimov} adopts a similar coloring scheme but relies on Asimov data to form its contours.

Overall, the figures demonstrate that when $\kappa_t$ is allowed to float, the combined single- and double-Higgs analyses still manage to provide nearly as stringent constraints on $\kappa_{\lambda}$ as when $\kappa_t$ is fixed, affirming the efficacy of the integrated analytical approach.

\begin{figure}[htbp]
    \centering
    \subfloat[\label{fig:7:subfig-kl-kt-data}]{\includegraphics[width=0.47\textwidth]{figures/7/H_HH_HHH_kl_kt_2D_data_final}}
    \quad
    \subfloat[\label{fig:7:subfig-kl-kt-asimov}]{\includegraphics[width=0.47\textwidth]{figures/7/H_HH_HHH_kl_kt_2D_asimov_final}}
    \caption{Constraints in the $\kappa_{\lambda}$--$\kappa_t$ parameter space.}\label{fig:7:h-hh-klambda-kt-scan}
\end{figure}

Finally, Table~\ref{tab:7:results_kl} provides a rigorous, multi-faceted summary of the constraints on the coupling modifier $\kappa_{\lambda}$. The table covers scenarios ranging from isolated double-Higgs ($HH$) and single-Higgs ($H$) combinations to increasingly inclusive fits where additional coupling modifiers are allowed to float. The most generic model is the least restrictive, allowing $\kappa_{\lambda}$, $\kappa_t$, $\kappa_b$, $\kappa_{\tau}$, and $\kappa_V$ to float, while fixing $\kappa_{2V}$ to unity due to the absence of a comprehensive parameterization for single-Higgs NLO EW corrections as a function of this modifier. In this most generic fit, we observe an exclusion of $-1.4 < \kappa_{\lambda} < 6.1$ at the 95\% confidence level, closely aligned with the expected exclusion of $-2.2 < \kappa_{\lambda} < 7.7$. These findings corroborate that all other coupling modifiers agree with the Standard Model predictions within uncertainties. Notably, the sensitivity of the results to $\kappa_{2V}$ is minor; a separate check revealed that allowing $\kappa_{2V}$ to float only weakens the observed constraints on $\kappa_{\lambda}$ by less than 5\%. This manifests the robustness of our analytical framework even when extended to accommodate more free parameters.

\begin{table}[!htb]
    \centering
    \resizebox{\columnwidth}{!}{
        \begin{tabular}{lccc}
            \toprule
            Combination assumption &  Obs.\ 95\% CL  & Exp.\ 95\% CL & Obs.\ value$^{+1\sigma}_{-1\sigma}$ \\
            \midrule
            $HH$ combination        & $-0.6 < \kappa_{\lambda} < 6.6$    & $-2.1 < \kappa_{\lambda} < 7.8$  &  $\kappa_{\lambda} = 3.1^{+1.9}_{-2.0}$ \\
            Single-$H$ combination & ~$-4.0 < \kappa_{\lambda} < 10.3$   & ~~$-5.2 < \kappa_{\lambda} < 11.5$ &  $\kappa_{\lambda} = 2.5^{+4.6}_{-3.9}$ \\
            $HH$+$H$ combination      & $-0.4 < \kappa_{\lambda} < 6.3$    & $-1.9 < \kappa_{\lambda} < 7.6$  &  $\kappa_{\lambda} = 3.0^{+1.8}_{-1.9}$ \\
            $HH$+$H$ combination, $\kappa_t$\ floating                   & $-0.4 < \kappa_{\lambda} < 6.3$  & $-1.9 < \kappa_{\lambda} < 7.6$  & $\kappa_{\lambda} = 3.0^{+1.8}_{-1.9}$\\
            $HH$+$H$ combination, $\kappa_t$, $\kappa_b$, $\kappa_{\tau}$, and $\kappa_V$ floating  & $-1.4 < \kappa_{\lambda} < 6.1$  & $-2.2 < \kappa_{\lambda} < 7.7$  & $\kappa_{\lambda} = 2.3^{+2.1}_{-2.0}$ \\
            \bottomrule
        \end{tabular}
    }
    \caption{Comprehensive summary of $\kappa_{\lambda}$ constraints.}\label{tab:7:results_kl}
\end{table}
