% \subsection{Kappa Framework}

The $\kappa$ framework offers a methodology for scrutinizing the Standard Model (SM) in the Higgs sector, particularly in relation to single and double-Higgs production~\cite{Heinemeyer:2013tqa,deFlorian:2016spz}. In this framework, the couplings of the Higgs boson to other SM particles at leading order (LO) are scaled by factors denoted as $\kappa_m$,  defined as the ratio of the coupling between the particle $m$ and the Higgs boson to its SM value. These scaling factors serve as a simplified yardstick for comparing experimental results to SM predictions. The framework principally focuses on four coupling modifiers for single-Higgs interactions: $\kappa_t$, $\kappa_b$, $\kappa_{\tau}$, and $\kappa_V$. A departure of $\kappa$ from unity indicates the presence of Beyond Standard Model (BSM) physics~\cite{Heinemeyer:2013tqa}.

% \subsection{Double-Higgs Production}

Double-Higgs production serves as a direct probe into the Higgs boson self-coupling. In the SM, the primary mechanism for double-Higgs production is gluon--gluon fusion, contributing to over 90\% of the $pp \rightarrow HH$ cross-section~\cite{hh-whitepaper}. The process is sensitive to two main coupling modifiers: $\kappa_{\lambda}$, and $\kappa_{t}$, which scale different amplitudes in the process, as shown in Figure~\ref{fig:2:lhc_hh_ggF}. Deviations from the SM predictions can be parameterized in terms of these coupling modifiers~\cite{deFlorian:2016spz,Dawson:1998py,Borowka:2016ehy,Baglio:2018lrj}. 

The $pp \rightarrow HH$ process allows for sensitivity to the relative sign of $\kappa_{\lambda}$ and the top-quark couplings, mainly because of the destructive interference between amplitudes. The second most abundant process, VBF, in Figure~\ref{fig:2:lhc_hh_VBF}, is parameterized using a combination of the $\kappa_{\lambda}$, $\kappa_{V}$, and $\kappa_{\text{2V}}$ coupling modifiers~\cite{Bishara:2016kjn}.

% \subsection{Next-to-Leading Order Effects and STXS Framework}

While single-Higgs processes are generally not sensitive to the Higgs boson self-coupling at leading order (LO), the complete next-to-leading order (NLO) electroweak (EW) corrections incorporate $\lambda_\text{HHH}$ through Higgs boson self-energy loops and additional diagrams, offering indirect constraints on $\kappa_{\lambda}$~\cite{Degrassi:2016wml,Maltoni:2017ims}. Concurrently, the Simplified Template Cross-Section (STXS) framework offers a refined methodology for analyzing single-Higgs processes. It employs fiducial phase space regions defined by kinematic variables, enabling the identification of localized deviations from Standard Model predictions~\cite{Badger:2016bpw}. This facilitates mapping back to constraints on $\kappa_m$ parameters, enriching the utility of the $\kappa$ framework. The STXS approach categorizes bins by production mode, decay channel, and additional criteria such as jet multiplicity and transverse momentum, thereby offering a more detailed understanding of Higgs properties~\cite{ATL-PHYS-PUB-2019-009,LHCHXSWG-Pubnote}.

% \section{Implications}

Any significant deviation in the measured $\kappa_m$ from unity, or discrepancies in the cross-sections of double-Higgs production processes from SM predictions, would be indicative of new physics. Such deviations would warrant further investigation, possibly heralding the discovery of particles or interactions not accounted for in the SM.

% \section{Summary}

The $\kappa$ framework serves as a valuable tool for exploring the Higgs sector. By employing coupling modifiers, it simplifies the analysis of both single and double-Higgs processes. While the framework has its limitations, such as the absence of considerations for new particles in loop-level diagrams, it remains a robust methodology for initial explorations into BSM physics in the Higgs sector.
