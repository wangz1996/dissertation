\subsection{Data Preparation and Analytical Framework}
\label{sec:5:derivation}

The analytical process employs data initially structured in the \myverb{xAOD} format, which is subsequently transformed into the \myverb{DxAOD} format via the \myverb{HIGG8D1} derivation framework. This transition, termed the GN1 framework, has been customized from the ttH multilepton analysis to specifically cater to signal events featuring multilepton final states.

Data size minimization is achieved through slimming (elimination of redundant variables), thinning (removal of entire objects from the event record), and event-level skimming applied to both the collision data and Monte Carlo samples.

This framework has been particularly optimized for the \bbllll channel, adhering to the lower \( p_T \) lepton thresholds as defined in the \textit{baseline} lepton specification (see Section~\ref{sec:5:leptonwp}). Contrasting this, other multilepton channels employ more stringent lepton conditions (refer to Table~\ref{tab:5:lepton_selection}), especially at the sample production phase (approximating the \textit{Loose} criteria in Table~\ref{tab:5:lepton_selection} but utilizing FCLoose for lepton isolation).

It is noteworthy that channels involving $\gamma\gamma + X$ utilize a distinct derivation framework, named \myverb{HIGG1D1}, for their \myverb{DxAOD} production. This is complemented by the HGam framework, which inherently offers different lepton and \tauh definitions when compared to multilepton channels.


\subsection{Data Collection and Quality}
\label{sec:5:data}
The current study utilizes a data set comprising 140~\ifb of proton-proton collision records acquired by the ATLAS detector in the energy range of $\sqrt{s}=13$~TeV spanning the years 2015-2018. The data have been collected with a bunch crossing interval of 25 ns, with the IBL (Insertable B-Layer) activated. Data quality verifications~\cite{DAPR-2018-01} have been made in accordance with the predefined Good Run List (GRL). The GRLs used in this study are listed in Table~\ref{table:5:GRL_files}. 

\begin{table}[!htb]
    \centering
    \resizebox{0.99\columnwidth}{!}{
        \begin{tabular}{ll}
            \toprule
            Year & GRL XML File \\
            \midrule
            2015 & \myverb{data15\_1V3TeV.periodAllYear\_DetStatus-v89-pro21-02\_Unknown\_PHYS\_StandardGRL\_All\_Good\_25ns.xml} \\
            2016 & \myverb{data16\_13TeV.periodAllYear\_DetStatus-v89-pro21-01\_DQDefects-00-02-04\_PHYS\_StandardGRL\_All\_Good\_25ns.xml} \\
            2017 & \myverb{data17\_13TeV.periodAllYear\_DetStatus-v99-pro22-01\_Unknown\_PHYS\_StandardGRL\_All\_Good\_25ns\_Triggerno17e33prim.xml} \\
            2018 & \myverb{data18\_13TeV.periodAllYear\_DetStatus-v102-pro22-04\_Unknown\_PHYS\_StandardGRL\_All\_Good\_25ns\_Triggerno17e33prim.xml} \\
            \bottomrule
        \end{tabular}
    }
    \caption{Good Run List XML Files by Year.}
    \label{table:5:GRL_files}
\end{table}
    

\subsection{Monte Carlo Simulations}
\label{subsec:MC_samples}

The simulation framework relies on three distinct MC campaigns: mc16a, mc16d, and mc16e. These campaigns are tailored to replicate the conditions of the LHC runs for the years 2015-2016, 2017, and 2018, respectively, with particular emphasis on the interaction multiplicity per bunch crossing.
To harmonize the simulated events with the actual collision data, a reweighting strategy is employed via the \myverb{PileupReweightingTool}~\cite{twiki-PileupReweighting}. The event samples generated from these simulations are subsequently scaled according to their theoretical cross-sections.

\subsubsection{Background samples}

Monte Carlo simulations for various signal and background processes are based on configurations specified in Table~\ref{tab:5:mcconfig}. These configurations are also employed to estimate systematic uncertainties (indicated in parentheses). Electroweak boson production (\(W\) or \(Z/\gamma^*\)) is denoted by \(V\). Specific parton distribution functions (PDFs) and their roles in matrix element (ME) calculations and parton showers are clarified. The underlying-event tune for the parton shower generator and the software versions used in event generation are outlined. Heavy flavor hadron decays are modeled using \textsc{EvtGen} 1.2.0, and photon emission is accounted for by either the parton shower generator or \textsc{PHOTOS}. The mass settings for the top quark and SM Higgs boson are at $172.5$~GeV and $125$~GeV, respectively. 

\input{contents/5-hhml/tables/bkg_samples.tex}

Pile-up is modeled through minimum-bias events generated via \textsc{Pythia} 8.186~\cite{Sjostrand:2007gs}, using the \textsc{NNPDF2.3LO} PDFs and the A3 parameter set~\cite{ATL-PHYS-PUB-2016-017}. These are then overlaid onto the primary hard-scatter events based on the luminosity profiles of the recorded data. The generated events are subjected to a simulation of the ATLAS detector's geometry and response through \textsc{Geant4}~\cite{Agostinelli:2002hh}. The same reconstruction software is applied as in the case of real data. Calibrations ensure that particle candidate selection efficiencies, energy scales, and resolutions are consistent with those determined from control samples in real data. The simulated datasets are normalized to their computed cross-sections, evaluated to the highest order available in perturbation theory.


\subsubsection{Signal Samples}

For ggF mode, events are generated at NLO accuracy using Powheg-Box-V2 for the matrix element and \PYTHIA{8} (A14 tune) for parton showering and hadronization. The PDF set employed is NNPDF 2.3 LO. Heavy-flavor hadron decays are modeled using EvtGen. Detector effects are accounted for by AltfastII (AF2). Lepton filters target multilepton final states, focusing on configurations such as $2\ell 0\tau$, $2\ell 1\tau$, $3\ell 0\tau$, among others. Lepton kinematics are restricted by a \myverb{MultiLeptonFilter} at $\pT > 7$ GeV and $|\eta| < 3$.
Alternative ggF samples are produced with \POWHEG{Box-V2} interfaced to \Herwig{7}, employing the PDF4LHC15 PDF set to assess parton shower uncertainties. They adopt the same filtering strategy as the nominal samples.

For VBF mode, events are generated at LO with \textsc{MadGraph5\_aMC@NLO}. Parton shower and hadronization are handled by \PYTHIA{8} using the A14 tune and the NNPDF 2.3 LO PDF set. HF hadron decays and detector effects are modeled similarly to the ggF case. Lepton filters are applied to target various final states.




