In this analysis, two primary categories of backgrounds exist: prompt and non-prompt backgrounds. The prompt backgrounds, which consist predominantly of diboson processes, are composed of events that feature three prompt leptons in the final state. On the other hand, the non-prompt backgrounds, often referred to as 'fakes,' emerge from events where jets or photons are misidentified as leptons.

The prompt backgrounds are rigorously modeled using Monte Carlo simulations. These include a variety of processes such as \( t\bar{t}V \), \( VV \), \( tV \), \( VH \), \( VVV \), and \( t\bar{t}H \). Notably, \( WZ \) processes account for over 85\% of all prompt backgrounds. A specialized control region, optimized for a \( WZ \)-enriched environment, is introduced to refine this background estimation. Further details on this control region and its corresponding plots can be found in Sections~\ref{sec:5:template_fit} and~\ref{sec:5:WZCRPlot}.

The non-prompt backgrounds primarily involve \( t\bar{t} \) and \( Z+\text{jets} \) processes. These backgrounds are estimated using a template fit method, detailed in Section~\ref{sec:5:template_fit}. The non-prompt category also encompasses other processes such as \( W+\text{jets} \) and \( V\gamma \), among others.

\begin{table}[!htb]
    \centering
    \resizebox{0.99\columnwidth}{!}{
        \begin{tabular}{llllllllll}
            \toprule
             & HH: ggF & HH: VBF & WZ & ExtConv\_e & HF\_e & HF\_m & Total Bkg & Data & Signal Contamination \\
            \midrule
            ExtConv\_e CR & $0.00 \pm 0.02$ & $0.00 \pm 0.00$ & $2.59 \pm 0.42$ & $292.97 \pm 17.59$ & $0.02 \pm 0.00$ & $1.59 \pm 0.32$ & $303.64 \pm 17.47$ & $303.00$ & $0.000\% \pm 0.005\%$ \\
            HF\_e CR & $0.00 \pm 0.04$ & $0.00 \pm 0.00$ & $1.03 \pm 0.15$ & $11.76 \pm 2.39$ & $145.97 \pm 16.48$ & $0.00 \pm 0.00$ & $239.98 \pm 15.41$ & $243.00$ & $0.002\% \pm 0.018\%$ \\
            HF\_m CR & $0.01 \pm 0.07$ & $0.00 \pm 0.00$ & $2.32 \pm 0.34$ & $0.00 \pm 0.00$ & $0.00 \pm 0.00$ & $470.00 \pm 24.89$ & $566.38 \pm 23.74$ & $567.00$ & $0.001\% \pm 0.013\%$ \\
            Low BDT VR & $0.48 \pm 5.51$ & $0.03 \pm 0.34$ & $1558.72 \pm 152.11$ & $79.34 \pm 13.74$ & $60.52 \pm 10.74$ & $138.51 \pm 13.98$ & $2363.74 \pm 165.71$ & $2405.00$ & $0.022\% \pm 0.234\%$ \\
            WZ CR & $0.25 \pm 2.90$ & $0.01 \pm 0.12$ & $7137.60 \pm 688.55$ & $8.39 \pm 6.18$ & $54.13 \pm 15.76$ & $143.45 \pm 22.88$ & $8270.25 \pm 704.82$ & $8090.00$ & $0.003\% \pm 0.035\%$ \\
            \bottomrule
        \end{tabular}
    }
    \caption{Summary of Monte Carlo (MC) samples with applied normalization factors and re-weighted \( WZ \) contributions, alongside data for all control and validation regions in the \lll channel. Only the principal background processes are enumerated.}
    \label{tab:5:3l_region_summary}
\end{table}

A comprehensive summary of all defined control regions and validation regions is provided in Table~\ref{tab:5:3l_region_summary}.


\subsubsection{Estimation of WZ Using fitting function}
\label{sec:5:WZ_fit}
The \( WZ \) process serves as a crucial contributor to the prompt background within the \( \lll \) channel. In order to optimize its simulation, a specialized control region is formulated. This region adheres to the general preselection criteria with two specific deviations:

\begin{itemize}
    \item In contrast to the Signal Region, where a veto based on the invariant mass of the SFOS lepton pair is applied around \( m_Z \), the control region is designed to \emph{retain} such events. This invariant mass is confined within a \( \pm 10 \) GeV window around \( m_Z \), ensuring orthogonality with respect to the Signal Region.
    \item An additional constraint of \( \EmT > 30 \) GeV is introduced to further refine the \( WZ \) control region.
\end{itemize}

To address the recognized discrepancy in modeling the \( WZ \) background, particularly in scenarios of high jet multiplicities, a fitting function that has been previously validated \cite{Agaras:2845251} is applied. The function, given as 

\begin{equation}
    \label{3l:WZfittingfunction}
    f(x) = \frac{b \times 2^c - a}{2^c - 1} + \frac{(b - a) \times 2^c}{(2^c - 1) x^c}
\end{equation}

is used to fit the \( WZ \) sample in the dedicated control region. Here, \( x \) corresponds to the number of jets in the event. The fitting is performed using a least-squares method and results in the parameters \( a = -0.706 \pm 0.018 \), \( b = -0.553 \pm 0.017 \), and \( c = 0.279 \pm 0.026 \).
For this fitting procedure, it should be noted that events with a jet count greater than 4 (nJets \( > 4 \)) are treated as if they have exactly 4 jets (nJets \( = 4 \)). This treatment is due to the known limitations in modeling \( WZ \) events with higher jet multiplicities.

The quality of the fit is exemplified by a \( \chi^2/\text{ndof} \) value of 0.84, substantiating the fitting function as a robust modeling correction for \( WZ \) samples across all analytical regions. With this fitting function, it is consequently employed as an additional weighting factor for all \( WZ \) samples. This additional weight is used alongside the original sample weights to improve the representation of the \( WZ \) background in this analysis. Importantly, this fitting function is not just used in the control region where it was derived, but is also applied to all other relevant regions in the study. This ensures a consistent and improved modeling of the \( WZ \) background throughout the analysis.

\begin{figure}[!h]
    \begin{center}
        \includegraphics[width=.47\textwidth]{figures/5/3l/WZ_reweight/WZ_nJets_OR_original}
        \includegraphics[width=.47\textwidth]{figures/5/3l/WZ_BDT/WZ_nJets_OR}
    \end{center}
    \caption{
        The pre-fit (let) and post-fit (right) nJets distribution plots of the WZ control regions, using fitting function of Eq.\ref{3l:WZfittingfunction}. \lllCaptionSyst
    }
    \label{fig:5:WZFitNjet}
\end{figure}

Number of jets distributions pre- and post-fitting are visualized in Fig.~\ref{fig:5:WZFitNjet}, while additional kinematic distributions subsequent to the fitting are discussed in Sec.~\ref{sec:5:WZCRPlot}. 
The corresponding fitting uncertainties can be found in Section~\ref{sec:5:WZ_reweighting_uncertainties}.

\FloatBarrier


\subsubsection{Control regions and background estimation using template fit method}
\label{sec:5:template_fit}
Despite strict lepton identification rules, fake leptons remain a significant background in the 3-lepton channel.
For the purposes of this analysis, the fake background encompasses both non-prompt and fake leptons. Non-prompt leptons are primarily generated from heavy-flavor hadron decays (\(b\) or \(c\) hadrons), while other contributions may arise from photon conversions or hadronic jet misidentification.
Owing to the intricate nature of fake backgrounds and the imprecision of MC simulations in adequately modeling these processes, a data-driven approach is essential for accurate estimation.
The template fit method is employed as a semi-data-driven strategy. It involves a simultaneous fit of all processes contributing to the fake background. All backgrounds, including those arising from charge misidentifications, are extracted from MC simulations.
Within this framework, normalization factors for different types of fakes are free parameters in a fit to data. These factors serve to correct MC estimates for fakes and are mainly derived from $t\bar{t}$ and $Z+$jets simulations.

Based on the truth classification of non-prompt lepton events, several key contributions are identified, with free-floating normalization factors (NF) as follows:

\begin{itemize}
    \item NF\(^\text{HF}_{e}\): For events with a non-prompt electron primarily from b or c decays.
    \item NF\(^\text{HF}_{\mu}\): For events with a non-prompt muon primarily from b or c decays.
    \item NF\(^\text{Conv}\): For photon conversion events, predominantly from \(V\gamma\) process.
\end{itemize}

Events are classified into the aforementioned categories based on their truth origin:

\begin{itemize}
    \item Prompt leptons: leptons originating directly from electroweak processes.
    \item Conversion: leptons arising from photon conversions.
    \item B decay and C decay: non-prompt leptons originating from b or c hadron decays.
    \item Other: leptons from light quark decays or other minor sources.
\end{itemize}


Each CR is defined to be orthogonal to the SR, adhering to the general preselection criteria while incorporating several specialized variations. The following outlines the definition criteria for three key CRs:

\begin{itemize}
    \item Electron from Heavy Flavor Decay Control Region (\textbf{HF-E CR}):
    \begin{itemize}
        \item No isolation requirements; all set to loose isolation and tight ID.
        \item Requires a minimum of 2 jets, including 2 or more b-jets to ensure orthogonality with the SR, which implements a b-jet veto.
        \item Requires both same-sign leptons to be electrons ($\ell ee$).
    \end{itemize}

    \item Muon from Heavy Flavor Decay Control Region (\textbf{HF-MU CR}):
    \begin{itemize}
        \item Adopts identical jet, b-jet, identification and isolation criteria as the HF-E CR for consistency.
        \item Requires both same-sign leptons to be muons ($\ell \mu\mu$).
    \end{itemize}

    \item Electron from External Conversion Control Region (\textbf{Conv CR}):
    \begin{itemize}
        \item Loose Isolation for $l_{0}$ and Tight Isolation for $l_{1}$/$l_{2}$, 
        while omitting the standard ID requirement; all set to loose ID.
        \item Requires the invariant mass of the three-lepton system to be within 10 GeV of the Z boson mass, thereby enriching \( V\gamma \) backgrounds.
        \item Adds a new condition for $l_{1}$ or $l_{2}$: a conversion vertex should be present at a radius greater than 20 mm, and the mass of this vertex should be between 0 and 100 MeV. This condition further assures orthogonality with the SR.
    \end{itemize}
\end{itemize}

To optimize the discrimination among the various NFs in the simultaneous template fit, specific kinematic distributions in different phase spaces are employed:

\begin{itemize}
    \item For the estimation of the electron heavy flavor normalization factor NF\(^{\textrm{HF}}_{e}\), the angular separation \(\Delta R_{l_{0}l_{1}}\) in the \(\ell ee\) channel is utilized. This distribution is binned into 3 intervals, spanning \(0 \le \Delta R_{l_{0}l_{1}} \le 3\).
    
    \item Similarly, the muon heavy flavor normalization factor NF\(^{\textrm{HF}}_{\mu}\) is estimated using the \(\Delta R_{l_{0}l_{1}}\) distribution in the \(\ell\mu\mu\) channel, also segmented into 3 bins in the range \(0 \le \Delta R_{l_{0}l_{1}} \le 3\).

    \item The external conversion electron normalization factor NF\(^{\textrm{Conv}}_{e}\) is estimated using a single-bin distribution.
\end{itemize}


\begin{figure}[!h]
    \begin{center}
        \includegraphics[width=.32\textwidth]{figures/5/3l/TF/HF_e}
        \includegraphics[width=.32\textwidth]{figures/5/3l/TF/HF_m}
        \includegraphics[width=.32\textwidth]{figures/5/3l/TF/ExtConv_e}
        \hspace{1cm}
        \includegraphics[width=.32\textwidth]{figures/5/3l/TF/HF_e_postFit}
        \includegraphics[width=.32\textwidth]{figures/5/3l/TF/HF_m_postFit}
        \includegraphics[width=.32\textwidth]{figures/5/3l/TF/ExtConv_e_postFit}
    \end{center}
    \caption{
        From left to right shows the pre-fit (top) and post-fit (bottom) plots of the HF-E, HF-MU, External Conversion control regions.
        \lllCaptionRaw \lllCaptionSyst Additionally, the WZ background is adjusted using a fitted scaling function.
        \label{fig:5:TFFit3l} }
\end{figure}

The Template Fit Control Regions are depicted in Figures\,\ref{fig:5:TFFit3l}, both before and after fitting to the data; the derived normalization factors are tabulated in Table\,\ref{tab:5:fake_uncertainties}. Due to the limited statistics in each specialized control region, additional sources of fake or non-prompt leptons, such as internal conversions and light-flavor leptons, are amalgamated in these three CRs. Specifically, the NF for external conversions is also applied to internal conversion samples, and the NFs for heavy-flavor leptons encompass those for light-flavor and other leptonic sources. 

\begin{table}[!htb]
    \centering
    \begin{tabular}{lccc}
        \toprule
        Source                  & Norm Factor     & Yields in SR & Uncertainty(\%) \\ 
        \midrule
        External Conversion     & $0.66 \pm 0.13$ & 2.8          & 13.29           \\
        Heavy Flavor (electron) & $1.50 \pm 0.50$ & 11.3         & 30.52           \\
        Heavy Flavor (muon)     & $1.51 \pm 0.23$ & 29.5         & 13.12           \\
        Prompt Background       & -               & 125.7        & -               \\
        \bottomrule
    \end{tabular}
    \caption{The systematics of three fake sources and the corresponding yields in the signal region, which is estimated by the template fit method. }
    \label{tab:5:fake_uncertainties}
\end{table}


\FloatBarrier

