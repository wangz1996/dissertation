\subsubsection{Signal Topology}
\label{sec:5:signal_topology}

In the targeted 3-lepton signal, we primarily focus on the di-Higgs decay channels \(WWWW\), \(WWZZ\), and \(WW\tau\tau\), which collectively yield the combined final state of \(3\ell 0\tau_h + \text{jets}\). Among these channels, \(WWWW\) is dominant, comprising over 60\% of the branching ratios.

The \lll channel can be categorized into various sub-channels based on the lepton flavor composition:

\begin{itemize}
    \item \textbf{\(\ell ee\)}: Features a same-flavour, same-sign electron pair. Represented as \(\mu^{\mp} e^{\pm} e^{\pm}\) or \(e^{\mp} e^{\pm} e^{\pm}\).
    \item \textbf{\(\ell \mu \mu\)}: Features a same-flavour, same-sign muon pair. Represented as \(\mu^{\mp} e^{\pm} e^{\pm}\) or \(e^{\mp} \mu^{\pm} \mu^{\pm}\).
    \item \textbf{\(\ell e \mu\)}: Contains no same-flavour, same-sign lepton pairs. Represented by four combinations: \(\mu^{\mp} e^{\pm} \mu^{\pm}\), \(\mu^{\mp} \mu^{\pm} e^{\pm}\), \(e^{\mp} \mu^{\pm} e^{\pm}\), \(e^{\mp} e^{\pm} \mu^{\pm}\).
\end{itemize}

Due to statistical limitations in each sub-channel, the analysis treats these sub-channels inclusively.
Variables that enhance the signal sensitivity are elaborated in Section~\ref{sec:5:MVA}. The analysis employs MVA techniques using BDT. Post-MVA, the background validation region and the signal region are defined based on the BDTG score, with \(\textrm{BDTG} \le 0.55\) and \(\textrm{BDTG} > 0.55\) respectively, accroding to the maximum of significance.


\subsubsection{Pre-Selection}
\label{subsec:5:preselection}

Events are required to pass the following common selection, 
and the corresponding cut-flow is summarized in Table~\ref{tab:5:tab_cutflow}:
\begin{itemize}

    % Trigger
    \item \textbf{Trigger}:
    \begin{itemize}
        \item Global Trigger Decision required.
        \item Utilization of either single-lepton or di-lepton triggers.
    \end{itemize}
    
    % Lepton multiplicity
    \item \textbf{Lepton multiplicity}: 
    \begin{itemize}
    \item Exactly three leptons with a total electric charge of $\pm1$.
        \item Events are classified by their lepton flavour/charge composition as $l_0 l_1 l_2$, where the lepton with opposite charge with respect to the other two is noted as lepton index \("0"\).
        The remaining lepton that is nearest to $l_0$ in $\Delta R$ is given the index \("1"\) and the final lepton is noted as lepton \("2"\).
        \item $p_\textrm{T}^0>10 \textrm{ GeV}$ and $p_\textrm{T}^{1,2}>15 \textrm{ GeV}$.
        \item Lepton 0 is required to pass the loose selection while lepton 1 and 2 are required to pass the tight selection.
        The details of the corresponding working points can be found in Section~\ref{sec:5:leptonwp}.
    \end{itemize}

    % Other cuts
    \item \textbf{Hadronic tau veto}: Events with at least one hadronic tau are vetoed. 
    
    \item \textbf{Jet multiplicity}: Events with at least one jet are selected: $N_\textrm{jet} \ge 1$.
    
    \item \textbf{b-jet veto}: Veto events if they contain any $b$-tagged jets.
    
    \item \textbf{Low mass veto}: 
    \begin{itemize}
    \item To remove leptons from quarkonium decays, events with at least one same-flavour opposite-sign (SFOS) lepton pair with an invariant mass less than 12~GeV are vetoed.
    \end{itemize}
    \item \textbf{$Z$-mass veto}:
    \begin{itemize}
        \item Events with SFOS lepton pair with an invariant mass within a $\pm 10$ GeV window around $m_Z$ (91.2 Gev) are vetoed.
        \item To remove potential backgrounds with Z decays to $ll\gamma^{\ast} \rightarrow lll^{\prime}l^{\prime}$ where one lepton has very low momentum and is not reconstructed, invariant mass of the tri-lepton within a $\pm 10$~GeV window around $m_Z$ are vetoed: $|m_{lll}-m_Z| > 10$ GeV
    \end{itemize}
    
\end{itemize}

\begin{table}[!htb]
    \centering
    \resizebox{0.95\columnwidth}{!}{
        \begin{tabular}{l|cccc|c|c}
            \toprule
            Selection Criteria                          & SM HH  &   prompt &      fake & total bkg &  S/B ($\times 1000$) &    data \\
            \midrule
            three leptons with a total charge of $\pm1$ &   6.32 & 75116.81 & 775243.60 & 850360.41 & 0.01 & 1213079 \\
            Triggers                                    &   5.88 & 69214.50 & 653599.08 & 722813.58 & 0.01 &  913339 \\
            Hadronic tau veto                           &   5.88 & 69214.50 & 653599.08 & 722813.58 & 0.01 &  906509 \\
            Electron quality                            &   5.38 & 63768.03 & 461547.24 & 525315.27 & 0.01 &  685064 \\
            $p_T^{0,1,2} > 10,15,15$ GeV                &   4.14 & 50731.84 & 152499.39 & 203231.22 & 0.02 &  251428 \\
            $N_\textrm{jet} \ge 1$                      &   3.60 & 29312.65 &  94923.35 & 124236.00 & 0.03 &  145937 \\
            b-jet veto                                  &   3.21 & 25094.06 &  53653.52 &  78747.58 & 0.04 &    96919 \\
            Low mass veto                               &   3.10 & 23760.92 &  51858.30 &  75619.22 & 0.04 &    78347 \\
            Z-mass veto                                 &   2.26 &  4169.97 &  19277.35 &  23447.32 & 0.10 &    25341 \\
            Lepton tight quality                        &   1.23 &  2391.73 &    357.53 &   2749.25 & 0.45 &     2566 \\
            \bottomrule
        \end{tabular}
    }
    \caption{The raw yields with pre-selection cut-flow for the \lll analysis. }
    \label{tab:5:tab_cutflow}
\end{table}

