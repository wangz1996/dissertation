\subsection{Overview of other channels}
\label{sec:5:other_channels}

\input{contents/5-hhml/tables/control_tables.tex}
\input{contents/5-hhml/tables/selection_tables.tex}

The definitions for the Signal Region, Control Regions, and Validation Regions across all channels featuring multi-lepton final states have been formulated. These regions are vital for both the optimization of the signal sensitivity and the validation of background estimations. Detailed tabulations of these region definitions for each channel have been systematically organized and can be found in Tables~\ref{tab:5:sr_definition} and~\ref{tab:5:ControlRegions}. These tables serve as comprehensive references for the specialized selections employed in each region.

\subsection{Treatment of the Normalization Factors (NFs)}

In the overarching framework of this integrated analysis, the workspaces for each individual channel are aggregated to perform a composite fit, with the parameter $\mu_{HH}$ designated as the unified Parameter of Interest (POI). To align the theoretical predictions with the empirical data, normalization factors are employed. These factors are channel-specific but are harmonized across channels when they address the same background source. Detailed information regarding these normalization factors, including their fitted values in both individual channels and the composite fit, is provided in Table~\ref{tab:5:NFinComb}. It is noteworthy that specific normalization factors such as NF\_ExtConv\_e, $\mu_{HF-e}$, and $\mu_{HF-\mu}$ are only fitted in a combined manner as they are common to multiple channels. Channels like \ltautau, \lltautau, and $\gamma\gamma+X$ do not utilize any normalization factors. Numbers in brackets in the \SSlltau channel denote values obtained from fitting specific Control Regions.

\begin{table}[!htb]
    \centering
    \resizebox{0.95\columnwidth}{!}{
        \begin{tabular}{llllll}
            \toprule
            NormFactor                & \SSll            & \lll          & \SSlltau              & \bbllll         & combined      \\ 
            \midrule
            NF\_IntConv\_e            & $2.01\pm0.28$  &  -            &   -                 &    -            &  -            \\ 
            NF\_ExtConv\_e            & $0.80\pm0.36$  & $0.66\pm0.13$ &   -                 &    -            & $0.79\pm0.16$ \\ 
            $\mu_{HF-e}$              & $1.18\pm0.27$  & $1.50\pm0.50$ & $1.27\pm1.14$(0.87) &    -            & $1.34\pm0.17$ \\ 
            $\mu_{HF-\mu}$            & $1.57\pm0.17$  & $1.51\pm0.23$ & $0.59\pm1.03$(0.75) &    -            & $1.56\pm0.12$ \\ 
            $\mu_{WZ}$                & $0.82\pm0.06$  &  -            &   -                 &    -            &  -            \\ 
            $\mu_{VVjj}$              & $1.62\pm0.13$  &  -            &   -                 &    -            &  -            \\ 
            $\mu_{ttW}$(fake)         & $1.24\pm0.36$  &  -            &   -                 &    -            &  -            \\ 
            NF\_VV                    &  -             &  -            & $0.98\pm0.42$(0.94) &    -            &  -            \\ 
            $\mu_{t\bar{t}}$          &  -             &  -            &   -                 &  $1.50\pm0.28$  &  -            \\ 
            $\mu_{t\bar{t}Z}$          &  -             &  -            &   -                 &  $1.27\pm0.22$  &  -            \\    
            $\mu_{VV}$                &  -             &  -            &   -                 &  $1.12\pm0.46$  &  -            \\    
            $\mu_{Higgs}$             &  -             &  -            &   -                 &  $1.09\pm0.42$  &  -            \\   
            $\mu_{Z+\mathrm{jets}}$   &  -             &  -            &   -                 &  $1.01\pm0.36$  &  -            \\ \bottomrule
            \end{tabular}
    }
    \caption{Summary table of employed normalization factors, detailing individual and composite fit values. Specific channels without normalization factors are also indicated.
    \label{tab:5:NFinComb}}
\end{table}

\subsubsection{Systematics Correlation Scheme}

\begin{figure}[h!]
    \centering
    \includegraphics[width = 0.95\textwidth]{figures/5/combination/Corr_scheme.pdf}
    \caption{Correlation scheme of the systematic uncertainties in the analysis.}
    \label{fig:5:corr_scheme}
\end{figure}

A comprehensive outline of the correlation landscape among various uncertainties is presented in Table~\ref{fig:5:corr_scheme}. Luminosity and pile-up re-weighting uncertainties are fully correlated across the board. Owing to channel-specific target objects, experimental uncertainties manifest correlations for common objects between channels. For theoretical uncertainties tied to Di-Higgs signal processes, a full correlation is maintained across all channels, with a notable exception being the \bbllll channel. In this channel, signal uncertainties are partitioned distinctly for ggF and VBF HH processes.

In the $\gamma\gamma+X$ channel, only uncertainties related to single Higgs processes are factored into the background modeling. The continuum background in this channel is regulated by data from sidebands and encapsulated within the domain of background modeling uncertainties. For other theoretical uncertainties, correlation is generally applied where relevant, particularly when the definition of "other background" overlaps between channels.

\subsubsection{Combination Results}

\begin{figure}[h!]\centering
    \includegraphics[width = 0.49\textwidth]{figures/5/combination/Limits_full.eps}
    \includegraphics[width = 0.49\textwidth]{figures/5/combination/Limits_stat.eps}
    \caption{Expected individual and combined upper limits in channels, with full systematic uncertainties (left) and statistics only (right).}
    \label{fig:5:comb_upperlimit}
\end{figure}

The synthesis of 95\% Confidence Level upper limits across all multi-lepton sub-channels is enumerated in Table~\ref{tab:5:comb_upperlimit} and visually represented in Fig.~\ref{fig:5:comb_upperlimit}. These limits are derived using Asimov datasets to separately assess the contributions of statistical uncertainties alone, statistical combined with Monte Carlo uncertainties, and a fully systematic scenario. It is noteworthy that most channels suffer from limited MC statistics, with the $\gamma\gamma+X$ sub-channels standing as an exception.

\begin{table}[!htb]
    \centering
    \resizebox{0.6\columnwidth}{!}{
        \begin{tabular}{lccc} 
          \toprule
          Channels    &  Stats. Only             & Stats. + MC syst.        & Stats.+ full syst.        \\ 
          \midrule
          \SSll         & $30.70^{43.47}_{22.12}$  & $31.62^{44.76}_{22.79}$  & $34.81^{49.61}_{25.08}$   \\
          \lll        & $23.82^{34.03}_{17.16}$  & $25.58^{37.00}_{18.43}$  & $28.13^{40.94}_{20.27}$   \\ 
          \bbllll     & $27.24^{40.90}_{19.63}$  & $27.62^{41.76}_{19.90}$  & $28.71^{44.41}_{20.68}$   \\ 
          \ltautau    & $34.64^{49.51}_{24.96}$  & $38.31^{54.33}_{27.60}$  & $41.21^{58.92}_{29.70}$   \\ 
          \lltautau   & $32.82^{48.34}_{23.65}$  & $33.46^{49.12}_{24.11}$  & $33.99^{50.09}_{24.49}$   \\ 
          \SSlltau      & $50.50^{72.83}_{36.39}$  & $62.37^{91.18}_{44.94}$  & $63.52^{93.27}_{45.77}$   \\ 
          \yyl     & $25.43^{36.95}_{18.32}$  & $25.43^{36.95}_{18.32}$  & $26.68^{39.53}_{19.23}$   \\ 
          \yytau   & $52.58^{76.54}_{37.89}$  & $52.50^{76.57}_{37.90}$  & $54.50^{80.98}_{39.27}$   \\ 
          \yyll       & $37.05^{54.86}_{26.70}$  & $37.05^{54.86}_{26.70}$  & $38.21^{57.76}_{27.53}$   \\ 
          \midrule
           Combined    & $8.93^{12.69}_{6.44}$    & $9.29^{13.22}_{6.70}$    & $9.74^{13.91}_{7.02}$     \\ 
           \bottomrule
        \end{tabular}
    }
    \caption{Table of 95\% C.L. upper limits on signal strength for multi-lepton channels. Limits are derived using Asimov datasets under varying scenarios of statistical and systematic uncertainties. }
    \label{tab:5:comb_upperlimit}
\end{table}


