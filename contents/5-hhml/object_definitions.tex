This section describes the methodologies for defining key objects with respect to multilepton channels. Criteria on data corruption or significant calorimeter noise result in event exclusion.

\subsubsection{Selection of Primary Vertices}
For multilepton channels, the primary vertex is designated as the one with the maximal $\sum \pT^2$ of its constituent tracks, following the criteria outlined in~\cite{ATL-PHYS-PUB-2015-026}.

\subsubsection{Event Triggering}
In multilepton channels, an array of single-lepton and di-lepton triggers, pertinent to the 2015 - 2018 dataset, is listed in Table~\ref{tab:5:triggers_mb}. These triggers are exempt from prescaling and are derived from the $ttH$ multilepton $80$~\ifb analysis~\cite{VazquezSchroeder:2314122}. An inclusive OR condition is imposed between di-lepton (DL) and single-lepton (SL) triggers when at least two light leptons are involved. For scenarios involving a single light lepton and two \bbtthadhad, non-prescaled single-lepton triggers are required.

\input{contents/5-hhml/tables/trigger.tex}

Trigger corrections for simulation are computed event-wise with the \myverb{TrigGlobalEfficiencyCorrection} package~\cite{TriggerEfficiency}. Any relevant scale factors for light lepton identification and isolation are appropriately integrated into the MC weight.


\subsubsection{Lepton Working Points and Definitions}
\label{sec:5:leptonwp}
In signal events, leptons are primarily produced through the leptonic decays of $WW/ZZ$, which are themselves produced by Higgs decays. Harmonized Working Points (WPs) have been developed to optimize performance in both \SSll and \lll channels while minimally affecting the tau-involved channels.

For multilepton channels, three distinct levels of light lepton selection criteria are implemented depending on the final state's object composition. These criteria are referred to as "Baseline" (B), "Loose" (L), and "Tight" (T), as defined in Table~\ref{tab:5:lepton_selection}. The Baseline criteria are exclusively used in \bbllll channels to augment the signal sensitivity. For categories other than \bbllll, the \textit{Loose} definition is applied to establish channel orthogonality. Stricter definitions are utilized in channels with up to two light leptons and a single hadronically decaying tau to maximize signal sensitivity and minimize background interference. Specific lepton criteria for electrons, muons are elaborated in Sections~\ref{subsec:5:ele},~\ref{subsec:5:muon}, respectively.

\begin{table}[!htb]
    \centering
    \resizebox{0.95\columnwidth}{!}{
        \begin{tabular}{l c c c c c c}
            \toprule
            & \multicolumn{3}{c}{$e$} & \multicolumn{3}{c}{$\mu$} \\
            \midrule
            & B  & L & T  & B  & L & T \\
            \midrule
            Isolation           & None & PLVLoose & PLVTight & None  & PLVLoose & PLVTight \\
            
            Identification      & \multicolumn{2}{c}{LooseLH} & TightLH & \multicolumn{2}{c}{Loose} & Medium  \\
        
            Charge MisID BDT    & No & Yes & Yes & N/A & N/A & N/A \\
        
            Ambiguity Resolution & No & Yes & Yes & N/A & N/A & N/A \\
        
            $|d_0|/\sigma_{d_0}$ & \multicolumn{3}{c}{$<5$} & \multicolumn{3}{c}{$<3$} \\
        
            $|z_0 \sin \theta|$ & \multicolumn{6}{c}{$< 0.5$ mm} \\
            \bottomrule
        \end{tabular}
    }
    \caption{Definitions of Baseline, Loose, and Tight criteria in multilepton channels.}
    \label{tab:5:lepton_selection}
\end{table}


\subsubsection{Electrons}
\label{subsec:5:ele}

The selection criteria for electron candidates in different multilepton channels involve several key elements:

\begin{itemize}
  \item Electrons are reconstructed through matching energy deposits in the electromagnetic calorimeter with tracks in the inner detector.
  
  \item Baseline electrons are required to have \( p_T > 4.5 \) GeV and \( |\eta| < 2.5 \). Electrons within the calorimeter transition region \(1.37 < |\eta| < 1.52\) are excluded.
  
  \item Stringent cuts on \(d_0\) and \(z_0\) ensure that electrons originate from a primary vertex.
  
  \item Employing a likelihood-based identification, the working points "LooseLH" and "TightLH" are used for Baseline/Loose and Tight electron candidates, respectively.
  
  \item Loose electrons should pass the \textit{PLVLoose} working point\footnote{PromptLeptonVeto} to ensure event isolation~\cite{Ospanov:2220954}.
  
  \item For stricter selection beyond Baseline, additional criteria like charge misidentification BDT and ambiguity selection (details in Section~\ref{sec:4:photon_id_electron_fakes}) are implemented to reduce background contributions.
  
  \item For Tight electrons, the \textit{PLVTight} Isolation working point and the \textit{TightLH} ID are mandated.
\end{itemize}


\subsubsection{Muons}
\label{subsec:5:muon}

The selection criteria for muon candidates in multilepton channels are outlined below:

\begin{itemize}
  \item Muons are reconstructed using data from both the Muon Spectrometer and the Inner Detector.
  
  \item Muon candidates must satisfy \( p_T > 3 \) GeV and \( |\eta| < 2.5 \).
  
  \item Two identification working points are used. Baseline muons are required to pass the "Loose" working point, while tighter criteria employ the "Medium" working point.
  
  \item Similar to electrons, muons must pass \textit{PLVLoose} isolation criteria for baseline selection. Loose muons are required to fulfill \textit{PLVLoose}, while Tight muons are selected from \textit{PLVTight}.
  
  \item The criteria for both transverse and longitudinal impact parameters are harmonized with electrons. Specifically, \( |d_0|/\sigma_{d_0} < 3 \) and \( z_0 < 0.5 \) mm.
\end{itemize}


\subsection{Jet and b-jet}
\label{subsec:5:jet_summary}

The key elements concerning the selection and definition of jets and b-jets in the analysis are itemized below:

\begin{itemize}
  \item The \antikt algorithm with a radius parameter \(R=0.4\) is used, operating on particle-flow (PFlow) objects.

  \item For ML channel, \myverb{AntiKt4EMPFlowJets\_BTagging201903} is employed.
  
  \item Events must pass the LooseBad working point as recommended by the Jet-\(E_T^\text{miss}\) group. A Tight Jet Vertex Tagger (JVT) working point is applied for \(p_T < 60\) GeV and \(|\eta| < 2.4\).
  
  \item Kinematic Selection:
  \begin{itemize}
      \item \(p_T > 25\)~GeV
      \item \(|\eta| < 2.5\)
      \item \(|y| < 4.4\)
  \end{itemize}
  
  \item Flavour Tagging: Utilizes a deep learning algorithm known as DL1r\cite{btagging}, which has been re-optimized in 2019. The b-tagging working point with a 77\% efficiency is selected.
\end{itemize}

The choices made for the jet and b-jet selections, including specific working points and efficiencies, are aimed at maximizing orthogonality to other diHiggs analyses. Associated scale factors for JVT and b-tagging are integrated into the MC event weight calculations. More details of these selection criteria can be found in the relevant studies~\cite{FTAG-2019-004,FTAG-2019-005}.



\subsection{Overlap Removal}
\label{subsec:removal}

Overlap removal is essential to eliminate double-counting of reconstructed objects, which may occur due to the parallel use of different algorithms for object reconstruction. The procedure employed in this analysis is based on the ASG overlap removal tool in AnalysisTop~\cite{Overlap_Removal}. The criteria for overlap removal are listed in Table~\ref{tab:5:overlap_removal}.


\begin{table}[!htb]
    \centering
    \resizebox{0.95\columnwidth}{!}{
        \begin{tabular}{lll}
            \toprule
            Kept Object & Removed Object & Condition \\
            \midrule
            Electron & Calorimeter Muon & Sharing same track \\
            Non-calorimeter Muon & Electron & Sharing same track \\
            Electron & Jet & \(\Delta R < 0.2\) \\
            Jet & Electron & \(\Delta R < 0.4\) \\
            Muon & Jet & \(n_{\text{track}} < 3\) and \(\Delta R < 0.2\) \\
            Muon & Jet & \(n_{\text{track}} < 3\) and Ghost-associated to muon ID track \\
            Jet & Muon & \(\Delta R < 0.4\) \\
            Electron & Tau & \(\Delta R < 0.2\) \\
            Combined-type Muon & Tau & \(p_T > 2\) GeV and \(\Delta R < 0.2\) \\
            Tau & Jet & \(\Delta R < 0.2\) \\
            Electron/Muon & Photon & \(\Delta R < 0.4\) \\
            Photon & Jet & \(\Delta R < 0.4\) \\
            \bottomrule
        \end{tabular}
    }
    \caption{Summary of Overlap Removal Criteria in Multilepton Channels.}
    \label{tab:5:overlap_removal}
\end{table}

