The analysis employs a multivariate approach using the Boosted Decision Trees (BDT) method, within the TMVA framework in ROOT 6.18/00\cite{Brun:1997pa}. An enhanced version, Gradient BDT (BDTG), has also been implemented for this analysis. The training dataset comprises both prompt backgrounds, such as \( t\bar{t}V \), \(VV\), \( tV \), \( VH \), \( VVV \), and \( t\bar{t}H \), as well as the non-prompt processes, henceforth referred to as "fake background."

\subsubsection{Input Variable and Correlation}
\label{sec:5:input_variable_correlation}

Most of the Higgs bosons in 3-lepton channel are moderately boosted, thereby yielding leptons in the final state that are often spatially close due to the W-boson spin correlations in the Higgs decay processes. Variables coming from the lepton kinematics, such as $\Delta R_{l_i l_j}$, $m_{l_i l_j}$ are expected to have strong separation power, with W-boson can be partially reconstructed. Other variables like the missing transverse energy ($\slashed{E}_T$), sum of the transverse momentum of all objects ($HT$), and the invariant mass of all three leptons and two leading jets ($m_{llljj}$) are also expected to be useful in separating signal from background.
The variables employed in BDTG training are itemized in Table~\ref{tab:5:var_table}, selected for their maximal discriminative power; the top 13 variables have been ranked according to their separation efficacy\footnote{The variables were selected based on their separation power as quantified during BDT training.}. 
Observable high correlations exist among certain invariant masses. Specifically, an 82\% correlation between \(m_{lll}\) and \(m_{l_1 l_2}\), and a 90\% correlation between \(m_{lll}\) and \(m_{l_0 l_2}\) are in line with expectations. Similarly, an 83\% correlation is noted between \(HT\) and \(m_{llljj}\). The respective correlation matrix is shown in Figures~\ref{fig:5:CorrelationMatrixS} and ~\ref{fig:5:CorrelationMatrixB}.

It is essential to note that despite these substantial correlations, the input variable set has been optimized based on the Area Under the Curve (AUC) metric. Empirical data substantiate a marked reduction in performance when the variable set is truncated. Consequently, the presence of high correlations among variables is tolerated in favor of optimized performance.

% Detailed distributions of these variables for both signal and background are presented in Appendix~\ref{app_3l:bdt_var}.

\begin{table}[!htb]
    \centering
    \resizebox{0.95\columnwidth}{!}{
        \begin{tabular}{lll}
            \toprule
            Variable                    & Description                                                                 & Separation \\ \midrule
            $\Delta R_{l_0 l_1}$                    & Distance in $\eta - \phi$ space between lepton 0 and lepton 1       & 32.62\%    \\
            $m_{l_0 l_1}$                           & Invariant mass of lepton 0 and lepton 1                             & 26.90\%    \\
            $\textrm{min. } m_{ll}^{\textrm{OS}}$   & Minimum invariant mass of opposite-sign lepton pairs                & 26.23\%    \\
            $\Delta R_{l_2 j}$                      & Distance in $\eta - \phi$ space between lepton 2 and nearest jet    & 23.90\%    \\
            $\Delta R_{l_1 l_2}$                    & Distance in $\eta - \phi$ space between lepton 1 and lepton 2       & 12.67\%    \\
            $\textrm{min. } m_{ll}^{\textrm{OSSF}}$ & Minimum invariant mass of opposite-sign same-flavor lepton pairs    & 11.41\%    \\
            $m_{ll}^\textrm{Z-matched}$             & Invariant mass of lepton pair closest to $Z$ mass                   & 11.38\%    \\
            $m_{llljj}$                             & Invariant mass of all three leptons and two leading jets            & 3.49\%     \\
            $m_{lll}$                               & Invariant mass of all three leptons                                 & 2.94\%     \\
            $m_{l_2 j}$                             & Invariant mass of lepton 2 and nearest jet                          & 2.40\%     \\
            $m_{l_0 l_2}$                           & Invariant mass of lepton 0 and lepton 2                             & 2.11\%     \\
            $\slashed{E}_T$                         & Missing transverse energy                                           & 1.80\%     \\
            $\Delta R_{l_0 j}$                      & Distance in $\eta - \phi$ space between lepton 0 and nearest jet    & 1.20\%     \\
            FlavorCategory                          & Categorization of lepton flavors, details in Sec.~\ref{sec:5:signal_topology} & 1.17\%     \\
            $HT_{\textrm{lep}}$                     & Scalar sum of lepton $p_T$'s and missing transverse momentum        & 0.96\%     \\
            $HT$                                      & Scalar sum of jet $p_T$'s                                           & 0.52\%     \\
            $\Delta R_{l_1 j}$                      & Distance in $\eta - \phi$ space between lepton 1 and nearest jet    & 0.33\%     \\
            $\Delta R_{l_0 l_2}$                    & Distance in $\eta - \phi$ space between lepton 0 and lepton 2       & 0.26\%     \\
            $m_{l_1 j}$                             & Invariant mass of lepton 1 and nearest jet                          & 0.19\%     \\
            $HT_{\textrm{jets}}$                    & Scalar sum of jet $p_T$'s                                           & 0.05\%     \\
            $m_{l_0 j}$                             & Invariant mass of lepton 0 and nearest jet                          & 0.01\%     \\
            $m_{l_1 l_2}$                           & Invariant mass of lepton 1 and lepton 2                             & 0.01\%     \\ 
            \bottomrule
        \end{tabular}
    }
    \caption{Discriminant variables used in BDTG training for \lll channel.}
    \label{tab:5:var_table}
\end{table}


\begin{figure}[!htb]
    \centering
    \subfloat[]{
        \begin{minipage}[b]{0.48\textwidth}
            \includegraphics[width=\textwidth]{figures/5/3l/correlation_bkg}
            \label{fig:5:CorrelationMatrixS}
        \end{minipage}
    }
    \hfill
    \subfloat[]{
        \begin{minipage}[b]{0.48\textwidth}
            \includegraphics[width=\textwidth]{figures/5/3l/correlation_sig}
            \label{fig:5:CorrelationMatrixB}
        \end{minipage}
    }
    \caption{
        The correlation matrix of MVA variables for signal and background (pure MC) training samples.
    }
    \label{fig:5:CorrelationMatrix}
\end{figure}


\subsubsection{3-Fold Cross-Validation}
Owing to the statistical constraints of the Monte Carlo training samples, a \(k\)-fold Cross-Validation (\(k\)-CV) strategy is employed, specifically 3-fold cross-validation. This approach uses the full statistical power of the dataset to generate robust training models and produce a smooth Receiver Operating Characteristic (ROC) curve. 

In this setup, the MC samples are partitioned into three distinct sets: one for training, another for testing, and the third for application, thereby maintaining orthogonality between training and application sets. Event numbers are employed as the basis for fold assignment. During the training phase, no additional weights, including WZ-reweighting and fake normalization factors, are applied, as their impact has been found negligible. Conversely, in the application phase, these normalization factors are incorporated.

The training methodology remains consistent across all folds. For the BDTG, hyperparameter optimization targets the maximization of the AUC value for the testing set. The optimized hyperparameters are as follows:
\begin{itemize}
    \item Number of Trees: 715
    \item Maximum Tree Depth: 4
    \item Boost Type: Gradient
    \item Bagged Boost Employed (Bagged Sample Fraction: 0.5)
    \item Number of Cuts (\myverb{nCuts}): 20
\end{itemize}

The ROC curves for each of the 3-fold cross-validation sets using the BDTG method are compiled in Figure~\ref{fig:5:BDTG_Result}.

\begin{figure}[!htb]
    \centering
    \includegraphics[width=0.75\textwidth]{figures/5/3l/roc_cv_3l}
    \caption{
        Training results of BDTG: Receiver Operating Characteristic (ROC) curve for 3-lepton channel, 
        illustrating the trade-off between signal efficiency (True Positive Rate) and background inefficiency (False Positive Rate). 
    }
    \label{fig:5:BDTG_Result}
\end{figure}


\FloatBarrier