As introduced in Section~\ref{sec:2:llps}, LLPs can be a potential probe for new physics beyond the Standard Model. 

\subsection{Methods and Subsystems for LLP Detection in Collider Experiments}
Collider-based searches for LLPs can be broadly classified into direct and indirect detection methods. In direct detection, the LLP interacts directly with the detector subsystems, whereas indirect detection involves the reconstruction of the LLP's decay into SM particles. A typical collider detector consists of several main subsystems: the inner detector (ID), electromagnetic calorimeter (ECAL), and hadronic calorimeter (HCAL), along with specialized systems for muon tracking (Muon Spectrometer). These subsystems work in tandem to measure various particle properties such as charge, momentum, and energy. It is crucial to note that while these subsystems are optimized for detecting SM particles, they also provide avenues for LLP detection.

\subsection{Challenges and Considerations in LLP Detection}
LLP detection poses unique challenges compared to SM particles. First, the efficiency of LLP detection decreases as the displacement of the LLP from the interaction point increases. Second, traditional tracking algorithms may not adequately capture LLPs, leading to missed or irregular tracks. Nonetheless, the versatility of collider detectors, when adjusted for these specific challenges, makes them powerful tools for LLP searches. In particular, careful consideration must be given to the differences in reconstruction techniques for prompt and displaced particles.

\begin{figure}
    \centering
    \includegraphics[width=0.99\textwidth]{figures/8/LLP_signature.pdf}
    \caption{The schematic representation of LLPs' decaying process in the detector.}
    \label{fig:8:llp_decay}
\end{figure}

While LLPs may themselves escape detection due to their long lifetimes, they often yield displaced vertices upon decay, creating anomalous tracks in the inner detector or atypical energy deposits in calorimeters. In the case of LLP decays in the muon system, specialized tracks distinct from typical muon signatures may emerge. These atypical features, collectively termed as Associated Objects (AOs), serve as key markers for LLP identification. However, LLP searches are complicated by a variety of Standard Model backgrounds. For instance, punch-through jets could mimic the energy deposit pattern of an LLP in calorimeters. Similarly, bremsstrahlung from high-energy muons can create misleading signatures. Furthermore, some heavy flavor hadrons have lifetimes that could lead to displaced vertices, thus contributing to the background. These backgrounds necessitate rigorous data analysis techniques to discern genuine LLP signatures. A schematic representation of LLPs' decaying process in the detector is shown in Figure~\ref{fig:8:llp_decay}.

\subsection{Signatures of Neutral Long-Lived Particles}
The focus of this study is solely on neutral LLPs. Given their neutral charge, these particles do not exhibit some of the signals commonly associated with charged LLPs, such as anomalous tracks in the inner detector or in the muon system. Instead, neutral LLPs are typically identified through their decay products. The following paragraphs describe the various signatures of neutral LLPs in collider experiments.

\subsubsection{Time-Delayed Detector Responses}
A neutral LLP traversing the detector at reduced velocity compared to SM particles exhibits time-delayed signals in various subsystems, such as calorimeters and Muon Spectrometers (MS). This delayed arrival serves as a unique identifying feature. Precise timing resolutions on the order of 1 ns, along with the detector's dimensions, allow for accurate speed measurements. These measurements, in combination with momentum data, facilitate mass determination for the neutral LLP. 
Unlike SM particles with identical momenta (almost at the speed of light), heavy, neutral LLPs take an extended period to traverse from the point of production to the detection subsystems. The timing disparity~\cite{Liu_2019}, $\Delta t$, can be quantified as
\begin{equation}
    \label{eq:refined_time_difference}
    \Delta t = t_{\text{hit}} - \frac{L_{\text{SM}}}{c},
\end{equation}
where \( t_{\text{hit}} \) is the timestamp of the detector hit, \( L_{\text{SM}} \) represents the distance from the primary vertex to the point of detection, and \( c \) is the speed of light. For neutral LLPs, \( \Delta t \) exhibits a distribution shifted significantly towards positive values, whereas for SM particles, it clusters around zero.

\subsubsection{Non-Prompt Track Signatures}
Tracks originating from neutral LLP decays often deviate considerably from the beam spot, typically characterized by the transverse impact parameter \(d_0\). Accounting for detector uncertainties, a large \(d_0/\sigma_{d_0}\) ratio is indicative of a displaced track. Due to data and computational resource constraints, tracks with large \(d_0\) or \(z_0\) values are not typically reconstructed by default algorithms. Specialized reconstruction methods have emerged to address these challenges.

\subsubsection{Identifying Displaced Vertices}
When multiple tracks from a neutral LLP decay are detected, they often converge at a displaced vertex (DV). The DV's position and its covariance matrix can be determined through vertex-fitting algorithms. This vertex distance is generally more accurate than \(d_0\), and various kinematic variables can further distinguish it from background. As analysis techniques mature, rejection of background vertices near dense material becomes increasingly effective.

\subsubsection{Signatures in Calorimeters}
While the Inner Detector (ID) is usually limited to small spatial measurements, calorimeters extend the search to larger distances. Longitudinal shower shapes and energy deposition ratios between the Electromagnetic Calorimeter (ECAL) and Hadronic Calorimeter (HCAL) can offer insights into neutral LLP decays occurring within the calorimeter volume. These features are notably distinct from standard SM jets and thus serve as valuable markers for neutral LLPs.

\subsubsection{Composite Detection Methods}
While individual signatures are informative, their combined use can enhance the sensitivity and robustness of neutral LLP searches. Correlations between different subsystems provide multiple, uncorrelated handles for background rejection. It's crucial to note that the standard reconstruction methods may introduce biases and additional systematic uncertainties when applied to displaced objects.

\subsubsection{LLP Detection at LHC and Future Lepton Colliders}
There are many existing studies on LLPs performed using data from the ATLAS~\cite{PhysRevD.106.032005} and CMS~\cite{CMS-PAS-EXO-21-008} experiments at the LHC. However, it's crucial to note that the sensitivity of these experiments to LLPs is currently suboptimal. A primary limiting factor lies in the high level of QCD background, colloquially referred to as \textit{dirty backgrounds.} These pervasive backgrounds impose a ceiling on the precision of exclusion limits set by these experiments.
Moreover, the detector designs for ATLAS and CMS were not originally optimized to efficiently search for LLP signatures. The constraints are more profound when one considers the increasing luminosity of the LHC, exacerbating the challenge of identifying relatively weak signals from LLPs amid an overwhelming amount of SM background noise.

In contrast, future lepton colliders such as the FCC-ee~\cite{agapov2022future} and the CEPC~\cite{cepc_1, cepc_2} present more promising avenues for LLP detection. These colliders are expected to offer a substantially cleaner environment with reduced QCD background, thus allowing for more stringent exclusion limits for LLPs. The well-defined initial state and fewer sources of systematic uncertainty enable these colliders to be highly effective in identifying the subtle signals associated with LLPs. Furthermore, the lower center-of-mass energy of these colliders allows for the production of LLPs with lower masses, which are challenging to detect at the LHC. 

Despite the abundance of theoretical models proposing various LLPs, focusing on final states presents a strategic approach to exploring LLPs in future lepton colliders. This focus is underpinned by the distinct signature of LLPs, setting them apart from SM particles. Typical final states in this context would contain one or two visible LLPs accompanied by easily-tagged SM particles, such as the Z-boson. One such production mechanism involves a resonance or scalar particle, exemplified by the Higgs boson. Lepton colliders offer a high occurrence rate for the $e^+ e^- \rightarrow ZH$ process, serving as a natural avenue for LLP production via the $H \rightarrow XX$ decay channel, where $X$ is the neutral LLP. Therefore, this study will concentrate on the physics process denoted as $e^+ e^- \rightarrow ZH \rightarrow Z_{\text{products}} + XX$, where $X$ could either decay into invisible particles or SM jets/leptons. However, it should be noted that the methodology employed is extensible to any model yielding similar final states.

To this end, this study will be primarily anchored in simulations and data from the CEPC. Leveraging the CEPC's advantageous features, such as its lower systematic uncertainties and cleaner background, we aim to provide a comprehensive analysis of LLP detection mechanisms and their underlying implications in a lepton-collider setting.