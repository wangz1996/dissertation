Event generation and the ensuing simulation processes employ a gamut of specialized software tools tailored to accurately model both the SM signal and background events. Specifically, Whizard~\cite{Kilian_2011} is utilized for generating a comprehensive dataset comprising Higgs boson signals alongside SM background events. Post-generation, these events are subjected to simulation and reconstruction via MokkaC~\cite{MoradeFreitas:2002kj}, which serves as the CEPC's official simulation software and is constructed upon the framework initially designed for ILC studies~\cite{theildconceptgroup2010international}.

Due to computational limitations, background event samples are occasionally subjected to pre-selection based on lax generator-level criteria or are alternatively processed using accelerated simulation tools. For a more granular simulation and reconstruction, Geant4~\cite{Geant4_1, Geant4_2} is applied to all Higgs boson signal samples and a subset of leading background samples. The remaining background samples employ a dedicated fast simulation tool where various parameters such as detector acceptance, efficiency, and intrinsic resolution are suitably parameterized.

Table~\ref{tab:8:samples} enumerates the expected event yields for various processes at an integrated luminosity of \(5.6~\text{ab}^{-1}\). It should be noted that interference effects can manifest between the same final states originating from different processes subsequent to the decay of W or Z bosons (see the main text for details). The cross-section calculations for most processes are carried out using the Whizard program~\cite{Kilian_2011}. For the Bhabha process specifically, cross-section values are computed using the BABAYAGA event generator~\cite{CARLONICALAME200448}, with constraints imposed on the final-state particles (\(|\cos\theta| < 0.99\)) and any resulting photons (if present) to have \(E_{\gamma} > 0.1~\text{GeV}\) and \(|\cos\theta_{e\pm\gamma}| < 0.99\).

\begin{table}[!htb]
    \centering
    \resizebox{0.60\columnwidth}{!}{
        \begin{tabular}{lcc}
            \toprule 
            Process & Cross section & Events in \(5.6~\mathrm{ab}^{-1}\) \\
            \midrule
            \multicolumn{3}{c}{Higgs boson production, cross section in \(\mathrm{fb}\)} \\
            \midrule
            \(e^{+} e^{-} \rightarrow Z H\) & 204.7 & \(1.15 \times 10^6\) \\
            \(e^{+} e^{-} \rightarrow \nu_e \bar{\nu}_e H\) & 6.85 & \(3.84 \times 10^4\) \\
            \(e^{+} e^{-} \rightarrow e^{+} e^{-} H\) & 0.63 & \(3.53 \times 10^3\) \\
            \midrule 
            Total & 212.1 & \(1.19 \times 10^6\) \\
             & & \\
            \multicolumn{3}{c}{ Background processes, cross section in pb } \\
            \midrule
            \(e^{+} e^{-} \rightarrow e^{+} e^{-}(\gamma)\) (Bhabha) & 850 & \(4.5 \times 10^9\) \\
            \(e^{+} e^{-} \rightarrow q \bar{q}(\gamma)\) & 50.2 & \(2.8 \times 10^8\) \\
            \(e^{+} e^{-} \rightarrow \mu^{+} \mu^{-}(\gamma)\) [or \(\left.\tau^{+} \tau^{-}(\gamma)\right]\) & 4.40 & \(2.5 \times 10^7\) \\
            \(e^{+} e^{-} \rightarrow W W\) & 15.4 & \(8.6 \times 10^7\) \\
            \(e^{+} e^{-} \rightarrow Z Z\) & 1.03 & \(5.8 \times 10^6\) \\
            \(e^{+} e^{-} \rightarrow e^{+} e^{-} Z\) & 4.73 & \(2.7 \times 10^7\) \\
            \(e^{+} e^{-} \rightarrow e^{+} \nu W^{-} / e^{-} \bar{\nu} W^{+}\) & 5.14 & \(2.9 \times 10^7\) \\
            \bottomrule
        \end{tabular}
    }
    \caption{Cross-Section of Higgs and Background Processes at CEPC}
    \label{tab:8:samples}
\end{table}

\subsection{LLP Signal Production}
The primary mechanism for LLP signal production in this study is the Higgsstrahlung process \(e^+ e^- \to ZH\), where the Z boson is allowed to decay inclusively—that is, into any of its possible decay modes. Subsequently, the Higgs boson decays into a pair of LLPs \(H \to X_1 X_2\). 
The specific channel under investigation assumes that \(X_1\) decays into a pair of quarks (\(X_1 \to qq\)) while \(X_2\) may either decay invisibly or similarly into quarks (\(X_2 \to qq\)). Figure~\ref{fig:8:Sig_Fey} presents Feynman diagrams of two specific final state configurations, viz., \(X_1\to qq\), \(X_2\to \text{invisible}\) and \(X_1 \to qq\), \(X_2 \to qq\). 

To simulate these processes, we utilize \MGNLO{3.0}~\cite{Alwall_2011} for event generation. Signal samples are generated for three distinct mass points of \(X_1\) and \(X_2\) which are \(1\, \text{GeV}, 10\, \text{GeV}, 50\, \text{GeV}\), and for five distinct lifetimes of \(10^{-3}, 10^{-1}, 1, 10, 100\) ns. At each point in this parameter space, a total of \(10^6\) events are generated for statistical robustness.

\begin{figure}[!htb]
  \subfloat[2-jet]{\includegraphics[width=0.48\linewidth]{figures/8/llp_2j.pdf}}
  \subfloat[4-jet]{\includegraphics[width=0.48\linewidth]{figures/8/llp_4j.pdf}}
  \caption{Feynman diagrams illustrating LLP production via the Higgsstrahlung process.}
  \label{fig:8:Sig_Fey}
\end{figure}

Signal acceptance is dictated in the generator level by the unique characteristics of LLPs, which decay according to exponential laws. Given the limitations of the CEPC detector, which has a maximum detection range of approximately 6 meters, special attention must be paid to the LLPs that decay within this confinement. To model this behavior accurately, LLPs are generated with intrinsic lifetimes and then Lorentz-boosted based on their respective masses and momenta. Subsequently, we apply stringent selection criteria to only include those LLPs whose decay vertices fall within the physical boundaries of the CEPC detector. These selected events are then tabulated for further analysis, as shown in Table~\ref{tab:8:signal-acceptance}.

\begin{table}[h]
    \centering
    \begin{tabular}{cccccc}
    \toprule
    Mass (GeV) & \multicolumn{5}{c}{Lifetime (ns)} \\
               & 0.001 & 0.1 & 1 & 10 & 100 \\
    \midrule
    50 & 1.0 & 1.0 & 1.0 & 0.993 & 0.404 \\
    10 & 1.0 & 1.0 & 0.998 & 0.468 & 0.062 \\
    1  & 1.0 & 1.0 & 0.488 & 0.065 & 0.007 \\
    \bottomrule
    \end{tabular}
    \caption{Signal Acceptance for Different Mass and Lifetime Parameters}
    \label{tab:8:signal-acceptance}
\end{table}
    

\FloatBarrier