The CEPC~\cite{cepc_1, cepc_2} is engineered to function both as a Higgs boson factory with a center-of-mass energy (\(\sqrt{s}\)) of 240 GeV and as a Z boson factory at \(\sqrt{s} = 91.2\) GeV. Additionally, it is capable of conducting threshold scans for W boson production around \(\sqrt{s} = 160\) GeV. Table~\ref{tab:8:CEPC_Scenarios} outlines the likely operational modes and the projected yields of H, W, and Z bosons.

\begin{table}[!htb]
    \centering
    \resizebox{0.85\columnwidth}{!}{
        \begin{tabular}{lccc}
            \toprule 
            Operation mode & \(Z\) factory & \(W W\) threshold & Higgs factory \\
            \midrule
            \(\sqrt{s}(\mathrm{GeV})\) & 91.2 & 160 & 240 \\
            Run time (year) & 2 & 1 & 7 \\
            Instantaneous luminosity \(\left(10^{34} \mathrm{~cm}^{-2} \mathrm{~s}^{-1}\right)\) & \(16-32\) & 10 & 3 \\
            Integrated luminosity \(\left(\mathrm{ab}^{-1}\right)\) & \(8-16\) & 2.6 & 5.6 \\
            \midrule 
            Higgs boson yield & - & - & \(10^6\) \\
            \(W\) boson yield & - & \(10^7\) & \(10^8\) \\
            \(Z\) boson yield & \(10^{11}-10^{12}\) & \(10^8\) & \(10^8\) \\
            \bottomrule
        \end{tabular}
    }
    \caption{Projected Operational Modes of CEPC and Corresponding Yields of Higgs, W, and Z Bosons; calculations Based on Two Interaction Points and Integrated Luminosity}
    \label{tab:8:CEPC_Scenarios}
\end{table}

During its seven-year tenure as a Higgs factory, the CEPC aims to generate roughly 1 million Higgs bosons at two interaction points. Concurrently, the collider is expected to yield nearly 100 million W bosons and approximately 1 billion Z bosons. Such large data sets serve dual purposes: detector calibration and precision measurements in electroweak theory.

When operating around the W boson threshold of \(\sqrt{s} = 160\) GeV, the CEPC is projected to produce about \(10^7\) W bosons within one year. In the Z boson factory mode, estimates suggest a production rate ranging from \(10^{11}\) to \(10^{12}\) Z bosons. These abundant samples facilitate highly precise measurements of a range of electroweak parameters.

\subsubsection{Preliminary Detector Design}
The primary aim of the CEPC experiments is an exhaustive study of Higgs boson properties. Consequently, detectors must exhibit high performance in identifying and reconstructing key physical entities such as charged leptons, photons, jets, and missing variables (energy and momentum). Flavor tagging of jets arising from b, c, or light quarks is particularly essential for isolating hadronic decay channels of the Higgs boson.

\begin{table}[!htb]
    \centering
    \resizebox{0.95\columnwidth}{!}{
        \begin{tabular}{ll}
            \toprule 
            Tracking system & \\
            \quad Vertex detector  & 6 pixel layers \\
            \quad Silicon tracker & 3 barrel layers, 6 forward disks on each side \\
            \quad Time projection chamber & 220 radial readouts \\
            \midrule
            Calorimetry & \\
            \quad ECAL & \(\mathrm{W} / \mathrm{Si}, 24 X_0, 5 \times 5 \mathrm{~mm}^2\) cell with 30 layers \\
            \quad HCAL & \(\mathrm{Fe} / \mathrm{RPC}, 6 \lambda, 10 \times 10 \mathrm{~mm}^2\) cell with 40 layers \\
            \midrule
            Performance & \\
            \quad Track momentum resolution & \(\Delta\left(1 / p_T\right) \sim 2 \times 10^{-5}(1 / \mathrm{GeV})\) \\
            \quad Impact parameter resolution & \(5 \mu \mathrm{m} \oplus 10 \mu \mathrm{m} /\left[(p / \mathrm{GeV})(\sin \theta)^{3 / 2}\right]\) \\
            \quad ECAL energy resolution & \(\Delta E / E \sim 16 \% / \sqrt{E / \mathrm{GeV}} \oplus 1 \%\) \\
            \quad HCAL energy resolution & \(\Delta E / E \sim 60 \% / \sqrt{E / \mathrm{GeV}} \oplus 1 \%\) \\
            \bottomrule
        \end{tabular}
    }
    \caption{Fundamental Specifications and Efficacy Metrics of the CEPC Detector System.}
    \label{tab:8:CEPC_Detector_Parameters}
\end{table}

Inspired by the International Large Detector (ILD)~\cite{theildconceptgroup2010international}, the preliminary detector model for the CEPC is fundamentally aligned with a particle flow paradigm. This strategy is rooted in the principle of optimally employing individual sub-detectors to reconstruct visible final-state particles. The particle flow~\cite{Thomson_2009, ruan2014arbor, CMS-PAS-PFT-09-001} approach thus provides a unified interpretation of a complete event and is especially beneficial for tagging complex objects like \( \tau \) leptons and jets.

To achieve these goals, the CEPC features a low-material tracking system and high-granularity calorimetry, encapsulated within a 3.5 Tesla magnetic field. Key components include Silicon-based vertex and tracking detectors, a Time Projection Chamber (TPC), and advanced calorimeters. The geometric configurations and performance metrics for the CEPC detector are summarized in Table~\ref{tab:8:CEPC_Detector_Parameters}.
