\subsubsection{Event Graph Construction}

To extend the predictive power and feature representation capabilities of our model, we employ Graph Neural Networks (GNNs). These are particularly useful for capturing the complex topological structures inherent in high-energy particle collision events.

\paragraph{Node Types and Edge Construction:}
In a single collision event, we represent the raw calorimeter and tracker hits as a heterogeneous graph, comprising two types of nodes: one for calorimeter hits, referred to as \textit{calorimeter-type nodes}, and the other for tracker hits, referred to as \textit{tracker-type nodes}. Within each type, all nodes are fully connected, meaning that every node is directly connected to every other node of the same type.

\paragraph{Calorimeter-Type Node Formation:}
For calorimeter-type nodes, a clustering algorithm is employed. The most energetic calorimeter hit is identified within the event, and it serves as a seed for the clustering process. This seed clusters together all neighboring hits that are within a pre-defined radial distance \(R = 50\, \text{mm}\). To control the quality of clustering, we enforce two specific criteria:
\begin{itemize}
    \item Minimum number of neighboring hits in the cluster, \(N_{\text{neighbor}} \geq 3\).
    \item Minimum number of total hits for forming a valid cluster, \(N_{\text{cluster}} \geq 10\).
\end{itemize}

\paragraph{Momentum Assignment to Calorimeter-Type Nodes:}
Following the clustering, each calorimeter-type node is assigned a four-momentum \( p^{\mu} \). This momentum is defined as being parallel to the node's position vector, with a magnitude determined by the energy of the calorimeter hit.

\paragraph{Tracker-Type Node Formation:}
Tracker hits are organized into spatial blocks based on their \(R-\phi\) coordinates. Given computational considerations, the division is designed into \(5 \times 6\) blocks. Within each block, tracker hits are aggregated into a tracker-type node, with the node's features being the average spatial position and the summed hit count within the block.

\paragraph{Feature Translation:}
Both calorimeter and tracker features are then transformed into a unified graph-based representation, creating a feature-rich event-level description. The specific definitions of the node and edge features are meticulously cataloged in Table~\ref{tab:8:gnn-fea}.

\begin{table}[!htb]
    \centering
    \resizebox{0.90\columnwidth}{!}{
        \begin{tabular}{cccc}
            \toprule
             Features & Variable & Definition  \\ 
             \midrule
             % calo node feat
            \multirow{6}*{calorimeter type node $i$} & $|x_{i}^{\mu}|$ & the space-time interval \\
            ~ & $|p_{i}^{\mu}|$ & the invariant mass\\ 
            ~ & $N_{i}$ & the number of hits \\
            ~ & $\eta_{i}$ & $\frac{1}{2}\ln{\frac{1+\frac{p_{z}}{p}}{1-\frac{p_{z}}{p}}}$  \\
            ~ & $\phi_{i}$ & arctan$\frac{p_{y}}{p_{x}}$ \\
            ~ & $R_{i}$ & $\sqrt{\eta^{2}+\phi^{2}}$ \\ 
            \midrule
            % calo edge feat
            \multirow{2}*{calorimeter type edge between node $i$ and $j$} & \multicolumn{2}{c}{$x_{i}^{\mu}x_{j\mu}$, $p_{i}^{\mu}p_{j\mu}$, $x_{i}^{\mu}p_{j\mu}$, $p_{i}^{\mu}x_{j\mu}$}  \\
            ~ & \multicolumn{2}{c}{$|x_{i}^{\mu}-x_{j}^{\mu}|$, $|p_{i}^{\mu}-p_{j}^{\mu}|$, $\eta_{i}-\eta_{j}$, $\phi_{i}-\phi_{j}$, $R_{i}-R_{j}$} \\ 
            \midrule
            % tracker node feat
            \multirow{5}*{tracker type node $i$} & $|r|$ & euclidean distance  \\
            ~ & $N_{i}$ & the number of hits \\
            ~ & $\eta_{i}$ & $\frac{1}{2}\ln{\frac{1+\frac{z}{r}}{1-\frac{z}{r}}}$ \\
            ~ & $\phi_{i}$ & arctan$\frac{y}{x}$ \\
            ~ & $R_{i}$ & $\sqrt{\eta^{2}+\phi^{2}}$ \\ 
            \midrule
            % tracker edge feat
            \multirow{1}*{tracker type edge between node $i$ and $j$} & \multicolumn{2}{c}{$|r_{i}-r_{j}|$, $r_{i}r_{j}$, $\eta_{i}-\eta_{j}$, $\phi_{i}-\phi_{j}$, $R_{i}-R_{j}$}  \\
            \bottomrule
        \end{tabular}
    }
    \caption{Node and edge features defined in the heterogenous graph.}
    \label{tab:8:gnn-fea}
\end{table}



\subsubsection{Network Configuration}

To use the topological features of LLP events, we utilize a sophisticated GNN-based heterogeneous architecture, as illustrated in Figure~\ref{fig:8:gnn_network}.


\begin{figure}[!htb]
    \centering
    \includegraphics[width=0.90\textwidth]{figures/8/GNNagg.png}
    \caption{Illustration of the architecture of the GNN-based neural network.}
    \label{fig:8:gnn_network}
\end{figure}

% \paragraph{Node and Edge Embedding:}
Initial node and edge features are embedded into a high-dimensional latent space. These embeddings serve as inputs to the Heterogeneous Detector Information Block (HDIB), which orchestrates message passing between calorimeter and tracker nodes.

% \paragraph{Heterogeneous Detector Information Block (HDIB):}
The HDIB comprises two Detector Information Blocks (DIBs) and two Multilayer Perceptrons (MLPs), designated as \(\phi_{g}\). Each DIB focuses on processing the node and edge embeddings of either tracker or calorimeter hits. Inspired by LorentzNet~\cite{lorentznet}, we have extended the DIB architecture to accommodate heterogeneous graphs. 

% \paragraph{Latent Space:}
The DIB outputs serve as the latent spaces for the node features \( h^{l} \) and edge features \( x^{l} \) at each layer \( l \). These are computed separately for tracker and calorimeter nodes as \( h_{t}^{l} \) and \( h_{c}^{l} \), respectively.

% \paragraph{Message Passing and Information Exchange:}
Since tracker and calorimeter nodes are not directly connected in the heterogeneous graph, their latent spaces are concatenated and processed through a MLP \(\phi_{g}\). The outputs of \(\phi_{g}\) are reshaped to obtain the updated latent spaces \( h^{l+1} \) and \( x^{l+1} \). This scheme facilitates the adaptive exchange of information between tracker and calorimeter nodes.

% \paragraph{Decoder Layer:}
Following \( L \) layers of HDIBs, the final node embeddings \( h^{L} \) are aggregated and fed into the decode layer to yield classification scores.

\paragraph{Training Configuration:}
\begin{itemize}
    \item \textbf{Hardware:} The model is trained on a cluster with 8 NVIDIA Tesla V100S PCIE 32 GB GPUs.
    \item \textbf{Batch Size:} Each GPU processes a batch size of 256 graphs.
    \item \textbf{Learning Rate:} An initial learning rate of \( 10^{-4} \) is employed.
    \item \textbf{Dropout Rate:} A dropout rate of 0.1 is applied to mitigate overfitting.
    \item \textbf{Optimizer:} Adam optimizer is used without weight decay.
    \item \textbf{Epochs:} The network is trained for a total of 30 epochs, with validation performed at the end of each epoch.
    \item \textbf{Model Selection:} The model exhibiting the minimum validation loss is chosen for application to the final test dataset.
\end{itemize}
