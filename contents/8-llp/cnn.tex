\subsubsection{Event Image Construction}

We initially embarked on an investigation that uses CNN to analyze events recorded by the CEPC detector. In this methodology, each captured event is converted into a two-dimensional, two-channel image with dimensions \( R \times \phi = 200 \times 200 \) pixels. Here, \( R \) signifies the radial distance from the interaction point to the position of a digital hit (digi), while \( \phi \) denotes the azimuthal angle of the digi. Each event's raw digital hits are mapped into this coordinate system to construct the image.

The two channels serve distinct purposes: the E-channel is responsible for capturing energy deposition, and the T-channel records time differences defined in Euqation~\ref{eq:refined_time_difference}. Specifically, the E-channel aggregates the energy of all digital hits contained within each pixel. In contrast, the T-channel computes the maximum time difference \(\Delta t = t_{\text{digi}} - \frac{R_{\text{digi}}}{c}\) among raw hits with an energy deposition greater than 0.1 GeV, aiming to filter out electronic noise.

\begin{figure}[!htbp]
    \subfloat[Signal: 2-jet]{
      \includegraphics[width=0.47\textwidth]{figures/8/LLP_sig_vsl_2j}
    }
    \hfill
    \subfloat[Signal: 4-jet]{
      \includegraphics[width=0.47\textwidth]{figures/8/LLP_sig_vsl_4j}
    } \\
    \subfloat[$ZH$]{
        \includegraphics[width=0.47\textwidth]{figures/8/LLP_bkg_vsl_ZH}
    }
      \hfill
    \subfloat[2-fermion: $e^+e^- \rightarrow q\bar{q}$]{
        \includegraphics[width=0.47\textwidth]{figures/8/LLP_bkg_vsl_eeqq}
    }
    \caption{Illustration of image built from raw detector hits for four different processes.}
    \label{fig:8:cnn_image}
\end{figure}

In Figure~\ref{fig:8:cnn_image}, four representative images are showcased, illustrating both signal and background events pertinent to two-jet and four-jet final states. In these images, three types of graphical symbols are employed for clarity: a star symbol marks the decay vertex of the LLP, serving as a reference point for the event topology, while circular symbols represent the hits as detected by the calorimeter and the muon spectrometer and square markers are the tracker hits.
The size of each circle is proportional to the energy deposition, with a larger circle indicating a greater amount of deposited energy. The color intensity of both the squares and circles signifies the time of arrival, with darker hues corresponding to earlier times.
This pixel-based representation proves effective in discriminating between signal and background events. Specifically, background events generally lack a displaced vertex and tend to show larger energy depositions in the inner detectors, distinguishing them from signal events.



\subsubsection{Network Configuration}

\begin{figure}
    \centering
    \includegraphics[width=0.99\textwidth]{figures/8/resnet18}
    \caption{llustration of the architecture of ResNet-18 based neural network.}
    \label{fig:8:resnet-18}
\end{figure}


The choice of the appropriate CNN model for our analysis is far from trivial, particularly due to the unique LLP signatures and their displaced vertices. To this end, we employ a well-known CNN model, ResNet~\cite{resnet}, originally developed for the ImageNet Large Scale Visual Recognition Challenge (ILSVRC) in 2015. The architecture of the ResNet-18 model is detailed in Figure~\ref{fig:8:resnet-18}. 
ResNet offers the advantage of scalability and efficiency; it can be configured to operate both as a shallow network for simpler tasks and as a deep network for more complex tasks. This flexibility minimizes the need for extensive model modifications tailored to specific tasks. 

The learning rate is a crucial hyperparameter that necessitates careful tuning. For our LLP-specific tasks across all mass and lifetime regimes, we have observed rapid model convergence with a relatively low number of epochs (\(\approx 15\)). This suggests that the learning rate can be kept relatively constant, although a task-specific learning rate schedule may further optimize the training process.

The network's raw output is further processed using the softmax function to generate a five-class prediction array, mathematically represented as follows:

\[
\text{Softmax}(\text{Score}) = \frac{e^{\text{Score}_i}}{\sum_{j} e^{\text{Score}_j}}
\]

For this multi-class classification problem, we employ the cross-entropy loss function, defined as:

\[
\mathcal{L} = -\sum_{i} y_i \log(p_i)
\]

Where \( y_i \) is the true label and \( p_i \) is the predicted probability for class \( i \).

