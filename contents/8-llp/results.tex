Contrary to the $CL_s$ approach commonly employed in di-Higgs search studies, the present analysis does not permit the use of approximate estimations for exclusion limits. This limitation is imposed by the specific condition of having zero background events. To circumvent this, we employ a statistical toy model to compute the upper limits. This model generates pseudo-experiments based on the predicted signal and observed data, allowing for a more robust estimation of the exclusion limits.

Several sources of systematic uncertainties are taken into account.
The uncertainty originating from the total number of Higgs bosons is estimated at 1.0\%~\cite{lum_sys}. 
The ML-related uncertainty mainly encompasses network initialization. 
To assess this uncertainty, we change the random seed during the training and evaluate the corresponding signal efficiency using XGBoost. 
We obtain 50 different signal efficiencies with different random seeds. The uncertainty is then estimated as half of the difference between the maximum and minimum values and is about 1.7\%.
Overall, the combined systematic uncertainties are quadratically summed to be 2.0\%.

To estimate the sensitivity of the study, we assume a null hypothesis for LLPs signals and obtain 95\% Confidence Level upper limits on the branching ratio \( \mathcal{B}(H \rightarrow \text{LLPs}) \) for the process $e^+e^-\to ZH (Z\to\text{inclusive}, H\to X_{1}+X_{2}$). The analyzed samples have a $5.6\text{~ab}^{-1}$ luminosity and about \(10^6\) Higgs bosons. 

For the two LLPs signal types, we have considered the following scenarios:
\begin{itemize}
 \item Type I and Type II signal yields have a fixed ratio. We define a parameter $\epsilon_{V}:= \frac{BR(X \to \nu\bar{\nu})}{BR(X \to q\bar{q})}$ as the ratio and set it with a fixed value of 0.2. A one-dimensional 95\% Confidence Level upper limit on \( \mathcal{B}(H \rightarrow \text{LLPs}) \) is derived and shown in Figure~\ref{fig:limit_total_fixed}.
 
 \item Type I and Type II signal yields has a floating ratio and  We allow \( \epsilon_V \) to be between $10^{-6}$ and $100$. A one-dimensional 95\% Confidence Level upper limit on \( \mathcal{B}(H \rightarrow \text{LLPs}) \) is derived and shown in Figure~\ref{fig:limit_total_floated}.
 
 \item Type I and Type II signal yields has a floating ratio and two-dimensional 95\% Confidence Level upper limits on \(\mathcal{B}_{\text{2-jet}}\) and \( \mathcal{B}_{\text{4-jet}} \) are derived. A bivariate statistical fit is performed to derive the upper limits and results are shown in Figure~\ref{fig:8:limit_2d}. 
More detailed results on 2-D upper limits are shown in Figure~\ref{fig:8:limit_2d_all}.
\end{itemize}

Both 1-D and 2-D exclusion limits are summarized in Table~\ref{tab:8:limit_combined}.


\begin{table}[!htb]
    \centering
    \resizebox{0.65\columnwidth}{!}{
        \begin{tabular}{ccccccc}
          \toprule
          \multirow{2}{*}{Scenario} & $\mathcal{B}$  ($\times 10^{-6}$) & \multicolumn{5}{c}{Lifetime [ns]} \\
         \cmidrule{2-7}
          & Mass [GeV] & 0.001 & 0.1 & 1 & 10 & 100 \\ 
          \midrule
         \multirow{3}{*}{Fixed} & 1 & 7 & 6 & 19 & 117 & 3394 \\ %\cline{2-7}
           & 10 & 10 & 7 & 8 & 18 & 99 \\ %\cline{2-7}
           & 50 & 6 & 4 & 5 & 5 & 13 \\ 
           \midrule
         \multirow{3}{*}{Floated} & 1 & 11 & 8 & 12 & 74 & 2395 \\ %\cline{2-7}
           & 10 & 12 & 10 & 5 & 11 & 65 \\ %\cline{2-7}
           & 50 & 8 & 10 & 7 & 8 & 9 \\ 
           \toprule
        \end{tabular}
    }
    \caption{The 95\% C.L. exclusion limit on BR($h \to X_1 X_2$) for all signal channels: fixed and floated $\epsilon_{V}$, based on CNN's efficiency.}
    \label{tab:8:limit_combined}
\end{table}


\begin{figure}[!htbp]
    \subfloat[\( \epsilon_V \) fixed\label{fig:limit_total_fixed}]{
      \includegraphics[width=0.48\textwidth]{figures/8/sen_yulei_fixed}
    }
    \hfill
    \subfloat[\( \epsilon_V \) floated\label{fig:limit_total_floated}]{
        \includegraphics[width=0.48\textwidth]{figures/8/sen_yulei_floated}
    }
    \caption{The 95\% C.L. upper limit on BR($h \to X_1 X_2$) for the process $e^+e^- \to Z h$ with the condition of 
    $\epsilon_{V}$ fixed (left) and $\epsilon_{V}$ floated (right), where $\epsilon_{V}:= \frac{BR(X \to \nu\bar{\nu})}{BR(X \to q\bar{q})}$. Different colored lines indicate different LLPs masses. Shaded area indicate statistical and systematic uncertainties combined.}
    \label{fig:8:sen_1D}
\end{figure}


\begin{figure}[!htbp]
    \subfloat[\( M_X = 1 \)~GeV/c$^2$]{
      \includegraphics[width=0.33\textwidth]{figures/8/fit_2d_1}
    }
    \subfloat[\( M_X = 10 \)~GeV/c$^2$]{
        \includegraphics[width=0.33\textwidth]{figures/8/fit_2d_10}
    }
    \subfloat[\( M_X = 50 \)~GeV/c$^2$]{
        \includegraphics[width=0.33\textwidth]{figures/8/fit_2d_50}
    }
    \caption{The 95\% C.L. 2-D upper limit on ($\mathcal{B}_{\textrm{2-jet}}$, $\mathcal{B}_{\textrm{4-jet}}$) for the process $e^+e^- \to Z h$ for three LLPs masses 50~GeV (left), 10~GeV (middle), 1~GeV (right). Different colored lines indicate different LLPs lifetimes. The uncertainties on the limits are omitted and a few limits are scaled by a factor for better visibility.}
    \label{fig:8:limit_2d}
\end{figure}

\begin{figure}[h]
    \includegraphics[width=0.24\linewidth]{figures/8/fit_2d/fit_2d_50_0001.pdf}
    \includegraphics[width=0.24\linewidth]{figures/8/fit_2d/fit_2d_50_01.pdf}
    \includegraphics[width=0.24\linewidth]{figures/8/fit_2d/fit_2d_50_1.pdf}
    \includegraphics[width=0.24\linewidth]{figures/8/fit_2d/fit_2d_50_10.pdf}\\
    %\hfill
    \includegraphics[width=0.24\linewidth]{figures/8/fit_2d/fit_2d_10_0001.pdf}
    \includegraphics[width=0.24\linewidth]{figures/8/fit_2d/fit_2d_10_01.pdf}
    \includegraphics[width=0.24\linewidth]{figures/8/fit_2d/fit_2d_10_1.pdf}
    \includegraphics[width=0.24\linewidth]{figures/8/fit_2d/fit_2d_10_10.pdf}\\
    %\hfill
    \includegraphics[width=0.24\linewidth]{figures/8/fit_2d/fit_2d_1_0001.pdf}
    \includegraphics[width=0.24\linewidth]{figures/8/fit_2d/fit_2d_1_01.pdf}
    \includegraphics[width=0.24\linewidth]{figures/8/fit_2d/fit_2d_1_1.pdf}
    \includegraphics[width=0.24\linewidth]{figures/8/fit_2d/fit_2d_1_10.pdf}
    \caption{The 2D fitting for ($\mathcal{B}_{\textrm{2-jet}}$, $\mathcal{B}_{\textrm{4-jet}}$). From top to bottom, the mass is 50, 10, 1 $GeV/c^2$; From left to right, the lifetime is 0.0001, 0.1, 1.0, 10 ns.}
    \label{fig:8:limit_2d_all}
  \end{figure}

\FloatBarrier

