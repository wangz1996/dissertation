\subsection{Comparison with Hadron Colliders: ATLAS and CMS}

We evaluate the effectiveness of our ML-based approach for LLPs detection at future lepton colliders by comparing our results with results at hadron colliders with the ATLAS experiment~\cite{PhysRevD.106.032005} and CMS experiment~\cite{CMS-PAS-EXO-21-008} as well as future HL-LHC experiments~\cite{HL-LHC}. The comparison is based on four primary metrics: signal acceptance, selection efficiency, analysis strategy and signal yields:
\begin{itemize}
    \item \textbf{Signal Acceptance:}
   Both ATLAS and CMS results have limited signal acceptance, typically a few percent, as they focus on LLPs decaying in the muon detector. In contrast, our ML-based approach covers the entire detector, resulting in 100\% signal acceptance except for LLPs with long lifetime ($> 1$~ns) and low mass ($< 1$~GeV).
    
    \item \textbf{Selection Efficiency:}
    For hadron colliders, LLPs events typically trigger on displaced decays and/or large missing transverse energy. The LLPs trigger efficiency at the ATLAS experiment is estimated to be between \(10^{-3}\) and  \(0.3\)~\cite{PhysRevD.106.032005}. Besides trigger efficiency, there are additional efficiencies involved such as displaced vertex/object reconstruction efficiencies which are typically in the order of a few percent. In contrast, LLPs event selection at lepton colliders can adapt to a triggerless approach owing to the clean environment. ML-based approach can be applied directly with low-level detector information without any event-level reconstruction. As a result, our ML-based approach with lepton colliders can achieve an overall selection efficiency as high as \(99\%\), an improvement of several orders in magnitude when comparing with LHC or HL-LHC efficiencies.

    \item \textbf{Analysis Strategy:}
    Traditionally, analyses of LLPs conducted elsewhere have employed a selection-based method, which involves categorizing events into multiple subsets with different decay modes and orthogonal signal types. These analyses necessitate manual re-tuning and re-optimization for each subset and different LLPs mass and lifetime configurations. In contrast, our ML-based approach eliminates the need for manual categorization and optimization. Deep neural networks can be retrained automatically for each LLPs mass and lifetime, resulting in higher efficiencies compared to the selection-based method. 
    
    \item \textbf{Signal Yields and Upper Limits Comparison:}
    LHC and HL-LHC can produce a significantly larger number of Higgs bosons compared to lepton colliders. Despite this, higher signal acceptance and selection efficiencies in our ML-based approach compensate for the relatively low number of Higgs bosons. We achieve upper limits as low as \(4 \times 10^{-6}\) on \( \mathcal{B}(H \rightarrow \text{LLPs}) \) with \(1 \times 10^6\) Higgs bosons. This upper limit is approximately three orders of magnitude better than the \(10^{-3}\) limit observed at the ATLAS and CMS experiments with \(10^7\) Higgs bosons and it is comparable to the projected HL-LHC limit with about \(10^8\) expected Higgs bosons.
   
     
\end{itemize}

% Comparison with ILC
Besides comparing with hadron colliders, we have also compared our result with a preliminary study~\cite{jeanty2022sensitivity} on the ILC sensitivity~\cite{ILC:2019gyn} with a traditional selection-based method. The ILC sensitivity study searches for long-lived dark photons produced in Higgstrahlung events via the Higgs portal. We have compared our result with the hadronic decay dark photo result since the event signature is similar. We have seen that the signal acceptance factors are similar between ILC and CEPC detectors but the signal efficiencies differ significantly. The signal efficiencies in the ILC study range from \(0.1\%\) to \(10\%\), which is at least an order of magnitude lower than ours. The upper limits on \( \mathcal{B}(H \rightarrow \text{LLPs}) \) in the ILC study are derived under the assumption of \(100\%\) truth-level signal efficiency. Only after making this assumption, two results show similar sensitivities in the low lifetime region (\(< 1~ns\)) of LLPs. In the long lifetime region, for example, for a 1 GeV LLP with a lifetime of a few nanoseconds, our result yields an upper limit of about \(10^{-5}\) which is an order of magnitude better than the ILC's upper limit of \(10^{-4}\). 


In summary, we have developed a ML-based approach for LLP searches that outperforms traditional selection-based methods on almost all fronts. 
Moreover, our ML-based approach can be easily applied to other future lepton collider experiments, including the ILC, FCC~\cite{FCC:2018byv}, and CLIC~\cite{CLIC:2018fvx}.




