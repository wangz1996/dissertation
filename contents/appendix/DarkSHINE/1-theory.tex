Although the dark photon does not engage in direct coupling with particles in the Standard Model (SM), it can manifest a minimal coupling to the electromagnetic current owing to the kinetic mixing of the SM hypercharge and the \( A' \) field strength tensor. The Lagrangian corresponding to this scenario is articulated as
\[
L = L_{\text{SM}} + \varepsilon F^{\mu \nu} F'_{\mu \nu} + \frac{1}{4} F'^{\mu \nu} F'_{\mu \nu} + m_{A'}^2 A'^\mu A'_\mu,
\]
where \( m_{A'} \) denotes the dark photon mass, \( A'_{\mu} \) signifies the dark photon field, and \( F'_{\mu \nu} \) is its associated field strength tensor. The kinetic mixing parameter \( \varepsilon \) varies from \( 10^{-8} \) to \( 10^{-2} \) depending on the mass point~\cite{Graham:2021ggy}.

Dark photons can be produced in laboratory conditions and can either decay into detectable SM end states or elusive dark sector final states, contingent on whether \( m_{A'} > 2m_{\chi} \). The minimal dark photon model incorporates merely three undetermined parameters: \( \varepsilon \), \( m_{A'} \), and the decay branching ratio to the dark sector, which is conventionally taken as either zero or unity. This investigation narrows its focus to the parameter space defined by \( [m_{A'}, \varepsilon] \), presuming a 100\% decay branching ratio of dark photons to dark matter.

Four primary mechanisms of dark photon production are highlighted, with three possible decay modes, as illustrated in figure~\ref{fig:DarkPhotonProduction} (left): bremsstrahlung in fixed-target experiments (\( eZ \rightarrow eZA' \) and \( pZ \rightarrow pZA' \)), annihilation in \( e^+ e^- \) colliders (\( e^+ e^- \rightarrow A'\gamma \)), decays of mesons (\( \pi^0 \rightarrow A'\gamma \) or \( \eta \rightarrow A'\gamma \)), and the Drell--Yan process (\( q\bar{q} \rightarrow A' \))~\cite{Andreas:2012mt}. 

The cross-section for bremsstrahlung production is governed by 
\[
\frac{d\sigma}{dx_e} = 4\alpha^3 \varepsilon^2 \xi \sqrt{1-\frac{m_{A'}^2}{E_e^2}} \frac{1-x_e + \frac{x_e^2}{3}}{m_{A'}^2 \frac{1-x_e}{x_e} + m_e^2 x_e},
\]
where \( x_e = \frac{E_{A'}}{E_e} \) represents the energy fraction of the incoming electron carried by the dark photon. The effective photon flux \( \xi \) is 
\[
\xi(E_e, m_{A'}, Z, A) = \int^{t_{\text{max}}}_{t_{\text{min}}} dt \frac{t-t_{\text{min}}}{t^2} G_{2}(t),
\]
where \( t_{\text{min}} \) and \( t_{\text{max}} \) are defined in the text~\cite{PhysRevD.80.075018}.

If the dark photon decays into dark matter (\( A' \rightarrow \chi \bar{\chi} \)) are kinematically permissible, i.e., \( m_{A'} > 2m_{\chi} \), the partial widths for these decays are given by
\[
\Gamma(A' \rightarrow \chi \bar{\chi}) = \frac{1}{3}\alpha_D m_{A'} \sqrt{1-\frac{4m_{\chi}^2}{m_{A'}^2}} \left(1 + \frac{2m_{\chi}^2}{m_{A'}^2}\right).
\]
For an incident electron with 8 GeV energy and assuming \( \varepsilon = 1 \), Figure~\ref{fig:cross-section} delineates the cross-section as a function of \( m_{A'} \).


\begin{figure}[!htb]
    \centering
    \includegraphics[width=0.49\textwidth]{figures/app/Figure-1.pdf}
    \includegraphics[width=0.49\textwidth]{figures/app/Figure-2.pdf}
    \caption{
        \textit{Left:} Production of dark photons: bremsstrahlung, annihilation, meson decay, and Drell--Yan.~\cite{Fabbrichesi_2021}
        \textit{Right:} Decay of the massive dark photon into visible (SM leptons or hadrons) and invisible (DM) modes.~\cite{Fabbrichesi_2021}
        }
    \label{fig:DarkPhotonProduction}
\end{figure} 
    
\begin{figure}[!htb]
        \centering
        \includegraphics[width=0.65\textwidth]{figures/app/Figure-3.pdf}
        \caption{Inclusive cross section of dark photon bremsstrahlung from electron interacting with tungsten target, which is produced by Calchep and is normalized to $\varepsilon=1$, with 8~GeV beam energy.}
        \label{fig:cross-section}
\end{figure}