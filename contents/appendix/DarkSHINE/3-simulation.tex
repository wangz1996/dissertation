This study employs simulations based on the baseline detector geometry delineated in Sec.~\ref{sec:DesignExperiment}. The incident electron energy is fixed at 8~GeV. Signal samples are generated parametrically as functions of the dark photon mass. Both inclusive backgrounds and key rare processes, such as photon--nuclear interactions, electron--nuclear interactions, and photon decays to muon pairs, are simulated to enhance the understanding and quantification of background events. Additional background contributions deemed negligible are explored in Sec.~\ref{sec:invisible_background}.

The \foods\ Software framework consolidates multiple functionalities, including detector simulation, digitization, event display, event reconstruction, and data analysis. It employs a proprietary data structure model for seamless data flow across various computational stages. GEANT4~v10.6.0~\cite{Geant4_1, Geant4_2} serves as the simulation engine, specifically tailored for the \foods\ detector.

\subsection{Signal Sample Production}

Dark photon signal events are generated using CalcHEP~v3.4~\cite{BELYAEV20131729}, while GEANT4~v10.6.0 is utilized for detector simulation. A total of 25 signal samples are produced, each comprising \(1\times10^5\) events, across a range of dark photon masses denoted as \(m_{A'}\), varying from 1~MeV to 2000~MeV.

\begin{figure}[!htb]
\centering
\subfloat[Electron recoil energy]{
  \includegraphics[width=0.47\textwidth]{figures/app/Figure-7-a.pdf}
}
\subfloat[Electron recoil angle]{
  \includegraphics[width=0.47\textwidth]{figures/app/Figure-7-b.pdf}
}\\
\subfloat[Electron recoil \(p_{T}\)]{
  \includegraphics[width=0.47\textwidth]{figures/app/Figure-7-c.pdf}
}
\subfloat[Transverse separation]{
  \includegraphics[width=0.47\textwidth]{figures/app/Figure-7-d.pdf}
}
\caption{Kinematic distributions of recoil electron variables, juxtaposing signal samples with an inclusive background sample.}
\label{fig:Signal_Kinetics}
\end{figure}

Figure~\ref{fig:Signal_Kinetics} exhibits the kinematic distributions of the signal and inclusive background. Signal events primarily transfer their momentum to the dark photon, leading to a recoil electron with typically less than a quarter of the initial momentum. Conversely, in background events, the recoil electron retains most of the incident momentum. Hence, imposing a cut on the recoil electron momentum proves pivotal for signal region delineation. For increasing dark photon masses in signal events, the recoil electron angle and transverse momentum, on average, manifest higher values, in contrast to their smaller magnitudes in background events. The transverse momentum in the background concentrates around 100~MeV due to the magnetic field's 1.5~T strength. Upon inspection of the transverse separation in the signal events, higher dark photon mass tends to yield greater average distances.



\subsection{Background Sample Production}

Figure~\ref{fig:flowofbackroundprocess} illustrates the hierarchy of major background processes, along with their corresponding relative rates. Predominantly, incident electrons traverse the target without undergoing any interaction. However, approximately 6.7\% of these electrons emit hard bremsstrahlung photons, leading to a final state comprising a recoil electron and a photon. Subsequently, these bremsstrahlung photons can engage in photon--nuclear interactions with the materials of \fooecal\ and the target, occurring with relative rates of \(2.31 \times 10^{-4}\) and \(1.37 \times 10^{-6}\) with respect to the inclusive rate, respectively. Furthermore, the bremsstrahlung photons can undergo conversions, resulting in muon pairs at \fooecal\ and the target, with relative rates of \(1.63 \times 10^{-6}\) and \(1.50 \times 10^{-8}\), respectively. Although bremsstrahlung photons can also yield electron pairs at a substantially high rate, these electrons are readily identifiable and reconstructable by the tracking system and \fooecal. Apart from the aforementioned processes instigated by hard bremsstrahlung photons, electron--nuclear interactions involving the materials of \fooecal\ and the target also contribute significantly to the background, with relative rates of \(3.25 \times 10^{-6}\) and \(5.10 \times 10^{-7}\).

\begin{figure}[!htb]
\centering
\includegraphics[width=0.85\textwidth]{figures/app/Figure-8.pdf}
\caption{Schematic depiction of the principal background processes and their associated relative rates. These rates serve as branching ratios and are tabulated in Table~\ref{tab:background-production-summary}.}
\label{fig:flowofbackroundprocess}
\end{figure}

An inclusive background sample comprising \(2.5 \times 10^{9}\) events was generated for this study. In addition, six rare processes were also simulated, the statistics of which, along with the corresponding \fooeot, are detailed in Table~\ref{tab:background-production-summary}.

\begin{table}[!htb]
    \centering
    \resizebox{0.75\columnwidth}{!}{
        \doublerulesep 0.1pt \tabcolsep 5pt %space between two columns.
        \begin{tabular}{cccc}
            \toprule
                \centering
                    Process & Generate Events & Branching Ratio & \fooeot\ \\ 
                    \midrule
                    Inclusive & $2.5\times10^9$ & 1.0 & $2.5\times10^9$ \\ \midrule
                    Bremsstrahlung & $1\times10^7$ & $6.70\times 10^{-2}$ & $1.5\times10^8$ \\ \midrule
                    \foogmmtarget\ & $1\times10^7$ & $1.5(\pm0.5)\times 10^{-8}$ & $4.3\times10^{14}$ \\ \midrule
                    \foogmmecal\ & $1\times10^7$ & $1.63(\pm0.06)\times 10^{-6}$ & $6.0\times10^{12}$ \\ \midrule
                    \foopntarget\ & $1\times10^7$ & $1.37(\pm0.05)\times 10^{-6}$ & $4.0\times10^{12}$ \\ \midrule
                    \foopnecal\ & $1\times10^8$ & $2.31(\pm0.01)\times 10^{-4}$ & $4.4\times10^{11}$ \\ \midrule
                    \fooentarget\ & $1\times10^8$ & $5.1(\pm0.3)\times 10^{-7}$ & $1.6\times10^{12}$ \\ \midrule
                    \fooenecal\ & $1\times10^7$ & $3.25(\pm0.08)\times 10^{-6}$ & $1.8\times10^{12}$ \\
            \bottomrule
        \end{tabular}
    }
    \caption{Summary of background sample production, specifying branching ratios and corresponding \fooeot\ for each rare process. The branching ratios are derived from the inclusive sample, conditioned on the energy of the electron being greater than 4~GeV.}
    \label{tab:background-production-summary}
\end{table}
