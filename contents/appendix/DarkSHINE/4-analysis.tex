Informed by the distribution of background processes as illustrated in figure~\ref{fig:flowofbackroundprocess}, we construct a set of event selection criteria, leveraging the following detector variables:

\begin{itemize}
    \item
    Count of reconstructed tracks in the tagging tracker, denoted as \(N_{\text{trk}}^{\text{tag,rec}} = 1\);
    \item
    Disparity in electron momentum, represented as \(p_{\text{tag}} - p_{\text{rec}} > 4~\text{GeV}\);
    \item
    Cumulative energy measured in \fooecal, quantified by \(E_{\fooecal}^{\text{total}} < 2.5~\text{GeV}\);
    \item
    Aggregate energy observed in \foohcal, not exceeding \(E_{\foohcal}^{\text{total}} < 0.1~\text{GeV}\);
    \item
    Peak energy registered in an individual \foohcal cell, confined to \(E_{\foohcal}^{\text{MaxCell}} < 2~\text{MeV}\).
\end{itemize}

Subsequent analysis confirms that these thresholds are optimized to effectively mitigate background events while retaining high signal sensitivity.

Focusing on tracking data, it is observed that dark photon signal events typically manifest as a solitary reconstructed track in both the tagging and recoil trackers. The momentum difference between these two tracks, depicted in the top-left quadrant of figure~\ref{fig:ECALAndHCALEnergy}, clearly segregates signal from background. Specifically, in the signal case, the 'missing momentum'—attributed to the dark photon—tends to be higher, whereas it decays rapidly in background scenarios dominated by hard bremsstrahlung events. To exclude such background contamination effectively, a preselection criterion on missing momentum, \(p_{\text{tag}} - p_{\text{rec}} > 4~\text{GeV}\), is instituted.


\begin{figure}[!htb]
    \centering
    \subfloat[]{
      \includegraphics[width=0.47\textwidth]{figures/app/Figure-9-a.pdf}
    }
    \subfloat[]{
      \includegraphics[width=0.47\textwidth]{figures/app/Figure-9-b.pdf}
    } 
    \\
    \subfloat[]{
      \includegraphics[width=0.47\textwidth]{figures/app/Figure-9-c.pdf}
    }
    \subfloat[]{
      \includegraphics[width=0.47\textwidth]{figures/app/Figure-9-d.pdf}
    }
    \caption{Distributions of key variables for different signal and background samples.}
    \label{fig:ECALAndHCALEnergy}
\end{figure}
    
Pivoting to the calorimeter system, Figure~\ref{fig:ECALAndHCALEnergy} contains several significant plots. Subfigure (a) shows the difference in momentum between the tagging and recoil electrons ($P_{\text{tag}} - P_{\text{rec}}$) for the inclusive background, as well as for signal samples of 1~MeV, 10~MeV, 100~MeV, and 1000~MeV. Subfigures (b, c, d) display the two-dimensional distribution of total energy in \fooecal\ and \foohcal\ for the inclusive background and the 10~MeV and 1000~MeV signal samples, respectively.
    
In the case where a dark photon decays into a Dark Matter (DM) pair, the decay products would evade detection, leaving no energy trace in \fooecal\ and \foohcal. Conversely, background processes generally register substantial energy deposits in these calorimeters, especially hadronic backgrounds which are predominantly absorbed in \foohcal. Hence, judicious selection criteria based on the total and maximum cell energy in \foohcal\ can effectively filter out such backgrounds while maintaining high signal fidelity.


To enhance the analytical sensitivity, cuts on \foohcal\ energy are rigorously optimized. In typical dark photon signal scenarios, no energy deposition in \foohcal\ is expected, thereby necessitating stringent \foohcal\ veto criteria to suppress specific backgrounds, including $\gamma\to\mu\mu$ events and hadronic final states. The acceptance efficiency, as a function of \foohcal\ energy, is depicted in Figure~\ref{fig:SignalAccEfficiency_HCAL} for a simulated 10~MeV signal sample. Here, the total \foohcal\ energy cut varies from 5~MeV to 100~MeV, and the maximum cell energy in \foohcal\ varies from 1~MeV to 20~MeV. Within this energy range, the efficiency remains relatively invariant. Specifically, the 10~MeV signal sample experiences a signal efficiency increase from $61\%$ to $63\%$ when the cut combination of ($E_{\foohcal}^{total}$, $E_{\foohcal}^{MaxCell}$) is relaxed from (5~MeV, 1~MeV) to (100~MeV, 10~MeV). Approximately a $1\text{--}2\%$ efficiency improvement is also observed for other signal samples. Consequently, the final cut values chosen for optimal background suppression are $E_{\foohcal}^{total} < 100$~MeV and $E_{\foohcal}^{MaxCell} < 2$~MeV.

\begin{figure}[!htb]
\centering
\includegraphics[width=0.85\textwidth]{figures/app/Figure-10.pdf}
\caption{Efficiency vs. \foohcal\ energy for the 10~MeV signal.}
\label{fig:SignalAccEfficiency_HCAL}
\end{figure}

As depicted in Figure~\ref{fig:SignalAccEfficiency}, the acceptance efficiencies for various simulated signal samples are plotted as a function of the dark photon mass, \(m_{A'}\). Overall, high signal efficiencies surpassing \(60\%\) are sustained across most of the \(m_{A'}\) range subsequent to the application of all selection criteria. However, the efficiencies witness a decline to approximately \(50\%\) when \(m_{A'}\) is in the range of a few MeV or above 1~GeV. For the lower \(m_{A'}\) values, the prevailing \fooecal\ and \foohcal\ energy cuts might be excessively stringent, thereby suggesting the need for tailored optimization. Additionally, events with large incident or recoil angles are likely to bypass the \fooecal\ and directly impact the \foohcal\ due to specific geometric configurations at the simulation level. This phenomenon becomes increasingly prevalent for larger \(m_{A'}\) values and results in their rejection by the \foohcal\ energy cuts, thereby leading to diminished selection efficiencies.

To summarize, a comprehensive overview of the background cut flow and corresponding selection efficiencies are documented in Tables~\ref{tab:cutflow1} and~\ref{tab:cutflow2}, respectively.

\begin{figure}[!htb]
\centering
\includegraphics[width=0.85\textwidth]{figures/app/Figure-11.pdf}
\caption{Dark photon signal efficiencies across \(m_{A'}\).}
\label{fig:SignalAccEfficiency}
\end{figure}


\begin{table}[!htb]
    \centering
    \resizebox{0.99\columnwidth}{!}{
        \tabcolsep 4pt %space between two columns.
        \begin{tabular}{cccccccccc}
        \toprule
            & \fooenecal            & \foopnecal           & \foogmmecal           & \fooentarget          & \foopntarget          & \foogmmtarget         & hard\_brem           & inclusive           \\ \midrule
            total events                    & $2.48\times 10^{7}$ & $1.66\times 10^{8}$ & $1.74\times 10^{7}$ & $1.09\times 10^{8}$ & $1.05\times 10^{7}$ & $1.05\times 10^{7}$ & $1.02\times 10^{7}$ & $2.50\times 10^{9}$ \\ \midrule 
            only 1 track                    & $1.46\times 10^{7}$ & $1.17\times 10^{8}$ & $1.52\times 10^{7}$ & $6.38\times 10^{6}$ & $6.17\times 10^{5}$ & 77                  & $8.03\times 10^{6}$ & $2.11\times 10^{9}$  \\ \midrule
            $p_{tag}-p_{rec}>4$~GeV & 1091                & 5531                & 707                 & $6.08\times 10^{6}$ & $5.73\times 10^{5}$ & 1                   & $7.19\times 10^{6}$ & $1.20\times 10^{8}$ \\ \midrule
            $E_{\foohcal}^{total}<100$~MeV      & 135                 & 1348                & 0                   & 322135               & 75501               & 0                   & $1.19\times 10^{8}$ & $2.89\times 10^{7}$ \\ \midrule
            $E_{\foohcal}^{MaxCell}<10$~MeV     & 56                  & 676                 & 0                   & 141808               & 27949               & 0                   & $1.12\times 10^{8}$ & $2.72\times 10^{7}$ \\ \midrule
            $E_{\foohcal}^{MaxCell}<2$~MeV      & 30                  & 363                 & 0                   & 63644                & 9999                & 0                   & $1.01\times 10^{8}$ & $2.46\times 10^{7}$ \\ \midrule
            $E_{\fooecal}^{total}<2.5$~GeV      & 0                   & 0                   & 0                   & 0                   & 0                   & 0                   & 0                   & 0                   \\
        \bottomrule
        \end{tabular}
    }
    \caption{Event cut flow for each background sample in Table~\ref{tab:background-production-summary}. The number of events remaining after each cut is listed in the table.}
    \label{tab:cutflow1}
\end{table}


\begin{table}[!htb]
    \centering
    \resizebox{0.99\columnwidth}{!}{
        \tabcolsep 4pt %space between two columns.
        \begin{tabular}{cccccccccc}
        \toprule
            & \fooenecal            & \foopnecal           & \foogmmecal           & \fooentarget          & \foopntarget          & \foogmmtarget         & hard\_brem           & inclusive           \\ \midrule
            total events                    & $100\%$    & $100\%$    & $100\%$    & $100\%$    & $100\%$    & $100\%$      & $100\%$   & $100\%$   \\ \midrule 
            only 1 track                    & $58.87\%$  & $70.48\%$  & $87.36\%$  & $5.85\%$   & $5.88\%$   & $<10^{-3}\%$ & $78.73\%$ & $84.40\%$  \\ \midrule
            $p_{tag}-p_{rec}>4$~GeV             & $0.0044\%$ & $0.0033\%$ & $0.0041\%$ & $5.58\%$   & $5.46\%$   & $<10^{-5}\%$ & $70.49\%$ & $4.80\%$  \\ \midrule
            $E_{\foohcal}^{total}<100$~MeV      & $<10^{-3}\%$ & $<10^{-3}\%$ & $0\%$  & $0.30\%$   & $0.72\%$   & $0\%$        & $69.61\%$ & $4.76\%$  \\ \midrule
            $E_{\foohcal}^{MaxCell}<10$~MeV     & $<10^{-3}\%$ & $<10^{-3}\%$ & $0\%$  & $0.13\%$   & $0.27\%$   & $0\%$        & $65.00\%$ & $4.48\%$ \\ \midrule
            $E_{\foohcal}^{MaxCell}<2$~MeV      & $<10^{-3}\%$ & $<10^{-3}\%$ & $0\%$  & $0.058\%$   & $0.095\%$ & $0\%$        & $58.14\%$ & $4.04\%$ \\ \midrule
            $E_{\fooecal}^{total}<2.5$~GeV      & $0\%$        & $0\%$        & $0\%$  & $0\%$       & $0\%$     & $0\%$        & $0\%$     & $0\%$ \\ \midrule
        \bottomrule
        \end{tabular}
    }
    \caption{Event cut flow for each background sample in Table~\ref{tab:background-production-summary}. The selection efficiencies of each cut are listed in the table.}
    \label{tab:cutflow2}
\end{table}
