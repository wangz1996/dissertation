As evidenced by the background cut flow tables (Tables~\ref{tab:cutflow1} and~\ref{tab:cutflow2}), none of the \(2.5 \times 10^9\) simulated inclusive background events survive the selection criteria, mirroring the outcome for each of the rare background processes. To project the background yields for a one-year operation of the \foods\ experiment, corresponding to \(3 \times 10^{14}~\fooeot\), an initial investigation into the background rejection efficiency within side-band regions is mandated, subsequently facilitating extrapolations into the signal region.

To this end, rare background samples of substantial statistical heft, ranging from \(10^{11}\) to \(10^{12}\)~\fooeot as per Table~\ref{tab:background-production-summary}, are deployed for the extraction of anticipated background yields via the fitting of event ratios at prespecified ECAL energy thresholds. For verification purposes, this extrapolation strategy is reciprocally applied to both the inclusive background sample and a series of low-beam energy samples.

Section~\ref{sec:rare-processes-extrapolation} elaborates on the extrapolation methodology specific to rare background processes. Section~\ref{sec:validationbkg} furnishes details concerning the validation via inclusive background simulation. Further, Sec.~\ref{sec:invisible_background} deliberates on rare backgrounds featuring neutrinos in their final states. A summarial discourse on background estimation is presented in Sec.~\ref{sec:bkgsummary}.


\subsection{Extrapolation from rare processes simulation}
\label{sec:rare-processes-extrapolation}

Figure~\ref{fig:stacking_plot} depicts the event ratios as a function of energy cut values on \fooecal\ for cases with only one tagged track and one recoil track. The data points for six rare background processes (\fooenecal, \fooentarget, \foopnecal, \foopntarget, \foogmmecal, and \foogmmtarget) are exhibited, each scaled in accordance with its corresponding branching ratio. In terms of event ratios, the individually generated samples for each of these rare processes align well with their inclusive background counterparts. The composite of these six processes, represented by gray circles, is derived from the rare background samples, whereas the gray dots are obtained from the inclusive background samples. Notably, concordance between these summed fractions is observed, particularly in regions of high energy where statistical robustness is sufficient. A nominal \fooecal\ energy cut at \(2.5\)~GeV is denoted by a blue dashed line.

\begin{figure}[!htb]
\centering
\includegraphics[width=0.85\textwidth]{figures/app/Figure-12.pdf}
\caption{Event ratio as a function of \fooecal\ energy cut.}
\label{fig:stacking_plot}
\end{figure}


The expected background yield at a given \fooecal\ energy cut is computed from the corresponding event ratio. For instance, with an event ratio below \(10^{-14}\) at an energy cut \(x\), it can be inferred that fewer than one background event will remain in a sample of \(10^{14}\) events when imposing the condition \(E_{\fooecal}^{total} < x\)~MeV. To derive the comprehensive background yield within the signal region, the full set of selections delineated in Sec.~\ref{sec:SignalRegion} must be applied. However, the limited statistics preclude a straightforward extrapolation. The efficacy of \foohcal\ energy cuts results in the depletion of statistics for most processes displayed in Figure~\ref{fig:stacking_plot}, particularly below the nominal \fooecal\ energy cut of \(2.5\)~GeV. Therefore, both high-statistics rare background samples and an extrapolation methodology are employed to extend event ratio trends into low-energy regimes. This approach facilitates the evaluation of background yields for each of the six rare processes, the sum of which serves as the final background yield after validation.

GMM processes are comparatively easier to eliminate. Given the present statistical breadth, no events from \foogmmecal\ (\(6\times10^{12}\)~\fooeot) or \foogmmtarget\ (\(4.3\times10^{14}\)~\fooeot) survive the signal region selection per Table~\ref{tab:cutflow1}. As shown in Figure~\ref{fig:stacking_plot}, the event ratio for the GMM channel is largely unaffected by \fooecal\ energy cuts. Most of the energy is deposited in the \foohcal\ due to the muon pair. Application of \foohcal\ energy cuts results in a residual GMM event fraction less than \(10^{-6}\), validating its exclusion from this analysis.

Figure~\ref{fig:RareProcessesExtrapolation} illustrates background extrapolation plots, showing the fraction of events below certain energy cutoffs for four key processes: EN\_\fooecal, EN\_target, PN\_\fooecal, and PN\_target. These fractions are scaled according to their respective branching ratios. The function \(e^{alog(x)+b}\) is fitted to these fractions, where \(x\) is the \fooecal\ energy cut value, and \(a\) and \(b\) are free parameters.

\begin{figure}[!htb]
\centering
\includegraphics[width=0.47\textwidth]{figures/app/Figure-13-a.pdf}
\includegraphics[width=0.47\textwidth]{figures/app/Figure-13-b.pdf}
\includegraphics[width=0.47\textwidth]{figures/app/Figure-13-c.pdf}
\includegraphics[width=0.47\textwidth]{figures/app/Figure-13-d.pdf}
\caption{Background extrapolation plots for four key processes.}
\label{fig:RareProcessesExtrapolation}
\end{figure}


\begin{table}[!htb]
    \centering
    \resizebox{0.65\columnwidth}{!}{
        \doublerulesep 0.1pt \tabcolsep 2pt %space between two columns.
        \begin{tabular}{cccccc}
        \toprule
            Rare process      & EN\_\fooecal & EN\_target &   PN\_\fooecal         &    PN\_target        & GMM \\ \midrule
            Estimated yield   & 0.0016   &   0.013    & $5.93\times10^{-5}$ & $2.53\times10^{-7}$ &     0      \\
        \bottomrule
        \end{tabular}
    }
    \caption{Expected number of each background process, estimated from the extrapolation method. The background yields listed in the table correspond to $3\times10^{14}$~\fooeot.}
    \label{tab:rareproceeyields}
\end{table}

To obtain the background yield at \(E_{\fooecal}^{total} = 2.5\)~GeV for the remaining four processes, an exponential-logarithmic function is employed to fit the event ratio while maintaining all other signal region cuts. It should be highlighted that each background channel exhibits distinct cut efficiencies, necessitating individualized functional forms and parameter tuning. Figure~\ref{fig:RareProcessesExtrapolation} displays the fit extrapolations for the \fooenecal, \fooentarget, \foopnecal, and \foopntarget\ channels. All the criteria defining the signal region, with the exception of the \fooecal\ energy cut, are applied. The fractions for each channel are scaled in accordance with their respective branching ratios. The blue solid line in the plots represents the fitted function, which satisfactorily captures the form of the event ratio within the fit range delineated by the orange dashed lines. The estimated background yields for these channels are summarized in Table~\ref{tab:rareproceeyields}. The aggregated background yield for these rare processes is calculated to be approximately 0.015 upon summing their extrapolated yields.


\subsection{Validation from inclusive background simulation}
\label{sec:validationbkg}

The baseline background yield is primarily derived from the rare processes; however, for cross-validation, an inclusive background sample is also analyzed, as depicted in Figure~\ref{fig:InclusiveExtrapolation}. Here, the event ratio curve is fitted using an exponential-quadratic function of the form \(e^{ax^{2} + bx + c}\), resulting in an expected background yield of \(2.53\times10^{-3}\) when all signal region criteria are applied. The low statistics in the low-energy region prompt further validation efforts. 

\begin{figure}[!htb]
    \centering
    \includegraphics[width=0.85\textwidth]{figures/app/Figure-14.pdf}
    \caption{Background Extrapolation from Inclusive Sample}
    \label{fig:InclusiveExtrapolation}
\end{figure}

Additional inclusive background samples, each with \(1\times 10^{7}\) events, are generated at discrete electron beam energies ranging from 3 to 7.5~GeV. The event selection criteria are adapted to suit these lower-energy beams, with the missing momentum requirement now set to exceed half of the beam energy. The energy cuts in the \foohcal\ detector are temporarily relaxed to \(E_{\foohcal}^{total} < 100\)~MeV and \(E_{\foohcal}^{MaxCell} < 20\)~MeV to preserve adequate statistics. The event fractions from these samples are scaled and aligned to corroborate with the 8~GeV sample, forming the basis for a direct extrapolation method shown in Figure~\ref{fig:InclusiveExtrapolation validation}.

\begin{figure}[!htb]
    \centering
    \includegraphics[width=0.85\textwidth]{figures/app/Figure-15.pdf}
    \caption{Background Extrapolation Validation}
    \label{fig:InclusiveExtrapolation validation}
\end{figure}

To obtain background yields conforming to the signal region conditions, a scale factor is established between the fit outcomes for different \foohcal\ maximum cell energy cuts. Figure~\ref{fig:fit_inclusive_8GeV_ECALE} illustrates the fitting procedure for the more relaxed \foohcal\ cut of \(E_{\foohcal}^{MaxCell} < 20\)~MeV. The scaling process yields an extrapolated background estimate of \(9.23\times10^{-3}\) for \(3\times10^{14}\) \fooeot, corroborating the results of the original fit extrapolation method.

\begin{figure}[!htb]
    \centering
    \includegraphics[width=0.85\textwidth]{figures/app/Figure-16.pdf}
    \caption{Background Extrapolation with Relaxed \foohcal\ Cut}
    \label{fig:fit_inclusive_8GeV_ECALE}
\end{figure}



\subsection{Invisible background estimation}
\label{sec:invisible_background}

Neutrino backgrounds originating from specific production reactions constitute a critical source of irreducible backgrounds, indistinguishable from the signal processes. According to Section \MakeUppercase{\romannumeral4} of~\cite{PhysRevD.91.094026}, there are primarily two leading reactions to consider. The first encompasses Moller scattering followed by charged-current quasi-elastic (CCQE) reactions, mathematically denoted as \(e^- e^- \rightarrow e^- e^-\) and \(e^- p \rightarrow \nu_e n\). For an incident electron energy of 10~GeV with a W target, the Moller cross-section \(\sigma_\text{Moller}\) is roughly 0.4 b and \(\sigma_\text{CCQE}\) is 8 fb per nucleon. Given that \foods\ employs a 0.035-cm W target, this results in an estimated \(10^{-4}\) events per \(10^{14}\) \fooeot. The second category involves neutrino pair production, \(e^- N \rightarrow e^- N \nu \bar{\nu}\), with a cross-section of 0.03 fb, yielding \(6 \times 10^{-6}\) events per \(10^{14}\) \fooeot. Notably, \foods\ utilizes an 8 GeV incident electron energy, which implies that the above ratios serve as upper limits for this experiment.

\begin{table}[!htb]
\centering
\footnotesize
\doublerulesep 0.1pt \tabcolsep 4pt
\begin{tabular}{ccc}
\toprule
The irreducible reaction & Moller scattering    & neutrino pair production \\ \midrule
estimated yield      & \(3\times10^{-4}\)  &  \( < 1.8\times10^{-5}\)  \\ \midrule
The irreducible reaction & Bremsstrahlung \(\bigoplus\) CCQE & charge current exchange \\ \midrule
estimated yield  & 0.3 & 0.3 \\
\bottomrule
\end{tabular}
\caption{Summary of Invisible Backgrounds}
\label{tab:invisible_background}
\end{table}

In addition to these irreducible reactions, two other background scenarios are outlined in~\cite{PhysRevD.91.094026}: bremsstrahlung \(\bigoplus\) CCQE and charge current exchange with exclusive \(e^- p \rightarrow \nu n \pi_0\). Both yield approximately 0.1 event per \(10^{14}\) \fooeot in \foods\ but can be effectively disregarded due to their inability to pass the one-track criterion, which forms part of the signal region selection. 

Furthermore, during the analysis, a small number of events were observed that left no energy deposition in \fooecal. Specifically, 2 out of \(1 \times 10^7\) hard bremsstrahlung samples and 6 out of \(5 \times 10^8\) inclusive samples exhibited this characteristic. These events were traced back to photons produced via hard bremsstrahlung, which managed to traverse the gaps between \fooecal\ cells to deposit energy exclusively in \foohcal. The minimum energy deposition noted in \foohcal\ was \(527.7\)~MeV. Such events are effortlessly discarded through the application of \foohcal\ energy cuts.


\subsection{Background Estimation Summary}
\label{sec:bkgsummary}

To consolidate our understanding of the background processes, we summarize the expected numbers of background events in the sensitivity study. These numbers are derived from multiple avenues: event cut flow, background extrapolation methods, and the invisible background estimation. Table~\ref{tab:background_estimation} displays the yield for each method, corresponding to a total exposure of \(3 \times 10^{14}\)~\fooeot. It should be noted that the cumulative yield from the rare background processes is conservatively high but is in agreement with the validation conducted on the inclusive sample.

\begin{table}[!htb]
    \centering
    \footnotesize
    \doublerulesep 0.1pt \tabcolsep 4pt
    \begin{tabular}{cccccc}
        \toprule
        Method & Cut Flow & Rare Extrapolation & Inclusive Extrapolation & Inclusive Validation & Invisible Background\\ \midrule
        Yield  &    0     & \(1.5\times10^{-2}\) & \(2.53\times10^{-3}\) & \(9.23\times10^{-3}\) & Negligible \\
        \bottomrule
    \end{tabular}
    \caption{Summary of Background Yields}
    \label{tab:background_estimation}
\end{table}



