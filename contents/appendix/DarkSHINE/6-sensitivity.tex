The relationship between the kinetic mixing parameter \(\varepsilon\) and the expected signal yield \(N_{\text{sig}}\) is governed by the formula:
\[
N_{\text{sig}}=\sigma_{A'} \times 0.1 X_{0} \times L \times N_{A} / M_{W} \times 10^{-36} \times \varepsilon^{2},
\]
where \(\sigma_{A'}\) is the production cross section for a dark photon of mass \(m_{A'}\) (see Figure~\ref{fig:cross-section}), \(0.1X_{0}=0.676\)~\(g/cm^{2}\) denotes the thickness of the tungsten target, \(L=3\times10^{14}\)~\(\fooeot\) signifies the number of events, \(N_{A}\) is the Avogadro constant, and \(M_{W}=184\) represents the atomic mass of tungsten.

Given the low estimated background yield elaborated in Section~\ref{sec:BackgroundEstimates}, it is reasonable to model all observed events as background following a Poisson distribution. Therefore, the upper limit on signal times acceptance efficiency at \(90\%\)-CL is
\[
s_{\text{up}} \times \varepsilon_{\text{sig}} = \frac{1}{2}F_{\chi^{2}}^{-1}(1-\alpha;~2(n_{\text{obs}}+1))-b,
\]
where \(\varepsilon_{\text{sig}}\) is the acceptance efficiency, dependent on \(m_{A'}\) (see Figure~\ref{fig:SignalAccEfficiency}), and \(n_{\text{obs}} = b = 0.015\) is the constant background yield. This leads to upper limits on \(\varepsilon^{2}\):
\[
\varepsilon^{2}=s_{\text{up}} / \varepsilon_{\text{sig}} \times \sigma_{A'} \times 0.1 X_{0} \times L \times N_{A} / M_{W} \times 10^{-36}.
\]

The current constraints and sensitivity estimates on \(\varepsilon^{2}\) as a function of \(m_{A'}\) are displayed in Figure~\ref{fig:LimitsOnEpsilonVsMa}. This figure visualizes the expected upper limits on \(\varepsilon^{2}\) for different data collection periods: \(3\times 10^{14}\)~\(\fooeot\) (1 year), \(9\times 10^{14}\)~\(\fooeot\) (3 years), and \(1.5\times 10^{15}\)~\(\fooeot\) (5 years). Results from other experiments are also integrated for comparison.

\begin{figure}[!htb]
\centering
\includegraphics[width=0.85\textwidth]{figures/app/Figure-17.pdf}
\caption{Constraints on \(\varepsilon^{2}\) vs \(m_{A'}\)}
\label{fig:LimitsOnEpsilonVsMa}
\end{figure}

In our investigation, we focus on the projected sensitivity in the dimensionless interaction strength \(y = \varepsilon^2 \alpha_{D} (m_{\chi} / m_{A'})^4\) as a function of Dark Matter (DM) mass \(m_{\chi}\). We adopt the conventional assumptions that \(m_{A'} = 3m_{\chi}\) and \(\alpha_{D} = 0.5\). Figure~\ref{fig:LimitsOnModelsVsMx} illustrates this sensitivity, with the calculated projections for the data collected by the \foods\ experiment under various conditions of event numbers: \(3 \times 10^{14}\), \(9 \times 10^{14}\), \(1.5 \times 10^{15}\), and \(10^{16}\) EOTs. 

\begin{figure}[!htb]
  \centering
  \includegraphics[width=0.85\textwidth]{figures/app/Figure-18.pdf}
  \caption{Projected 90\% CL exclusion limits on \(y\) at different event numbers (\foods\ experiment).}
  \label{fig:LimitsOnModelsVsMx}
\end{figure}


The results indicate promising prospects for the \foods\ experiment. For example, with a data-taking duration of at least one year, the \foods\ experiment will likely commence probing the existence of thermal relic DM in the MeV mass range. Existing and anticipated constraints are also plotted for comparison~\cite{NA64,LSND1,LSND2,E137,BaBar,MiniBooNE,PhysRevLett.109.021301}.

Additionally, our results allow for direct comparison with the Linac to End-Station A (LESA) at the LDMX R\&D experiment~\cite{LDMX}. LESA at LCLS-II will provide a 4 GeV electron beam during its Phase 1, commencing in 2025, and will upgrade to an 8 GeV electron beam for Phase 2 in 2027. Our study indicates that the DarkSHINE experiment offers competitive or superior sensitivity, particularly at higher mass ranges owing to the higher incident electron energy and the utilization of LYSO crystal with superior energy resolution. By contrast, LDMX, with its Si-W sampling calorimeter, excels in spatial resolution and thus shows greater sensitivity in the high-mass region. Given the similar aims of these two experiments, they can serve as immediate cross-checks in the discovery phase space.
