\subsection{SHINE Facility}

In this investigation, a high-frequency electron beam is utilized, which is furnished by the under-construction Shanghai High Repetition-Rate XFEL and Extreme Light Facility (SHINE)~\cite{WAN2022166200,Zhao:2017ood,ZHAO2016720}. The SHINE facility is designed to generate a beam with an energy level of 8~GeV and a frequency of 1~MHz. Figure~\ref{fig:kicker} illustrates the configuration of the SHINE linac and the various kicker systems, including those designated for the Free Electron Lasers (FEL-I, FEL-II, FEL-III) and for the present experiment. Specifically, SHINE's microwave system, operating at 1.3~GHz, supplies 1.3 billion buckets per second. For every set of 1300 buckets, a 100~pC charge of electrons is loaded into a single bucket, which corresponds to a frequency of 1~MHz. This produces a bunch population of \(6.25 \times 10^{8}\) electrons, which is too high for efficient dark photon detection.

To remedy this, a specialized single-electron beamline is proposed to be incorporated into the existing SHINE linac. This additional beamline aims to make use of the empty buckets by injecting a single electron into each. Post-acceleration, these electrons are distributed by a dedicated kicker system. When considering the response time of the kicker and the detection systems, an effective rate of 10~MHz is achieved. This configuration is projected to yield around \(3 \times 10^{14}\) electron-on-target (EOT) events during a single year of commissioning for the \foods experiment.

\begin{figure}[!htb]
\centering
\includegraphics[width=0.95\textwidth]{figures/app/Figure-4.pdf}
\caption{Schematic representation of the beamline and kicker systems.}
\label{fig:kicker}
\end{figure}


\subsection{Detector Design}

The detector for the \foods\ experiment is composed of three primary sub-detector systems: a silicon tracker, an electromagnetic calorimeter (denoted as \fooecal), and a hadronic calorimeter (referred to as \foohcal). Figure~\ref{fig:detectorsketch} provides a schematic representation of these systems in spatial relation to each other. Along the direction of the incident electron from left to right, one first encounters a dipole magnet, depicted in red with a blue brace in the figure. The magnet operates at a field strength of 1.5~T. Situated at the center of this magnet is the tagging tracker, which is designed to identify and tag particles. Adjacent to the edge of the magnet lies the recoil tracker. The experimental target is strategically positioned in the middle of these tracking systems. Following the recoil tracker, the \fooecal\ is placed, and it is subsequently succeeded by the \foohcal. The parameters based on the geometry of these sub-detectors are summarized in Table~\ref{tab:DetectorGeometry}, which serves as the baseline setup for this study.

\begin{figure}[!htb]
    \centering
    \includegraphics[width=0.75\textwidth]{figures/app/Figure-5.pdf}
    \caption{Schematic layout of the detector and its components.}
    \label{fig:detectorsketch}
\end{figure}

\begin{table}[!htb]
    \centering
    \resizebox{0.99\columnwidth}{!}{
        \tabcolsep 10pt
        \begin{tabular}{cccccccc}
            \toprule  
            \centering
            \multirow{2}{*}{Node}   & \multicolumn{1}{r}{Centre (mm)} & \multicolumn{3}{c}{Size (mm)} & Arrangement & Comments \\
            \cline{2-2}
            \cline{3-5}
                & \multicolumn{1}{c}{z} & \multicolumn{1}{c}{x} & \multicolumn{1}{c}{y} & \multicolumn{1}{c}{z} & & \\ 
            \midrule
                Tagging tracker & -307.783 & 200   & 400   & 600.216 & 7 layers & Second layer rotation: 0.1 rad \\ 
            \midrule
            Target & 0     & 100   & 200   & 0.35 & - & \\ 
            \midrule
            Recoil tracker & 94.032 & 500   & 800   & 172.714 & 6 layers & Second layer rotation: 0.1 rad \\ \midrule
            \fooecal\  & 408.539 & 506   & 506   & 454.3 & $20\times 20\times 11 $ cells & - \\ 
            \midrule
            \foohcal\  & 2660.69 & 4029.51 & 4029.51 & 4048.01 & $4\times 4\times 1 $ modules & - \\
            \bottomrule
        \end{tabular}
    }
    \caption{The detector geometry overview.}
    \label{tab:DetectorGeometry}%
\end{table}

The \foods\ experiment incorporates a sophisticated tracking system embedded within a dipole magnet generating a magnetic field of 1.5~T. A silicon-based tracker is employed for the purpose of reconstructing both incident and recoil electron trajectories, thereby enabling the calculation of their respective momenta. Illustrated in Figure~\ref{fig:tracker}, the tagging tracker comprises seven layers of silicon strip detectors, while the recoil tracker is designed with six such layers. Situated between these two tracking modules is a tungsten (W) target with a decay length of \(0.1X_{0}\). To enhance the spatial resolution, each layer of both tracking modules accommodates two silicon strip sensors, which are oriented at a small angle of 100~mrad relative to one another. This configuration facilitates the precise measurement of particle positions in the x--y plane. The system achieves an impressive horizontal resolution of approximately \(6~\mu \text{m}\) and a vertical resolution of around \(60~\mu \text{m}\).

\begin{figure}[!htb]
\centering
\includegraphics[width=0.75\textwidth]{figures/app/Figure-6.pdf}
\caption{Illustration of tracking system components.}
\label{fig:tracker}
\end{figure}

The \foods\ experiment features a high-resolution electromagnetic calorimeter (\fooecal) situated immediately downstream of the recoil tracker. This \fooecal\ is designed to capture and reconstruct the energy deposition of incident particles. The scintillation material chosen for this application is LYSO(Ce) crystal, selected for its advantageous properties, such as high light yield, rapid decay time, and minimal electronic noise. The \fooecal\ is comprised of a \(20\times20\times11\) array of these LYSO crystals. Each individual crystal has a cross-sectional area measuring \(2.5\times2.5~\mathrm{cm^{2}}\) and a length of 4~cm. The total decay length facilitated by this LYSO array amounts to 44 \(X_{0}\), ensuring that final state electrons and photons are fully absorbed, thereby allowing for a complete energy reconstruction. The energy resolution of the LYSO crystal array has been experimentally determined to be approximately \(10\%\) when calibrated using a \(^{22}\mathrm{Na}\) radioactive source.

Subsequent to the \fooecal\, a hadronic calorimeter (\foohcal) is installed to capture and veto hadronic background, with a particular emphasis on muon elimination. The \foohcal\ is engineered as a scintillator-based sampling calorimeter featuring steel absorbers. It has dimensions of \(100~\mathrm{cm} \times 100~\mathrm{cm}\) in the x--y plane. Each of the scintillators in \foohcal\ is encapsulated in a carbon envelope and contains a centrally located wavelength-shifting fiber, enhancing its detection capabilities.


