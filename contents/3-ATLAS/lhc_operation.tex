In the context of assessing the operational status of the LHC, it is paramount to examine the data from its detectors, 
as the collider itself does not record collision outcomes. 
Among the suite of detectors around the LHC's ring, ATLAS stands as one of the primary general-purpose instruments, 
meticulously capturing a wide range of physics events. 
Given our affiliation with the ATLAS collaboration, and its comprehensive dataset covering numerous runs and periods, 
we will focus on the data from ATLAS to provide an illustrative example of the LHC's operational status over the years. 
The following figures and tables offer a detailed insight into the luminosities achieved and the associated uncertainties throughout different operational periods.

The graph on the left of Figure \ref{fig:3:lhc_luminosity} details the evolution of luminosity delivered to ATLAS across the years 2011 to 2018. 
This measurement pertains specifically to stable beams and high-energy proton-proton (p-p) collisions.
The LHC's primary function is to provide high luminosity, 
ensuring that ATLAS and other experiments have a sufficient number of collision events to carry out precision measurements and search for new phenomena. 
The increase in luminosity over the years is a testament to the constant enhancements in the LHC's machine operation and maintenance.
Each point on the curve represents a specific operational period, 
and the rise in luminosity can be attributed to various machine upgrades, 
operational optimizations, and the introduction of more intense beams. 
The increase in delivered luminosity means that a larger number of collisions is available for data analysis, 
thus enhancing the potential for scientific discoveries.

While the right one offers insight into the average interactions per crossing from 2015 to 2018 at a center-of-mass energy of 13 TeV. 
During this phase, the LHC resumed operations after its initial extended shutdown, 
operating at an energy level nearly double that of its inaugural run, signaling a significant advancement in its operational capabilities.
The displayed data encompasses all records from ATLAS during stable beams within this time frame. 
A key feature of the graph is the indication of the integrated luminosity and the mean value of $\mu$ (denoted as the average number of interactions per crossing). 
This average is aligned with the Poisson distribution's mean, a statistical measure used to predict the probability of events in a fixed interval of time or space.
The formula $\mu = \mathcal{L} _{\text{bunch}} \times \sigma_{\text{inel}} / f $ used in the caption provides a method for calculating this average. 
Here, $ \mathcal{L} _{\text{bunch}}$ represents the instantaneous luminosity for each bunch. 
In layman terms, it measures the density of particles in a specific beam bunch. 
On the other hand, $ \sigma_{\text{inel}} $ denotes the inelastic cross-section, with its value approximated at 80 mb for 13 TeV collisions. 
It provides a measure of the probability of a particular interaction between particles. 
Lastly, $f$ stands for the LHC revolution frequency, indicating how frequently the particles in the beam complete one circuit of the main ring of the accelerator.
In collider physics, the number of interactions per crossing determines the event complexity. 
A higher value means more simultaneous interactions. 
This increases the chance of significant events, but also requires advanced techniques to isolate desired signals from numerous interactions.

\begin{figure}[!htb]
    \centering
    \resizebox{\columnwidth}{!}{%
        \includegraphics[width=0.5\textwidth]{figures/3/intlumivsyear}
        \includegraphics[width=0.5\textwidth]{figures/3/mu_2015_2018}
    }
    \caption{
        Public ATLAS Luminosity Results for Run-2 of the LHC\cite{web:atlas:luminosity}.
        \bf{Left}: Delivered Luminosity Over Time (2011-2018): 
        The graph depicts the cumulative luminosity delivered to ATLAS as a function of time during stable beams and for high-energy p-p collisions.
        \bf{Right}: Interactions per Crossing (2015-2018 at 13 TeV): 
        This illustration showcases the luminosity-weighted distribution of the average interactions per crossing. 
        The data encompasses all records from ATLAS during stable beams between 2015 and 2018. 
        The integrated luminosity and the mean mu value are indicated. 
        The average interactions per crossing align with the Poisson distribution's mean, ascertained for each bunch.
    }
    \label{fig:3:lhc_luminosity}
\end{figure}

\begin{table}[!htb]
    \centering
    \resizebox{0.8\columnwidth}{!}{%
        \begin{tabular}{cccccc}
            \toprule Data sample & 2015 & 2016 & 2017 & 2018 & Comb \\
            \midrule Integrated luminosity $\left[\mathrm{fb}^{-1}\right]$ & 3.24 & 33.40 & 44.63 & 58.79 & 140.07 \\
                     Total uncertainty $\left[\mathrm{fb}^{-1}\right]$ & 0.04 & 0.30 & 0.50 & 0.64 & 1.17 \\
            \midrule \multicolumn{6}{l}{ Uncertainty contributions [\%]: } \\
            \midrule Statistical uncertainty & 0.07 & 0.02 & 0.02 & 0.03 & 0.01 \\
                     Fit model* & 0.14 & 0.08 & 0.09 & 0.17 & 0.12 \\
                     Background subtraction* & 0.06 & 0.11 & 0.19 & 0.11 & 0.13 \\
                     FBCT bunch-by-bunch fractions* & 0.07 & 0.09 & 0.07 & 0.07 & 0.07 \\
                     Ghost-charge and satellite bunches* & 0.04 & 0.04 & 0.02 & 0.09 & 0.05 \\
                     DCCT calibration* & 0.20 & 0.20 & 0.20 & 0.20 & 0.20 \\
                     Orbit-drift correction & 0.05 & 0.02 & 0.02 & 0.01 & 0.01 \\
                     Beam position jitter & 0.20 & 0.22 & 0.20 & 0.23 & 0.13 \\
                     Non-factorisation effects* & 0.60 & 0.30 & 0.10 & 0.30 & 0.24 \\
                     Beam-beam effects* & 0.27 & 0.25 & 0.26 & 0.26 & 0.26 \\
                     Emittance growth correction* & 0.04 & 0.02 & 0.09 & 0.02 & 0.04 \\
                     Length scale calibration & 0.03 & 0.06 & 0.04 & 0.04 & 0.03 \\
                     Inner detector length scale* & 0.12 & 0.12 & 0.12 & 0.12 & 0.12 \\
                     Magnetic non-linearity & 0.37 & 0.07 & 0.34 & 0.60 & 0.27 \\
                     Bunch-by-bunch $\sigma_{\mathrm{vis}}$ consistency & 0.44 & 0.28 & 0.19 & 0.00 & 0.09 \\
                     Scan-to-scan reproducibility & 0.09 & 0.18 & 0.71 & 0.30 & 0.26 \\
                     Reference specific luminosity & 0.13 & 0.29 & 0.30 & 0.31 & 0.18 \\
                     Subtotal vdM calibration & 0.96 & 0.70 & 0.99 & 0.93 & 0.65 \\
                     Calibration transfer* & 0.50 & 0.50 & 0.50 & 0.50 & 0.50 \\
                     Calibration anchoring & 0.22 & 0.18 & 0.14 & 0.26 & 0.13 \\
                     Long-term stability & 0.23 & 0.12 & 0.16 & 0.12 & 0.08 \\
                     Total uncertainty [\%] & 1.13 & 0.89 & 1.13 & 1.10 & 0.83 \\
            \bottomrule
        \end{tabular}
    }
    \caption{
        Summary of integrated luminosities post standard data-quality checks, 
        alongside uncertainties for the calibration of each yearly data sample from Run 2 pp at $\sqrt{s} = 13$ TeV, 
        including the cumulative sample\cite{atlascollaboration2022luminosity}. 
        The table presents the integrated luminosities, total uncertainties, 
        a detailed split of contributions to the vdM calibration's absolute accuracy, 
        extra uncertainties associated with the physics data sample, 
        and the overall relative uncertainty in percentage. 
    }
    \label{tab:3:lhc_luminosity}
\end{table}


In the ATLAS experiment during the LHC Run 2 at $\sqrt{s} = 13$ TeV,
a comprehensive summary of integrated luminosities, post rigorous data-quality checks, is provided in Table \ref{tab:3:lhc_luminosity}. 
The luminosity data span the years 2015 to 2018, presenting an ascending trend: from 3.24 fb\(^{-1}\) in 2015, 
the values increase progressively to 33.40, 44.63, and 58.79 fb\(^{-1}\) in 2016, 2017, and 2018, respectively. 
The cumulative sample across these years totals an impressive 140 fb\(^{-1}\). 
This table not only enumerates the integrated luminosities for each year but also delves into the specific uncertainties associated with the calibration of each yearly data sample. 
Importantly, certain contributors to uncertainty, marked with an asterisk (*), are considered to be fully correlated across the years. 
In contrast, other sources of uncertainty are treated as uncorrelated. 
This meticulous breakdown serves as an essential resource, quantifying both the vast amount of collision data ATLAS has accumulated during this period 
and the precision with which the luminosity measurements were made. 
The various factors influencing the absolute accuracy of the vdM calibration and the overall relative uncertainty percentages further underscore the diligence and detail invested in these measurements. 


