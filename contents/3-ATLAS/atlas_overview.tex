Situated at CERN, the European Organization for Nuclear Research, 
the ATLAS (A Toroidal LHC ApparatuS) experiment is one of the largest collaborative efforts 
in the domain of experimental particle physics. 
It is a general-purpose particle detector with a forward-backward symmetric cylindrical geometry 
and nearly $4-\pi$ coverage in solid angle.
ATLAS is not just an experiment, but a testament to international collaboration, 
with over 3,000 physicists from 38 countries and 180 institutions coming together for a common scientific pursuit.
It stands as tall as a five-story building at about 25 meters and stretching 44 meters in length. 
It weighs approximately 7,000 tons.

ATLAS is a particle detector that has several scientific goals\cite{ATLAS:technical}. 
One of its primary goals is to search for the Higgs boson, 
a particle predicted by the Standard Model to explain why other particles possess mass. 
This quest bore fruit in 2012 when the ATLAS and CMS collaborations announced the discovery of the Higgs boson.
ATLAS is also constantly searching for discrepancies with Standard Model predictions that could be indicative of new physics phenomena, 
including probing supersymmetry, a popular extension to the Standard Model, 
and hunting for dark matter candidates directly or via potential mediators.
In addition, by analyzing lead-lead ion collisions, 
ATLAS seeks to understand the state of matter known as quark-gluon plasma, 
which is believed to have been prevalent shortly after the Big Bang.
Finally, ATLAS aims to provide a deeper understanding of the forces and particles that make up our universe. 
It investigates the properties of the top quark, probes the electroweak symmetry breaking mechanism, 
and studies various aspects of quantum chromodynamics.

The conceptualization of the ATLAS detector was influenced by a range of intricate physics analyses.
The fundamental design principles of the detector were shaped around the following core objectives\cite{Airapetian:391176, ATLAS:technical}:
\begin{itemize}
    \item Achieve superior electromagnetic calorimetry to accurately identify and measure electrons and photons. 
    This should be supplemented by comprehensive hadronic calorimetry to ensure precise measurements of jets and missing transverse energy ( \EmT ).
    \item Ensure high-precision measurements of muon momentum. 
    The design must facilitate accurate measurements even at peak luminosity, relying primarily on the external muon spectrometer.
    \item Maintain efficient tracking capabilities during high luminosities. 
    This is vital for measurements of high-$p_T$ lepton momentum, identification of electrons, photons, $\tau$-leptons, and heavy flavors, 
    as well as enabling a complete event reconstruction during periods of lower luminosity.
    \item The detector must offer a broad acceptance in pseudorapidity ($\eta$), 
    combined with nearly complete azimuthal angle ($\phi$) coverage. 
    Here, $\phi$ denotes the angle measured around the beam's axis, 
    while $\eta$ is associated with the polar angle ($\theta$), where $\theta$ represents the angle from the z-direction.
    \item The design should support low-\(p_T\) threshold triggers and measurements, 
    ensuring high-efficiency capture of most physics processes that are integral to LHC operations.
\end{itemize}

The fundamental design principles of the ATLAS detector were meticulously crafted to cater to a spectrum of intricate physics studies. 
These principles revolve around ensuring precise measurements, large acceptance, 
and robust performance even under the challenging environment of high luminosities. 
Each subsystem\cite{Airapetian:391176} of the detector is designed with specific characteristics that when combined, 
form the complete and formidable capability of the ATLAS detector.
\begin{itemize}
    \item \textbf{Magnet Configuration}: The detector is equipped with an inner superconducting solenoid, 
    surrounding the inner detector, and large external superconducting toroids having an eight-fold symmetry.
    \item \textbf{Inner Detector (ID)}: Situated within a 7 m by 1.15 m cylinder, 
    the ID functions in a 2 T solenoidal magnetic field. 
    The inner part comprises semiconductor pixel and strip detectors for accuracy, 
    while the outer portion contains straw-tube trackers ensuring a wide tracking range.
    \item \textbf{Calorimetry}: Liquid-argon (LAr) electromagnetic calorimetry offers precise energy 
    and position measurements up to $|\eta|< 3.2$. 
    The end-caps, using LAr technology, stretch this to $|\eta|= 4.9$. 
    Most hadronic measurements come from the uniquely designed scintillator-tile calorimeter, 
    segmented into a barrel and two extended barrels.
    \item \textbf{Dimensions and Weight}: The LAr calorimetry is contained in a 2.25 m by $\pm$6.65 m cylinder. 
    Adjacent to it, the tile calorimeter spans an outer radius of 4.25 m and a half-length of 6.10 m, 
    together weighing around 4,000 Tons.
    \item \textbf{Muon Spectrometer}: Surrounding the calorimeters, the spectrometer features an air-core toroid system. 
    It boasts three stations of high-precision tracking chambers, 
    ensuring stellar muon momentum resolution, supplemented by rapid-response trigger chambers.
    \item \textbf{Overall Dimensions}: The muon spectrometer determines the detector's vast scale. 
    Its boundaries, at about 11 m in radius, combined with the 12.5 m barrel toroid coils and 23 m distant forward muon chambers, 
    sum up the detector's total weight to an impressive 7,000 Tons.
\end{itemize}

The ATLAS detector is designed with a comprehensive framework consisting of various subsystems and components, 
of which a computer generated image is shown in figure~\ref{fig:3:atlas_detector_image}. 
These elements collectively contribute to its functionality, enabling it to fulfill its scientific objectives effectively. 
In the subsequent sections, we will provide a detailed explanation of each subsystem, outlining its design, functionality, 
and its vital role within the broader goals of the ATLAS experiment.

\begin{figure}[!htb]
    \centering
    \resizebox{\columnwidth}{!}{%
        \includegraphics[width=\textwidth]{figures/3/ATALS_Detector}
    }
    \caption{
        The layout of the whole ATLAS detector\cite{ATLAS:Detector_Image}.
    }
    \label{fig:3:atlas_detector_image}
\end{figure}


\subsubsection{Detector Coordinate}
\label{sec:3:coordinate}

The LHC beam's direction sets the z-axis, while the x-y plane is perpendicular to this beam direction. 
The positive x-axis extends from the interaction point towards the LHC ring's center, and the positive y-axis points upward. 
The azimuthal angle, $\phi$, is determined around the beam direction, while $\theta$ represents the angle from this axis. 
Pseudorapidity is expressed as $\eta$, defined as $\eta \equiv -\ln\tan{(\theta/2)}$. 
Both the transverse momentum $p_T=\sqrt{p_x^2+p_y^2}$ and the transverse energy $E_T=\sqrt{p_T^2+m^2}$, 
as well as the missing transverse energy $E_T^{\text{miss}}$, are generally defined in the x-y plane. 
The distance $\Delta R$ in $\eta-\phi$ space is given by $\Delta \mathrm{R}=\sqrt{\Delta^2 \eta+\Delta^2 \phi}$.

Charged particle trajectories in a consistent magnetic field are characterized using five parameters of a helix. 
In ATLAS, a specific helix parameterization $\left(d_0, z_0, \theta, \phi, q / p\right)$ is employed, 
which is shown in figure~\ref{fig:3:track_parameters},
considering all measurements at the point nearest to the beam line where x and y are zero.
Parameters in $x-y$ plane are:
\begin{itemize}
    \item \textbf{$d_0$}: The transverse impact parameter, which is the sideways distance from the beam axis at the closest approach point. 
    Its sign is determined by the reconstructed angular momentum of the track around the axis.
    \item \textbf{$\phi$}: Azimuthal angle, where $\tan \phi \equiv p_{\mathrm{y}} / p_{\mathrm{x}}$, ranged from $[0, \pi]$.
    \item \textbf{$q/p_T$}: The charge-to-momentum ratio in the transverse plane. 
    "q" is the charge of the particle (either +1 or -1 for singly charged particles) and "$p_T$" is the transverse momentum. 
    It determines the curvature of the trajectory.
\end{itemize}
Parameters in the $R-z$ plane are:
\begin{itemize}
    \item \textbf{$\theta$}: the polar angle, where $\cot \theta \equiv p_{\mathrm{z}} / p_{\mathrm{T}}$. 
    \item \textbf{$z_0$}: The longitudinal impact parameter, which is the z-coordinate of the track at the closest approach point.
\end{itemize}

\begin{figure}[!htb]
    \centering
    \resizebox{0.8\columnwidth}{!}{
        \tdplotsetmaincoords{80}{35} % Adjusted viewpoint
        \begin{tikzpicture}[tdplot_main_coords,scale=3.5]

            % Coordinate Axes
            \draw[thick,->] (0,0,0) -- (1.25,0,0) node[anchor=north east]{$e_y$}; %x
            \draw[thick,->] (0,-3.5,0) -- (0,1.25,0) node[anchor=north west]{$e_z$}; %y
            \draw[thick,->] (0,0,0) -- (0,0,0.75) node[anchor=south]{$e_x$}; %z

            % Drawing a square on the x-y plane
            \filldraw[opacity=0.5, fill=black!30] (-1.25,0,-0.75) -- (1.25,0,-0.75) -- (1.25,0,0.75) -- (-1.25,0,0.75) -- cycle 
            node[anchor=south, rotate=-6] at (0.75,-0.05,0.75) {x-y plane};

            % Dashed lines
            \draw[dashed] (0.95,-4.5,0.65) -- (0.95,1.5,0.65);
            \draw[dashed] (1,-4.5,0) -- (1,1.25,0);

            % Trajectory
            \tdplotdefinepoints(0,-4,0)(0.45,-4,0)(7.5,-1,4.5)
            \node[anchor=north west] at (0.2,-1.35,-0.5){Trajectory};
            \tdplotdrawpolytopearc[ultra thick,->]{2}{anchor=north}{}

            % track circle
            \tdplotsetrotatedcoords{80}{87}{-80}
            \coordinate (Shift) at (0,-1.62,0);
            \tdplotsetrotatedcoordsorigin{(Shift)}
            % \draw[thick,color=blue,tdplot_rotated_coords,->] (0,0,0) --
            % (.7,0,0) node[anchor=north]{$x'$};
            % \draw[thick,color=blue,tdplot_rotated_coords,->] (0,0,0) --
            % (0,.7,0) node[anchor=west]{$y'$};
            % \draw[thick,color=blue,tdplot_rotated_coords,->] (0,0,0) --
            % (0,0,.7) node[anchor=south]{$z'$};      
            \tdplotdrawarc[tdplot_rotated_coords,dashed]{(0,0,0)}{0.53}{0}{360}{anchor=south west}{}
            
            % dz point
            \draw[fill=white] (1.,-2.42,0) circle (0.25pt) node[above right] {};
            % Momentum
            \draw[thick,color=black,tdplot_main_coords,->] (1.,-2.42,0) -- (0.95,-2.15,.65) node[anchor=north west]{$p$};   
            % d0
            \draw[thin,<->] (.99,-2.42,0) -- (0.01,-1.62,0) node[midway,above]{$\mathbf{d_0}$};
            % z0
            \draw[thin] (1.01,-2.43,0) -- (1.22,-2.6,0) node[]{};
            \draw[thin,<->] (1.1,-2.50,0) -- (1.1,0,0) node[midway,below]{$\mathbf{z_0}$};

            % pt point
            \draw[fill=white] (1.,-0.42,0) circle (0.25pt) node[] {};
            % Transverse
            \draw[thick,color=black,opacity=0.5,tdplot_main_coords,->] (1.,-0.42,0) -- (0.95,-0.12,.65) node[anchor=north west]{$p_T$}; 
            \draw[thin] (1.,-0.42,0) -- (0.95,-0.35,.65) node[]{}; 
            \draw[thin,<->] (0.01,0,0) -- (0.99,-0.42,0) node[]{};

            % Phi
            \tdplotsetrotatedcoords{0}{-90}{0}
            \coordinate (Shift) at (1.,-0.42,0);
            \tdplotsetrotatedcoordsorigin{(Shift)}
            % \draw[thick,color=blue,tdplot_rotated_coords,->] (0,0,0) --
            % (.7,0,0) node[anchor=north]{$x'$};
            % \draw[thick,color=blue,tdplot_rotated_coords,->] (0,0,0) --
            % (0,.7,0) node[anchor=west]{$y'$};
            % \draw[thick,color=blue,tdplot_rotated_coords,->] (0,0,0) --
            % (0,0,.7) node[anchor=south]{$z'$};      
            \tdplotdrawarc[tdplot_rotated_coords]{(0,0,0)}{0.5}{0}{19}{anchor=south}{$\boldsymbol{\phi}$}

            % Theta
            \tdplotsetrotatedcoords{0}{-79}{0}
            \coordinate (Shift) at (1.,-2.42,0);
            \tdplotsetrotatedcoordsorigin{(Shift)}
            % \draw[thick,color=blue,tdplot_rotated_coords,->] (0,0,0) --
            % (.7,0,0) node[anchor=north]{$x'$};
            % \draw[thick,color=blue,tdplot_rotated_coords,->] (0,0,0) --
            % (0,.7,0) node[anchor=west]{$y'$};
            % \draw[thick,color=blue,tdplot_rotated_coords,->] (0,0,0) --
            % (0,0,.7) node[anchor=south]{$z'$};      
            \tdplotdrawarc[tdplot_rotated_coords]{(0,0,0)}{0.1}{0}{90}{anchor=south west}{$\boldsymbol{\theta}$}

        \end{tikzpicture}
    }
    \caption{Illustration of the global track coordinate with respect to perigee.}
    \label{fig:3:track_parameters}
\end{figure}



