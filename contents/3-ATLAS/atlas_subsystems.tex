In this section, a comprehensive overview of the subsystems that constitute the ATLAS detector is provided, 
progressing from the innermost components to the outer structures. 
Each subsystem is pivotal in the detection, tracking, and measurement of particles, 
contributing to the high precision and reliability for which the ATLAS detector is known. 
The performance characteristics of these integral components are summarized in Table~\ref{tab:3:detector_performance}.

\begin{table}[!htb]
    \centering
    \resizebox{\columnwidth}{!}{
        \begin{tabular}{llcc}
            \toprule
            Subdetectors & Resolution & \multicolumn{2}{c}{$\eta$ coverage } \\
            & & Measurement & Trigger \\
            \midrule
            Trackers & $\sigma_{p_T} / p_T=0.05 \% p_T \oplus 1 \%$ & $\pm 2.5$ & \\
            \midrule
            ECAL & $\sigma_E / E=10 \% \sqrt{E} \oplus 0.7 \%$ & $\pm 3.2$ & $\pm 2.5$ \\
            \midrule
            HCAL(jets) & & & \\
            ~~Barrel \& Cap & $\sigma_E / E=50 \% \sqrt{E} \oplus 3 \%$ & $\pm 3.2$ & $\pm 3.2$ \\
            ~~Forward & $\sigma_E / E=100 \% \sqrt{E} \oplus 10 \%$ & $3.1<|\eta|<4.9$ & $3.1<|\eta|<4.9$ \\
            \midrule
             Muon Spectrometer & $\sigma_{p_T} / p_T=10 \%$ at $p_T=1 \mathrm{TeV}$ & $\pm 2.7$ & $\pm 2.4$ \\
            \bottomrule
        \end{tabular}
    }
    \caption{
        Summary of the resolution and $\eta$ coverage for various subdetectors. 
        ECAL and HCAL represent for electromagnet calorimeter and hadronic calorimeter, respectively.
        The listed resolutions for trackers and the ECAL are parametrized forms dependent on either $p_T$ or $E$ (in GeV), 
        while the Muon Spectrometer provides a specific resolution at a given $p_T$ value. 
        The $\eta$ coverage is further categorized into measurement and trigger regions.
    }
    \label{tab:3:detector_performance}
\end{table}

\subsubsection{Inner tracking system}
\label{sec:3:ID}
In every 25 ns proton bunch collision event, approximately 1,000 particles emerge from the collision point, 
producing extremely dense tracks within the detector region of $|\eta| \le 2.5$. 
To achieve the momentum and vertex requirements necessary for measuring critical physical processes, 
a fine granularity of the detector is essential for precise measurements.
The ATLAS Inner tracking system is pivotal in this context and is responsible for detecting and measuring these charged particles. 
Designed to offer full tracking coverage over $|\eta| \le 2.5$, 
it employs high-resolution detectors at inner radii and continuous tracking elements at outer radii. 
The incorporation of Semiconductor Tracking (SCT) detectors and the TRT (Transition Radiation Tracker) further accentuates the precision of these measurements.

The Inner Detector (ID) is mechanically divided into three units\cite{ATLAS:inner_detector}: 
\begin{itemize}
    \item \textbf{Pixel Detector}: The pixel detector, positioned closest to the interaction point in the ATLAS Inner Detector, 
    provides three high-precision measurements across its full acceptance, 
    aiding in identifying short-lived particles like b-quarks and $\tau$-leptons. 
    With its 140 million elements, each sized 50 \um in the $R-\phi$ direction and 300 \um in z, 
    the pixel detector offers unambiguous two-dimensional space point segmentation. 
    It comprises three barrels at average radii of approximately 4 cm, 11 cm, and 14 cm, 
    complemented by four disks on each side, spanning radii from 11 to 20 cm. 
    This modular system consists of about 1500 identical barrel modules and 1000 disk modules. 
    Each barrel module, measuring 62.4 mm by 22.4 mm, is equipped with 61440 pixel elements, serviced by 16 readout chips. 
    The design ensures overlapping modules for hermetic coverage, 
    and the thickness of each layer in the simulation is less than 1.39\% of a radiation length.

    \item \textbf{Semiconductor Tracker (SCT)}: The SCT system, positioned in the intermediate radial range of the ATLAS Inner Detector, 
    provides four precision measurements per track, contributing to momentum, impact parameter, vertex positioning, 
    and pattern recognition via its high granularity. 
    The SCT system boasts a significantly larger surface area compared to previous silicon microstrip detectors, 
    and it is designed to withstand radiation intensities that can modify the inherent characteristics of its silicon wafers.
    The barrel SCT utilizes four layers of silicon microstrip detectors, 
    each measuring $6.36 \times 6.40 \text{~cm}^2$ with 768 readout strips at an 80 \um pitch. 
    A module encompasses four detectors, and the system's readout comprises a front-end amplifier 
    and discriminator connected to a binary pipeline. 
    The detector covers a surface of $61 \text{~m}^2$, having 6.2 million readout channels, 
    offering a spatial resolution of 16 \um in $R-\phi$ and 580 \um in z. 
    This structure is further enhanced with specific design elements for cooling, 
    and thermal stability, using materials with minimal thermal expansion coefficients 
    and a unique cooling method employing a methanol-water mixture.
    
    \item \textbf{Transition Radiation Tracker (TRT)}: The TRT employs straw detectors, 
    recognized for their capability to handle very high rates due to their small diameter and the isolation of their sense wires.
    Enhanced by xenon gas, these detectors can identify electrons 
    by detecting transition-radiation photons produced in a specific radiator positioned between the straws. 
    The design accommodates the LHC's high counting rates and large occupancy. 
    Each straw is 4 mm in diameter and can span up to 150 cm in length. 
    In total, the system integrates about 420,000 electronic channels which offer a spatial resolution of 170 \um per straw. 
    The barrel structure encompasses modules that range radially from 56 to 107 cm. 
    In contrast, the two end-caps are structured with 18 wheels, 
    covering various radial ranges to maintain consistent straw crossing. 
    The system's design priority is to deliver high performance under substantial occupancy and counting rates. 
    Even at peak rates, the majority of straws provide accurate drift time measurements, 
    ensuring a track measurement precision under 50 \um. 
    The TRT enhances the momentum measurement precision in the Inner Detector 
    and plays a pivotal role in pattern recognition and electron-hadron differentiation.

\end{itemize}


The fundamental design specifications and space-point measurement resolutions are detailed in Table~\ref{tab:3:ID}.
Additionally, an overview and a cross-sectional view of the Inner Detector, showing layers from the innermost to the outermost
, is provided in Figure~\ref{fig:3:ID_image}.


\begin{table}[!htb]
    \centering
    \setlength{\arraycolsep}{0pt}
    \resizebox{\columnwidth}{!}{%
        \begin{tabular}{llcccc}
            \toprule
            System         & Position                                                                                     & $\begin{array}{c}\text { Area } \\            \left(\mathbf{m}^2\right)\end{array}$ & $\begin{array}{c}\text { Resolution } \\            \sigma(\mu \mathbf{m})\end{array}$ & $\begin{array}{c}\text { Channels } \\            (\mathbf{10^6})\end{array}$ & $\eta$ coverage \\ 
            \midrule
            Pixels         & 1 removable barrel layer                                                                     & 0.2                                                                                 & $\mathrm{R} \phi=12, z=66$                                                             & 16                                                                            & $\pm$ 2.5       \\
                        & 2 barrel layers                                                                              & 1.4                                                                                 & $\mathrm{R} \phi=12, z=66$                                                             & 81                                                                            & $\pm$ 1.7       \\
                        & $\begin{array}{l}\text{4 end-cap disks } \\ \text{on each side }\end{array}$                 & 0.7                                                                                 & $\mathrm{R} \phi=12, \mathrm{R}=77$                                                    & 43                                                                            & $1.7-2.5$       \\
            Silicon strips & 4 barrel layers                                                                              & 34.4                                                                                & $\mathrm{R} \phi=16, z=580$                                                            & 3.2                                                                           & $\pm$ 1.4       \\
                        & $\begin{array}{l}\text{9 end-cap wheels } \\                \text{on each side }\end{array}$ & 26.7                                                                                & $\mathrm{R} \phi=16, \mathrm{R}=580$                                                   & 3.0                                                                           & $1.4-2.5$       \\
            TRT            & Axial barrel straws                                                                          &                                                                                     & 170 (per straw)                                                                        & 0.1                                                                           & $\pm$ 0.7       \\
                        & Radial end-cap straws                                                                        &                                                                                     & 170 (per straw)                                                                        & 0.32                                                                          & $0.7-2.5$       \\
            \bottomrule
        \end{tabular}
    }
    \caption{
        Inner Detector Parameters and Resolutions\cite{ATLAS:inner_detector}: 
        The provided resolutions are indicative values 
        (the precise resolution in each detector varies with $|\eta|$).
    }
    \label{tab:3:ID}
\end{table}


\begin{figure}[!htb]
    \centering
    \resizebox{\columnwidth}{!}{%
        \includegraphics[width=0.60\textwidth]{figures/3/ID_1}
        \includegraphics[width=0.40\textwidth]{figures/3/ID_2}
    }
    \caption{
        The layout of the ATLAS Inner Detector\cite{ATLAS:inner_detector_image}.
        Left: Overview of the ID;
        Right: Cross-sectional view of the Inner Detector, with layers from innermost to outermost being the pixel detector, SCT, and TRT.
    }
    \label{fig:3:ID_image}
\end{figure}



\subsubsection{Electromagnetic and hadronic calorimeters}

The physics requirements for calorimeters at LHC\cite{ATLAS:calorimeter}, are of utmost importance. 
They must accurately measure the energy and position of electrons and photons, 
the energy and direction of jets, and the missing transverse momentum of events. 
Particle identification, event selection at the trigger level, and high radiation resistance are also crucial.
The LHC's high luminosity and energy range require fast detector response, 
fine granularity, and rejection of backgrounds such as jets faking photons. 
The performance specifications come from benchmark channels such as 
the search for a Higgs boson through the decays $H \to \gamma\gamma$ and $H \to 4e$, 
and the search for heavy vector bosons ($W'$, $Z'$) with masses up to $5-6$ TeV through the decays $W' \to e\nu$ and $Z' \to e^+e^-$. 
The SM Higgs search imposes specific requirements on the hadronic calorimetry, 
such as $W \to jj$ mass reconstruction, forward jet tagging, and $H \to b\bar{b}$ mass reconstruction using jet spectroscopy. 
These requirements are critical for discovering high and low mass Higgs bosons, studying top physics, 
and detecting non-interacting particles in supersymmetric models. 

The ATLAS Electromagnetic calorimeter\cite{ATLAS:calorimeter} (ECAL) has stringent requirements tailored to its pivotal role in ATLAS experiments. 
It boasts a broad rapidity coverage and possesses the capability to reconstruct electrons 
within an energy range spanning from $1-2$ GeV up to 5 TeV. In terms of energy resolution, 
the device excels, demonstrating exceptional performance over the energy bracket of $10-300$ GeV. 
With a structural robustness, it maintains a total thickness of at least 24 radiation lengths at $\eta=0$. 
Its dynamic range is set between 50 MeV and 3 TeV, and it stands out with its energy-scale precision of 0.1\%. 
The linearity of its response is noteworthy, surpassing benchmarks by remaining better than 0.5\% for energies up to 300 GeV. 
One of its notable features is its ability to measure the shower direction in $\theta$, 
achieving a resolution of approximately 50 mrad per the square root of the energy in GeV. 
As for particle differentiation, the calorimeter has an exceptional capacity for 
photon/jet, electron/jet, and τ/jet separations. 
Furthermore, it is designed to have a rapid response, low noise, and offers high granularity. 
The coherent noise level is maintained below E = 3 MeV for each channel, and it is adept at identifying individual bunch crossings.

\begin{figure}[!htb]
    \centering
    \resizebox{\columnwidth}{!}{%
        \includegraphics[width=\textwidth]{figures/3/Calorimeters}
    }
    \caption{
        The layout of the ATLAS calorimeters\cite{ATLAS:calorimeter_image}.
    }
    \label{fig:3:calo_image}
\end{figure}

The EM calorimeter of the LHC is divided into a central barrel ($|\eta|<1.475$) and two end-caps ($1.375 <|\eta|< 3.2$). 
The barrel consists of two half-barrels, while each end-cap is further segmented into outer and inner wheels. 
Designed as a lead-LAr detector, the calorimeter features accordion-shaped Kapton electrodes 
to ensure complete $\phi$ symmetry without azimuthal gaps. 
The thickness of the calorimeter varies, exceeding 24$X_0$ in the barrel and 26$X_0$ in the end-caps. 
Within the precision-focused region ($|\eta| < 2.5$), it is divided into three longitudinal layers. 

\begin{itemize}
    \item \textbf{First Sampling (Preshower Detector)}: This layer has a consistent thickness of 6$X_0$ 
    and is furnished with narrow strips oriented in the $\eta$ direction. 
    It acts primarily as a "preshower" detector, aiding in particle identification and 
    precise position measurements in the $\eta$ dimension.
    \item \textbf{Second Sampling}: This layer is divided into square towers each spanning $\Delta \eta \times \Delta \phi = 0.025 \times 0.025$. 
    By the end of this second sampling, the total calorimeter thickness reaches around 24 $X_0$.
    \item \textbf{Third Compartment}: This final layer has coarser granularity in the $\eta$ direction 
    and its thickness varies between 2$X_0$ and 12$X_0$.
\end{itemize}

In total, the calorimeter has about 190,000 channels. Materials in front of the calorimeter, 
including the inner detector and solenoid coil, sum up to about 1.8$X_0$ at $\eta = 0$. 
Signals from the calorimeter are routed to external preamplifiers, digitized upon trigger events, 
and forwarded to the Data Acquisition system using a three-gain scale for optimum noise management.

The ATLAS hadronic calorimetry\cite{ATLAS:calorimeter} (HCAL) covers a pseudo-rapidity range of $|\eta| < 5$, 
employing various techniques tailored for the radiation environment and diverse requirements. 
The barrel and extended barrel Tile calorimeters, which span up to $|\eta| < 1.6$, use iron-scintillating-tiles. 
The Liquid Argon calorimetry is employed from $\sim 1.5 < |\eta| < 4.9$. 
Total calorimeter thickness at $\eta = 0$ is 11 interaction lengths, 
providing sufficient containment for hadronic showers and reducing punch-through for the muon system.
\begin{itemize}
    \item \textbf{Tile Calorimeter}: The hadronic barrel calorimeter operates as a sampling device, 
    using iron as the absorber and scintillating tiles as the active medium. 
    Tiles, 3 mm thick, are oriented perpendicular to the colliding beams and 
    staggered in depth with a periodic structure along the z-axis.
    Both tile sides are readout using wavelength shifting fibers, channeling into two separate photomultiplier tubes (PMTs). 
    Comprising one barrel and two extended barrels, the calorimeter spans from an inner radius of 2.28 m to 4.23 m. 
    It features three layers segmented to thicknesses of approximately 1.4, 4.0, and 1.8 interaction lengths at $\eta = 0$. 
    The granularity results in a $ \Delta \eta \times \Delta \phi = 0.1 \times 0.1$ (or $0.2 \times 0.1$ in the last layer). 
    The calorimeter is positioned behind the EM calorimeter and the solenoid coil, 
    leading to a total active calorimeter thickness of 9.2 interaction lengths at $\eta = 0$. 
    An Intermediate Tile Calorimeter (ITC) enhances the thickness in the gap between the barrel and extended barrels.

    \item \textbf{End-cap Liquid Argon calorimetry}: The end-cap Liquid Argon calorimetry, 
    covering $ \sim 1.5 < |\eta| < 3.2 $, features two wheels of equal diameter. 
    The first wheel uses 25 mm copper plates, while the second wheel has 50 mm plates.
    The gap between consecutive plates is 8.5 mm, segmented with 3 electrodes, forming 4 drift spaces of around 1.8 mm each. 
    The first wheel is bifurcated into two longitudinal segments, consisting of 8 and 16 layers, 
    while the second has a single 16-layer segment. The end-cap's active portion has a thickness of around 12 interaction lengths.

    \item \textbf{Frward Liquid Argon calorimetry}: Located within the end-cap cryostat, 
    the forward calorimeter covers $3.2 < |\eta| < 4.9 $. 
    Its close proximity to the interaction point (5 meters) exposes it to high radiation levels. 
    Its design ensures uniformity of coverage, minimizing crack and dead space effects around $ \eta = 3.1 $. 
    This calorimeter is high-density, with three longitudinal sections: 
    the first is copper, and the subsequent two are tungsten. 
    Each section features a metal matrix with spaced channels filled with rods, 
    with Liquid Argon serving as the sensitive medium in the gaps. 
    The forward calorimeter's electronic noise for a jet cone of $\Delta R = 0.5$ is roughly 1 GeV $E_T$ at $ \eta = 3.2 $, 
    decreasing sharply to 0.1 GeV $E_T$ at $ \eta = 4.6 $.
\end{itemize}

The overall layout of the ATLAS ECAL and HCAL is shown in Figure~\ref{fig:3:calo_image}.



\subsubsection{Muon spectrometer}


Theoretically, particles that can pass through the entire depth of the calorimeter without interacting, 
or only weakly interacting, include neutrinos and muons which do not deposit their full energy. 
The muon spectrometer is used to identify muons and calculate the energy that is not detected.

The ATLAS Muon Spectrometer\cite{ATLAS:muon_detector}, as shown in Figure~\ref{fig:3:muon_detector_image}, 
includes a barrel region and three wheel-shaped endcap regions. 
In the barrel region ($|\eta| < 1.4$), the magnetic field is provided by barrel toroidal magnets. 
In the endcap region ($1.6 < |\eta| < 2.7$), the magnetic field is provided by the endcap toroidal magnets in Section~\ref{sec:3:magnet}. 
In the transition region of $1.4 < |\eta| < 1.6$, the magnetic field is jointly provided by the endcap and barrel toroidal magnets. 
The muon spectrometer employs four drift chamber technologies: 
Monitored Drift Tubes (MDTs), Cathode Strip Chambers (CSCs), Resistive Plate Chambers (RPCs), and Thin Gap Chambers (TGCs). 
In the barrel region, the drift chambers are arranged in three concentric cylindrical layers along the beam axis. 
In both endcap regions, the drift chambers are also divided into three layers placed on discs perpendicular to the beam direction.


\begin{figure}[!htb]
    \centering
    \resizebox{0.8\columnwidth}{!}{%
        \includegraphics[width=\textwidth]{figures/3/MuonDetector}
    }
    \caption{
        The layout of the ATLAS muon system\cite{ATLAS:muon_detector_image}.
    }
    \label{fig:3:muon_detector_image}
\end{figure}

\begin{table}[!htb]
    \centering
    % \setlength{\arraycolsep}{0pt}
    \resizebox{\columnwidth}{!}{%
        \begin{tabular}{ccccccccc}
            \toprule 
            \multicolumn{2}{c}{} & \multicolumn{3}{c}{ Chamber resolution (RMS) in } & \multicolumn{2}{c}{ Measurements/track } & \multicolumn{2}{c}{ Number of } \\
            \midrule Type & Function & $z / R$ & $\phi$ & time & barrel & end-cap & chambers & channels \\
            \midrule MDT & tracking & 35~\um(z) & - & - & 20 & 20 & $1088(1150)$ & $339 \mathrm{k}(354 \mathrm{k})$ \\
            CSC & tracking & 40~\um(R) & $5 \mathrm{~mm}$ & $7 \mathrm{~ns}$ & - & 4 & 32 & $30.7 \mathrm{k}$ \\
            RPC & trigger & $10\mathrm{~mm}(z)$ & $10 \mathrm{~mm}$ & $1.5 \mathrm{~ns}$ & 6 & - & $544(606)$ & $359 \mathrm{k}(373 \mathrm{k})$ \\
            TGC & trigger & $2-6\mathrm{~mm}(R)$ & $3-7 \mathrm{~mm}$ & $4 \mathrm{~ns}$ & - & 9 & 3588 & $318 \mathrm{k}$ \\
            \bottomrule
        \end{tabular}
    }
    \caption{
        Specifications for the four subsystems of the muon detector\cite{ATLAS:General}: 
        The stated spatial resolution (in columns 3 and 4) excludes uncertainties from chamber alignment. 
        The intrinsic time accuracy for each chamber type is detailed in column 5, 
        with additional time required for signal propagation and electronic factors. 
        Numbers in parentheses relate to the full detector setup anticipated for 2009.
    }
    \label{tab:3:muon_detector}
\end{table}

The muon spectrometer is divided into two sets of systems: precision chambers and trigger chambers. 
High-precision track measurements are based on MDTs in the barrel and CSCs in the endcaps, 
providing 6-8 measurement points. 
MDTs cover the region $|\eta| < 2.7$ and consist of drift tubes with a diameter of 3 cm and 50 \um tungsten-rhenium signal wires, 
filled with a mixture of 93\% argon gas and 7\% carbon dioxide. 
They operate under a voltage of 3kV and function similarly to the TRT, 
working in a drift chamber mode with a maximum drift time of 750 ns. 
A single drift tube offers a spatial resolution of ($R-\phi$) 80 \um, while the average resolution of MDTs can reach 35 \um. 
In a region of higher pseudorapidity ($2.0 < |\eta| < 2.7$), closer to the interaction point, 
higher granularity CSCs are used to adapt to higher particle flux and beam background. 
CSCs are based on Multiwire Proportional Chamber (MRPC) detectors, with a maximum drift time of 40 ns.

Trigger chambers are primarily used for rapid triggering of muon events and are based on RPCs and TGCs. 
These detectors achieve intrinsic time resolutions of 1.5 ns and 4 ns, respectively. 
Additionally, the trigger chambers provide a second set of position measurements independent of the precision measurements 
(for $|\eta| < 2.4$), aligned approximately with the magnetic field lines, though with slightly inferior spatial resolution (5-10 mm). 
RPCs operate as MRPC detectors in saturation mode, filled with a mixture of 55\% carbon dioxide and 45\% n-pentane, 
covering the barrel region ($1.05 < |\eta| < 2.4$). RPCs function as parallel plate detectors in amplification mode, 
filled with $\text{C}_2\text{H}_2\text{F}_4$, with electrode distances of 2 mm, covering the barrel region ($|\eta| < 1.05$). 
Detailed parameters are given in Table~\ref{tab:3:muon_detector}.



\subsubsection{Magnet system}
\label{sec:3:magnet}

The ATLAS experiment is equipped with a distinctive combined system comprising four extensive superconducting magnets. 
Spanning a diameter of 22 m and extending 26 m in length, this magnetic setup houses an energy storage capacity of 1.6 GJ. 
Following roughly a decade and a half dedicated to its design, its assembly in industrial settings, 
and the eventual integration at CERN, this system now stands fully operational in its subterranean chamber. 
This section describes the characteristics of these magnets along with their auxiliary services. 

Illustrated in Figure~\ref{fig:3:magnet_image} is the overarching design, 
spotlighting the four primary layers of detectors as well as 
the quartet of superconducting magnets responsible for generating the magnetic field across a vast expanse of about $12000~\text{m}^3$ 
(this volume is demarcated by regions where the field strength surpasses 50 mT). 
Central attributes of the ATLAS magnetic system encompass:
\begin{itemize}
    \item A central solenoid, strategically positioned along the beam trajectory, 
    furnishing a 2T axial magnetic field purposed for the inner detector. 
    This ensures the reduction of radiative thickness ahead of the barrel electromagnetic calorimeter.
    \item A cylindrical barrel toroid paired with twin end-cap toroids.
     Together, they generate a toroidal magnetic field, averaging between 0.5T in the core region and 
     amplifying up to 1T for the muon detectors located at the end-cap zones.
\end{itemize} 

\begin{figure}[!htb]
    \centering
    \resizebox{0.8\columnwidth}{!}{%
        \includegraphics[width=\textwidth]{figures/3/MagnetSystem}
    }
    \caption{
        The layout of the ATLAS magnet system\cite{ATLAS:magnet_image}.
    }
    \label{fig:3:magnet_image}
\end{figure}

\subsubsection{Trigger and data acquisition systems}

The ATLAS experiment employs a sophisticated, multi-level event selection mechanism, often referred to as the "trigger system". 
This system has been designed to sift through the vast number of potential event candidates, 
selecting only those of highest interest for in-depth analysis.

The trigger system is divided into three hierarchical stages, which is shown in Figure~\ref{fig:3:triggers_image}:
\begin{enumerate}
    \item \textbf{Level-1 (L1) Trigger}: The most immediate and rapid-fire of the selection processes. 
    Designed with custom electronics, it employs low-resolution data from select detectors, 
    chiefly the Resistive Plate Chambers (RPC) and Thin-Gap Chambers (TGC) for high-$p_T$ muons, 
    along with all calorimeter sub-systems to detect electromagnetic clusters, jets, $\tau$-leptons, 
    missing transverse energy \EmT, and overall large transverse energy. 
    This stage primarily searches for signatures from high-$p_T$ muons, electrons/photons, jets, 
    and $\tau$-leptons that decay into hadrons. 
    It is tailored to make fast decisions, with a decision window of just 2.5 $\mu \text{s}$ post a bunch-crossing. 
    It operates at an impressive maximum rate of 75 kHz, which holds the potential to be upgraded to 100 kHz.
    \item \textbf{Level-2 (L2) Trigger (High-Level Trigger - HLT)}: 
    Unlike L1, this stage is powered primarily by commercial computing and networking equipment. 
    The L2 trigger operates in a more refined environment, with its activities being directed 
    by the Regions-of-Interest (RoI's) highlighted by the L1 trigger. 
    These RoI's are essentially detector zones where potential event candidates have been spotted.
    The L2 trigger then utilizes the RoI data to curtail the data volume that needs to be fetched from the detector readout, 
    thereby optimizing the event selection process. 
    At this stage, event rates are scaled down to a manageable 3.5 kHz, while maintaining an average processing time of around 40 ms.
    \item \textbf{Event Filter}: This serves as the final layer of online event selection. 
    Adopting offline analysis techniques on completely assembled events, it filters the event rate down to approximately 200 Hz. 
    This stage, with an average processing duration of about 4 seconds, 
    ensures only the most pertinent events are stored for further offline scrutiny. 
    The algorithms deployed within the HLT use the full detail of the calorimeter, muon chamber data, 
    and data from the inner detector to perfect their selections. 
    Improved data on energy deposition aids in refining threshold limits, 
    while inner detector track reconstruction considerably augments particle identification capabilities, 
    such as differentiating between electrons and photons.
\end{enumerate}

\begin{figure}[!htb]
    \centering
    \resizebox{0.9\columnwidth}{!}{%
        \includegraphics[width=0.47\textwidth]{figures/3/triggers}
        \includegraphics[width=0.47\textwidth]{figures/3/L1_trigger}
    }
    \caption{
        Left: A schematic view of the ATLAS trigger system\cite{ATLAS:triggers_image};
        Right: Block scheme of the first level trigger\cite{ATLAS:triggers_image}. 
    }
    \label{fig:3:triggers_image}
\end{figure}

Alongside the trigger system, the Data Acquisition System (DAQ)\cite{ATLAS:DAQ, ATLAS:General} plays a pivotal role,
as illustrated in Figure~\ref{fig:3:DAQ_image}. 
It is the backbone that oversees the capture, buffering, 
and transmission of event data from the specialized readout hardware at the L1 trigger rate. 
Through dedicated point-to-point Readout Links (ROL's), the DAQ dispatches data requested by the L2 trigger, 
predominantly related to the RoI's. 
Upon fulfilling L2 selection criteria, events are compiled and then shuttled by the DAQ to the event filter. 
Events that pass this stringent filter are then preserved in a permanent storage system.

Additionally, the DAQ is not just a passive data handler. 
It actively aids in orchestrating the configuration, control, and oversight of the entire ATLAS detector during data collection phases. 
For hands-on supervision of the myriad elements of the detector infrastructure, like gas systems and power supplies, 
the Detector Control System (DCS) is brought into play.

In summary, the interplay of the multi-layered trigger system and 
the DAQ ensures the ATLAS experiment efficiently manages the vast influx of data, 
making informed selections and effectively archiving events of significance.

\begin{figure}[!htb]
    \centering
    \resizebox{\columnwidth}{!}{%
        \includegraphics[width=\textwidth]{figures/3/DAQ}
    }
    \caption{
        A schematic view of the ATLAS DAQ system\cite{ATLAS:DAQ}.
    }
    \label{fig:3:DAQ_image}
\end{figure}
