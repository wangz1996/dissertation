The complex array of accelerators at CERN has been meticulously constructed to prepare and accelerate beams of particles to unprecedented energies, with each accelerator playing a distinct role. 
The pipeline's efficiency ensures that particles, mainly protons, achieve near-light speeds, making them fit for the high-energy collisions that the LHC facilitates.
Followed the details of the accelerator system shown in figure~\ref{fig:3:lhc_accelerator}, 
the journey of the particles go through 5 main stages:
\begin{itemize}
    \item \textbf{Linear Accelerators}: Particles begin in the Linac 4, a linear accelerator. 
    Here, protons are derived from hydrogen gas by stripping away the electrons from hydrogen atoms. 
    These protons are then accelerated to an energy of about 160 MeV using radio-frequency quadrupole (RFQ) and drift tube linac (DTL) structures.    
    \item \textbf{Proton Synchrotron Booster (PSB)}: After Linac 4, the protons move to the PSB. 
    This is where they are grouped into bunches and further accelerated. 
    Using magnetic fields, the PSB boosts the protons to energies up to 2 GeV.
    \item \textbf{Proton Synchrotron (PS)}: The next phase of acceleration occurs in the PS. 
    A circular accelerator with a circumference of approximately 628 meters, the PS elevates the proton energies to 25 GeV. 
    The PS is also responsible for compressing the proton bunches, ensuring they're even tighter and more focused before they're passed onto the next stage.     
    \item \textbf{Super Proton Synchrotron (SPS)}: Acting as the final preparatory accelerator before the LHC, 
    the Super Proton Synchrotron is a large circular accelerator with a circumference of 7 kilometers. 
    Here, the proton bunches are further accelerated to energies of 450 GeV. 
    Beyond serving the LHC, the SPS has its own experimental halls and has been pivotal in numerous significant discoveries in particle physics over the years.    
    \item \textbf{Large Hadron Collider (LHC)}: The culmination of this sequential system is the LHC, where the protons, now moving at $99.9999991\%$ of the speed of light, undergo their final acceleration. 
    Within the LHC's 27-kilometer ring, the protons reach energies up to 6.5 TeV. 
    The LHC's twin beam pipes ensure that proton bunches can be accelerated and steered in opposite directions, 
    leading to head-on collisions at four main interaction points where the primary experiments are situated.    
\end{itemize}

\begin{figure}[!htb]
    \centering
    \resizebox{\columnwidth}{!}{%
        \includegraphics[width=0.9\textwidth]{figures/3/LHC_figure.png}
    }
    \caption{
        Diagram of CERN's sequential accelerator system\cite{Lopienska:2800984}.
    }
    \label{fig:3:lhc_accelerator}
\end{figure}


The complex infrastructure of the LHC's accelerators is characterized by a series of technical parameters, fundamental to understanding their capabilities. 
There's a summary of some basic beam parameters of the accelerators in table~\ref{tab:3:lhc_beam}.

The starting point, Linac 4, accelerates protons to an energy of 160 MeV. 
From here, they are directed to the Proton Synchrotron Booster (PSB), where their energy is further boosted to 2 GeV. 
The Proton Synchrotron (PS) then takes over, its 628-meter circumference serving to escalate the proton energy to 25 GeV. 
Beyond this, the Super Proton Synchrotron (SPS), with its vast 7-kilometer ring, elevates the energy of these protons to 450 GeV. 
Ultimately, the protons find their way to the LHC, where they are accelerated to a staggering 6.5 TeV per beam, resulting in a collision energy of 13 TeV.

In the LHC, approximately 2,808 bunches are operated per beam. 
Intriguingly, each of these bunches holds roughly $1.2 \times 10^{11}$ protons, leading to a dense traffic of particles that enhance the collision chances. 
The measure of this potential for collisions is captured by the concept of luminosity. 
For the LHC, its peak luminosity stands around $1 \times 10^{34} \mathrm{~cm}^{-2} \mathrm{~s}^{-1}$, 
a figure indicative of the LHC's exceptional capability to produce particle interactions.

Complementing this data, it's worth noting the LHC's revolution frequency, registering at about 11.245 kHz. 
When it comes to beam operations, after being injected into the LHC at 450 GeV, 
the energy of the beam is ramped up to its operational 6.5 TeV in a timespan of approximately 20 minutes.

The accelerator works at a chilling 1.9 K. 
This ultra-cold environment is essential to ensure the superconducting state of its magnets, 
which in turn produce a magnetic field of 8.33 T, a critical factor in directing the high-energy beams along the 27-km ring.

\begin{table}
    \centering
    \resizebox{0.9\columnwidth}{!}{%
        \begin{tabular}{lccccc}
            \toprule machine & $\mathrm{L}[\mathrm{m}]$ & relative & $\rho[\mathrm{m}]$ & beam momentum $[\mathrm{GeV} / \mathrm{c}]$ & bunches \\
            \midrule LINAC & 30 & & - & $10^{-4}$ & $4 \times 2$ \\
            PSB & 157 & & 8.3 & 0.05 & $4 \times 2$ \\
            PS & 628.318 & 1 & 70.676 & 1.4 & 72 \\
            SPS & 6911.56 & $11 \times$ PS & 741.257 & 26 & $4 \times 72$ \\
            LHC & 26658.883 & $27 / 7 \times$ SPS & 2803.98 & 450 & $2 \times 2808$ \\
            \bottomrule
        \end{tabular}
    }
    \caption{Circumference, curvature radius $\rho$, and beam momentum upon injection for the primary accelerators in the LHC injection sequence\cite{Brüning:1443022}.}
    \label{tab:3:lhc_beam}
\end{table}

The instantaneous luminosity is given by
\begin{equation}
    L=\frac{N_1 N_2 n_b f_{\text {rev }}}{\pi \sqrt{\left(\sigma_{x, 1}^2+\sigma_{x, 2}^2\right)} \sqrt{\left(\sigma_{y, 1}^2+\sigma_{y, 2}^2\right)}} F H,
\end{equation}
where:
\begin{itemize}
    \item $f_{\text{rev}}$ is the revolution frequency,
    \item $n_b$ is the number of bunches colliding at the IP,
    \item $N_{1,2}$ represent the number of particles in each bunch, 
    \item $\sigma_{x, 1,2}$ and $\sigma_{y, 1,2}$ are the horizontal and vertical beam sizes of the colliding bunches respectively,
    \item $F$ signifies the geometric luminosity reduction factor due to transverse offset or crossing angle collisions at the IP,
    \item $H$ denotes the reduction factor for the hourglass effect significant when the bunch length is similar or larger than the β-functions at the IP.
\end{itemize}

Assuming round beams at the IP and neglecting any spurious dispersion at the IP one can write the instantaneous luminosity in the LHC IPs as:
\begin{equation}
L=\frac{\gamma f_{\mathrm{rev}} n_b N_b^2}{4 \pi \epsilon_n \beta^*} F .
\end{equation}
Maximizing the instantaneous luminosity in the LHC therefore implies (in order of priority):
\begin{itemize}
    \item Ensure optimum overlap of the two beams at the IP, which for head-on collisions entails matching the optics functions and steering the beam orbits transversely.
    \item Minimize beam size at the IPs. This doesn't necessarily increase total beam power but demands adequate aperture. 
    \item Increase the number of particles per bunch.
    \item Maximize the number of bunches in the collider. In the LHC, operating with more than 150 bunches necessitates a crossing angle at the IP to prevent undesired parasitic beam interactions. 
\end{itemize}