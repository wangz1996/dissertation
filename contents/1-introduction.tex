\chapter{Introduction}

What makes up the very fabric of the universe? How do its smallest constituents interact to form the world as we understand it? These overarching questions are central to the field of particle physics, which investigates the fundamental particles that make up matter. High-energy colliders, machines that accelerate particles to nearly the speed of light and smash them together, offer unique opportunities to address these questions by allowing scientists to study particle interactions in conditions that mimic the early universe.

The Standard Model of particle physics, a well-established theory developed over the past half-century, successfully explains these interactions. It provides a framework for understanding how known particles, like quarks and electrons, interact through fundamental forces—namely, the electromagnetic, weak, and strong forces. The discovery of the Higgs boson in 2012 at CERN's Large Hadron Collider, a 27-kilometer ring beneath the France-Switzerland border, provided the capstone to this model. The Higgs boson explains why some particles have mass, affirming the monumental success of the Standard Model.

Despite these successes, there are phenomena that cannot be fully accounted for by the Standard Model alone, such as dark matter, a mysterious substance that makes up about 27\% of the universe but doesn't emit, absorb, or reflect any electromagnetic radiation. These gaps in understanding call for exploration into Beyond the Standard Model (BSM) physics. One interesting avenue is the study of long-lived particles (LLPs), which are hypothetical particles that do not decay as rapidly as others and could possibly interact weakly with normal matter.

In response to the challenges and opportunities posed by these questions, scientists have deployed the Large Hadron Collider and are planning future machines known as lepton colliders. These include projects like the International Linear Collider (ILC), the Circular Electron-Positron Collider (CEPC), and the Future Circular Collider (FCC-ee). These colliders aim to provide more precise measurements of the Higgs boson and to explore potential new physics.

The discovery of the Higgs boson marks a monumental milestone in particle physics, serving as both a confirmation of the Standard Model and a gateway to further explorations. My research contributes to the next crucial phase of this field by focusing on di-Higgs search, where two Higgs bosons are produced simultaneously. This is a pivotal area of study to understand unique aspects of the Higgs, particularly its self-coupling, which governs how the Higgs boson interacts with itself. My analyses particularly center on events with multi-lepton and \bbtt final states.

Additionally, to provide a more comprehensive understanding of the Higgs sector, my work incorporates a combination of both single- and double-Higgs production events. This integrated approach aims to constrain the Higgs boson self-coupling with increased precision, thereby offering critical insights into both Standard Model and Beyond the Standard Model physics.

Beyond the scope of di-Higgs and single-Higgs analyses, my research extends to BSM phenomena, particularly focusing on LLPs. Utilizing machine learning techniques that directly interpret raw detector data, I've developed a novel methodology for distinguishing these hypothetical particles from Standard Model particles in simulations.

Finally, while the main body of this dissertation focuses on collider-based experiments, my interests extend to other forms of particle physics research. Specifically, I have worked on an experiment aiming to detect dark photons, particles that are proposed as force carriers for dark matter, using a fixed-target experiment that utilizes high-energy electron beams.

In sum, this dissertation offers a coherent narrative of my Ph.D. research journey. It spans from probing fundamental questions in particle physics to making significant contributions in both collider-based Higgs boson studies and BSM long-lived particle searches, while also venturing into experiments beyond colliders. Each of these research endeavors adds a piece to the complex puzzle of understanding the universe at its most basic level.
This dissertation is organized as follows:
\begin{itemize}
    \item \textbf{Chapter~\ref{chap:2}:} Provides an overview of Higgs physics within the Standard Model and beyond.
    \item \textbf{Chapter~\ref{chap:3}:} Discusses the Large Hadron Collider and the ATLAS experiment.
    \item \textbf{Chapter~\ref{chap:4}:} Focuses on event simulation and reconstruction techniques in ATLAS.
    \item \textbf{Chapter~\ref{chap:5}:} Presents SM di-Higgs searches in multi-lepton final states.
    \item \textbf{Chapter~\ref{chap:6}:} Covers SM di-Higgs searches in \(bb\tau\tau\) final states.
    \item \textbf{Chapter~\ref{chap:7}:} Aims to constrain the Higgs boson self-coupling using data from single- and double-Higgs production.
    \item \textbf{Chapter~\ref{chap:8}:} Discusses search strategies for long-lived particles with future lepton colliders.
    \item \textbf{Chapter~\ref{chap:9}:} Summarizes the findings and discusses future prospects.
\end{itemize}
