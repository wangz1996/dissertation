The analytical likelihood function is expressed as:

\begin{equation}
\mathcal{L}(\vec{\mu}, \vec{\theta}; \text{data}) = 
\prod_{c=1}^{N_{\text{cat}}} \mathcal{L}_{c}(\vec{\mu}, \vec{\theta}; \text{data})
\prod_{k\in \text{NP constraints}} g_{k}(\theta_{k})
\end{equation}

Here, \(\vec{\mu}\) and \(\vec{\theta}\) symbolize the vectors for the parameters of interest (POIs) and nuisance parameters (NPs), respectively. \(N_{\text{cat}}\) represents the total number of categories, and \(\mathcal{L}_{c}\) signifies the likelihood for each specific category \(c\). Constraint terms for some of the NPs are represented by \(g_{k}\). Further elucidation of the likelihood template can be found in Ref.~\cite{HistFactory}.

POIs can encompass diverse parameters like the signal strength, denoted as \(\mu\), and coupling variables such as \kl and \kvv. The NPs are either unconstrained, i.e., determined solely by data, or constrained through auxiliary measurements. 
In each category \(c\), the likelihood is a product of individual Poisson distributions for each bin:
\begin{equation}
\label{eq:likelihood_cat}
\mathcal{L}_c(\vec{\mu}, \vec{\theta}; \text{data}) = \prod_{i=1}^{n_{\text{bin}}} P\left(\sum_{s} N^{c}_{S_s}(\vec{\mu}) + \sum_{b} N^{c}_{B_b}, n_i\right)
\end{equation}

The Poisson distribution uses \(n_i\) as the observed event count for each bin, and \( \sum_{s} N^{c}_{S_s}(\vec{\mu}) + \sum_{b} N^{c}_{B_b}\) as the sum of signal and background yields.

The profile likelihood ratio, \(\Lambda(\mu)\), for statistical tests is formulated as:

\begin{equation}
    \Lambda(\mu) = \frac{\mathcal{L}(\mu, \hat{\hat{\theta}}(\mu))}{\mathcal{L}(\hat{\mu}, \hat{\theta})}
\end{equation}

Here, the terms \(\hat{\hat{\theta}}\) and \(\hat{\theta}\) are the profiled values of the NPs that maximize the likelihood conditionally and unconditionally, respectively. 

In the regime of large sample sizes, the test statistic \( -2\ln\Lambda(\mu) \) asymptotically follows a \(\chi^{2}\) distribution. The degrees of freedom (d.o.f) of this distribution correspond to the dimensionality of the parameter vector \(\vec{\mu}\). This is predicated on the central limit theorem, which states that the sampling distribution of the likelihood will approach a Gaussian distribution as the sample size increases. This Gaussian approximation allows the transformation \( -2\ln\Lambda(\mu) \) to adhere to a \(\chi^{2}\) distribution, facilitating hypothesis testing for the parameters of interest (POIs).

The \(CL_s\) method~\cite{CLs} is employed to establish upper limits at a 95\% Confidence Level (CL) on the \(\HH\) signal strength \(\mu_{HH}\) and the associated production cross-section. \(CL_s\) is a modified frequentist approach for hypothesis testing that avoids the issue of overly-optimistic exclusion limits, a problem encountered in the traditional \(CL\) methodology. It does so by normalizing the \(p\)-value of the signal hypothesis by the \(p\)-value of the background-only hypothesis, thereby incorporating systematic uncertainties in a more conservative manner. 

The test statistic \(\tilde{q}_{\mu}\) utilized in the \(CL_s\) framework is formulated as:

\begin{equation}
    \tilde{q}_{\mu} = 
     \begin{dcases}
        - 2 \ln{\frac{\mathcal{L}(\mu, \hat{\hat{\vec{\theta}}}(\mu))}{\mathcal{L}(0, \hat{\vec{\theta}}(0))}} & \hat{\mu} < 0, \\
        - 2 \ln{\frac{\mathcal{L}(\mu, \hat{\hat{\vec{\theta}}}(\mu))}{\mathcal{L}(\hat{\mu}, \hat{\vec{\theta}})}} & 0 \leq \hat{\mu} \leq \mu, \\
        0  & \hat{\mu} > \mu.
     \end{dcases}
\end{equation}

The \(CL_s\) values are computed from the distribution of \(\tilde{q}_{\mu}\) under the signal-plus-background and background-only hypotheses, which in turn allows one to set data-driven upper limits.

