\subsubsection{CR-Only Fit}

In the analysis, a crucial step is the validation of the $Z+$HF background model in the CR. The default fit configuration includes floating normalisation factors for $Z+$HF and $\ttbar$ processes. No explicit normalisation uncertainties from modelling are considered.

Figure~\ref{fig:6:Z_CR_NP} illustrates the variations in the nuisance parameters (NPs) obtained from fitting the CR data. Notably, only a limited set of NPs are either pulled or constrained in the fit. Of particular significance is the constraining effect on the egamma energy resolution, which is a carry-over from the previous iteration of the analysis. Additionally, constraints on the $Z+$HF generator NP are imposed, owing to its substantial impact on the $m_{ll}$ distribution.

\begin{figure}[htb]
    \centering
    \includegraphics[width=0.85\textwidth]{figures/6/Z_CR_NPs_CR_only_MCttbar.pdf}
    \caption{Post-fit values of the NPs included in the fit of the di-lepton invariant mass distribution in the CR.}
    \label{fig:6:Z_CR_NP}
\end{figure}

The fitted normalisation factors are as follows:
\begin{align*}
\text{NF}_{Z+\text{HF}}  = 1.33 \pm 0.09, \\
\text{NF}_{\ttbar}  = 0.96 \pm 0.04. 
\end{align*}
A correlation of +72\% between these factors is mainly driven by the FTag systematics. The post-fit distributions in $m_{ll}$ and $p_{T}^{ll}$ are shown in Figure~\ref{fig:6:Z_CR_postfit_MCttbar}.

\begin{figure}[tb]
    \centering
    \subfloat[]{\includegraphics[width=0.45\textwidth]{figures/6/Z_CR_postfit_CR_only_MCttbar.pdf}}
    \subfloat[]{\includegraphics[width=0.45\textwidth]{figures/6/Z_CR_postfit_CR_only_MCttbar_pTLL.pdf}}
    \caption{Post-fit modelling of the di-lepton invariant mass (left) and \pT (right) distribution in the CR. In both cases the NP extracted from the fit to $m_{ll}$ are used.}
    \label{fig:6:Z_CR_postfit_MCttbar}
\end{figure}

\subsection{Data-driven $\ttbar$ Modelling}
As a cross-check to minimize reliance on simulations, a data-driven approach for the $\ttbar$ background is performed. A dedicated CR, the $e\mu$ CR, is defined Section~\ref{subsec:6:selection_redefinition_ZllCR}. The normalisation factor for $Z+$HF in this setup is $1.35 \pm 0.09$, in agreement with the default setup. The post-fit results are presented in Figure~\ref{fig:6:Z_CR_postfit_DDttbar}.

\begin{figure}[tb]
    \centering
    \subfloat[]{\includegraphics[width=0.37\textwidth]{figures/6/Z_CR_postfit_CR_only_DDttbar.pdf}}
    \subfloat[]{\includegraphics[width=0.60\textwidth]{figures/6/Z_CR_NPs_CR_only_DDttbar.pdf}}
    \caption{
        \textbf{a} Post-fit modelling of the di-lepton invariant mass distribution in the CR, when the \ttbar contribution is taken from a dedicated $e\mu$ CR. 
    \textbf{b} Post-fit values of the nuisance parameters included in the fit.
    }
    \label{fig:6:Z_CR_postfit_DDttbar}
\end{figure}

\FloatBarrier


\subsection{\bbtthadhad Fitting Results}

There are three distinct signal regions in the \bbtthadhad fit: low-\mHH ggF, high-\mHH ggF, and VBF, as well as the $Z+$ HF control region. In this fitting scheme, the normalisations for both \ttbar and $Z+$HF backgrounds are represented as freely floating NPs. For the purpose of the fit, the signal strength is fixed at zero.

% \subsection{Nuisance Parameter Ranking}
Figure~\ref{fig:6:NPrank-hadhad} depicts the ranking of the NPs based on the fit to the Asimov dataset. 

\begin{figure}[!htb]
    \centering
    \includegraphics[width=0.6\textwidth]{figures/6/results/fitcrosschecks/hadhad/Aug22nd/Ranking_postfit_SigXsecOverSM.pdf}
    \caption{Nuisance parameter ranking in the \bbtthadhad fit using the Asimov dataset.}
    \label{fig:6:NPrank-hadhad}
\end{figure}

The predominant uncertainties affecting the determination of signal strength are:
\begin{enumerate}
    \item Uncertainty on the single Higgs plus heavy flavour production.
    \item Scale uncertainty on the \HH cross-section.
    \item Monte Carlo statistical uncertainties, particularly in the high BDT bins of the high $m_{HH}$ region.
\end{enumerate}

It is worth mentioning that the expected data statistical uncertainty on the combined signal strength in all 3 SRs is estimated to be 1.15, which is significantly larger than all these contributions.

Figure~\ref{fig:6:corrNoMCstat-hadhad} portrays the correlations among the highly correlated\footnote{Here, 'highly correlated' refers to NPs having a correlation greater than 25\% with at least one other NP.} nuisance parameters as derived from the \bbtthadhad fit to both the Asimov dataset and the actual data. It is important to note that the correlations concerning Monte Carlo (MC) statistical uncertainties are deliberately excluded from this representation. An analogous degree of NP correlation is noticeable between the two fit configurations, further solidifying the robustness of the model.

\begin{figure}[!htb]
    \centering
    \includegraphics[width=0.49\textwidth]{figures/6/results/fitcrosschecks/hadhad/Aug22nd/Cov_HighCorr_Asimov.pdf}
    \includegraphics[width=0.49\textwidth]{figures/6/results/fitcrosschecks/hadhad/Aug22nd/Cov_HighCorr_Data.pdf}
    \caption{Correlations among NPs in the \bbtthadhad SM fit to the Asimov dataset (left) and real data (right). MC statistical uncertainties are purposefully omitted.}
    \label{fig:6:corrNoMCstat-hadhad}
\end{figure}

Finally, blinded post-fit distributions of the fitted variables in the 4 regions are shown in Figure~\ref{fig:6:postfitBDT_HadHad}. The choice of the binning is discussed in Section~\ref{subsec:binning}.

\begin{figure}[tbh!]
\centering
\subfloat[low-\mHH ggF SR]{\includegraphics[width=0.45\textwidth]{figures/6/results/fitcrosschecks/hadhad/Aug22nd/Region_distSMBDT_DLLOSGGFSR_BMax350_BMin0_T2_L0_SpcTauHH_Y6051_GlobalFit_conditionnal_mu0log.pdf}}
\subfloat[high-\mHH ggF SR]{\includegraphics[width=0.45\textwidth]{figures/6/results/fitcrosschecks/hadhad/Aug22nd/Region_distSMBDT_DLLOSGGFSR_BMin350_T2_L0_SpcTauHH_Y6051_GlobalFit_conditionnal_mu0log.pdf}} \\
\subfloat[VBF SR]{\includegraphics[width=0.45\textwidth]{figures/6/results/fitcrosschecks/hadhad/Aug22nd/Region_distSMBDT_DLLOSVBFSR_BMin0_T2_L0_SpcTauHH_Y6051_GlobalFit_conditionnal_mu0log.pdf}}
\subfloat[CR]{\includegraphics[width=0.45\textwidth]{figures/6/results/fitcrosschecks/hadhad/Aug22nd/Region_distmLL_DZllbbCR_BMin0_T2_L2_Y6051_GlobalFit_conditionnal_mu0.pdf}}
\caption{Post-fit BDT score distributions in the \bbtthadhad signal regions.}
\label{fig:6:postfitBDT_HadHad}
\end{figure}


\FloatBarrier

