\subsubsection{Electrons}

Electron candidates in the ATLAS experiment undergo a multi-criteria reconstruction and identification process, as summarized below~\cite{PERF-2013-03}:
\begin{itemize}
    \item \textbf{Identification Criteria:} 
    \begin{itemize}
        \item Track properties: Imposed requirements on measured track attributes.
        \item Calorimetric clustering: Conditions on energy deposit cluster shape.
        \item Track-to-cluster matching: Quality metrics for association.
        \item Track quality: Additional hit requirements.
    \end{itemize}
    
    \item \textbf{Likelihood Technique:} Utilizes a \textit{loose} working point to achieve \(95\%\) electron identification efficiency.

    \item \textbf{Kinematic Requirements:}
    \begin{itemize}
        \item \(p_T > 7 \, \text{GeV}\)
        \item \( | \eta | < 2.47 \) (excluding \(1.37 < | \eta | < 1.52\))
    \end{itemize}

    \item \textbf{Isolation Criteria:}
    \begin{itemize}
        \item \textit{fixed cut loose} working point.
        \item \(p_T\)-dependent upper bounds on track momenta and topo-clusters.
    \end{itemize}

    \item \textbf{Trigger Specifics:}
    \begin{itemize}
        \item LTT involvement necessitates \textit{tight} isolation working point.
        \item Trigger scale factors only available for \textit{tight} isolation electrons.
    \end{itemize}
\end{itemize}


\subsubsection{Muons}

Muon candidates undergo specific reconstruction and identification procedures, as outlined below~\cite{PERF-2015-10}:
\begin{itemize}
    \item \textbf{Track Reconstruction:}
    \begin{itemize}
        \item Inner Detector (ID): Independent track reconstruction.
        \item Muon Spectrometer (MS): Independent track reconstruction.
        \item Minimum hit requirements: Enforced in both ID and MS.
        \item Combined fit: Utilizes both ID and MS for momentum refinement.
    \end{itemize}
    
    \item \textbf{Kinematic Criteria:}
    \begin{itemize}
        \item \(p_T > 7 \, \text{GeV}\)
        \item \( |\eta| < 2.7 \)
    \end{itemize}

    \item \textbf{Identification:} 
    \begin{itemize}
        \item Working Point: Required to pass \textit{loose} identification criteria.
    \end{itemize}

    \item \textbf{Isolation:} 
    \begin{itemize}
        \item Criteria: Required to pass \textsf{PflowLoose\_VarRadIso}~\cite{isoWP}.
        \item Inversion: Performed for background control region establishment.
    \end{itemize}
\end{itemize}


\subsubsection{hadronic-\(\tau\) Leptons}

\begin{itemize}    
    \item \textbf{Reconstruction}~\cite{Aad:2014rga}:
    \begin{itemize}
        \item Seeding: Jets formed via \antikt algorithm, \( \Delta R = 0.4 \).
        \item BDT Classification: Sorts tracks into core and isolation tracks within \( \Delta R = 0.4 \) of \tauhadvis axis.
        \item Prongs: Defined by the number of core (charged) tracks.
    \end{itemize}
    
    \item \textbf{Identification}~\cite{ATL-PHYS-PUB-2019-033}:
    \begin{itemize}
        \item RNN Classifier: Utilized for discrimination against jet-like signatures.
        \item Algorithms: Specific for 1-prong and 3-prong \tauhadvis.
        \item Working Points: \( \text{Efficiency} = 85\% \) for 1-prong, \( 75\% \) for 3-prong.
    \end{itemize}

    \item \textbf{Selection Criteria:}
    \begin{itemize}
        \item \( p_T > 20 \, \text{GeV} \)
        \item \( |\eta| < 2.5 \) and veto region \(1.37 < |\eta| < 1.52\)
        \item Tracks: One or three.
        \item Charge: Unit charge.
        \item Working Point: \textit{loose} with 85\% efficiency for 1-prong and 75\% for 3-prong.
    \end{itemize}

    \item \textbf{Additional Rejection:} Against electrons.
    \begin{itemize}
        \item BDT: Utilizing track and shower shape information.
        \item Efficiency: Approximately 95\% for true \(\tau_{\text{had-vis}}\).
    \end{itemize}

    \item \textbf{Anti-\(\tau_{\text{had}}\) Definition}~\cite{Betti:2743097,HDBS-2018-40}:
    \begin{itemize}
        \item Fail RNN loose \(\tau_{\text{had}}\)-ID with an RNN score \( > 0.01 \).
        \item Defined by the Fake-Tau-Task-Force, used for background estimation.
        \item Efficiency: Approximately 99\% for true-\(\tau_{\text{had}}\) in \(\gamma^*\rightarrow \tau\tau\) events.
    \end{itemize}
    
    \item \textbf{Anti-\(\tau_{\text{had}}\) Selection}~\cite{Betti:2743097}:
    \begin{itemize}
        \item Selected in events where the number of offline \(\tau_{\text{had}}\) passing the \(\tau_{\text{had-ID}}\) is less than the channel-specific requirement (e.g., one for the \bbttlephad channel, two for the \bbtthadhad channel).
        \item Requirement: Ensures total number of selected \(\tau_{\text{had}}\) (loose and anti-\(\tau_{\text{had}}\)) corresponds to the required multiplicity.
        \item Trigger-Level Matching: For channels with \(\tau_{\text{had}}\)-ID at the trigger level, only matched anti-\(\tau_{\text{had}}\) are considered.
        \item Random Selection: Employed when multiple anti-\(\tau_{\text{had}}\) satisfy criteria and a \(\tau_{\text{had}}\) trigger is not used.
    \end{itemize}
\end{itemize}

\subsubsection{Jets}
Jets are reconstructed using the \antikt algorithm~\cite{Cacciari:2008gp} with a distance parameter \(R=0.4\), and further refined using the Particle-Flow (PF) algorithm~\cite{Aaboud:2017aca}. Jet cleaning is performed to mitigate the effects of non-collision backgrounds (NCB) and calorimeter noise, utilizing the jet cleaning algorithm based on certain working points~\cite{ATL-DAQ-PUB-2016-001}. The jet-vertex-tagger (JVT) algorithm is applied for pile-up suppression, specifically with a JVT threshold that varies based on \(p_{T}\) of the jet~\cite{ATL-PHYS-PUB-2014-021}.

Calibrations for the Jet Energy Scale (JES) and Jet Energy Resolution (JER) are meticulously conducted, adhering to the protocols established in the previously cited work\cite{Aaboud:2017aca}. 
Additional corrections for pile-up and underlying event effects are also applied~\cite{ATL-PHYS-PUB-2015-015}.

\subsubsection{\(b\)-jet Identification and Calibration}
\(b\)-jets are identified using a deep learning-based algorithm, the DL1r tagger~\cite{FTAG-2018-01,ATL-PHYS-PUB-2017-013}. The tagger utilizes jet kinematics, impact parameters, and the presence of displaced vertices to discriminate between \(b\)-jets and lighter jets. It takes inputs from a recurrent neural network for impact parameter scoring (RNNIP)~\cite{ATL-PHYS-PUB-2017-003}.

The analysis utilizes pseudo-continuous \(b\)-tagging, applying multiple working points corresponding to 77\%, 70\%, and 60\% \(b\)-tagging efficiencies, following internal documentation~\cite{Betti:2743097}. These working points are used for multiple discriminant analysis (MDA) optimizations.

\begin{figure}[tbh!]
    \centering
    \begin{subfigure}[b]{0.47\textwidth}
        \includegraphics[width=\textwidth]{figures/6/pcbt/Dl1r_quant_SR}
        \caption{\label{fig:6:pcbt:SR}}
    \end{subfigure}
    \quad
    \begin{subfigure}[b]{0.47\textwidth}
        \includegraphics[width=\textwidth]{figures/6/pcbt/DL1r_quant_bkg}
        \caption{\label{fig:6:pcbt:bkg}}
    \end{subfigure}
    \caption{Sub-leading \(b\)-jet DL1r quantiles. Figure (a) shows the SM ggF, ggF (\(\kappa_{\lambda} = 10.0\)) and SM VBF signals, along with the sum of the backgrounds. Figure (b) shows the same variable for each background component. }
    \label{fig:6:pcbt}
\end{figure}

Shape comparison plots of the DL1r quantiles of the sub-leading \(b\)-tagged jet are shown in Figure~\ref{fig:6:pcbt:SR} for the SM ggF, ggF (\(\kappa_{\lambda} = 10.0\)) and SM VBF signals, along with the sum of the backgrounds. Figure~\ref{fig:6:pcbt:bkg} shows the same variable for each background component. 

The \(b\)-jet energy scale (bJES) and \(b\)-jet energy resolution (bJER) are specifically calibrated based on Ref.~\cite{Betti:2743097}. Scale factors (SF) for \(b\)-tagging efficiencies are obtained from the CDI file \myverb{2020-21-13TeV-MC16-CDI-2021-04-16\_v1.root}~\cite{FTAG-2018-01}.

\subsubsection{Missing Transverse Momentum (\MET)}

The \MET is determined as the negative of the accumulated transverse momentum vectors of calibrated leptons, \(\tau\)-leptons decaying via hadronic channels, and jets. Additionally, a `soft term' is included, computed as the vector sum of the transverse momenta (\(p_{T}\)) of tracks that are linked to the primary vertex but are not connected to any recognized lepton or jet~\cite{Aaboud:2018tkc}.

\subsubsection{Overlap Removal}

\begin{table}[!htb]
    \centering
    \resizebox{0.99\columnwidth}{!}{
        \begin{tabular}{ccc l}
            \toprule
            Pair & Condition & Action & Notes \\
            \midrule
            \(e_1\) - \(e_2\) & Shared track & Reject \(e_1\) if \(p_{T1} < p_{T2}\) & \\
            
            \(\tau_{\text{had-vis}}\) - \(e\) & \(\Delta R_y < 0.2\) & Reject \(\tau_{\text{had-vis}}\) & \(e\) must pass 
            \myverb{DFCommonElectronsLHLoose} \\
            
            \(\tau_{\text{had-vis}}\) - \(\mu\) & \(\Delta R_y < 0.2\) & Reject \(\tau_{\text{had-vis}}\) & Case 1: \(p_{T,\tau} > 50 \, \text{GeV}\), \(p_{T,\mu} > 2 \, \text{GeV}\) and combined \( \mu \) \\
            & & & Case 2: \(p_{T,\tau} \leq 50 \, \text{GeV}\), \(p_{T,\mu} > 2 \, \text{GeV}\) \\
            
            \(\mu\) - \(e\) & Calo-muon & Reject \(\mu\) & Shared ID track \\
            
            \(e\) - \(\mu\) & -- & Reject \(e\) & Shared ID track \\
            
            jet - \(e\) & \(\Delta R_y < 0.2\) & Reject jet & \\
            
            \(e\) - jet & \(\Delta R_y < 0.4\) & Reject \(e\) & \\
            
            jet - \(\mu\) & \(N_{\text{track}} < 3\) or \(\Delta R_y < 0.2\) & Reject jet & \(p_{T, \text{track}} > 500 \, \text{MeV}\) \\
            
            \(\mu\) - jet & \(\Delta R_y < 0.4\) & Reject \(\mu\) & \\
            \midrule
            jet - \(\tau_{\text{had-vis}}\) & \(\Delta R_y < 0.2\) & Reject jet & Analysis-specific \\
            
            anti-\(\tau_{\text{had-vis}}\) - jet & \(\Delta R_y < 0.2\) & Reject anti-\(\tau\) & Only if jet is \textit{b-tagged} \\
            
            jet - anti-\(\tau_{\text{had-vis}}\) & \(\Delta R_y < 0.2\) & Reject jet & \\
            \bottomrule
            \end{tabular}
    }
    \caption{Summary of overlap-removal procedures with the standard working point.}
    \label{tab:6:obj_removal}
\end{table}

After event reconstruction, an overlap-removal algorithm is utilized to address instances where a single physical object is identified as multiple particle types in the ATLAS detector. The algorithm measures the angular separation between two reconstructed objects using the metric \(\Delta R_y = \sqrt{\Delta y^2 + \Delta \phi^2}\).

For most particle combinations, a standard toolkit called \myverb{AssociationUtils} is employed with a default working point~\cite{olrWorkingPoints}. However, the overlap between reconstructed \(\tau_{\text{had, vis}}\) and jets follows a unique, analysis-specific procedure. Specialized overlap-removal algorithms are also applied to \(\tau_{\text{had, vis}}\), anti-\(\tau_{\text{had, vis}}\), and jets, adhering to a predefined priority sequence (\(\tau_{\text{had, vis}} > \) anti-\(\tau_{\text{had, vis}} > \) jets).
The summary of the overlap removal steps and the standard working point is presented in Table~\ref{tab:6:obj_removal}.

