In total, four sets of BDTs are utilized in the \bbtthadhad channel, described below.

\subsubsection{BDT for ggF and VBF Classification}
\label{subsubsec:BDTClassification-hadhad}

The purpose of the ggF/VBF distinction BDT is to segregate SM ggF HH events from SM VBF HH events (see Section~\ref{sec:6:event_categorization} for details). 
The BDT employs only VBF preselected events in both its training and application phases. VBF preselected events are characterized as those having a minimum of two jets that are distinct from the $H\rightarrow bb$ jets. Approximately 50\% of ggF HH events and 80\% of VBF HH events meet this VBF preselection criterion. The BDT is calibrated such that events resembling ggF have scores approaching 1.0, whereas events similar to VBF have scores nearing -1.0. A specific score threshold (or working point) is chosen to delineate events into the VBF category.
The methodology for input variable selection and hyperparameter tuning for BDT training is elaborated upon in the following sections.

\paragraph{Input Variable Selection}
The performance trajectory of the BDT in ggF/VBF classification is visualized in Figure \ref{fig:6:variable_opt_ggF_VBF}. The significance is calculated in accord with the binning methodology outlined in Section \ref{subsec:binning}, albeit without imposing a minimum threshold on the number of background events per bin. Here, $z_s = z_b = 7$.

\begin{figure}[tbh]
\centering
\includegraphics[angle=-90,width=0.3\textwidth]{figures/6/hadhad/input_variable_opt/opt_ggF_VBF}
\caption{Performance progression of the ggF/VBF classification BDT as supplementary variables are incrementally appended to the baseline variables, which consist of $m_\text{jj}$ and $\Delta \eta_\text{jj}$.}
\label{fig:6:variable_opt_ggF_VBF}
\end{figure}

As a foundation, two variables, $m_\text{jj}$ and $\Delta \eta_\text{jj}$, are selected. Ultimately, the seven most impactful variables are employed as BDT inputs. These include a Fox-Wolfram moment~\cite{Bernaciak_2013}, \mHH, $\Delta R_{\tau\tau}$, along with additional VBF-specific variables. A comprehensive listing of these input variables is provided in Table~\ref{tab:6:ggFVBFtrainVars}. Their respective distributions for the ggF and VBF HH signal events are illustrated in Figure \ref{fig:6:ggFVBFBDTvars}.

\begin{table}[!htb]
    \centering
    \resizebox{0.95\columnwidth}{!}{
        \begin{tabular}{c l}
            \toprule
            Variable & Description\\
            \midrule
            \mHH & Invariant mass of the HH system, reconstructed from the $\tau$-lepton pair (using the MMC) and $b$-tagged jet pair\\
            $\Delta R_{\tau\tau}$ & The $\Delta R$ between the two visible $\tau$ decay products\\
            VBF $\eta_0 \times \eta_1$ & Product of the pseudorapidities of the leading and sub-leading VBF jets\\
            $\Delta \eta_\text{jj}^{\mathrm{VBF}}$ & The $\Delta \eta$ between the two VBF jets\\
            $\Delta \phi\text{jj}^{\mathrm{VBF}}$ &The $\Delta \phi$ between the two VBF jets \\
            $m_\text{jj}^{\mathrm{VBF}}$ & Invariant mass of the VBF jet system\\
            fwm2$(\tau\tau jf)$ & $2^{nd}$ order Fox-Wolfram moment, taking into account the $\tau$-lepton pair and central and forward jets \\
            \bottomrule
            \end{tabular}
    }
    \caption{Input variables used for the ggF/VBF BDT training in the \bbtthadhad channel.}
    \label{tab:6:ggFVBFtrainVars}
\end{table}

\begin{figure}[tbh!]
    \centering
    \includegraphics[width=0.32\textwidth]{figures/6/hadhad/ggFVBFBDTvars/C_SvB_mHH}
    \includegraphics[width=0.32\textwidth]{figures/6/hadhad/ggFVBFBDTvars/C_SvB_dRTauTau}
    \includegraphics[width=0.32\textwidth]{figures/6/hadhad/ggFVBFBDTvars/C_SvB_fwm2_ttjf}\\
    \includegraphics[width=0.32\textwidth]{figures/6/hadhad/ggFVBFBDTvars/C_SvB_vbf_eta0eta1}
    \includegraphics[width=0.32\textwidth]{figures/6/hadhad/ggFVBFBDTvars/C_SvB_vbf_dEtajj}
    \includegraphics[width=0.32\textwidth]{figures/6/hadhad/ggFVBFBDTvars/C_SvB_vbf_dPhijj}\\
    \includegraphics[width=0.32\textwidth]{figures/6/hadhad/ggFVBFBDTvars/C_SvB_vbf_mjj}
    \caption{Histograms of input variables for the ggF/VBF BDT in the \bbtthadhad channel, illustrating the distinction between ggF HH events (signal in blue) and VBF HH events (background in red).}
    \label{fig:6:ggFVBFBDTvars}
\end{figure}
    

\paragraph{Hyperparameter Tuning}
\label{sec:ggFVBF_hyperparameter_tuning}

In accordance with the method described in Section \ref{subsec:mva_hyperparameter_tuning}, we focus our optimization efforts on the principal hyperparameters \textsf{NTrees} and \textsf{MaxDepth}. Figure \ref{fig:6:hyper_opt_ggF_VBF} illustrates how the binned significance, evaluated on the validation folds, varies with changes in these hyperparameters. Optimal parameter settings are identified by maximizing this performance metric.

For the other hyperparameters, a marginal impact on the BDT's performance is observed. Thus, they are assigned default values. A comprehensive enumeration of these hyperparameter choices is provided in Table \ref{tab:6:ggFVBFtrainParams_hyper}.

\begin{figure}[tbh]
\centering
\includegraphics[width=0.5\textwidth]{figures/6/hadhad/hpar_opt/hyper_ggF_VBF}
\caption{Variation of binned significance, as assessed on the validation partitions, with respect to the ensemble's tree count (\textsf{NTrees}) and each tree's maximum depth (\textsf{MaxDepth}).}
\label{fig:6:hyper_opt_ggF_VBF}
\end{figure}

\begin{table}[tbh!]
\centering
\begin{tabular}{c c}
\toprule
Hyperparameter & Selected Value\\
\midrule
\texttt{NTrees} & 109\\
\texttt{MaxDepth} & 6\\
MinNodeSize & 1\%\\
BoostType & GradBoost\\
IgnoreNegWeightsInTraining & True\\
\bottomrule
\end{tabular}
\caption{Chosen hyperparameters for the ggF/VBF BDT training in the \bbtthadhad channel.}
\label{tab:6:ggFVBFtrainParams_hyper}
\end{table}


\paragraph{Determination of Optimal Cut Threshold}

\begin{figure}[!tbh]
\centering
\includegraphics[width=0.45\textwidth]{figures/6/hadhad/7_var}
\includegraphics[width=0.45\textwidth]{figures/6/hadhad/ggFVBF_sob_7var}
\caption{Dependence of HH signal strengths on the ggF vs VBF BDT cut value for the refined set of variables (left). The right figure shows the aggregate signal-to-background ratio also as a function of the cut value. A vertical dashed line marks the chosen cut value of 0.1.}
\label{fig:6:optimal_cut_threshold}
\end{figure}

The SRs for ggF and VBF are demarcated by applying a threshold cut on the ggF/VBF classification BDT output. To identify the optimal cut threshold, we evaluate the analysis sensitivity across multiple potential cut values. Figure~\ref{fig:6:optimal_cut_threshold} illustrates the influence of the cut value on the ggF, VBF, and inclusive HH signal strengths (left), along with the overall signal-to-background ratio (right). These are computed via a likelihood fit incorporating the three analysis categories as well as the Z CR. 

It should be noted that the discriminant scores used for this initial evaluation are derived from a provisional BDT training, prior to the application of the hyperparameter and input variable optimization procedures outlined in earlier sections. Various tests have confirmed that subsequent retraining does not materially alter the resulting significance.

Based on these evaluations, a cut value of 0.1 is selected for optimal sensitivity to both ggF and VBF HH signals.Additional analyses are conducted to compare the sensitivity of this optimized method with a more basic strategy that employs only the baseline variables $m_\text{jj}$ and $\Delta \eta_\text{jj}$. 


\paragraph{Score Distribution Analysis}

\begin{figure}[tbh!]
\centering
\includegraphics[width=0.65\textwidth]{figures/6/hadhad/ggFVBF_BDT_k2V}
\caption{Distribution of ggF/VBF BDT scores for various VBF processes under signal region preselection conditions.}
\label{fig:6:7var_k2v_analysis}
\end{figure}

Figure~\ref{fig:6:7var_k2v_analysis} illustrates the score distributions for multiple VBF processes under the preselection criteria in the signal region, utilizing the seven-variable ggF/VBF BDT setup. The histograms filled in blue and red correspond to SM ggF and VBF HH events, respectively. Variations in \kvv are represented by histograms in different colors. The robustness of the seven-variable ggF/VBF BDT in segregating ggF HH events from \kvv fluctuations is thereby demonstrated. The absence of a significant systematic trend in the non-SM VBF samples underscores the stable categorization performance of the BDT for VBF events.

\FloatBarrier

\subsubsection{SR-Specific BDTs}
\label{subsubsec:RegionSpecificBDTs-hadhad}

Within each of the three SRs, specialized BDTs are constructed to distinguish HH signal from the aggregate of all SM backgrounds.

For the low-\mHH region, the BDT is trained utilizing ggF events with $\kl=10$. Conversely, the high-\mHH region BDT employs the ggF SM signal sample for training. In both scenarios, training sets are restricted to events that meet the full SR criteria, including the previously applied ggF/VBF BDT cut.

The BDT aimed at the VBF region employs SM VBF HH signal events for its training. Importantly, events from both VBF and ggF SRs contribute to the training dataset to optimize statistical power. This approach has been verified to substantially enhance the VBF sensitivity when compared to training solely on VBF SR events.

Each BDT's hyperparameters and input variables are individually optimized for their respective regions. The resulting BDT scores are subsequently employed in a profile likelihood fit to derive the ultimate findings, elaborated in Section~\ref{sec:6:results}.


\paragraph{Hyperparameter Tuning for Signal Region BDTs}
\label{sec:SRhadhad_hyperparam_tuning}

The approach to hyperparameter optimization is akin to the methodology described in Section~\ref{sec:ggFVBF_hyperparameter_tuning}. The key hyperparameters \textsf{NTrees} and \textsf{MaxDepth} are specifically fine-tuned. The target for optimization is the binned validation significance, computed in accordance with the binning schema detailed in Section~\ref{subsec:binning}. The constraints for this are a minimum of 1.0 background events per bin, an upper limit on the MC background prediction's statistical uncertainty of 20\%, and the use of $z_s = 10$ and $z_b = 5$.

\begin{figure}[tbh]
    \centering
    \subfloat[low-\mHH]{\includegraphics[width=0.33\textwidth]{figures/6/hadhad/hpar_opt/hyper_ggF_low}}
    \subfloat[high-\mHH]{\includegraphics[width=0.33\textwidth]{figures/6/hadhad/hpar_opt/hyper_ggF_high}}
    \subfloat[VBF]{\includegraphics[width=0.33\textwidth]{figures/6/hadhad/hpar_opt/hyper_VBF}}
    \caption{Impact of the \textsf{NTrees} and \textsf{MaxDepth} hyperparameters on the validation significance for the respective BDT training sets in the \bbtthadhad channel.}
    \label{fig:6:hyperparam_dependency_srbdt}
    \end{figure}

\begin{table}[!htb]
    \centering
    \resizebox{0.95\columnwidth}{!}{
        \begin{tabular}{c c c c}
            \toprule
            Hyperparameter & low-\mHH ggF SR & high-\mHH ggF SR & VBF SR\\
            \midrule
            NTrees & 204 & 241 & 465\\
            MaxDepth & 2 & 3 & 3\\
            MinNodeSize & 1\% & 1\% & 1\%\\
            BoostType & Grad & Grad & Grad \\
            Shrinkage & 0.2 & 0.2 & 0.2\\
            IgnoreNegWeightsInTraining & True & True & True\\
            \bottomrule
            \end{tabular}
    }
    \caption{Training hyperparameters chosen for the BDTs used in the three \bbtthadhad analysis categories.}
    \label{tab:6:ggFVBFtrainParams}
\end{table}


Figure~\ref{fig:6:hyperparam_dependency_srbdt} provides a visualization of how \textsf{NTrees} and \textsf{MaxDepth} affect this metric across the three distinct BDT sets. Optimal hyperparameters were chosen based on their ability to maximize the validation significance. The chosen parameters are cataloged in Table~\ref{tab:6:ggFVBFtrainParams}.


\paragraph{Optimization of Input Variables}
\label{sec:optimized_input_variables}

\begin{figure}[!htb]
    \centering
    \resizebox{0.85\columnwidth}{!}{%
        \subfloat[low-\(m_{HH}\)]{\includegraphics[angle=-90,width=0.33\textwidth]{figures/6/hadhad/input_variable_opt/opt_ggF_low}}
        \subfloat[high-\(m_{HH}\)]{\includegraphics[angle=-90,width=0.33\textwidth]{figures/6/hadhad/input_variable_opt/opt_ggF_high}}
        \subfloat[VBF]{\includegraphics[angle=-90,width=0.33\textwidth]{figures/6/hadhad/input_variable_opt/opt_VBF}}
    }
    \caption{Trajectory of the validation significance as additional predictor variables are incorporated. The baseline set consists of \(m_{jj}\), \(m_{bb}\), \mMMC, \(\Delta R_{bb}\), and \(\Delta R_{\tau\tau}\). Red curves (\textsf{trafo60\_5\_total\_sig\_val}) represent binned significance with at least 5.0 expected background events per bin, whereas black curves (\textsf{trafo60\_total\_sig\_val}) require only a minimum of 1.0 expected background events per bin.}
    \label{fig:6:combined_optimization}
\end{figure}
    
In line with the iterative optimization strategy delineated in Section~\ref{subsec:mva_optimized_input_variables}, we begin with a set of five foundational variables: \(m_{jj}\), \(m_{bb}\), \mMMC, \(\Delta R_{bb}\), and \(\Delta R_{\tau\tau}\). Due to statistical constraints imposed by the limited size of the background MC datasets, we utilize a validation significance metric that employs a coarser binning, ensuring a minimum of 5.0 expected background events per bin. This is visualized alongside a finer binning strategy in Figures~\ref{fig:6:combined_optimization}.

Especially in the high-\mHH regime, noticeable statistical fluctuations are apparent in the early optimization phases when employing the finer binning. These instabilities stabilize as more variables are incorporated. Ultimately, the top $N$ predictors are chosen for the final model, where $N$ is determined by the point at which performance stabilization is achieved in both binnings under investigation.

The finalized input sets for the individual BDTs are cataloged in Table~\ref{tab:6:ggFlowtrainVars}, Table~\ref{tab:6:ggFhightrainVars} and Table~\ref{tab:6:VBFtrainVars}. 

\input{contents/6-hhbbtautau/tables/mva-hadhad-variables.tex}

The subsequent illustrations present a characteristic ensemble of pre-fit MVA predictor variables for each signal region in the \bbtthadhad channel. In these graphical representations, background adjustments are performed using scaling coefficients derived from a comprehensive fit to empirical data. Figure~\ref{fig:prefit_inputVars_hadhad1} delineates the distribution of essential variables across the triad of signal regions. Concurrently, Figure~\ref{fig:prefit_inputVars_hadhad2} introduces novel variables specifically tailored for the discrimination of signal from background through BDT in each designated region. Lastly, Figure~\ref{fig:prefit_inputVars_hadhad3} showcases the BDT employed for the stratification between ggF and VBF signal domains, restricting the analysis to events featuring a minimum of four jets.

\begin{figure}[tb]
    \centering
    \subfloat[low-\mHH ggF SR ]{\includegraphics[width=0.32\textwidth]{figures/6/hadhad/C_2tag_0_350mHH_LL_OS_GGFSR_dRTauTau.pdf}}
    \subfloat[high-\mHH ggF SR ]{\includegraphics[width=0.32\textwidth]{figures/6/hadhad/C_2tag_350mHH_LL_OS_GGFSR_dRTauTau.pdf}}
    \subfloat[VBF SR]{\includegraphics[width=0.32\textwidth]{figures/6/hadhad/C_2tag_350mHH_LL_OS_VBFSR_dRTauTau.pdf}} \\
    
    \subfloat[low-\mHH ggF SR ]{\includegraphics[width=0.32\textwidth]{figures/6/hadhad/C_2tag_0_350mHH_LL_OS_GGFSR_mBB.pdf}}
    \subfloat[high-\mHH ggF SR ]{\includegraphics[width=0.32\textwidth]{figures/6/hadhad/C_2tag_350mHH_LL_OS_GGFSR_mBB.pdf}}
    \subfloat[VBF SR]{\includegraphics[width=0.32\textwidth]{figures/6/hadhad/C_2tag_350mHH_LL_OS_VBFSR_mBB.pdf}} \\
    
    
    \subfloat[low-\mHH ggF SR ]{\includegraphics[width=0.32\textwidth]{figures/6/hadhad/C_2tag_0_350mHH_LL_OS_GGFSR_mHH.pdf}}
    \subfloat[high-\mHH ggF SR ]{\includegraphics[width=0.32\textwidth]{figures/6/hadhad/C_2tag_350mHH_LL_OS_GGFSR_mHH.pdf}}
    \subfloat[VBF SR]{\includegraphics[width=0.32\textwidth]{figures/6/hadhad/C_2tag_350mHH_LL_OS_VBFSR_mHH.pdf}} \\
    
    \subfloat[low-\mHH ggF SR ]{\includegraphics[width=0.32\textwidth]{figures/6/hadhad/C_2tag_0_350mHH_LL_OS_GGFSR_b0_corr_pt.pdf}}
    \subfloat[high-\mHH ggF SR ]{\includegraphics[width=0.32\textwidth]{figures/6/hadhad/C_2tag_350mHH_LL_OS_GGFSR_b0_corr_pt.pdf}}
    \subfloat[VBF SR]{\includegraphics[width=0.32\textwidth]{figures/6/hadhad/C_2tag_350mHH_LL_OS_VBFSR_b0_corr_pt.pdf}}
    \caption{Representative set of pre-fit MVA input variable distributions in the \bbtthadhad SRs.}
    \label{fig:prefit_inputVars_hadhad1}
\end{figure}
    
    
\begin{figure}[tb]
    \centering
    \subfloat[low-\mHH ggF SR ]{\includegraphics[width=0.32\textwidth]{figures/6/hadhad/C_2tag_0_350mHH_LL_OS_GGFSR_MT2.pdf}}
    \subfloat[low-\mHH ggF SR ]{\includegraphics[width=0.32\textwidth]{figures/6/hadhad/C_2tag_0_350mHH_LL_OS_GGFSR_quant_b1.pdf}}
    \subfloat[low-\mHH ggF SR ]{\includegraphics[width=0.32\textwidth]{figures/6/hadhad/C_2tag_0_350mHH_LL_OS_GGFSR_spher_bbtt.pdf}}\\
    
    \subfloat[high-\mHH ggF SR ]{\includegraphics[width=0.32\textwidth]{figures/6/hadhad/C_2tag_350mHH_LL_OS_GGFSR_n_jets.pdf}}
    \subfloat[high-\mHH ggF SR ]{\includegraphics[width=0.32\textwidth]{figures/6/hadhad/C_2tag_350mHH_LL_OS_GGFSR_T1.pdf}}
    \subfloat[high-\mHH ggF SR ]{\includegraphics[width=0.32\textwidth]{figures/6/hadhad/C_2tag_350mHH_LL_OS_GGFSR_pTBB.pdf}}\\
    
    \subfloat[VBF SR]{\includegraphics[width=0.32\textwidth]{figures/6/hadhad/C_2tag_350mHH_LL_OS_VBFSR_vbf_dRjj.pdf}}
    \subfloat[VBF SR]{\includegraphics[width=0.32\textwidth]{figures/6/hadhad/C_2tag_350mHH_LL_OS_VBFSR_vbf_mjj.pdf}}
    \subfloat[VBF SR]{\includegraphics[width=0.32\textwidth]{figures/6/hadhad/C_2tag_350mHH_LL_OS_VBFSR_vbf_eta0eta1.pdf}}
    \caption{Representative set of pre-fit MVA input variable distributions in the \bbtthadhad SRs.}
    \label{fig:prefit_inputVars_hadhad2}
\end{figure}
    
    
\begin{figure}[tb]
    \centering
    \subfloat[low-\mHH ggF SR ]{\includegraphics[width=0.32\textwidth]{figures/6/hadhad/C_2tag_0_350mHH_LL_OS_GGFSR_VBFBDT.pdf}}
    \subfloat[high-\mHH ggF SR ]{\includegraphics[width=0.32\textwidth]{figures/6/hadhad/C_2tag_350mHH_LL_OS_GGFSR_VBFBDT.pdf}}
    \subfloat[VBF SR]{\includegraphics[width=0.32\textwidth]{figures/6/hadhad/C_2tag_350mHH_LL_OS_VBFSR_VBFBDT.pdf}}
    
    \caption{Categorisation BDT variable distributions in the \bbtthadhad SRs.}
    \label{fig:prefit_inputVars_hadhad3}
\end{figure}

\FloatBarrier

\paragraph{BDT Score Distribution Components}

Figure~\ref{fig:6:had_had_yields} displays the constituent background yields in the bins most indicative of the signal, based on the binning scheme employed within the likelihood fitting procedure. Comprehensive enumerations of prefit background yields across all three SRs are itemized in Tables~\ref{tab:6:had_had_high_mHH_yields}, ~\ref{tab:6:had_had_low_mHH_yields} and ~\ref{tab:6:had_had_VBF_yields}.


\begin{figure}[tbh!]
    \centering
    \subfloat[low-\mHH ggF SR]{\includegraphics[width=0.33\textwidth]{figures/6/results/fitcrosschecks/hadhad/Aug22nd/Region_distSMBDT_DLLOSGGFSR_BMax350_BMin0_T2_L0_SpcTauHH_Y6051_Prefitlog.pdf}}
    \subfloat[high-\mHH ggF SR]{\includegraphics[width=0.33\textwidth]{figures/6/results/fitcrosschecks/hadhad/Aug22nd/Region_distSMBDT_DLLOSGGFSR_BMin350_T2_L0_SpcTauHH_Y6051_Prefitlog.pdf}}
    \subfloat[VBF SR]{\includegraphics[width=0.33\textwidth]{figures/6/results/fitcrosschecks/hadhad/Aug22nd/Region_distSMBDT_DLLOSVBFSR_BMin0_T2_L0_SpcTauHH_Y6051_Prefitlog.pdf}}
    \caption{Pre-fit BDT score distributions in the \bbtthadhad signal regions.}
    \label{fig:6:prefitBDTsmll}
\end{figure}

\begin{figure}[tb]
    \centering
    \subfloat[low-\mHH SR]{\includegraphics[width=0.33\textwidth]{figures/6/hadhad/binned_yields/13TeV_TauHH_2tag_0_350mHH_LL_OS_GGFSR_SMBDT.pdf}}
    \subfloat[high-\mHH SR]{\includegraphics[width=0.33\textwidth]{figures/6/hadhad/binned_yields/13TeV_TauHH_2tag_350mHH_LL_OS_GGFSR_SMBDT.pdf}}
    \subfloat[VBF SR]{\includegraphics[width=0.33\textwidth]{figures/6/hadhad/binned_yields/13TeV_TauHH_2tag_0mHH_LL_OS_VBFSR_SMBDT.pdf}}
    \caption{Prefit yields of the main background components, the total signal, as well as the total background in the most signal-like
      BDT bins in the \bbtthadhad SRs.}
    \label{fig:6:had_had_yields}
  \end{figure}
  

In specific scenarios, such as for the QCD fake background in the most signal-like bin within the high-\mHH SR, a prefit background projection may yield a negative value accompanied by significant statistical uncertainty. These negative yields pose no issue; they are adjusted to zero during the generation of the fitting workspace, with the corresponding uncertainties accurately integrated into the constraint term for the Beeston-Barlow nuisance parameter (NP) specific to that bin.

\input{contents/6-hhbbtautau/tables/mva-hadhad-yields.tex}

Figure~\ref{fig:6:prefitBDTsmll} illustrates the comparison between the data and model predictions for prefit BDT scores within the \bbtthadhad SRs. 
These regions are presently subject to blinding in the most sensitive bins, as elaborated in Section~\ref{sec:6:blinding_strategy}. 

\FloatBarrier

\subsubsection{ggF and VBF Classification: MVA versus Cut-Based}

To systematically assess the benefits of utilizing a BDT for differentiating ggF and VBF \HH processes in contrast to a basic cut-based methodology, the BDT in question is retrained. This retraining also involves a hyperparameter optimization and is restricted to two parameters, $m_{jj}^{\mathrm{VBF}}$ and $\Delta \eta_{jj}^{\mathrm{VBF}}$, thereby mimicking the cut-based method. This is juxtaposed with a 7-var BDT and a legacy 15-var BDT, which included variables unsuitable for VBF \HH characterization and was consequently supplanted.

Figure~\ref{fig:6:combined_ggF_VBF} exhibits a comparative analysis of the 2-var and 7-var ggF/VBF configurations, focusing on \HH signal strengths and signal-to-background ratios\footnote{The ggF \HH process is viewed as the signal in ggF SRs, while the VBF \HH is for the VBF SR.}. The evaluations span across all three signal regions and vary with BDT thresholds. The low-\mHH and high-\mHH regions are color-coded in blue and red, respectively, and the VBF region is denoted in purple. A nominal BDT threshold of 0.1 is indicated by a dashed grey line. It is observed that the 2-var BDT yields superior signal-to-background ratio in the VBF region at the nominal point, while the 7-var BDT performs more effectively in the ggF regions.


\begin{figure}[tbh]
    \centering
    \subfloat[Sensitivity: 2-var]{\includegraphics[width=0.49\textwidth]{figures/6/ggF_VBF_performance/2_var}}
    \subfloat[Sensitivity: 7-var]{\includegraphics[width=0.49\textwidth]{figures/6/ggF_VBF_performance/7_var}}\\
    \subfloat[Inclusive S/B: 2-var]{\includegraphics[width=0.49\textwidth]{figures/6/ggF_VBF_performance/ggFVBF_sob_2var}}
    \subfloat[Inclusive S/B: 7-var]{\includegraphics[width=0.49\textwidth]{figures/6/ggF_VBF_performance/ggFVBF_sob_7var}}
    \caption{Top row: Sensitivity as a function of the cut on the ggF vs VBF classification BDT for different variable sets. Bottom row: Inclusive S/B as a function of the cut on the ggF vs VBF classification BDT for different variable sets.}
    \label{fig:6:combined_ggF_VBF}
\end{figure}


Table~\ref{tab:6:2varVs7varVs15Var} presents a comparative summary of the 15-var BDT, implemented at the stage of EB request, against the 2-var and 7-var BDTs, discussing the ultimate evaluation metrics (95\% CL upper boundaries as well as \kl and \kvv intervals).


\begin{table}[!htb]
    \centering
    \resizebox{0.90\columnwidth}{!}{
        \begin{tabular}{ c c c c c c} 
            \toprule
            & \multicolumn{3}{c}{95\% CL Upper Limits on Signal Strength} & \multicolumn{2}{c}{95\% CL Intervals} \\
            \midrule
            &  $\mu_{HH}$ & $\mu_{\textrm{ggF}}^{\textrm{2D}}$ & $\mu_{\textrm{VBF}}^{\textrm{2D}}$ & \kl & \kvv \\
            \midrule
            15-var BDT  & 2.92 & 3.04 & 90 & 11.19 &2.85 \\
            2-var BDT & 3.08 (5\%) & 3.22 (6\%) & 93.05 (3\%) & 11.29 (0.9\%) & 2.91(2\%) \\
            7-var BDT & 2.90 (-0.7\%)& 3.01 (-1\%) & 93.56 (4\%) & 11.27 (0.7\%) & 2.92 (2\%) \\
            \bottomrule
        \end{tabular} 
    }
    \caption{Comparison of BDT configurations for ggF/VBF in relation to the ultimate fit outcomes. These limits are derived from a fit that incorporates solely floating normalizations and MC statistical uncertainties. A consistent BDT threshold of 0.1 is employed across all scenarios. The percentage enhancement relative to the 15-var BDT outcome is indicated in parentheses.}
    \label{tab:6:2varVs7varVs15Var}
\end{table}

Eliminating variables with low sensitivity to the VBF \HH process yields minimal impact on the BDT's effectiveness. Notably, a performance enhancement is evident when contrasting the 2-var ggF/VBF BDT with its 7-var counterpart. Consequently, the 7-var BDT is chosen for classification tasks.




\FloatBarrier

