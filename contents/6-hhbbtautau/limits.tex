\subsubsection{\bbtthadhad}

Figure~\ref{tab:6:limits-intervals-hadhad} presents the estimated 95\% CL upper limits on the \HH signal strength, further subdivided into ggF and VBF production modes. These limits are derived from both one-dimensional and two-dimensional fits of $\mu_{\textrm{ggF}}$ and $\mu_{\textrm{VBF}}$, where the other parameter is allowed to float when assessing one\footnote{When evaluating the limit on $\mu_{\textrm{ggF}}$, the normalisation factor for $\mu_{\textrm{VBF}}$ is not constrained, and vice versa.}. These assessments are conducted within the context of the \bbtthadhad decay channel, with calculations carried out under a background-only hypothesis.
Subsequently, the two rightmost columns present the 95\% CL expected intervals for the \kl and \kvv coupling constants. These intervals are extracted based on the negative log-likelihoods (NLLs), computed as functions of \kl and \kvv. It is pertinent to note that the NLL evaluations are performed under the standard model \HH production assumption.

\begin{table}[!htb]
    \centering
    \resizebox{0.95\columnwidth}{!}{
        \begin{tabular}{ c c c c c c c c } 
            \toprule
             & \multicolumn{5}{c}{95\% CL Upper Limits on Signal Strength} & \multicolumn{2}{c}{95\% CL Intervals} \\
             \midrule
             &  $\mu_{HH}$ & $\mu_{\textrm{ggF}}^{\textrm{1D}}$ &  $\mu_{\textrm{VBF}}^{\textrm{1D}}$ & $\mu_{\textrm{ggF}}^{\textrm{2D}}$ & $\mu_{\textrm{VBF}}^{\textrm{2D}}$ &  \kl & \kvv \\
             \midrule
            Baseline w/o syst. & 3.46 	& 3.52 & 219 	& 12.5 	& 778 	& [-2.79, 9.58] & [-0.58, 2.71] \\
            Baseline w/ syst.  & 4.2 	& 4.25 & 260 	& 18.0 	& 1086 & [-3.36, 10.3] & [-0.63, 2.76] \\
            \midrule
            Updated w/o syst.   & 2.95 	& 2.98 & 90 	& 3.07 	& 93 	& [-2.35, 8.94] & [-0.37, 2.51] \\
            Updated w/ syst.    & 3.55 	& 3.60 & 86 	& 3.66 	& 88 	& [-2.74, 9.40] & [-0.34, 2.51] \\
            \bottomrule
            \end{tabular} 
    }
    \caption{Summarized 95\% CL expected upper boundaries on \HH signal strength and intervals for \kl and \kvv, categorized by production mode in the \bbtthadhad channel. All estimations with and without systematic uncertainties are included.}
    \label{tab:6:limits-intervals-hadhad}
\end{table}


\begin{figure}[!htb]
    \centering
    \subfloat[]{\includegraphics[width=0.47\textwidth]{figures/6/results/fitcrosschecks/hadhad/Aug22nd/kappa_L_comp.pdf}}
    \subfloat[]{\includegraphics[width=0.47\textwidth]{figures/6/results/fitcrosschecks/hadhad/Aug22nd/kappa_2V_comp.pdf}}
    \caption{Negative logarithm of the likelihood ratio comparing different \kl (a) and \kvv (b) hypotheses to an Asimov dataset constructed under the SM hypothesis in the \bbtthadhad channel.}
    \label{fig:6:hadhad_singleScan}
\end{figure}

Pre-fit Asimov dataset-based limits are depicted both with and without the inclusion of systematic uncertainties for all relevant figures of merit. A comparative assessment is performed between the limits obtained from the baseline and updated analysis categories.

Likelihood scans are conducted across all three individual analysis categories, as well as for an aggregated case that incorporates all categories. Figure~\ref{fig:6:hadhad_singleScan} displays the likelihood scans for the coupling constants \kl and \kvv. When evaluating the likelihood for a specific parameter, either \kl or \kvv, all other coupling constants that influence both single-Higgs and di-Higgs production are maintained at their Standard Model values.

\subsubsection{Combined \bbtthadhad and \bbttlephad}

Tables~\ref{tab:6:combined-limits-intervals} combine the consolidated anticipated upper constraints on signal strength and the confidence intervals for \kl and \kvv parameters, as obtained from both the updated and baseline analyses.
The baseline data are generated using a pre-fit Asimov dataset, serving as a robust benchmark for comparative evaluation.
Moreover, the table includes metrics of relative enhancement over the baseline results.

For the \bbtthadhad decay channel specifically, the composite analysis yields a 16\% enhancement in the upper limit on HH production. Furthermore, the specialized region dedicated to VBF production facilitates the concurrent establishment of upper constraints on both VBF and ggF mechanisms.

\begin{table}[!htb]
    \centering
    \resizebox{0.95\columnwidth}{!}{
        \begin{tabular}{ c c c c c c c c } 
            \toprule
            & \multicolumn{5}{c}{95\% CL Upper Limits on Signal Strength} & \multicolumn{2}{c}{95\% CL Intervals} \\
            \midrule
            & $\mu_{HH}$ & $\mu_{\textrm{ggF}}^{\textrm{1D}}$ &  $\mu_{\textrm{VBF}}^{\textrm{1D}}$ & $\mu_{\textrm{ggF}}^{\textrm{2D}}$ & $\mu_{\textrm{VBF}}^{\textrm{2D}}$ &  \kl & \kvv \\
            \midrule
            Baseline w/o syst. 	& 2.84 & 2.89 & 189  & 9.24  & 572  & [-2.35, 9.19] & [-0.42, 2.55] \\
            Baseline w/ syst. 	& 3.61 & 3.68 & 241  & 13.2  & 772  & [-2.97, 10.1] & [-0.50, 2.63] \\
            \midrule
            Legacy w/o syst. 	& 2.41 & 2.44 & 61   & 2.49  & 62   & [-1.97, 8.58] & [-0.14, 2.32] \\
            Legacy w/ syst. 	& 2.98 & 3.01 & 63   & 3.05  & 64   & [-2.40, 9.11] & [-0.17, 2.34] \\
            \midrule
            Rel. improvement w/o syst. & 15\% & 16\% & 68\% & 73\% & 89\% & 8.6\%  & 17.2\% \\
            Rel. improvement w/ syst.  & 17\% & 18\% & 74\% & 77\% & 92\% & 11.9\% & 19.8\% \\
            \bottomrule
        \end{tabular} 
    }
    \caption{Expected 95\% CL limits on \HH signal strength and intervals for \kl and \kvv from the combined fit. The statistics are reported both with and without systematic uncertainties.}
    \label{tab:6:combined-limits-intervals}
\end{table}

In Figure~\ref{fig:6:kl-k2v_comb_revised}, panels (a) and (b) depict the NLL scans against \(\kl\) and \(\kvv\), respectively. These scans employ a fit to the SM-based Asimov dataset and consider both the aggregated scenario as well as the isolated \bbtthadhad and \bbttlephad channels.

\begin{figure}[!htb]
\centering
\subfloat[\kl]{\includegraphics[width=0.47\textwidth]{figures/6/results/fitcrosschecks/combined/Sept1st/C_ChannelComp_KLambda.pdf}}
\subfloat[\kvv]{\includegraphics[width=0.47\textwidth]{figures/6/results/fitcrosschecks/combined/Sept1st/C_ChannelComp_K2V.pdf}}
\caption{Negative log-likelihood ratios assessing various \(\kl\) and \(\kvv\) scenarios, in reference to an Asimov dataset created under the SM framework. Analyses incorporate both individual channels and their amalgamation. Solid lines intersecting dashed horizontal lines indicate 68\% and 95\% confidence levels.}
\label{fig:6:kl-k2v_comb_revised}
\end{figure}

The graphical data reinforce the \bbtthadhad channel as the main contributor to the analytic rigor. The inclusion of the \bbttlephad channel has a more pronounced impact on the \(\kvv\) interval than on the \(\kl\) interval.
