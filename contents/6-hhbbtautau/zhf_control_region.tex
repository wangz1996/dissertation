The \Sherpa Monte Carlo (MC) simulation is recognized to inadequately model the cross-section of $Z$ boson production when in concert with heavy flavour ($b,c$) jets. Therefore, data-driven normalization is implemented within a designated control region, in alignment with the protocol established in the prior analytical cycle, as described in the internal note\footnote{\href{https://cds.cern.ch/record/2743097/files/ATL-COM-PHYS-2020-766.pdf?}{ATL-COM-PHYS-2020-766}}~\cite{Betti:2743097} and the preceding analysis round~\cite{HDBS-2018-40}. Given that jet production is decoupled from the $Z$ boson's decay channel, a high-purity sample featuring $Z \rightarrow \mu\mu/ee$ coupled with heavy flavour jets is chosen. This sample is orthogonal to the selection criteria for signal regions and is used in the simultaneous fit to obtain data-based $Z+$HF normalization. The criteria delineating the $Z+$HF control region have been updated for this phase of the analysis, expounded in Section~\ref{subsec:6:selection_redefinition_ZllCR}.


\subsubsection{Redefinition of the $Z+$HF control region phase space}
\label{subsec:6:selection_redefinition_ZllCR}
The cross-section for $Z$-boson production in conjunction with heavy-flavor jets ($b,c$) is often inaccurately predicted by the \Sherpa MC. Therefore, normalization to data is performed in a designated control region from the previous analysis cycle~\cite{HDBS-2018-40}. Utilizing $Z \rightarrow \mu\mu/ee$ + heavy-flavor jets ensures a high-purity, orthogonal sample to the signal regions, allowing for data-driven $Z+$HF normalization.

\begin{figure}[!htb]
\centering
\includegraphics[width=0.45\textwidth]{figures/6/ZCR/C_DataMC_Sherpa221.png}
\hspace{5mm}
\includegraphics[width=0.45\textwidth]{figures/6/ZCR/C_DataMC_Sherpa2211.png}
\caption{Data/MC comparison in the prior $Z+HF$ control region.}
\label{fig:ZCR_DataMC_SherpaComp}
\end{figure}

To mitigate phase-space extrapolation uncertainties between the SR and CR, adjustments have been made to the CR criteria. Event selection in the revamped control region is determined as follows:

\begin{itemize}
\item $bb\ell\ell$ trigger selection using single-lepton and di-lepton triggers.
\item Exactly two opposite-sign charged muons or electrons.
\item Two $b$-tagged jets with DL1r tagger and 77\% working point.
\item $75 \, \text{GeV} < m_{\ell\ell} < 110 \, \text{GeV}$.
\item $m_{bb} < 40 \, \text{GeV}$ or $m_{bb} > 210 \, \text{GeV}$.
\item Leading $b$-jet $p_T > 45 \, \text{GeV}$.
\item Lepton $p_T > 40 \, \text{GeV}$.
\end{itemize}

\begin{figure}[!htb]
\centering
\includegraphics[width=0.45\textwidth]{figures/6/ZCR/C_VPt_Comp_HadHad.pdf}
\hspace{5mm}
\includegraphics[width=0.45\textwidth]{figures/6/ZCR/C_VPt_Comp_LepHad.pdf}
\caption{Comparison of $V$ \pT shapes between the SRs and the $Z$ CR.}
\label{fig:6:VPt_Shape_Comparison}
\end{figure}

These refinements have brought the $V$ \pT distributions of the SR and CR into closer alignment, as shown in Figure~\ref{fig:6:VPt_Shape_Comparison}. Subsequent analysis indicates a normalization factor for $Z+$HF of $1.20\pm0.05$, closely aligning with the 1.3 factor from prior analysis rounds. The impact of these changes on the systematic uncertainties and signal strength will be further elaborated.



\subsubsection{Initial Normalization Coefficients}
\label{subsec:revised_norm_factor_Z_HF}

Equations~\ref{eq:revisedNormSher221} and~\ref{eq:revisedNormSher2211} present the calculation for the pre-fit normalization constants $\mu_{Z+\text{HF}}$. These constants are obtained for the $Z+$HF ($Z+(bb,bc,cc)$) terms within the $Z+$HF control region, computed via the ratio depicted in Equation~\ref{eq:standardNormHF}\footnote{The equation serves as an approximation attributing to the Z+HF background any data/MC discrepancies. In the full scope of the analysis, the mass distribution fit in the CR will normalize \ttbar to data, encapsulating only excesses that align with the Z process.}.

\begin{equation}
\mu_{Z+\text{HF}} = \frac{\text{data} - (\text{sum of backgrounds} + Z(ee,\mu\mu)+\text{HF} + Z(\tau\tau)+\text{HF})}{Z(ee,\mu\mu)+\text{HF} + Z(\tau\tau)+\text{HF}}.
\label{eq:standardNormHF}
\end{equation}

The normalization constants are extracted using Equation~\ref{eq:standardNormHF} and are specifically detailed in Equations~\ref{eq:revisedNormSher221} and~\ref{eq:revisedNormSher2211} for Sherpa 2.2.1 and Sherpa 2.2.11 configurations, respectively.

\begin{equation}
\mu^{\Sherpa 2.2.1}_{Z+\text{HF}} = \frac{(21458 - 18566.0578 + 9970.59 + 1.210)}{(9970.59 + 1.210)} \approx 1.29
\label{eq:revisedNormSher221}
\end{equation}

\begin{equation}
\mu^{\Sherpa 2.2.11}_{Z+\text{HF}} = \frac{(21458 - 18596.707 + 10837.434 + 1.309)}{(10837.434 + 1.309)} \approx 1.26
\label{eq:revisedNormSher2211}
\end{equation}

For context, the Sherpa 2.2.11 normalization factor derived from the original $Z+$HF control region, unadjusted for the new cuts, stands at 1.05. The modification in the control region criteria brings the normalization factors of Sherpa 2.2.1 and Sherpa 2.2.11 into closer concordance. 



\subsubsection{Projection to Signal Regions}
\label{subsubsec:Vpt_Correction}

To account for the observed discrepancies in the $V$ transverse momentum ($V$ \pT) between the data and MC simulations, a specialized shape uncertainty is formulated. The uncertainty is quantified by the complete difference between the experimental data and the baseline MC estimations in varying bins of the reconstructed $V$ \pT. This is documented in Figure~\ref{fig:6:Extracted_Vpt_Shape_Uncert}.

\begin{figure}[tb]
\centering
\includegraphics[width=0.45\textwidth]{figures/6/ZCR/Z_CR_shape_extraction.png}
\hspace{5mm}
\includegraphics[width=0.45\textwidth]{figures/6/ZCR/Z_CR_extracted_shape.png}
\caption{(a) Quantification of the $V$ \pT shape uncertainty, representing the full discrepancy between data and nominal MC forecast in the Z CR. (b) The subsequent discrete parameterization of the shape uncertainty.}
\label{fig:6:Extracted_Vpt_Shape_Uncert}
\end{figure}

Subsequently, this shape uncertainty is propagated to the Sherpa 2.2.11 predictions for Z+HF within the three signal regions, in relation to the true-level $p_T(\tau\tau)$. This variable is the closest analog to the reconstructed $V$ \pT assessed in the control region. 