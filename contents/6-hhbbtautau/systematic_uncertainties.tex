\label{sec:systs_modified}
The dominant uncertainty in our analysis arises from the limited dataset. Nonetheless, systematic uncertainties affecting both signal and background estimates cannot be overlooked. For details on detector-response-based uncertainties in object selection and reconstruction, readers are referred to our previous study~\cite{HDBS-2018-40}.

We apply the Beeston-Barlow method in a simplified form to model statistical uncertainties in background predictions~\cite{BARLOW1993219}. A luminosity uncertainty of 0.8\% is applied, derived from LUCID-2 measurements~\cite{DAPR-2021-01, ATLAS-Lumi2}.

For processes reliant on MC simulations, various sources of theoretical uncertainties are studied. The BDT shape variations are handled through a specific rebinning algorithm, emphasizing statistically significant variations. Details of these procedures and criteria are elaborated in Section~\ref{sec:6:mva}. 

% \ttbar
Uncertainties related to \ttbar production involve comparisons between default and alternative MC configurations. For instance, uncertainties originating from hard-scattering and parton shower mechanisms are scrutinized through different generator setups. Contributions from scales, PDF, and \(\alpha_s\) are also studied. For the $Wt$ process, uncertainties related to the interference with $t\bar{t}$ are evaluated by comparing the diagram removal and subtraction schemes. These are parameterized based on the BDT output.


% Z+hf
For the $Z+$HF process, hard-scatter and parton-shower uncertainties are similarly quantified using alternative MC setups. Scale and PDF variations are also considered. Effects of higher-order corrections are examined and deemed negligible.

% ggF HH
For the Standard Model ggF \HH signal, the parton shower-related uncertainties are extracted by contrasting the default sample with a \Herwig{7}-modelled sample. Uncertainties related to \(\kappa_\lambda\) reweighting are also assessed. These uncertainties have a direct bearing on the categories selected for analysis.

% VBF HH
For VBF \HH, the study involves comparisons with \Herwig{7} models and variations in PDF and QCD scales. 

% single-top
For single-top processes, interference effects are assessed through diagram removal and subtraction methods. Uncertainties are parameterized based on BDT output scores.

% single-Higgs
Cross-section and decay branching ratio uncertainties are implemented according to the LHC Higgs Cross Section Working Group~\cite{LHC-HH-twiki}. Additionally, uncertainties specific to the modeling of $b$ or $c$ jets in the absence of genuine heavy-flavor quarks are applied to relevant single-Higgs processes. The uncertainties from parton-shower models and NLO matching are also accounted for. Furthermore, PDF and scale uncertainties are evaluated using standard techniques. For the $t\bar{t}H$ process, additional uncertainties related to the modeling of initial and final state radiation are also assessed.


% minor background
Systematic uncertainties for minor backgrounds, such as single-top $s$- and $t$-channels, $Z+$light-flavour jets, $W+$jets, and di-boson processes, are mainly constrained to acceptance uncertainties affecting only the normalisation.
For the $s$- and $t$-channels of single-top production, an acceptance uncertainty of 20\% is levied based on results from the SM $VHbb$ analysis~\cite{ATLAS-CONF-2020-006}.
An acceptance uncertainty of 23\% is assigned to the $Z+$light-flavour jets background, consistent with previous analyses.
In the $W+$jets background, different uncertainties are assigned for the two channels: 37\% for the \bbttlephad channel and 50\% for the \bbtthadhad channel to account for the fake-$\tau$ contribution.
Acceptance uncertainties for di-boson processes are delineated as 25\% for $WW$, 26\% for $WZ$, and 20\% for $ZZ$.
For the irreducible $ZZ$ background, additional studies reveal that scale uncertainties negligibly impact acceptance and shape within each analysis category.
Apart from acceptance uncertainties, cross-section uncertainties are also applied to all minor backgrounds. However, their magnitude is comparatively smaller and thus less impactful.


%% DD fakes
Data-driven estimates for backgrounds involving objects mimicking hadronic $\tau$ leptons contribute to the analysis selection. These are collectively termed "Fake" in the \bbttlephad channel and further subdivided into "Fake" and "ttbarFake" in the \bbtthadhad channel. The estimation techniques closely align with prior work~\cite{Betti:2743097}. Specifically, the $\tau$ fake factors are parameterized based on variables such as transverse momentum and prong-ness. Since the regions under the current analysis are finer sub-categories of those used previously, we uphold the validity of the existing fake factors. 
Given this foundation, the systematic uncertainties for Fake-$\tau_{had}$ backgrounds are modeled consistently with the prior analysis for both the \bbttlephad and \bbtthadhad channels. The sources and impact of these uncertainties follow the same framework as earlier work, with special attention given to instances where the current methodology diverges.

%% Signal 
The methodology for evaluating uncertainties on the HH signal, encompassing cross-section, acceptance, and shape, is principally inherited from the previous round of analysis. Alternative MC samples are used to probe the sensitivity to parton shower variations. Internal weights present in the nominal samples facilitate the assessment of PDF and scale variations.
Parton Shower uncertainties are evaluated by contrasting nominal samples showered with \Pythia{8} against alternatives with \Herwig{7}. Internal alternative weights in the nominal samples are utilized for assessing uncertainties arising from PDF and scale variations. The PDF4LHC recommendations are followed for the combination of PDF and $\alpha_s$ uncertainties.
