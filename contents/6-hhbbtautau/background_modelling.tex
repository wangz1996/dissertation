Background estimation in our analysis utilizes a combination of simulation and data-driven techniques. Simulated event samples, as outlined in Section~\ref{sec:6:data_backgrounds}, provide the foundation for modeling most background processes. For fake-\(\tau_\text{had}\) backgrounds, data-driven methods are employed differently across channels.

In the \bbttlephad channel, an inclusive fake-factor method handles fake-\(\tau_\text{had}\) contributions from \(t\bar{t}\) and multi-jet processes. Conversely, in the \bbtthadhad channel, we deploy a two-pronged approach: multi-jet backgrounds rely on a data-driven fake-factor method in Section~\ref{sec:6:multijet}, while \(t\bar{t}\) backgrounds utilize scale-factors extracted from data to correct MC predictions in Section~\cite{sec:6:ttbarfake}.

The separation of estimation methods for multi-jet and \(t\bar{t}\) in \bbtthadhad is necessitated by distinct fake-\(\tau_\text{had}\) features and trigger selections in Section~\ref{sec:6:trigger_selection}. Given the statistical limitations of the Run 2 dataset and the minor impact of fake-\(\tau_\text{had}\) backgrounds on sensitivity, this segregated approach is justified.

Furthermore, templates for \(t\bar{t}\) with true-\(\tau_\text{had}\) and \(Z+\text{HF}\) are MC-based but their normalizations are data-driven and integrated into the final fit model. Minor backgrounds arising from misidentified electrons or muons are treated in simulation and aligned with true-\(\tau_\text{had}\) \(t\bar{t}\) events~\cite{Betti:2743097, HDBS-2018-40}.

% \subsection{Fake-$\tau$ backgrounds in the \bbttlh channel}
\subsection{Multijet with fake-$\tau$ backgrounds in the \bbtthh channel}
\label{sec:6:multijet}

\begin{figure}
    \centering
  \includegraphics[width=0.75\linewidth]{figures/6/hadhad_multijet/FFMetod_hadhad_v4}
   \caption{
    Schematic illustration of the application of the fake-factor method for estimating multi-jet backgrounds featuring fake-$\tau_{\text{had-vis}}$ in the $\tau_{\text{had}}\tau_{\text{had}}$ channel. In all control regions, non-multi-jet backgrounds are simulated and subsequently deducted from the observed data, as denoted by 'Subtracted Non-Multi-Jet Backgrounds' in the legend.
   }.
  \label{fig:6:hadhad_BG1}
  \end{figure}

In the \bbtthadhad channel, the estimation of the multi-jet background employs a data-driven fake-factor approach, consistent with the methodology applied in preceding analyses of the \bbbar\bbtthadhad~\cite{HIGG-2016-16,HDBS-2018-40}. The fake factors (FFs) used in the prior analysis~\cite{HDBS-2018-40} are retained. The Monte Carlo settings' transition from \Sherpa 2.2.1 to \Sherpa 2.2.11 for the $V$+jets samples does not alter the multi-jet background calculations. The leading $\tau_{\text{had}}$ $p_T$ distributions for each category in the 1-tag SS and 1-tag OS regions show that the new samples do not affect the estimation strategy. The different regions used for the multijet estimation are schematically
depicted in Figure~\ref{fig:6:hadhad_BG1}.  

FFs are obtained in a control region with two $\tau_{\text{had-vis}}$ having same-sign charges, as a ratio of events with two loose $\tau_{\text{had}}$ to those with one loose and one anti-$\tau_{\text{had}}$. FFs are computed separately for 1- and 3-prong $\tau_{\text{had}}$, for STT and DTT trigger categories, and for the 0-, 1-, and 2-b-tag regions. Additionally, FFs are year-specific to consider changing $\tau_{\text{had}}$ identification criteria and trigger selections. Due to low statistics, the 1-b-tag FFs are used in the 2-b-tag region with appropriate transfer factors for corrections.

\begin{equation}
FF_i(p_T \, \tau_{\text{had}}^i, \eta \, \tau_{\text{had}}^i, N_{\text{prong}} \, \tau_{\text{had}}^i, \dots) = \frac{N_{\text{data}}(\text{loose} \, \tau_{\text{had}}^i)-N_{\text{non-multijet MC}}(\text{loose} \, \tau_{\text{had}}^i)}{N_{\text{data}}(\text{anti-}\tau_{\text{had}}^i) - N_{\text{non-multijet MC}}(\text{anti-}\tau_{\text{had}}^i)}
\label{eq:HH:hadhad:multijet:FFi}
\end{equation}

Two types of FFs, labeled as $FF_0$ and $FF_1$, are then averaged. Their statistical compatibility has been validated, and they are averaged with respect to their statistical significance in a given $p_T$ bin.

\begin{equation}
FF_{\text{avg}}(p_T \, \tau_{\text{had}}, \eta \, \tau_{\text{had}}, N_{\text{prong}} \, \tau_{\text{had}}, \dots) = \frac{N_{\text{data}}(\text{loose} \, \tau_{\text{had}}) - N_{\text{non-multijet MC}}(\text{loose} \, \tau_{\text{had}})}{N_{\text{data}}(\text{anti-}\tau_{\text{had}}) - N_{\text{non-multijet MC}}(\text{anti-}\tau_{\text{had}})}
\label{eq:HH:hadhad:multijet:FFavg}
\end{equation}

The final fake factors for \bbtthadhad channel are shown in Figure~\ref{fig:6:hadhad_fake_factors}.

\begin{figure}[!htb]
    \centering
  
    \includegraphics[width=.32\textwidth]{figures/6/hadhad_multijet/fake_factors/15_16/FF_1tag_TauPt_1P}
    \includegraphics[width=.32\textwidth]{figures/6/hadhad_multijet/fake_factors/17/FF_1tag_TauPt_1P}
    \includegraphics[width=.32\textwidth]{figures/6/hadhad_multijet/fake_factors/18/FF_1tag_TauPt_1P}
  
    \includegraphics[width=.32\textwidth]{figures/6/hadhad_multijet/fake_factors/15_16/FF_1tag_TauPt_3P}
    \includegraphics[width=.32\textwidth]{figures/6/hadhad_multijet/fake_factors/17/FF_1tag_TauPt_3P}
    \includegraphics[width=.32\textwidth]{figures/6/hadhad_multijet/fake_factors/18/FF_1tag_TauPt_3P}
  
    \includegraphics[width=.32\textwidth]{figures/6/hadhad_multijet/fake_factors/15_16/FF_STT_1tag}
    \includegraphics[width=.32\textwidth]{figures/6/hadhad_multijet/fake_factors/17/FF_STT_1tag}
    \includegraphics[width=.32\textwidth]{figures/6/hadhad_multijet/fake_factors/18/FF_STT_1tag}
  
    \caption{Fake factors for 1-prong DTT (top), 3-prong DTT (middle), and STT
      (bottom) for the data-taking periods 15-16 (left), 17 (centre), and 18
      (right) for the di-Higgs \bbtthadhad channel.}
  
    \label{fig:6:hadhad_fake_factors}
\end{figure}

\FloatBarrier

\subsection{\ttbar with fake-$\tau_\textrm{had}$ in the \bbtthh channel with scale-factor method}
\label{sec:6:ttbarfake}

The background events attributed to $t\bar{t}$ production that contain fake-$\tau_{\text{had-vis}}$ in the $\tau_{\text{had}}\tau_{\text{had}}$ channel are quantified through simulation. However, for enhanced accuracy, the misidentification efficiencies of fake-$\tau_{\text{had-vis}}$ are modified by data-derived SFs. A graphical overview of this methodology is portrayed in Figure~\ref{fig:6:hadhad_BG2}.

\begin{figure}
  \centering
  \includegraphics[width=0.60\linewidth]{figures/6/SFMetod_hadhad_v4}
  \caption{Illustrative summary of the data-corrected scale-factor technique employed for evaluating $t\bar{t}$ background containing fake-$\tau_{\text{had-vis}}$ in the $\tau_{\text{had}}\tau_{\text{had}}$ channel.}
  \label{fig:6:hadhad_BG2}
\end{figure}

The SFs are a function of the fake-$\tau_{\text{had-vis}}$ transverse momentum ($p_\text{T}$), segregated for 1-prong and 3-prong decay modes. A profile-likelihood fit to the transverse mass ($m^{W}_{\text{T}}$) distribution effectively calibrates the SFs. Separate fitting processes are carried out for distinct trigger categories.

The numerical behavior of SFs varies with $p_\text{T}$: for 1-prong fake-$\tau_{\text{had-vis}}$, SFs are approximately 1 for $p_\text{T}$ below 40~GeV and diminish to around 0.6 for $p_\text{T}$ exceeding 70~GeV. For the 3-prong mode, the SFs are typically about 20\% greater than their 1-prong counterparts.

To evaluate the $t\bar{t}$ background in the $\tau_{\text{had}}\tau_{\text{had}}$ SR, the simulation is scaled by the appropriate SFs for each fake-$\tau_{\text{had-vis}}$ in the event. Various uncertainties, including detector response and theoretical modeling, are accounted for during the likelihood fit for SF extraction.

The resulting covariance matrix, encompassing both statistical and systematic uncertainties, is diagonalized to produce independent nuisance parameters (NPs), which are subsequently propagated into the final signal extraction fit.

For the estimation of fake-$\tau_{\text{had-vis}}$ background from multi-jet processes, a considerable fraction of $t\bar{t}$ events containing at least one fake-$\tau_{\text{had-vis}}$ must be removed from the data in the opposite-sign 2-$b$-tagged-jet anti-identification (anti-ID) region to accurately gauge the multi-jet influence. The modelling of $t\bar{t}$ events in this region is corrected via data-derived SFs, obtained using a methodology similar to the one described.
