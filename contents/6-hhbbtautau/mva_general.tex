The discriminant employed for isolating the signal is derived from a multivariate algorithm, as elaborated in Section~\ref{sec:5:statistical_model}.
Distinct from the complete Run 2 data analysis cycle, Boosted Decision Trees (BDTs) are now utilized uniformly for both sub-channels in all categories, as specified in Section~\ref{sec:6:event_selection}.
An expanded set of predictor variables has been examined, and optimal hyperparameters have been selected to enhance the model's efficacy.
Uniform training protocols are applied to both \bbtthadhad and \bbttlephad scenarios, utilizing a standardized training infrastructure\cite{Moser2023} and based on the TMVA toolkit~\cite{TMVA}.

The training procedures have been specifically designed for various channels: \bbtthadhad, \bbttlephad SLT, and \bbttlephad LTT, as detailed in their respective sections. However, the focus of the ensuing discussion will be solely on the \bbtthadhad channel.

\subsubsection{Partitioning Technique for Model Validation}
\label{subsec:mva_partitioning}

The architectural layout of the MVA approach necessitates a dependable and impartial forecast
of the anticipated analytical sensitivity.
This constraint dictates that the same events cannot be employed for both the calibration of the BDT
(covering its hyperparameters and predictor variables) and the formulation of the histogram schematics
for the resultant BDT scores.

A straightforward methodology fulfilling this stipulation is to subdivide the obtainable
ensemble of simulated events into three equally-sized subsets.
In this specific adaptation, the approach is executed based on the event identification number.

\begin{table}[!htb]
    \centering
    \resizebox{0.85\columnwidth}{!}{
        \begin{tabular}{cccc}
            \toprule 
            \multirow{2}{*}{Model} & Fold 0 & Fold 1 & Fold 2 \\
            & event\_number \(\% 3=0\) & event\_number \(\% 3=1\) & event\_number \(\% 3=2\) \\
            \midrule
            BDT 0 & Training & Validation & Testing \\
            BDT 1 & Testing & Training & Validation \\
            BDT 2 & Validation & Testing & Training \\
            \bottomrule
        \end{tabular}
    }
    \caption{Demarcation of the simulated event pools used for the learning, fine-tuning, and scrutiny
    of the BDT algorithms.}
  \label{tab:6:partitioning}
\end{table}

Consequently, a trio of distinct BDT models are developed, each exploiting an individual subset of the available simulated
data (referred to as \myverb{Training folds}) as outlined in Table \ref{tab:6:partitioning}.
A consistent set of hyperparameters and predictor variables is applied across all trainings.
These are ascertained through the maximization of BDT efficacy on the \myverb{Validation folds}, which remain
unseen during the training phase.

The synthetic events encapsulated in the \myverb{Testing folds} contribute to the formation of the histogram fit blueprints.
Intrinsically, these events are excluded from the BDT training steps, thereby safeguarding an impartial forecast
of the forthcoming analytical sensitivity.


\subsubsection{Hyperparameter Tuning Strategy}
\label{subsec:mva_hyperparameter_tuning}

The performance of the BDT is highly sensitive to specific training hyperparameters employed within TMVA. Two primary hyperparameters warrant special attention: the number of ensemble trees (\myverb{NTrees}) and the maximum depth of each individual tree (\myverb{MaxDepth}).

To optimize these hyperparameters effectively, a comprehensive grid search is undertaken. The search is implemented following the partitioning mechanism detailed in Section \ref{subsec:mva_partitioning}. Subsequently, the score distribution of the BDT output is analyzed, both for the signal and the aggregated backgrounds, utilizing the events within the validation subsets.
(The bin structure is determined using the same algorithm that defines the binning for the likelihood fitting, as discussed in Section \ref{subsec:binning}.)
For performance assessment, the metric of binned signal significance is employed, given by

\begin{equation}
  Z = \sqrt{\sum_{i\in\mathrm{bins}} 2\left((s_i + b_i) \log\left(1 + \frac{s_i}{b_i}\right) - s_i \right)},
  \label{eq:optimized_significance}
\end{equation}

where \(s_i\) and \(b_i\) denote the yields of signal and background events in the \(i^{th}\) bin, respectively.

A defined hyperparameter space is explored, varying according to the specific analysis region and BDT variant. Bayesian optimization techniques are employed to favor hyperparameter configurations that yield elevated binned significance metrics.

The optimal hyperparameter set is identified from the ensemble of configurations evaluated during the tuning process, and utilized in the training, validation, and test phases.


\subsubsection{Optimization of Feature Selection}
\label{subsec:mva_optimized_input_variables}

For the $bb\tau\tau$ system, many variables can help distinguish between different outcomes. However, using too many variables can make the model complex and less reliable. So, even though it's not a strict requirement, it's simpler and more effective to choose a limited but useful set of variables.

The feature selection procedure operates as follows:
\begin{itemize}
  \item A set of "core" variables is invariably included. The specific constituents of this core set are BDT-dependent and will be detailed subsequently.
  \item Using a greedy algorithm, further variables are incrementally chosen from the remaining list based on their ability to enhance the binned signal significance, as quantified by Eq.~\ref{eq:optimized_significance}, evaluated over the validation partitions.
  \item If adding a new variable doesn't improve the performance based on the validation data, then the variable that has the smallest negative impact on performance is included instead.
  \item The process terminates after \(N_p\) consecutive steps without a significant boost in significance.
\end{itemize}

Such a heuristic optimization strategy is prone to statistical noise, potentially distorting the variable selection. To mitigate this, the binned significance \(Z\) is calculated using a coarser binning scheme than that applied in the likelihood fitting procedure, as elaborated upon in a later section.
