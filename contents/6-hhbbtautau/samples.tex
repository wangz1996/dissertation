The analysis presented in this chapter uses proton-proton collision data, taken at a centre-of-mass energy of $\sqrt{s}=13$~TeV by the ATLAS experiment from 2015 to 2018. The data corresponds to an integrated luminosity of $\mathcal{L} = \SI[parse-numbers=false]{140.1 \pm 1.2}{\ifb}$~\cite{DAPR-2021-01}. Event selections were based on the ATLAS Good-Run-List (GRL) to ensure the operational integrity of all relevant detector components. 

To simulate the SM backgrounds and both SM and BSM \HH signal, we utilize Monte Carlo event samples. These samples are processed through the ATLAS detector simulation based on Geant4~\cite{Agostinelli:2002hh}. Pile-up effects are incorporated by overlaying minimum-bias events generated via \Pythia[8.186]~\cite{Sjostrand:2007gs} with A3 tune~\cite{ATL-PHYS-PUB-2016-017} and the \NNPDF[2.3lo]~\cite{Ball:2012cx} PDFs. The hadronic decays of $b$ and $c$ quarks are modeled by the \EVTGEN program~\cite{Lange:2001uf}, except in samples generated with \Sherpa~\cite{Bothmann:2019yzt}, where generator-specific models are used.

For all MC samples containing a SM Higgs boson, a mass of 125~GeV is assumed for consistency in both decay branching fractions and cross-section calculations. The cross-sections are calculated with expansions in the strong coupling constant ($\alpha_s$), unless otherwise specified.

\input{contents/6-hhbbtautau/tables/table_samples_1.tex}

The MC samples are summarized in Table~\ref{tab:samples}, which details the generators used for simulating the signal and background processes. The nomenclatures ME, PS, and UE refer to matrix element, parton shower, and underlying event, respectively. Note that
for samples with $(\dagger)$, the $qq \rightarrow ZH$ process is normalized to the NNLO~(QCD)~+~NLO~(EW) cross-section for the $pp \rightarrow ZH$ process, after the $gg \rightarrow ZH$ contribution is subtracted.




\subsection{Simulation of Signal Datasets}
\label{sec:6:simulation_signal}

The \HH signal is modeled considering both gluon-gluon fusion (ggF) and vector-boson fusion (VBF) as contributing processes.

\subsubsection{ggF HH production}
Samples\footnote{\href{https://its.cern.ch/jira/browse/ATLMCPROD-8884}{https://its.cern.ch/jira/browse/ATLMCPROD-8884}} for \kl values of 1.0 and 10.0 were generated using \POWHEGBOX[v2] at NLO with finite top-quark mass considerations~\cite{Alioli:2010xd}. The \PDFforLHC[15\_nlo\_30\_pdfas] PDF set (code 90400 in LHAPDF~\cite{LHAPDF6}) was employed~\cite{Butterworth:2015oua}. Parton showering and hadronisation were performed via \Pythia[8.244]~\cite{Sjostrand:2007gs} using the A14 tune~\cite{ATL-PHYS-PUB-2014-021,ATLAS:2012uec} and \NNPDF[2.3lo] PDF set. Figure~\ref{fig:6:ggf-mc-validation} depicts the invariant mass distribution \mHH for ggF \HH samples in the \bbtthadhad channel, highlighting the potential for analysis categorization as elaborated in Section~\ref{sec:6:event_categorization}.

\begin{figure}[tbh!]
\centering
 \includegraphics[width=0.6\textwidth]{figures/6/HH_m_linear}
 \caption{Invariant mass (\mHH) distribution at the parton level for the ggF \HH signal in the \bbtthadhad channel, with overlaid \kl values of 1.0 and 10.0.}
 \label{fig:6:ggf-mc-validation}
\end{figure}

To model ggF \HH events with varied \kl values in the range \kl $\in[-20,20]$, either of the two base samples can be reweighted using the \HH \kl reweighting tool\footnote{\href{https://gitlab.cern.ch/atlas-physics/HDBS/DiHiggs/combination/klambdareweighttool}{kLambdaReweightTool}}, which offers event-specific weightings as a function of the desired \kl value and the true $m_{HH}$. 

The SM process normalization is defined by the di-Higgs cross-section in the ggF channel, $\sigma_{\text{ggF}}=31.05$~fb, as computed at NNLO FTApprox~\cite{Grazzini:2018bsd}, and then multiplied by the \bbtt branching ratio, leading to a value of $2.2683967$~fb.

Alternative ggF samples\footnote{\href{https://its.cern.ch/jira/browse/ATLMCPROD-9170}{https://its.cern.ch/jira/browse/ATLMCPROD-9170}} for \kl = 1.0, 10.0 were also generated using the \POWHEGBOX[v2] at NLO and were interfaced with \Herwig7~\cite{Bahr:2008pv} to examine parton showering uncertainties.



% VBF Section
\subsubsection{VBF HH production}

VBF signal samples were synthesized at leading order (LO) utilizing the \MGNLO[2.7.3]~\cite{Alwall:2014hca} framework, in combination with the \NNPDF[3.0nlo] PDF model~\cite{Ball:2014uwa}. The parton showers and subsequent hadronization steps were conducted through \Pythia[8.244] with the A14 configuration and \NNPDF[2.3lo] PDF set. Various coupling modulators \kl, \kvv, and \kv were employed, as cataloged in Table~\ref{tab:6:vbf-coupling-samples}. A linear blend of six reference points in the (\kl, \kvv, \kv) space allows for more refined granularity in \kvv values. 

\begin{table}[htbp!]
  \centering
  \caption{Considered \kl, \kvv, and \kv coupling modifiers for VBF \HH simulations.}
  \begin{tabular}{c c c}
    \toprule
    \kl  & \kvv  & \kv \\
    \hline
    1 & 1 & 1 \\
    1 & 0 & 1 \\
    1 & 0.5 & 1 \\
    1 & 1.5 & 1 \\
    1 & 2 & 1 \\
    1 & 3 & 1 \\
    0 & 1 & 1 \\
    2 & 1 & 1 \\
    10 & 1 & 1 \\
    1 & 1 & 0.5 \\
    1 & 1 & 1.5 \\
    0 & 0 & 1 \\
    -5 & 1 & 0.5 \\
    \bottomrule
   \end{tabular}

  \label{tab:6:vbf-coupling-samples}
\end{table}

Figures \ref{fig:6:vbf-mc-validation3} display representative parton-level distributions of these samples, featuring key observables and their relation to the coupling parameters~\cite{Bishara:2016kjn}. 

\begin{figure}[tbh!]
    \centering
    \begin{subfigure}{.45\textwidth}
      \centering
      \includegraphics[width=1\linewidth]{figures/6/HadHad_kvv_HH_m}
      \caption{}
      \label{fig:sub1}
    \end{subfigure}
    \begin{subfigure}{.45\textwidth}
      \centering
      \includegraphics[width=1\linewidth]{figures/6/LepHad_kvv_HH_m}
      \caption{}
      \label{fig:sub2}
    \end{subfigure}
    \caption{Inclusive distributions of VBF \HH samples for varying \kvv at parton-level, presented for the \bbtthadhad and \bbttlephad channels. Couplings are adjusted as outlined in Table~\ref{tab:6:vbf-coupling-samples}.}
    \label{fig:6:vbf-mc-validation3}
  \end{figure}

The SM normalization is determined by the VBF \HH cross-section $\sigma_{\text{VBF}}=\SI{1.726}{fb}$, calculated at N$^3$LO in QCD, and the \bbtt branching ratio, resulting in a final value of $\sigma_{\text{VBF}} \times \mathcal{B}(\bbtt) = \SI{0.126}{fb}$. BSM normalization follows a quadratic function of \kvv~\cite{LHC-HH-twiki}.

For systematic variations in parton shower, alternate VBF \HH samples were produced at specific coupling values, as denoted in Table~\ref{tab:6:vbf-coupling-alternative-samples}, using the \MGNLO[2.7.3] and \NNPDF[3.0nlo] models, interfaced to \Herwig7~\cite{Bahr:2008pv}.

\begin{table}[htbp]
  \centering
  \caption{Coupling modifier values for generating alternative VBF \HH samples.}
  \begin{tabular}{c c c}
    \toprule
    \kl  & \kvv  & \kv \\
    \hline
    1 & 1 & 1 \\
    1 & 0 & 1 \\
   10 & 1 & 1 \\
    \bottomrule
\end{tabular}
  \label{tab:6:vbf-coupling-alternative-samples}
\end{table}


\subsection{Background Simulation Samples}
\label{sec:6:data_backgrounds}

\textbf{Top Quark Processes}
The simulation of both top-antitop (\ttbar) and individual top quark productions in $Wt$, $s$, and $t$-channels are generated through the \POWHEGBOX v2 generator~\cite{Powheg1, Powheg2, Powheg3}. The PDF utilized is NNPDF30NLO~\cite{NNPDF}. Post-generation event dynamics, including parton showers and hadronization, are conducted with \PYTHIA8 version 8.230~\cite{PYTHIA82}. The tuning parameter set used is A14~\cite{A14tune, ATLAS:2012uec}. The decays of bottom and charm hadrons are handled via EvtGen v1.6.0~\cite{EvtGen}.

\textbf{Vector Boson + Jets ($V+$jets)}
These events, containing either $W$ or $Z$ bosons accompanied by jets, are generated through the \SHERPA 2.2.11~\cite{Bothmann:2019yzt}. A mixed NLO and LO framework for matrix elements is used, with NLO matrix elements considering up to two additional partons and LO matrix elements accounting for up to five additional partons. These are computed through Comix~\cite{Gleisberg:2008fv} and \OPENLOOPS~\cite{Buccioni:2019sur,Cascioli:2011va,Denner:2016kdg}.

\textbf{Diboson Processes}
Diboson events, characterized by one boson decaying hadronically and the other leptonically, are modeled using \SHERPA version 2.2.1~\cite{Bothmann:2019yzt}. PDFs are based on the \NNPDF[3.0nnlo] set~\cite{Ball:2014uwa}.

\textbf{Top-Quark and Vector Boson Associated Production ($ttV$)} 
Simulations are executed using \SHERPA v2.2.1 for $ttZ$ and \SHERPA v2.2.8 for $ttW$, both employing multileg NLO merging techniques. PDFs are based on \NNPDF[3.0nnlo]~\cite{Ball:2014uwa}.

\textbf{SM Single Higgs Production} 
This analysis incorporates SM Higgs boson as a part of the background and is elaborated below:

\begin{itemize}
    \item \textbf{$ttH$}: Produced via \POWHEGBOX, it employs NNPDF30NLO for PDFs and \PYTHIA8 v8.230 for parton showers~\cite{PYTHIA82}.
    \item \textbf{$ZH$}: Utilizes \POWHEGBOX v2 and considers both $qqZH$ and $ggZH$ channels. PDFs are \NNPDF[3.0nnlo]~\cite{Ball:2014uwa}.
    \item \textbf{$WH$}: Generated using \POWHEGBOX v2 with PDFs from \NNPDF[3.0nnlo]~\cite{Ball:2014uwa}.
    \item \textbf{ggF $H \rightarrow \tau^+\tau^-$}: Simulated through \POWHEGBOX v2 with \NNPDF[3.0nnlo] PDFs~\cite{Ball:2014uwa}.
    \item \textbf{VBF $H \rightarrow \tau^+\tau^-$}: Produced via \POWHEGBOX v2, using \NNPDF[3.0nnlo]~\cite{Ball:2014uwa} for PDFs.
\end{itemize}
