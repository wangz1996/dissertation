The analysis procedure is divided into two sub-channels according to the di-$\tau$ decay mode. A schematic representation of this approach is provided in Figure~\ref{fig:6:full_analysis_sketch}. The first sub-channel, denoted as \bbbar\bbtthadhad, focuses on events featuring two oppositely charged $\tau_{\text{had-vis}}$ along with two $b$-jets. The second, termed \bbbar\bbttlephad, targets events with one lepton (electron or muon), one oppositely charged $\tau_{\text{had-vis}}$, and two $b$-jets. For both sub-channels, the two $b$-jets are required to pass a working point with 77\% efficiency. 

The schematic in Figure~\ref{fig:6:full_analysis_sketch} illustrates the sequence of triggers and event selection criteria for the two sub-channels. It also depicts the BDT framework that establishes the Signal Regions (SR) orthogonality between ggF and VBF categories. Further, the ggF SR is partitioned by an invariant mass threshold of 350~GeV to enhance sensitivity to the parameter $\kappa_\lambda$. 

Event classification into ggF and VBF SRs is achieved through specialized BDTs. The ggF SRs are further divided into low- and high-mass regions using an invariant mass cut-off of 350 GeV for the $\mathrm{HH}$ system. 

A Control Region (CR), designated as $Z$+HF CR, is established to calibrate the $Z$+HF background normalization. This CR utilizes $bb\ell\ell$ triggers and focuses on the $m_{\ell\ell}$ shape as the parameter of interest, consistent with the schematic representation. 

Both sub-channels are required to pass a common set of object selection criteria as in Section~\ref{sec:6:object_reconstruction}.
Detailed trigger configurations for these sub-channels are elaborated in Section~\ref{sec:6:trigger_selection}. Subsequent sections will elucidate the updated definition of the $Z$+HF CR phase space relative to the prior analysis~\cite{HDBS-2018-40}.

\begin{figure}[tbh]
\centering
\includegraphics[width=0.99\textwidth]{figures/6/C_bbtautau_eventselection_sketch_v3}
\caption{Schematic representation of the analysis strategy.}
\label{fig:6:full_analysis_sketch}
\end{figure}

\begin{table}[!htb]
    \centering
    \resizebox{0.95\columnwidth}{!}{
        \begin{tabular}{cccc}
            \toprule
            \multicolumn{2}{c}{$\tau_\text{had}\tau_\text{had}$ category} & \multicolumn{2}{c}{$\tau_\text{lep}\tau_\text{had}$ categories}\\
            %% Single-$\tau_\text{had}$ trigger (STT) & Di-$\tau_\text{had}$ trigger (DTT) & Single-$e/\mu$ trigger (SLT) & $e/\mu+\tau_\text{had}$ trigger (LTT)\\
            STT & DTT & SLT & LTT\\
            \midrule
            \multicolumn{4}{c}{\BF{$e/\mu$ selection}}\\
            \multicolumn{2}{c}{No loose $e/\mu$} & \multicolumn{2}{c}{Exactly one loose $e/\mu$}\\
            & & \multicolumn{2}{c}{$e$ ($\mu$) must be tight (medium and have $\vert\eta\vert<2.5$)}\\
            %% & & \multicolumn{2}{c}{Exactly one tight $e$ or medium $\mu$ with $\vert\eta^\mu\vert<2.5$}\\
            & & $p_\text{T}^e>25,27~\text{GeV}$ & $18~\text{GeV}<p_\text{T}^e<\text{SLT cut}$\\
            & & $p_\text{T}^\mu>21,27~\text{GeV}$ & $15~\text{GeV}<p_\text{T}^\mu<\text{SLT cut}$\\
            \midrule
            \multicolumn{4}{c}{\BF{$\tau_\text{had-vis}$ selection}}\\
            \multicolumn{2}{c}{Two loose $\tau_\text{had-vis}$} & \multicolumn{2}{c}{One loose $\tau_\text{had-vis}$}\\
            & & \multicolumn{2}{c}{$\vert\eta\vert<2.3$}\\
            $p_\text{T}>100,140,180~(25)~\text{GeV}$ & $p_\text{T}>40~(30)~\text{GeV}$ & & $p_\text{T}>30~\text{GeV}$\\
            \midrule
            \multicolumn{4}{c}{\BF{Jet selection}}\\
            \multicolumn{4}{c}{$\geq 2$ jets with $\vert\eta\vert<2.5$}\\
            %% $p_\text{T}>45~(20)~\text{GeV}$ & Trigger dependent & $p_\text{T}>45~(20)~\text{GeV}$ & Trigger dependent\\
            Leading jet $p_\text{T}>45~\text{GeV}$ & Trigger dependent & Leading jet $p_\text{T}>45~\text{GeV}$ & Trigger dependent\\
            \midrule
            \multicolumn{4}{c}{\BF{Event-level selection}}\\
            \multicolumn{4}{c}{Trigger requirements passed}\\
            \multicolumn{4}{c}{Collision vertex reconstructed}\\
            \multicolumn{4}{c}{$\mMMC>60~\text{GeV}$}\\
            \multicolumn{4}{c}{Opposite-sign electric charges of $e/\mu/\tau_\text{had-vis}$ and $\tau_\text{had-vis}$}\\
            \multicolumn{4}{c}{Exactly two $b$-tagged jets}\\
            %% \multicolumn{4}{c}{$b$-tagged jet $p_\text{T}>45~(20)~\text{GeV}$}\\
            & & \multicolumn{2}{c}{$m_{bb}<150~\text{GeV}$}\\
             \bottomrule
        \end{tabular}
    }
    \caption{Summary of the event selections, shown separately for events that are selected by different triggers.}
    \label{tab:6:DiHiggsEventSelection}
\end{table}

An overview of the event selection procedures is provided in Table~\ref{tab:6:DiHiggsEventSelection}. This table disaggregates the event criteria according to the different triggers that are employed for selection. For events requiring pairs of reconstructed objects of identical nature, the table specifies distinct \pT thresholds for both leading and sub-leading objects. These are indicated outside and inside parentheses, respectively. In cases where the selection constraints are influenced by the year of data acquisition, multiple acceptable parameter values are enumerated and separated by commas. Notably, the jet selection criteria under the LTT and DTT triggers are an exception and adhere to a more intricate set of guidelines, as detailed in Section~\ref{sec:6:event_selection}. It is essential to note that the listed trigger \pT thresholds are imposed on offline physics objects that have been appropriately matched to their corresponding trigger entities.
For the scope of this study, the event selection will be exclusively focused on the \bbtthadhad channel.

